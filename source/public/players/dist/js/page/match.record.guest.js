$(function () {
    'use strict';
    //
    $("select[name='weather'] option:first, select[name='wind_direction'] option:first").attr({'disable':'disable', 'hidden':'hidden'});
    
    //set default app match config and state
    var point_trans;
    var data_match;
    var backup_data_match;
    var down = false;
    var scrollLeft = 0;
    var court_x = 0;
    var is_tie_breaker = false;

    point_trans = [0, 15, 30, 40, "AD"];
    
    data_match = {
        match_name: '',
        weather: '',
        weather_value: 1,
        wind_direction: '',
        wind_direction_value: 1,
        place: '',
        coat: '',
        coat_value: 1,
        match_format: '',
        first_f:[],
        advantage: 1,
        preferred_hand: 1,
        change_coat: 2,
        points: [0, 0],
        tie_breaker_active: {},
        current_set: 0,
        player_sets_won: [0, 0],
        winner: -1,
        games: {},
        tie_breaker_score: {},
        latest_event: { level: 0, text: "Match just started!" },
        won_sets: [],
        games_per_set: 6,
        sets: 3,
        tie_breaker_points: 7,
        players: ['自分', '相手'],
        coat_selection: 1,
        serve_right: 1,
        serve_fault: 0,
        is_serve: 1,
        current_hit: 1,
        switch_site: 0,
        current_game: 0,
        games_points: {0: {0: [[0, 0]]}}
    };

    for (data_match.games = []; data_match.games.push([0, 0]) < 5;);
    for (data_match.tie_breaker_score = []; data_match.tie_breaker_score.push([0, 0]) < 5;);

    //draw config sets, serve and score
    restore_app_backup();
    
    function backup_app() {
        window.localStorage.setItem('match_detail_guest', JSON.stringify(data_match));
    }
    
    function restore_app_backup() {
        var data_match_get = window.localStorage.getItem('match_detail_guest');
        if (data_match_get === null)
                ;
        else {
            data_match = JSON.parse(data_match_get);
            draw_config();
            draw_scores();
            set_data_table();
            set_data_form();
            set_location_change_point();
            set_point();
            if(data_match.latest_event.level != 4) {
                $(".net-court, .court-go button, .court-back button").removeAttr('hidden');
            }
            $(".btn-ask-delete-match, .chevron-icon").removeAttr('hidden');
            $("#match-info, #detail-collapse").collapse('hide');
            $('.btn-match-info').text('修正');
        }
    };
    
    $(document).on('click', '.btn-delete-match', function () {
        window.localStorage.removeItem('match_detail_guest');
        location.reload();
    });
    
    $(document).on('click', '.court-input-out.enable, .n-return.net-court.enable', function () {
        if ($(this).text() == 'Out') $('.btn-win').addClass('disabled');
        else {
            $('.btn-win').removeClass('disabled');
            $(".commit-point button[point='A']").addClass("disabled");
        }
        $('.btn-lose').removeClass('disabled');
        $('.commit-point').collapse('show');
    });
    
    $(document).on('click', '.commit-point .btn-close', function () {
        $('.commit-point').collapse('hide');
    });
    
    $(document).on('click', '.match-register .btn-close', function () {
        $("#match-info, #start-game").collapse('hide');
    });
    
    var delay = 700, clicks = 0, timer = null;
    $(document).on('click', '.court-input-return.enable', function (e) {
        var self = this;
        clicks++;  //count clicks
        if(clicks === 1) {
            set_f();
            timer = setTimeout(function() {
                //perform single-click action
                if ($(self).hasClass('m-serve')) {
                    data_match.is_serve = 0;
                    $(".court-go button").addClass("disabled");
                }
                if(data_match.current_hit == 1) {
                    data_match.current_hit = 2;
                } else if(data_match.current_hit == 2) {
                    data_match.current_hit = 1;
                }
                draw_config();
                backup_app();
                clicks = 0;             //after action performed, reset counter
            }, delay);
        } else {
            clearTimeout(timer);    //prevent single-click action
            //perform double-click action
            $('.btn-win').removeClass('disabled');
            if (data_match.is_serve == 1) {
                $(".commit-point button[point='A']").removeClass("disabled");
                $(".commit-point button[point!='A']").addClass("disabled");
            }
            else {
                $(".commit-point button[point='A']").addClass("disabled");
                $(".commit-point button[point!='A']").removeClass("disabled");
            }
            $('.commit-point').collapse('show');
        }
    });

    $('.commit-point').on('hidden.bs.collapse', function () {
        clicks = 0;
    });

    $(document).on('dblclick', '.court-input-return.enable', function (e) {
        e.preventDefault();  //cancel system double-click event
    });
    
    function draw_config() {
        hide_show_sets();
        if(data_match.is_serve == 1){ //check if is serve then show serve and button
            set_serve();
        } else {
            set_return();
        }
        set_location_change_point();
    }
    
    function hide_show_sets() {
        for (var i = 1; i <= data_match.sets; i++) {
            $("#set" + i + "head").removeAttr('hidden');
            for (var j = 1; j <= 2; j++) {
                $("#p" + j + "set" + i).removeAttr('hidden');
            }
        }
        for (i = data_match.sets + 1; i <= 5; i++) {
            $("#set" + i + "head").attr("hidden",true);
            for (var j = 1; j <= 2; j++) {
                $("#p" + j + "set" + i).attr("hidden",true);
            }
        }
    }
    
    function set_data_table() {
        var d = new Date();
        var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate() + "  " + d.getHours() + ":" + d.getMinutes();
        $('#p1name').text(data_match.players[0]);
        $('#p2name').text(data_match.players[1]);
        $('#match-name').text(data_match.match_name);
        $('#time-weather').text(strDate + "  " + data_match.weather + "  " + data_match.wind_direction);
        $('#place').text(data_match.place);
        var side = 'デュースサイド';
        if(data_match.serve_right == 2){
            side = 'アドサイド';
        }
        $('#coat').text(data_match.coat + "  （" + side + "）");
        var adv = 'アドバンテージ';
        if(data_match.advantage == 2){
            adv = 'ノーアド';
        }
        $('#sets-ad').text(data_match.match_format + "  （" + adv + "）");
        var hand = '右';
        if(data_match.preferred_hand == 2){
            hand = '左';
        }
        $('#opponent').text(data_match.players[1] + "  （" + hand + "）");
        
        if ($(".font-weight-normal").hasClass("font-weight-bold")) {
            $(".font-weight-normal").removeClass('font-weight-bold');
        }
    }
    
    function set_data_form() {
        $("input[name='match_name']").val(data_match.match_name);
        $("select[name='weather']").val(data_match.weather_value);
        $("select[name='wind_direction']").val(data_match.wind_direction_value);
        $("input[name='place']").val(data_match.place);
        $("select[name='coat']").val(data_match.coat_value);
        $("select[name='match_format']").val(data_match.sets);
        $("input[name=advantage][value=" + data_match.advantage + "]").attr("checked", true);
        $("input[name='opponent']").val(data_match.players[1]);
        $("input[name=preferred_hand][value=" + data_match.preferred_hand + "]").attr("checked", true);
        $("input[name=coat_selection][value=" + data_match.coat_selection + "]").attr("checked", true);
        $("input[name=change_coat][value=" + data_match.change_coat + "]").attr("checked", true);
        $("input[name=serve_right][value=" + data_match.serve_right + "]").attr("checked", true);
    }

    function draw_scores() {
        draw_points();
        var current_set_old = data_match.current_set;
        for (var i = 0; i < data_match.sets; i++) {
            data_match.current_set = i;
            draw_games_in_set(i, data_match.tie_breaker_active[data_match.current_set]);
        }
        data_match.current_set = current_set_old;
        if (data_match.winner == -1)
            ;
        else
            draw_winner();
        for (var i = 0; i < data_match.won_sets.length; i++)
            $(data_match.won_sets[i]).addClass('font-weight-bold');
        if(data_match.serve_right == 1) {
            fadeHtml('#p1serve', '●');
            fadeHtml('#p2serve', '');
        } else if(data_match.serve_right == 2){
            fadeHtml('#p2serve', '●');
            fadeHtml('#p1serve', '');
        }
    };
    
    function draw_points() {
        if (!data_match.tie_breaker_active[data_match.current_set]) {
            fadeHtml('#p1points', point_trans[data_match.points[0]]);
            fadeHtml('#p2points', point_trans[data_match.points[1]]);
        }
        else {
            fadeHtml('#p1points', data_match.tie_breaker_score[data_match.current_set][0]);
            fadeHtml('#p2points', data_match.tie_breaker_score[data_match.current_set][1]);
        }
    }
    
    function draw_games_in_set(set, show_tie_breaker_score) {
        fadeHtml('#p1set' + parseInt(set + 1), data_match.games[set][0] + (show_tie_breaker_score ? "<sup>" + data_match.tie_breaker_score[data_match.current_set][0] + "</sup>" : ""));
        fadeHtml('#p2set' + parseInt(set + 1), data_match.games[set][1] + (show_tie_breaker_score ? "<sup>" + data_match.tie_breaker_score[data_match.current_set][1] + "</sup>" : ""));
    }

    function draw_winner() {
        data_match.latest_event.level = 4;
        data_match.latest_event.text = data_match.players[data_match.winner] + " won the match!";
        if (!is_tie_breaker) {
            delete data_match.games_points[data_match.current_set][data_match.current_game]; // delete unnecessary object create when won the match
        }
        else is_tie_breaker = false;
        $('#player-winner').text(data_match.players[data_match.winner]);
        $('#winner-match').modal('show');
        winning_state();
    }

    function player_scores(winning_player, point_name) {
        data_match.latest_event.level = 1;
        is_tie_breaker = false;
        var losing_player = (winning_player + 1) % 2;
        var set_old = data_match.current_set;
        if (data_match.tie_breaker_active[data_match.current_set]) {
            data_match.tie_breaker_score[data_match.current_set][winning_player]++;
            add_point_name(data_match.tie_breaker_score[data_match.current_set][0], data_match.tie_breaker_score[data_match.current_set][1], point_name, winning_player);
            if (data_match.tie_breaker_score[data_match.current_set][winning_player] >= data_match.tie_breaker_points && data_match.tie_breaker_score[data_match.current_set][winning_player] - data_match.tie_breaker_score[data_match.current_set][losing_player] >= 2) {
                is_tie_breaker = true;
                player_wins_set(winning_player, point_name, is_tie_breaker);
                //push point next set when win the tie break make empty first point
                if (set_old != data_match.current_set) {
                    add_point_name(point_trans[data_match.points[0]], point_trans[data_match.points[1]], null, winning_player);
                }
            }
        } else {
            // 0 to 30
            if (data_match.points[winning_player] <= 2) {
                data_match.points[winning_player]++;
            // 40
            } else if (data_match.points[winning_player] == 3) {
                // l = 40 - both had 40
                if (data_match.points[losing_player] == 3) {
                    data_match.points[winning_player]++;
                    if(data_match.advantage == 2) { // no AD
                        player_wins_game(winning_player, point_name);
                    }
                } else if (data_match.points[losing_player] == 4) {// l = AD
                    data_match.points[losing_player]--;
                } else {
                    player_wins_game(winning_player, point_name);
                }
            } else if (data_match.points[winning_player] == 4) { // AD
                player_wins_game(winning_player, point_name);
            }
            add_point_name(point_trans[data_match.points[0]], point_trans[data_match.points[1]], point_name, winning_player);
        }
        if (data_match.latest_event.level <= 1) {
            data_match.latest_event.text = data_match.players[winning_player] + " scored the point";
            if(data_match.tie_breaker_active[data_match.current_set]) {
                set_hit_tie_breaker();
            }
            else {
                if (data_match.switch_site == 0)
                data_match.switch_site = 1;
                else if(data_match.switch_site == 1)
                    data_match.switch_site = 0;
                if(data_match.serve_right == 1)
                    data_match.current_hit = 1;
                else if(data_match.serve_right == 2)
                    data_match.current_hit = 2;
            }
            data_match.is_serve = 1;
            data_match.serve_fault = 0;
            draw_config();
            enable_back_button();
        }
        set_point();
    }
    
    function set_hit_tie_breaker() {
        var score = data_match.tie_breaker_score[data_match.current_set]
        var tie_breaker_hit = score[0] + score[1];
        if (tie_breaker_hit == 1) {
            data_match.switch_site = 0;
            change_hit();
        }
        else {
            if (data_match.switch_site == 0) {
                data_match.switch_site = 1;
            }
            else if (data_match.switch_site == 1) {
                data_match.switch_site = 0;
                change_hit();
            }
        }
        if (tie_breaker_hit % 6 == 0) {
            change_coat();
        }
    }

    function change_hit() {
        if(data_match.serve_right == 1) {
            data_match.serve_right = 2;
            data_match.current_hit = 2;
        }
        else if(data_match.serve_right == 2) {
            data_match.serve_right = 1;
            data_match.current_hit = 1;
        }
    }

    function player_wins_game(winning_player, point_name) {
        var losing_player = (winning_player + 1) % 2;
        data_match.first_f = [];
        var won_game = ['WON '+ point_name, "0"];
        if(winning_player != 0) {
            won_game = ["0", 'WON '+ point_name];
        }
        //reset point
        data_match.points[0] = 0;
        data_match.points[1] = 0;

        data_match.games[data_match.current_set][winning_player]++;
        if (data_match.games[data_match.current_set][winning_player] >= data_match.games_per_set) {
            // oponent will have at least 2 games difference
            if (data_match.games[data_match.current_set][winning_player] - data_match.games[data_match.current_set][losing_player] >= 2)
                player_wins_set(winning_player, point_name, false);
            else if (data_match.games[data_match.current_set][winning_player] == data_match.games_per_set && data_match.games[data_match.current_set][losing_player] == data_match.games_per_set)
                data_match.tie_breaker_active[data_match.current_set] = true;
        }
        
        if (data_match.latest_event.level <= 2) {
            data_match.is_serve = 1;
            data_match.switch_site = 0;
            data_match.serve_fault = 0;
            data_match.latest_event.level = 2;
            data_match.latest_event.text = data_match.players[winning_player] + " won the game";
            data_match.games_points[data_match.current_set][data_match.current_game].push(won_game); // push last point for winner
            data_match.current_game++; //set current game here to avoid increase to 1 when won the game
            data_match.games_points[data_match.current_set][data_match.current_game] = []; //recreate array current game
            check_coat();
            change_hit();
            draw_config();
            enable_back_button();
        }
    }

    function player_wins_set(winning_player, point_name, is_tb) {
        var won_game = ['WON', 0];
        if(winning_player != 0) {
            won_game = [0, 'WON'];
        }

        if (is_tb) {
            var m_point = data_match.games_points[data_match.current_set][data_match.current_game].pop();
            m_point[0] = won_game[0] == 0 ? parseInt(m_point[0]) : parseInt(m_point[0]) +' '+ won_game[0];
            m_point[1] = won_game[1] == 0 ? parseInt(m_point[1]) : parseInt(m_point[1]) +' '+ won_game[1];
            won_game = [m_point[0], m_point[1]];
            data_match.games[data_match.current_set][winning_player]++;
        }

        if (winning_player == 0) won_game = [won_game[0] +' '+ point_name, won_game[1].toString()];
        else if (winning_player == 1) won_game = [won_game[0].toString(), won_game[1] +' '+ point_name];
        
        data_match.player_sets_won[winning_player]++;
        data_match.won_sets.push("#p" + parseInt(winning_player + 1) + "set" + parseInt(data_match.current_set + 1));
        if (data_match.player_sets_won[winning_player] == (data_match.sets + 1) / 2) {
            data_match.winner = winning_player;
            if (is_tb) data_match.games_points[data_match.current_set][data_match.current_game].push(won_game);
            return;
        }
        
        if (data_match.latest_event.level <= 3) {
            data_match.games_points[data_match.current_set][data_match.current_game].push(won_game); // push last point for winner
            data_match.current_set++;
            //reset to serve
            data_match.is_serve = 1;
            data_match.switch_site = 0;
            data_match.current_game = 0;
            data_match.games_points[data_match.current_set] = {}; //recreate object current set
            data_match.games_points[data_match.current_set][data_match.current_game] = []; //recreate array current game
            //change serve right and coat odd set 
            data_match.latest_event.level = 3;
            data_match.latest_event.text = data_match.players[winning_player] + " took the set";
            check_coat(true, is_tb);
            draw_config();
            enable_back_button();
        }
    }
    
    $(document).on('click', '#btn-match-info', function () {
        if (!validate_form_create()) return;
        $('#start-game').collapse('show');
        $('#detail-collapse').collapse('hide');
        $(".chevron-icon, .btn-ask-delete-match").removeAttr('hidden');
        if (localStorage.getItem("match_detail_guest") === null) {
            $(".start-game-wp").removeAttr('hidden').addClass('d-flex');
        }
        $('.btn-match-info').text('修正');
        data_match.match_name = $("input[name='match_name']").val();
        data_match.players[1] = $("input[name='opponent']").val();
        if (data_match.players[0] == "")
            data_match.players[0] = "自分";
        if (data_match.players[1] == "")
            data_match.players[1] = "相手";
        data_match.sets = parseInt($("select[name='match_format'] option:selected").val());
        data_match.weather = $("select[name='weather'] option:selected").text();
        data_match.weather_value = parseInt($("select[name='weather'] option:selected").val());
        data_match.wind_direction = $("select[name='wind_direction'] option:selected").text();
        data_match.wind_direction_value = parseInt($("select[name='wind_direction'] option:selected").val());
        data_match.place = $("input[name='place']").val();
        data_match.coat = $("select[name='coat'] option:selected").text();
        data_match.coat_value = parseInt($("select[name='coat'] option:selected").val());
        data_match.match_format = $("select[name='match_format'] option:selected").text();
        data_match.advantage = parseInt($("input[name=advantage]:checked").val());
        data_match.preferred_hand = parseInt($("input[name=preferred_hand]:checked").val());
        set_point_default();
        set_data_table();
        backup_app();
    });
    
    $(document).on('click', '#btn-start-game', function () {
        data_match.coat_selection = parseInt($("input[name=coat_selection]:checked").val());
        data_match.change_coat = parseInt($("input[name=change_coat]:checked").val());
        data_match.serve_right = parseInt($("input[name=serve_right]:checked").val());
        $(".net-court, .court-go button, .court-back button, .match-register .btn-close").removeAttr('hidden');
        $(".start-game-wp").removeClass('d-flex').attr("hidden",true);
        $('#start-game, #detail-collapse').collapse('hide');
        set_point_default();
        backup_app();
        draw_config();
        draw_scores();
        set_data_table();
        set_data_form();
        set_location_change_point();
        set_point();
    });
    
    //tab button 1STサーブ
    $(document).on('click', '#serve-start', function () {
//        back_up();
        draw_config();
        draw_scores();
        backup_app();
        set_f();
        $(this).addClass('disabled');
        $('.m-serve').removeClass('disabled').addClass('enable');
        $('.net-court').removeClass('disabled').addClass('enable');
    });
    
    //tab fault then enable 2NDサーブ
    $(document).on('click', '.m-serve.court-input-fault, .n-serve.court-input-fault', function () {
        $('#serve-start').removeClass('disabled').addClass('enable').children('span').text('2ND');
        if(data_match.serve_fault == 0){
            data_match.serve_fault = 1;
        } else if(data_match.serve_fault == 1) {
            data_match.serve_fault = 2;
            if(data_match.serve_right == 1){
                player_scores(1, 'SvM'); // score point for opponent
            }else {
                player_scores(0, 'SvM'); // score point for myself
            }
            $('#serve-start').removeClass('disabled').addClass('enable').children('span').text('1ST');
        }
        draw_config();
        draw_scores();
        backup_app();
        set_point();
    });
    
    $(document).on('click', '.btn-win', function (event) {
        $('.btn-lose, .btn-win').addClass('disabled');
        back_up();
        data_match.first_f.push("F");
        var point_name = $(this).attr('point') + 'W';
        if (data_match.is_serve == 1) point_name = $(this).attr('point') == 'A' ? 'SvA' : 'SvW';
        else point_name = $(this).attr('point') == 'A' ? 'RtA' : point_name;
        if(data_match.current_hit == 2){
            player_scores(1, point_name); // score point for opponent
        } else if(data_match.current_hit == 1) {
            player_scores(0, point_name); // score point for myself
        }
        $('.commit-point').collapse('hide');
        $('.court-go button').addClass('disabled');
        draw_scores();
        backup_app();
    });
    
    $(document).on('click', '.btn-lose', function (event) {
        $('.btn-lose, .btn-win').addClass('disabled');
        back_up();
        data_match.first_f.push("F");
        var point_name = $(this).attr('point') + 'M';
        if (data_match.is_serve == 1) point_name = 'SvM';
        if(data_match.current_hit == 2){
            player_scores(0, point_name); // score point for myself
        } else if(data_match.current_hit == 1) {
            player_scores(1, point_name); // score point for opponent
        }
        $('.commit-point').collapse('hide');
        $('.court-go button').addClass('disabled');
        draw_scores();
        backup_app();
    });
    
    $(document).on('click', '.court-back button', function () {
        restore_backup();
        $(this).addClass('disabled');
        $('.court-go button').removeClass('disabled');
        set_point();
    });

    $(document).on('click', '.court-go button', function () {
        restore_app_backup();
        $('.court-back button').removeClass('disabled');
        $(this).addClass('disabled');
        set_point();
    });

    $('.match-points').mousedown(function(e) {
        down = true;
        scrollLeft = this.scrollLeft;
        court_x = e.clientX;
    }).mouseup(function() {
        down = false;
    }).mousemove(function(e) {
        if (down) {
           this.scrollLeft = scrollLeft + court_x - e.clientX;
        }
    }).mouseleave(function() {
        down = false;
    });

    function set_serve() {
        $('.m-serve').removeClass('enable').addClass('disabled');
        $('.net-court').removeClass('enable n-return').addClass('disabled n-serve').removeAttr('hidden');
        $('.return-in-nml, .return-in-ad, .return-in-dc, .out-top, .out-top-left, .out-top-right, .out-bottom, .out-bottom-left, .out-bottom-right').attr("hidden",true);
        if((data_match.coat_selection == 1 && data_match.serve_right == 1) || (data_match.coat_selection == 2 && data_match.serve_right == 2)){
            $('div[class*="serve-right-up-"], div[class*="serve-left-up-"]').attr("hidden",true);
            $('.court-btn').removeClass('court-btn-top-wrapper').addClass('court-btn-bottom-wrapper');
            if(data_match.switch_site == 0){//switch site after each point
                $('div[class*="serve-right-down-"]').removeAttr('hidden');
                $('div[class*="serve-left-down-"]').attr("hidden",true);
                $('.court-btn').find('.col-6').removeClass('mr-auto').addClass('ml-auto');
            } else {
                $('div[class*="serve-left-down-"]').removeAttr('hidden');
                $('div[class*="serve-right-down-"]').attr("hidden",true);
                $('.court-btn').find('.col-6').removeClass('ml-auto').addClass('mr-auto');
            }
        }else if((data_match.coat_selection == 1 && data_match.serve_right == 2) || (data_match.coat_selection == 2 && data_match.serve_right == 1)){
            $('div[class*="serve-left-down-"], div[class*="serve-right-down-"]').attr("hidden",true);
            $('.court-btn').removeClass('court-btn-bottom-wrapper').addClass('court-btn-top-wrapper');
            if(data_match.switch_site == 0){//switch site after each point
                $('div[class*="serve-left-up-"]').removeAttr('hidden');
                $('div[class*="serve-right-up-"]').attr("hidden",true);
                $('.court-btn').find('.col-6').removeClass('ml-auto').addClass('mr-auto');
            } else {
                $('div[class*="serve-right-up-"]').removeAttr('hidden');
                $('div[class*="serve-left-up-"]').attr("hidden",true);
                $('.court-btn').find('.col-6').removeClass('mr-auto').addClass('ml-auto');
            }
        }
        $("#serve-start").removeAttr('hidden').removeClass('disabled').addClass('enable');
        if (data_match.serve_fault == 1) $('#serve-start').children('span').text('2ND');
        else $('#serve-start').children('span').text('1ST');
    }
    
    function set_return() {
        //hide serve
        $('#serve-start, div[class*="serve-right-up-"], div[class*="serve-left-up-"], div[class*="serve-left-down-"], div[class*="serve-right-down-"]').attr("hidden",true);
        //show return
        $('.return-in-nml, .return-in-ad, .return-in-dc, .out-top, .out-top-left, .out-top-right, .out-bottom, .out-bottom-left, .out-bottom-right').removeAttr('hidden');
        //net
        $('.net-court').removeClass('disabled n-serve').addClass('enable n-return').removeAttr('hidden');
        if(data_match.current_hit == 2 && data_match.coat_selection == 2){
            $(".stroke-up").removeClass("disabled").addClass("enable");
            $(".stroke-down").removeClass("enable").addClass("disabled");
        } else if(data_match.current_hit == 1 && data_match.coat_selection == 2) {
            $(".stroke-up").removeClass("enable").addClass("disabled");
            $(".stroke-down").removeClass("disabled").addClass("enable");
        } else if(data_match.current_hit == 2 && data_match.coat_selection == 1) {
            $(".stroke-up").removeClass("enable").addClass("disabled");
            $(".stroke-down").removeClass("disabled").addClass("enable");
        } else if(data_match.current_hit == 1 && data_match.coat_selection == 1) {
            $(".stroke-up").removeClass("disabled").addClass("enable");
            $(".stroke-down").removeClass("enable").addClass("disabled");
        }
    }
    
    function enable_back_button() {
        //disable button
        $('.court-back button').removeClass('disabled').removeAttr('hidden');
    }
    
    function winning_state() {
        //hide serve
        $('#serve-start, div[class*="serve-right-up-"], div[class*="serve-left-up-"], div[class*="serve-left-down-"], div[class*="serve-right-down-"]').attr("hidden",true);
        //hide return
        $('.return-in-nml, .return-in-ad, .return-in-dc, .out-top, .out-top-left, .out-top-right, .out-bottom, .out-bottom-left, .out-bottom-right').attr("hidden",true);
        //disable button and hide go and back button
        $(".m-serve").addClass('disabled')
        $(".m-stroke").addClass('disabled')
        $('.court-go button').addClass('disabled').attr("hidden",true);
        $('.court-back button').addClass('disabled').attr("hidden",true);
        //disable and hide net
        $('.net-court').removeClass('enable').addClass('disabled').attr("hidden",true);
    }
    
    function back_up() {
        backup_data_match = jQuery.extend(true, {}, data_match);
    };

    function restore_backup() {
        data_match = jQuery.extend(true, {}, backup_data_match);
        draw_config();
        draw_scores();
        set_point();
    };
    
    function fadeHtml(object, html) {
        if ($(object).html() != html) {
            $(object).fadeOut('fast', function () {
                $(this).html(html)
            }).fadeIn('fast');
        }
    }

    function set_point_default() {
        data_match.current_game = 0;
        data_match.current_hit = data_match.serve_right == 1 ? 1 : 2;
        data_match.current_set = 0;
        for (data_match.games = []; data_match.games.push([0, 0]) < 5;);
        for (data_match.tie_breaker_score = []; data_match.tie_breaker_score.push([0, 0]) < 5;);
        data_match.latest_event = { level: 0, text: "Match just started!" };
        data_match.is_serve = 1;
        data_match.switch_site = 0;
        data_match.player_sets_won = [0, 0];
        data_match.points = [0, 0];
        data_match.games_points = {0: {0: [[0, 0]]}};
        data_match.winner = -1;
        data_match.tie_breaker_active = {};
        data_match.won_sets = [];
        data_match.serve_fault = 0;
    }

    function set_location_change_point() {
        $('.match-points').removeAttr('hidden');
        if (data_match.coat_selection == 1) {
            $('.match-points-top .player-pos-text').text('相手');
            $('.match-points-bottom .player-pos-text').text('自分');
        }
        else {
            $('.match-points-top .player-pos-text').text('自分');
            $('.match-points-bottom .player-pos-text').text('相手');
        }
    }

    function set_f() {
        var cr_point = data_match.points[0] + data_match.points[1] + 2;
        if((data_match.coat_selection == 1 && data_match.serve_right == 1) || (data_match.coat_selection == 2 && data_match.serve_right == 2)){
            $(`.point-top-${cr_point} .serve_first`).text('');
            $(`.point-bottom-${cr_point} .serve_first`).text('F');
        }else if((data_match.coat_selection == 1 && data_match.serve_right == 2) || (data_match.coat_selection == 2 && data_match.serve_right == 1)){
            $(`.point-top-${cr_point} .serve_first`).text('F');
            $(`.point-bottom-${cr_point} .serve_first`).text('');
        }
    }

    function add_point_name(point_0, point_1, point_name, winning_player) {
        var player_0 = '', player_1 = '';
        
        if (winning_player == 0) {
            player_0 = (point_0 != 0 || point_1 != 0) ? point_0 +' '+ point_name : point_0.toString();
            player_1 = point_1.toString();
        }
        else if (winning_player == 1) {
            player_0 = point_0.toString();
            player_1 = (point_0 != 0 || point_1 != 0) ? point_1 +' '+ point_name : point_1.toString();
        }
        data_match.games_points[data_match.current_set][data_match.current_game].push([player_0, player_1]);
    }

    function set_point() {
        var g_points = data_match.games_points[data_match.current_set][data_match.current_game];
        $('.serve_first').text('');
        $('.result_point').html('&nbsp;');
        $('.new-point').remove();
        if (g_points) {
            for (var i = 1 ; i < g_points.length ; i++) {
                var point_1 = (String(g_points[i][0]).lastIndexOf(' ') > 0) ? g_points[i][0].substr(g_points[i][0].lastIndexOf(' ')).trim() : null;
                var point_2 = (String(g_points[i][1]).lastIndexOf(' ') > 0) ? g_points[i][1].substr(g_points[i][1].lastIndexOf(' ')).trim() : null;
                var downCoat = data_match.coat_selection == 1 ? 'bottom' : 'top';
                var upCoat = data_match.coat_selection == 2 ? 'bottom' : 'top';
                var idx = i + 1;
                if(point_1) {
                    var court = $('.point-'+downCoat+'-'+idx+' .result_point');
                    if (court.length) court.text(point_1);
                    else add_court_name(downCoat, point_1);
                }
                else if(point_2) {
                    var court = $('.point-'+upCoat+'-'+idx+' .result_point');
                    if (court.length) court.text(point_2);
                    else add_court_name(upCoat, point_2);
                }
                if (data_match.serve_right == 1) $('.point-'+downCoat+'-'+idx+' .serve_first').text(data_match.first_f[i-1]);
                else $('.point-'+upCoat+'-'+idx+' .serve_first').text(data_match.first_f[i-1]);
            }
        }
    }

    function add_court_name(location, point) {
        if (location == 'bottom') {
            set_court_bottom(point);
        }
        else if (location == 'top') {
            set_court_top(point);
        }
    }

    function set_court_top(point) {
        $('.match-points-top').append('<div class="point-name new-point"><div class="point-name-inner py-0"><div class="serve_first">F</div><div class="result_point">'+point+'</div></div></div>');
        $('.match-points-bottom').append('<div class="point-name new-point"><div class="point-name-inner py-0"><div class="result_point">&nbsp;</div></div></div>');
        var l = $('.match-points-top').width() + $('.point-name:first').width();
        $('.match-points-top').scrollLeft(l);
    }

    function set_court_bottom(point) {
        $('.match-points-top').append('<div class="point-name new-point"><div class="point-name-inner py-0"><div class="result_point">&nbsp;</div></div></div>');
        $('.match-points-bottom').append('<div class="point-name new-point"><div class="point-name-inner py-0"><div class="serve_first">F</div><div class="result_point">'+point+'</div></div></div>');
        var l = $('.match-points-bottom').width() + $('.point-name:first').width();
        $('.match-points-bottom').scrollLeft(l);
    }

    function validate_form_create() {
        var match_name = $("input[name='match_name']");
        var place = $("input[name='place']");
        var opponent = $("input[name='opponent']");

        var is_match_name = validate_input(match_name);
        var is_place = validate_input(place);
        var is_opponent = validate_input(opponent);

        if (is_match_name && is_place && is_opponent) {
            return true;
        }

        return false;
    }

    function validate_input(el) {
        var txt_null = 'を入力してください。';
        var txt_max = '文字以内で入力してください。';
        var ip_value = el.val();
        var message = null;
        if (ip_value == '') message = el.attr('title') + txt_null;
        else if (ip_value.length > el.attr('max_lenght')) {
            message = el.attr('title') +'を'+ el.attr('max_lenght') + txt_max;
        }

        if (message) {
            el.parent().find('div.invalid-feedback').text(message).show();
            return false;
        }
        else {
            el.parent().find('div.invalid-feedback').text('').hide();
            return true;
        }
    }

    function validate_select(el) {
        var txt_select = 'を選択してください。';
        var sl_value = el.val();
        var message = null;
        if (sl_value == '') {
            message = el.parent().attr('title') + txt_select;
            el.parent().parent().find('div.invalid-feedback').text(message).show();
            return false;
        }
        else {
            el.parent().parent().find('div.invalid-feedback').text('').hide();
            return true;
        }
    }

    function check_coat(is_win_set = false, is_tb = false) {
        if (is_win_set) {
            var games = data_match.games_points[data_match.current_set - 1];
            var size = Object.keys(games).length;
            if (is_tb) change_coat();
            else if (size % 2 == 1) change_coat();
        }
        else if ((data_match.current_game + 1) % 2 == 0) change_coat();
    }

    function change_coat() {
        if (data_match.coat_selection == 1) data_match.coat_selection = 2;
        else data_match.coat_selection = 1;
    }
});
