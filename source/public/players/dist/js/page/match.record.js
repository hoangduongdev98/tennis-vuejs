$(function () {
    'use strict';
    //
    $("select[name='weather'] option:first, select[name='wind_direction'] option:first").attr({ 'disable': 'disable', 'hidden': 'hidden' });
    //set default app match config and state
    var point_trans;
    var data_match = JSON.parse($("input[name='match_detail']").val());
    var backup_data_match;
    var down = false;
    var scrollLeft = 0;
    var court_x = 0;
    var is_tie_breaker = false;
    var target_point = null;
    var player_hit = null;
    var side = false;
    var turn_back = false;
    var old_data = null;
    window.localStorage.setItem('match_backup', JSON.stringify(data_match));
    point_trans = [0, 15, 30, 40, "AD"];
    //draw config sets, serve and score
    backup_app();
    restore_app_backup();

    update_screen();
    set_margin_court();
    function backup_app() {
        window.localStorage.setItem('match_detail', JSON.stringify(data_match));
        $.ajax({
            type: "POST",
            url: window.location.href,
            data: {
                match_detail: window.localStorage.getItem('match_detail')
            },
            headers: {
                'X-CSRF-Token': $("input[name='_token']").val()
            },
            success: function (res) {
            },
            error: function () {
            }
        });
    }

    function restore_app_backup(match) {
        if (match) window.localStorage.setItem('match_detail', JSON.stringify(match));
        var data_match_get = window.localStorage.getItem('match_detail');
        if (data_match_get === null);
        else {
            data_match = JSON.parse(data_match_get);
            draw_config();
            draw_scores();
            set_location_change_point();
            set_point();
            backup_app();
        }
    };

    $(document).on('click', '.court-input-out.enable, .n-return.net-court.enable', function () {
        target_point = this;
        check_current_hit();
        if (!player_hit) player_hit = data_match.current_hit;
        if ($(this).text() == 'Out' || $(this).text() == 'Net') $('.btn-win').addClass('disabled');
        else {
            $('.btn-win').removeClass('disabled');
            $(".commit-point button[point='A']").addClass("disabled");
        }
        $('.btn-lose').removeClass('disabled');
        $('.commit-point').collapse('show');
    });

    $(document).on('click', '.commit-point .btn-close', function () {
        $('.commit-point').collapse('hide');
    });

    $(document).on('click', '.match-register .btn-close', function () {
        $("#match-info, #start-game").collapse('hide');
    });

    $(document).on('click', '.a-side, .d-side', function () {
        $('#change_serve_side').modal('hide');
        data_match.switch_site = $(this).attr("side");
        set_serve();
    });

    var delay = 700, clicks = 0, timer = null;
    $(document).on('click', '.court-input-return.enable', function (e) {
        var self = this;
        clicks++;  //count clicks
        target_point = this;
        if (!player_hit) player_hit = data_match.current_hit;
        if (data_match.is_serve == 1) {
            if (data_match.switch_site == 0) side = true;
            else side = false;
            data_match.course_point = $(this).attr('location');
            delay = 700;
        }
        else delay = 0;
        if (clicks === 1) {
            // set_f();
            timer = setTimeout(function () {
                //perform single-click action
                set_stats_serve();
                count_miss_serve(target_point, true);
                clicks = 0;             //after action performed, reset counter
                if ($(self).hasClass('m-serve')) {
                    data_match.is_serve = 0;
                    $(".court-go button").addClass("disabled");
                    draw_config();
                    return;
                }
                if (data_match.is_serve == 0) show_commit(data_match);
            }, delay);
        } else {
            clearTimeout(timer);    //prevent single-click action
            show_commit(data_match);
        }
    });

    function show_commit(data_match) {
        $('.btn-win').removeClass('disabled');
        if (data_match.is_serve == 1) {
            $(".commit-point button[point='A']").removeClass("disabled");
            $(".commit-point button[point!='A']").addClass("disabled");
        }
        else {
            $(".commit-point button[point='A']").addClass("disabled");
            $(".commit-point button[point!='A']").removeClass("disabled");
        }
        $('.btn-lose').addClass('disabled');
        check_current_hit();
        $('.commit-point').collapse('show');
    }

    $('.commit-point').on('hidden.bs.collapse', function () {
        clicks = 0;
    });

    $(document).on('dblclick', '.court-input-return.enable', function (e) {
        e.preventDefault();  //cancel system double-click event
    });

    function draw_config() {
        if (data_match.is_serve == 1) { //check if is serve then show serve and button
            set_serve();
        } else {
            set_return();
        }
        set_location_change_point();
    }

    function draw_scores() {
        var won_sets = data_match.current_set + 1;
        if (turn_back) {
            $('#p1set' + won_sets).removeClass('font-weight-bold').addClass('font-weight-normal');
            $('#p2set' + won_sets).removeClass('font-weight-bold').addClass('font-weight-normal');
        }
        draw_points();
        var current_set_old = data_match.current_set;
        for (var i = 0; i < data_match.sets; i++) {
            data_match.current_set = i;
            draw_games_in_set(i, data_match.tie_breaker_active[data_match.current_set]);
        }
        data_match.current_set = current_set_old;
        if (data_match.winner != -1) draw_winner();
        for (var i = 0; i < data_match.won_sets.length; i++)
            $(data_match.won_sets[i]).removeClass('font-weight-normal').addClass('font-weight-bold');
        if (data_match.serve_right == 1) {
            fadeHtml('#p1serve', '●');
            fadeHtml('#p2serve', '');
        } else if (data_match.serve_right == 2) {
            fadeHtml('#p2serve', '●');
            fadeHtml('#p1serve', '');
        }
    };

    function draw_points() {
        if (!data_match.tie_breaker_active[data_match.current_set]) {
            fadeHtml('#p1points', point_trans[data_match.points[0]]);
            fadeHtml('#p2points', point_trans[data_match.points[1]]);
        }
        else {
            fadeHtml('#p1points', data_match.tie_breaker_score[data_match.current_set][0]);
            fadeHtml('#p2points', data_match.tie_breaker_score[data_match.current_set][1]);
        }
    }

    function draw_games_in_set(set, show_tie_breaker_score) {
        fadeHtml('#p1set' + parseInt(set + 1), data_match.games[set][0] + (show_tie_breaker_score ? "<sup>" + data_match.tie_breaker_score[data_match.current_set][0] + "</sup>" : ""));
        fadeHtml('#p2set' + parseInt(set + 1), data_match.games[set][1] + (show_tie_breaker_score ? "<sup>" + data_match.tie_breaker_score[data_match.current_set][1] + "</sup>" : ""));
    }

    function draw_winner() {
        data_match.latest_event.level = 4;
        data_match.latest_event.text = data_match.players[data_match.winner] + " won the match!";
        if (!is_tie_breaker) {
            delete data_match.games_points[data_match.current_set][data_match.current_game]; // delete unnecessary object create when won the match
        }
        else is_tie_breaker = false;
        $('#player-winner').text(data_match.players[data_match.winner]);
        $('#winner-match').modal('show');
        winning_state();
    }

    function player_scores(winning_player, point_name) {
        data_match.latest_event.level = 1;
        is_tie_breaker = false;
        var losing_player = (winning_player + 1) % 2;
        var set_old = data_match.current_set;
        if (data_match.tie_breaker_active[data_match.current_set]) {
            data_match.tie_breaker_score[data_match.current_set][winning_player]++;
            add_point_name(data_match.tie_breaker_score[data_match.current_set][0], data_match.tie_breaker_score[data_match.current_set][1], point_name, winning_player);
            if (data_match.tie_breaker_score[data_match.current_set][winning_player] >= data_match.tie_breaker_points && data_match.tie_breaker_score[data_match.current_set][winning_player] - data_match.tie_breaker_score[data_match.current_set][losing_player] >= 2) {
                is_tie_breaker = true;
                player_wins_set(winning_player, point_name, is_tie_breaker);
                //push point next set when win the tie break make empty first point
                if (set_old != data_match.current_set) {
                    add_point_name(point_trans[data_match.points[0]], point_trans[data_match.points[1]], null, winning_player);
                }
            }
        } else {
            // 0 to 30
            if (data_match.points[winning_player] <= 2) {
                data_match.points[winning_player]++;
                // 40
                if (data_match.advantage == 2 && data_match.points[0] == 3 && data_match.points[0] == data_match.points[1]) {
                    $('#change_serve_side').modal('show');
                }
            } else if (data_match.points[winning_player] == 3) {
                // l = 40 - both had 40
                if (data_match.points[losing_player] == 3) {
                    data_match.points[winning_player]++;
                    if (data_match.advantage == 2) { // no AD
                        player_wins_game(winning_player, point_name);
                    }
                } else if (data_match.points[losing_player] == 4) {// l = AD
                    data_match.points[losing_player]--;
                } else {
                    player_wins_game(winning_player, point_name);
                }
            } else if (data_match.points[winning_player] == 4) { // AD
                player_wins_game(winning_player, point_name);
            }
            add_point_name(point_trans[data_match.points[0]], point_trans[data_match.points[1]], point_name, winning_player);
        }
        if (data_match.latest_event.level <= 1) {
            data_match.latest_event.text = data_match.players[winning_player] + " scored the point";
            if (data_match.tie_breaker_active[data_match.current_set]) {
                set_hit_tie_breaker();
            }
            else {
                if (data_match.switch_site == 0)
                    data_match.switch_site = 1;
                else if (data_match.switch_site == 1)
                    data_match.switch_site = 0;
                if (data_match.serve_right == 1)
                    data_match.current_hit = 1;
                else if (data_match.serve_right == 2)
                    data_match.current_hit = 2;
            }
            data_match.is_serve = 1;
            data_match.serve_fault = 0;
            draw_config();
            enable_back_button();
        }
        set_point();
        player_hit = null;
    }

    function set_hit_tie_breaker() {
        var score = data_match.tie_breaker_score[data_match.current_set]
        var tie_breaker_hit = score[0] + score[1];
        if (tie_breaker_hit == 1) {
            data_match.switch_site = 1;
            change_hit();
        }
        else {
            if (data_match.switch_site == 1) {
                data_match.switch_site = 0;
            }
            else if (data_match.switch_site == 0) {
                data_match.switch_site = 1;
                change_hit();
            }
        }
        if (tie_breaker_hit % 6 == 0 && data_match.change_coat == 1) {
            change_coat();
        }
    }

    function change_hit() {
        if (data_match.serve_right == 1) {
            data_match.serve_right = 2;
            data_match.current_hit = 2;
        }
        else if (data_match.serve_right == 2) {
            data_match.serve_right = 1;
            data_match.current_hit = 1;
        }
    }

    function player_wins_game(winning_player, point_name) {
        var losing_player = (winning_player + 1) % 2;
        data_match.first_f = [];
        var won_game = ['WON ' + point_name, "0"];
        if (winning_player != 0) {
            won_game = ["0", 'WON ' + point_name];
        }

        //reset point
        data_match.points[0] = 0;
        data_match.points[1] = 0;

        data_match.games[data_match.current_set][winning_player]++;
        if (data_match.games[data_match.current_set][winning_player] >= data_match.games_per_set) {
            // oponent will have at least 2 games difference
            if (data_match.games[data_match.current_set][winning_player] - data_match.games[data_match.current_set][losing_player] >= 2)
                player_wins_set(winning_player, point_name, false);
            else if (data_match.games[data_match.current_set][winning_player] == data_match.games_per_set && data_match.games[data_match.current_set][losing_player] == data_match.games_per_set)
                data_match.tie_breaker_active[data_match.current_set] = true;
        }

        if (data_match.latest_event.level <= 2) {
            data_match.is_serve = 1;
            data_match.switch_site = 0;
            data_match.serve_fault = 0;
            data_match.latest_event.level = 2;
            data_match.latest_event.text = data_match.players[winning_player] + " won the game";
            data_match.games_points[data_match.current_set][data_match.current_game].push(won_game); // push last point for winner
            data_match.current_game++; //set current game here to avoid increase to 1 when won the game
            data_match.games_points[data_match.current_set][data_match.current_game] = []; //recreate array current game
            check_coat();
            change_hit();
            draw_config();
            enable_back_button();
        }
        player_hit = null;
    }

    function player_wins_set(winning_player, point_name, is_tb) {
        var won_game = ['WON', 0];
        if (winning_player != 0) {
            won_game = [0, 'WON'];
        }
        if (is_tb) {
            var m_point = data_match.games_points[data_match.current_set][data_match.current_game].pop();
            data_match.first_f = [];
            m_point[0] = won_game[0] == 0 ? parseInt(m_point[0]) : parseInt(m_point[0]) + ' ' + won_game[0];
            m_point[1] = won_game[1] == 0 ? parseInt(m_point[1]) : parseInt(m_point[1]) + ' ' + won_game[1];
            won_game = [m_point[0], m_point[1]];
            data_match.games[data_match.current_set][winning_player]++;
        }

        if (winning_player == 0) won_game = [won_game[0] + ' ' + point_name, won_game[1].toString()];
        else if (winning_player == 1) won_game = [won_game[0].toString(), won_game[1] + ' ' + point_name];

        data_match.player_sets_won[winning_player]++;
        data_match.won_sets.push("#p" + parseInt(winning_player + 1) + "set" + parseInt(data_match.current_set + 1));
        if (data_match.player_sets_won[winning_player] == (data_match.sets + 1) / 2) {
            data_match.winner = winning_player;
            if (is_tb) data_match.games_points[data_match.current_set][data_match.current_game].push(won_game);
            return;
        }

        if (data_match.latest_event.level <= 3) {
            if (is_tie_breaker) {
                var count_game = data_match.games_points[data_match.current_set][data_match.current_game].length;
                if (count_game % 4 == 0 || count_game % 4 == 1) {
                    if (data_match.current_hit == 1) {
                        data_match.current_hit = 2;
                        data_match.serve_right = 2;
                    }
                    else {
                        data_match.current_hit = 1;
                        data_match.serve_right = 1;
                    }
                }
                else {
                    if (data_match.current_hit == 1) {
                        data_match.current_hit = 1;
                        data_match.serve_right = 1;
                    }
                    else {
                        data_match.current_hit = 2;
                        data_match.serve_right = 2;
                    }
                }
            }
            else change_hit();
            data_match.games_points[data_match.current_set][data_match.current_game].push(won_game); // push last point for winner
            data_match.current_set++;
            //reset to serve
            data_match.is_serve = 1;
            data_match.switch_site = 0;
            data_match.current_game = 0;
            data_match.games_points[data_match.current_set] = {}; //recreate object current set
            data_match.games_points[data_match.current_set][data_match.current_game] = []; //recreate array current game

            data_match.latest_event.level = 3;
            data_match.latest_event.text = data_match.players[winning_player] + " took the set";
            check_coat(true, is_tb);
            draw_config();
            enable_back_button();
        }
    }

    //tab button 1STサーブ
    $(document).on('click', '#serve-start', function () {
        draw_config();
        draw_scores();
        player_hit = data_match.current_hit;
        if (data_match.serve_fault == 0) back_up();
        $(this).addClass('disabled');
        $('.m-serve').removeClass('disabled').addClass('enable');
        $('.net-court.n-serve').removeClass('disabled').addClass('enable');
        $('.court-back button').removeClass('disabled');
    });

    //tab fault then enable 2NDサーブ
    $(document).on('click', '.m-serve.court-input-fault, .n-serve.court-input-fault', function () {
        $('#serve-start').removeClass('disabled').addClass('enable').children('span').text('2ND');
        if (!player_hit) player_hit = data_match.current_hit;
        count_1st_2nd();
        count_miss_serve(this, false);
        set_stats_serve(false);
        if (data_match.serve_fault == 0) {
            data_match.serve_fault = 1;
        } else if (data_match.serve_fault == 1) {
            $('.court-go button').addClass('disabled');
            data_match.serve_fault = 0;
            data_match.first_f.push({ [player_hit]: '' });
            turn_back = false;
            if (data_match.serve_right == 1) {
                player_scores(1, 'SvM'); // score point for opponent
                set_won_lose_breakdown(this, 0, false);
                data_match.stats[0].fault++;
            } else {
                player_scores(0, 'SvM'); // score point for myself
                set_won_lose_breakdown(this, 1, false);
                data_match.stats[1].fault++;
            }
            $('#serve-start').removeClass('disabled').addClass('enable').children('span').text('1ST');
        }
        draw_config();
        draw_scores();
        backup_app();
    });

    $(document).on('click', '.btn-win', function () {
        $('.btn-lose, .btn-win').addClass('disabled');
        count_1st_2nd();
        count_miss_serve(null, true);
        set_stats_serve();
        data_match.serve_fault == 0 ? data_match.first_f.push({ [player_hit]: 'F' }) : data_match.first_f.push({ [player_hit]: 'S' });
        var point_name = $(this).attr('point');
        if (point_name == 'Bs' || point_name == 'Fs') {
            point_name = 'St'
        }
        point_name = point_name + 'W';
        if (data_match.is_serve == 1) point_name = $(this).attr('point') == 'A' ? 'SvA' : 'SvW';
        if (data_match.current_hit == 2) {
            set_course_breakdown(1);
            set_won_lose_breakdown(this, 1, true);
            set_stats(this, 1);
            player_scores(1, point_name); // score point for opponent
        } else if (data_match.current_hit == 1) {
            set_course_breakdown(0);
            set_won_lose_breakdown(this, 0, true);
            set_stats(this, 0);
            player_scores(0, point_name); // score point for myself
        }
        turn_back = false;
        setTimeout(function () { $('.commit-point').collapse('hide') }, 200);
        $('.court-go button').addClass('disabled');
        draw_scores();
        backup_app();
    });

    $(document).on('click', '.btn-lose', function () {
        $('.btn-lose, .btn-win').addClass('disabled');
        count_1st_2nd();
        count_miss_serve(null, false);
        set_stats_serve(false);
        data_match.serve_fault == 0 ? data_match.first_f.push({ [player_hit]: 'F' }) : data_match.first_f.push({ [player_hit]: 'S' });
        var point_name = $(this).attr('point');
        if (point_name == 'Bs' || point_name == 'Fs') {
            point_name = 'St'
        }
        point_name = point_name + 'M';
        if (data_match.is_serve == 1) point_name = 'SvM';
        if (data_match.current_hit == 2) {
            set_course_breakdown(0);
            set_won_lose_breakdown(this, 1, false);
            set_stats(this, 0);
            player_scores(0, point_name); // score point for myself
        } else if (data_match.current_hit == 1) {
            set_course_breakdown(1);
            set_won_lose_breakdown(this, 0, false);
            set_stats(this, 1);
            player_scores(1, point_name); // score point for opponent
        }
        turn_back = false;
        setTimeout(function () { $('.commit-point').collapse('hide') }, 200);
        $('.court-go button').addClass('disabled');
        draw_scores();
        backup_app();
    });

    $(document).on('click', '.court-back button', function () {
        turn_back = true;
        old_data = data_match;
        restore_backup();
        $(this).addClass('disabled');
        $('.court-go button').removeClass('disabled');
        set_point();
    });

    $(document).on('click', '.court-go button', function () {
        turn_back = false;
        restore_app_backup(old_data);
        $('.court-back button').removeClass('disabled');
        $(this).addClass('disabled');
        set_point();
    });

    $('.match-points').mousedown(function (e) {
        down = true;
        scrollLeft = this.scrollLeft;
        court_x = e.clientX;
    }).mouseup(function () {
        down = false;
    }).mousemove(function (e) {
        if (down) {
            this.scrollLeft = scrollLeft + court_x - e.clientX;
        }
    }).mouseleave(function () {
        down = false;
    });

    function check_current_hit() {
        if (data_match.is_serve == 0) {
            if (($(target_point).hasClass('stroke-down') && data_match.coat_selection != 2)
                || ($(target_point).hasClass('stroke-up') && data_match.coat_selection == 2))
                data_match.current_hit = 2;
            else
                data_match.current_hit = 1;
        }
    }

    function set_serve() {
        $('.m-serve').removeClass('enable').addClass('disabled');
        $('.net-court.n-serve').addClass('disabled n-serve').removeAttr('hidden');
        $('.net-court.n-return').attr('hidden', true);
        $('.return-in-nml, .return-in-ad, .return-in-dc, .out-top, .out-top-left, .out-top-right, .out-bottom, .out-bottom-left, .out-bottom-right').attr("hidden", true);
        $('.court-note-serve').removeAttr('hidden');
        if ((data_match.coat_selection == 1 && data_match.serve_right == 1) || (data_match.coat_selection == 2 && data_match.serve_right == 2)) {
            $('div[class*="serve-right-up-"], div[class*="serve-left-up-"]').attr("hidden", true);
            $('.court-btn').removeClass('court-btn-top-wrapper').addClass('court-btn-bottom-wrapper');
            if (data_match.switch_site == 0) {//switch site after each point
                $('div[class*="serve-right-down-"]').removeAttr('hidden');
                $('div[class*="serve-left-down-"]').attr("hidden", true);
                $('.court-btn').find('.col-6').removeClass('mr-auto').addClass('ml-auto');
            } else {
                $('div[class*="serve-left-down-"]').removeAttr('hidden');
                $('div[class*="serve-right-down-"]').attr("hidden", true);
                $('.court-btn').find('.col-6').removeClass('ml-auto').addClass('mr-auto');
            }
        } else if ((data_match.coat_selection == 1 && data_match.serve_right == 2) || (data_match.coat_selection == 2 && data_match.serve_right == 1)) {
            $('div[class*="serve-left-down-"], div[class*="serve-right-down-"]').attr("hidden", true);
            $('.court-btn').removeClass('court-btn-bottom-wrapper').addClass('court-btn-top-wrapper');
            if (data_match.switch_site == 0) {//switch site after each point
                $('div[class*="serve-left-up-"]').removeAttr('hidden');
                $('div[class*="serve-right-up-"]').attr("hidden", true);
                $('.court-btn').find('.col-6').removeClass('ml-auto').addClass('mr-auto');
            } else {
                $('div[class*="serve-right-up-"]').removeAttr('hidden');
                $('div[class*="serve-left-up-"]').attr("hidden", true);
                $('.court-btn').find('.col-6').removeClass('mr-auto').addClass('ml-auto');
            }
        }
        $("#serve-start").removeAttr('hidden').removeClass('disabled').addClass('enable');
        if (data_match.serve_fault == 1) $('#serve-start').children('span').text('2ND');
        else $('#serve-start').children('span').text('1ST');
    }

    function set_return() {
        //hide serve
        $('#serve-start, div[class*="serve-right-up-"], div[class*="serve-left-up-"], div[class*="serve-left-down-"], div[class*="serve-right-down-"]').attr("hidden", true);
        //show return
        $('.return-in-nml, .return-in-ad, .return-in-dc, .out-top, .out-top-left, .out-top-right, .out-bottom, .out-bottom-left, .out-bottom-right').removeAttr('hidden');
        //net
        $('.net-court.n-serve').addClass('disabled n-serve').attr('hidden', true);
        $('.net-court.n-return').removeAttr('hidden');
        //note
        $('.court-note-serve').attr("hidden", true);
        $(".stroke-up").removeClass("disabled").addClass("enable");
        $(".stroke-down").removeClass("disabled").addClass("enable");
    }

    function enable_back_button() {
        //disable button
        $('.court-back button').removeClass('disabled').removeAttr('hidden');
    }

    function winning_state() {
        //hide serve
        $('#serve-start, div[class*="serve-right-up-"], div[class*="serve-left-up-"], div[class*="serve-left-down-"], div[class*="serve-right-down-"]').attr("hidden", true);
        //hide return
        $('.return-in-nml, .return-in-ad, .return-in-dc, .out-top, .out-top-left, .out-top-right, .out-bottom, .out-bottom-left, .out-bottom-right').attr("hidden", true);
        //disable button and hide go and back button
        $(".m-serve").addClass('disabled')
        $(".m-stroke").addClass('disabled')
        $('.court-go button').addClass('disabled').attr("hidden", true);
        $('.court-back button').addClass('disabled').attr("hidden", true);
        //disable and hide net
        $('.net-court').removeClass('enable').addClass('disabled').attr("hidden", true);
    }

    function back_up() {
        data_match.is_serve = 1;
        if (player_hit) data_match.current_hit = player_hit;
        data_match.serve_fault = 0;
        backup_data_match = jQuery.extend(true, {}, data_match);
    };

    function restore_backup() {
        if (backup_data_match != undefined) data_match = jQuery.extend(true, {}, backup_data_match);
        else {
            data_match.is_serve = 1;
            if (player_hit) data_match.current_hit = player_hit;
            data_match.serve_fault = 0;
        }
        draw_config();
        draw_scores();
        backup_app();
    };

    function fadeHtml(object, html) {
        if ($(object).html() != html) {
            $(object).fadeOut('fast', function () {
                $(this).html(html)
            }).fadeIn('fast');
        }
    }

    function set_location_change_point() {
        $('.match-points').removeAttr('hidden');
        if (data_match.coat_selection == 1) {
            $('.match-points-top .player-pos-text').text('相手');
            $('.match-points-bottom .player-pos-text').text('自分');
        }
        else {
            $('.match-points-top .player-pos-text').text('自分');
            $('.match-points-bottom .player-pos-text').text('相手');
        }
    }

    function add_point_name(point_0, point_1, point_name, winning_player) {
        var player_0 = '', player_1 = '';

        if (winning_player == 0) {
            player_0 = (point_0 != 0 || point_1 != 0) ? point_0 + ' ' + point_name : point_0.toString();
            player_1 = point_1.toString();
        }
        else if (winning_player == 1) {
            player_0 = point_0.toString();
            player_1 = (point_0 != 0 || point_1 != 0) ? point_1 + ' ' + point_name : point_1.toString();
        }
        if (player_0 == "0" && player_1 == "0") {
            if (data_match.serve_right == 1) player_0 = player_0 + " F";
            else player_1 = player_1 + " F";
        }
        data_match.games_points[data_match.current_set][data_match.current_game].push([player_0, player_1]);
    }

    function set_point() {
        var g_points = data_match.games_points[data_match.current_set][data_match.current_game];
        $('.serve_first').html('&nbsp;');
        $('.result_point').html('&nbsp;');
        $('.new-point').remove();
        if (g_points) {
            for (var i = 1; i < g_points.length; i++) {
                var point_1 = (String(g_points[i][0]).lastIndexOf(' ') > 0) ? g_points[i][0].substr(g_points[i][0].lastIndexOf(' ')).trim() : null;
                var point_2 = (String(g_points[i][1]).lastIndexOf(' ') > 0) ? g_points[i][1].substr(g_points[i][1].lastIndexOf(' ')).trim() : null;
                var downCoat = data_match.coat_selection == 1 ? 'bottom' : 'top';
                var upCoat = data_match.coat_selection == 2 ? 'bottom' : 'top';
                var idx = i + 1;
                var item_f = data_match.first_f[i - 1];
                var name_f = '&nbsp;';
                var key_f = null;
                if (item_f != undefined) {
                    key_f = Object.keys(item_f);
                    name_f = item_f[key_f];
                }
                if (point_1) {
                    var court = $('.point-' + downCoat + '-' + idx + ' .result_point');
                    if (court.length) court.text(point_1);
                    else add_court_name(downCoat, point_1);
                }
                else if (point_2) {
                    var court = $('.point-' + upCoat + '-' + idx + ' .result_point');
                    if (court.length) court.text(point_2);
                    else add_court_name(upCoat, point_2);
                }
                if (key_f) {
                    if (key_f[0] == 1) {
                        var court = $('.point-' + downCoat + '-' + idx + ' .serve_first');
                        if (court.length) court.html(name_f);
                        else add_f(downCoat, name_f);
                    }
                    else {
                        var court = $('.point-' + upCoat + '-' + idx + ' .serve_first');
                        if (court.length) court.html(name_f);
                        else add_f(upCoat, name_f);
                    }
                }
            }
        }
    }

    function add_f(location, name_f) {
        if (location == 'bottom') {
            $('.match-points-bottom .serve_first:last').html(name_f);
        }
        else if (location == 'top') {
            $('.match-points-top .serve_first:last').html(name_f);
        }
    }

    function add_court_name(location, point) {
        if (location == 'bottom') {
            set_court_bottom(point);
        }
        else if (location == 'top') {
            set_court_top(point);
        }
    }

    function set_court_top(point) {
        $('.match-points-top').append('<div class="point-name new-point"><div class="point-name-inner py-0"><div class="result_point">' + point + '</div><div class="serve_first">&nbsp;</div></div></div>');
        $('.match-points-bottom').append('<div class="point-name new-point"><div class="point-name-inner py-0"><div class="serve_first">&nbsp;</div><div class="result_point">&nbsp;</div></div></div>');
        var l = $('.match-points-top .point-name').length * $('.point-name:first').width();
        $('.match-points-top').scrollLeft(l);
        $('.match-points-bottom').scrollLeft(l);
        set_margin_court();
    }

    function set_court_bottom(point) {
        $('.match-points-top').append('<div class="point-name new-point"><div class="point-name-inner py-0"><div class="result_point">&nbsp;</div><div class="serve_first">&nbsp;</div></div></div>');
        $('.match-points-bottom').append('<div class="point-name new-point"><div class="point-name-inner py-0"><div class="serve_first">&nbsp;</div><div class="result_point">' + point + '</div></div></div>');
        var l = $('.match-points-bottom .point-name').length * $('.point-name:first').width();
        $('.match-points-bottom').scrollLeft(l);
        $('.match-points-top').scrollLeft(l);
        set_margin_court();
    }

    function check_deuce_side() {
        var is_deuce_side = false;
        if (data_match.is_serve == 1) {
            if (data_match.switch_site == 0) is_deuce_side = true;
        }
        else {
            var location = $(target_point).attr('location');
            if (location == 'right') is_deuce_side = true;
        }
        return is_deuce_side;
    }

    function set_course_breakdown(winning_player) {
        var player = player_hit - 1;
        var serve_side = 'ad_side';
        var serve_name = '1st';
        if (side) serve_side = 'deuce_side';
        if (data_match.serve_fault == 1) serve_name = '2nd';
        data_match.serve_course_breakdown[player][serve_side][serve_name].total++;
        data_match.serve_course_breakdown[player][serve_side][serve_name][data_match.course_point].total++;
        if (player == winning_player) data_match.serve_course_breakdown[player][serve_side][serve_name][data_match.course_point].win++;
    }

    function set_won_lose_breakdown(target, winning_player, is_won) {
        var n_fail = $(target_point).attr('fail');
        if (data_match.is_serve == 1) {
            var fail = $(target).attr('fail');
            if (fail == 'net') fail = 'net';
            else if (fail = 'out') fail = 'out';
            else if (fail = 'out_ad_side') fail = 'out_ad_side';
            else fail = 'out_deuce_side';
            if (is_won) data_match.won_lose_breakdown[winning_player].serve.won++;
            else data_match.won_lose_breakdown[winning_player].serve.lose[fail]++;
        }
        else {
            var n_point = $(target).attr('point');
            var n_point = $(target).attr('point');
            if (n_point == 'Rt') n_point = 'return';
            else if (n_point == 'Fs') n_point = 'forehandStroke';
            else if (n_point == 'Vo') n_point = 'volley';
            else if (n_point == 'Bs') n_point = 'backhandStroke';
            else if (n_point == 'Sm') n_point = 'smash';
            if (is_won && n_point) data_match.won_lose_breakdown[winning_player][n_point].won++;
            else if (n_fail) data_match.won_lose_breakdown[winning_player][n_point].lose[n_fail]++;
        }
    }

    function count_1st_2nd() {
        if (data_match.serve_fault == 0) {
            data_match.stats.total_1st++;
            if (player_hit == 1) data_match.stats.total_self_1st++;
            else data_match.stats.total_rival_1st++;
        }
        else {
            if (player_hit == 1) data_match.stats.total_self_2nd++;
            else data_match.stats.total_rival_2nd++;
        }
    }

    function set_ace_fault(target, player) {
        var point = $(target).attr('point');
        if (point == 'A') data_match.stats[player].ace++;
    }

    function set_analytic_serve(player, self, side, is_in) {
        data_match.stats[player][self][side].total++;
        if (is_in) data_match.stats[player][self][side].in++;
    }

    function set_stats_serve(is_in = true) {
        var self = 'self_1st';
        var side = 'ad_side';
        var player = 0;

        if (data_match.serve_fault == 1) self = 'self_2nd';
        if (check_deuce_side()) side = 'deuce_side';
        if (data_match.current_hit == 2) player = 1;
        if (data_match.is_serve == 1) {
            set_analytic_serve(player, self, side, is_in);
        }
    }

    function set_stats(target, winning_player) {
        set_ace_fault(target, winning_player);
        var player = null;
        if (data_match.serve_fault == 0) {
            player = 'self_1st_return';
            if ((player_hit - 1) != winning_player) player = 'rival_1st_return';
        }
        else if (data_match.serve_fault == 1) {
            player = 'self_2nd_return';
            if ((player_hit - 1) != winning_player) player = 'rival_2nd_return';
        }

        if (player) {
            if (side) data_match.stats[winning_player][player].deuce_side++;
            else data_match.stats[winning_player][player].ad_side++;
        }
    }


    function count_miss_serve(target, is_won) {
        var player = data_match.current_hit - 1;
        var result = null;
        if (!is_won) {
            target = target ? target : target_point;
            add_result_point(target, data_match.miss[player]);
        }

        if (data_match.is_serve == 1) {
            if (data_match.serve_fault == 0) {
                result = data_match.serve[player].serve_1st;
            }
            else {
                result = data_match.serve[player].serve_2nd;
            }
            add_result_point(target, result);
        }
    }

    function add_result_point(target, result) {
        var fail = $(target).attr('fail');
        if (fail == 'out' || $(target).text() == 'Out') result.out++;
        else if (fail == 'out_ad_side') result.out_ad_side++;
        else if (fail == 'out_deuce_side') result.out_deuce_side++;
        else if ($(target).text() == 'Net') result.net++;
        else {
            if (check_deuce_side()) result.deuce_side++;
            else result.ad_side++;
        }
    }

    function check_coat(is_win_set = false, is_tb = false) {
        if (data_match.change_coat == 1) {
            if (is_win_set) {
                var games = data_match.games_points[data_match.current_set - 1];
                var size = Object.keys(games).length;
                if (is_tb) change_coat();
                else if (size % 2 == 1) change_coat();
            }
            else if ((data_match.current_game + 1) % 2 == 0) change_coat();
        }
    }

    function change_coat() {
        if (data_match.coat_selection == 1) data_match.coat_selection = 2;
        else data_match.coat_selection = 1;
    }
});

function set_margin_court() {
    var width = $('.player-pos').width();
    $('.point-bottom-2').css({ 'margin-left': (width) + 'px' });
    $('.point-top-2').css({ 'margin-left': (width) + 'px' });
}
window.addEventListener('resize', set_margin_court);
