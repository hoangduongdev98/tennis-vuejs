$(function () {
    'use strict';
    
    // Sidebar
    $(document).on('click','.dismiss, .overlay',function(){
        $('.sidebar').removeClass('active');
        $('.overlay').removeClass('active');
        $('body').removeClass('overflow-hidden');
    });
    $(document).on('click','.open-menu',function(){
        $('.sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('body').addClass('overflow-hidden');
    });
    
    //locale and daterangepicker
    moment.locale('ja');
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false, 
        autoApply: true,
        startDate: moment()
    });
    //apply datepicker
    $('.datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });
    //clear datepicker button
    $('.datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    
    //delete match local storage
    $(document).on('click','.log-out',function(){
        if (localStorage.getItem("match_detail") !== null) {
            window.localStorage.removeItem('match_detail');
        }
    });
    
    // Get IE or Edge browser version
    var version = detectIE();

    if (version === false) {
//        console.log('not IE/Edge');
    } else if (version >= 12) {
//        console.log('Edge' + version);
    } else {
//        console.log('IE' + version);
    }

    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */
    function detectIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    }

    update_screen();
    window.addEventListener('resize', update_screen);
});

function update_screen() {
    var screen_h = $(window).height() - $('.navbar-custom').innerHeight() - $('#match-table').height() - $('.fixed-bottom').height();
    var court_h = screen_h;
    if ($('.match-points-top').height() && $('.match-points-bottom').height()) {
        court_h = screen_h - $('.match-points-top').height() - $('.match-points-bottom').height();
    }
    if ($(window).width() < 992 & $(window).height() > 768) {
        $('#accordion-court').css('height', screen_h + 'px');
        $('#court-body').css('height', court_h + 'px');
    }
    else {
        $('#accordion-court').css('height', '100%');
        $('#court-body').css('height', '100%');
    }
}