$(function () {
    'use strict';
    //input mask
    $('[data-mask]').inputmask();
    
    //input mask for remove default value of time picker (input mask conflict with datetimepicker when cleate time value)
    $('.datepicker-time .datetimepicker-input').inputmask({
        jitMasking: true
    });
    
    //select month
    $('.datepicker-month').datetimepicker({
        viewMode: 'years',
        format: 'YYYY/MM',
        locale: 'ja',
        allowInputToggle: true
    });
    
    //select time
    $('.datepicker-time').datetimepicker({
        format: 'LT',
        locale: 'ja',
        allowInputToggle: true
    });
    
    //locale and daterangepicker
    moment.locale('ja');
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false, 
        autoApply: true,
        startDate: moment()
    });
    //apply datepicker
    $('.datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });
    //clear datepicker button
    $('.datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    
    //increase z-index for open multi modal
    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1050 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
});