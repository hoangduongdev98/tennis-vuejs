<?php

define('PAGINATE_LIMIT', 50);

define('NO_ERROR', 0);
define('UNDELETED', 0);
define('DELETED', 1);
define('INFO_TYPE_SCHOOL', 1);
define('INFO_TYPE_TRAINER', 2);
define('INFO_TYPE_LESSON', 3);
define('INFO_TYPE_MESSAGE_TRAINER', 1);
define('INFO_TYPE_MESSAGE_PLAYER', 2);
define('INFO_TYPE_MESSAGE_SCHOOL', 3);
define('CODE_AJAX_ERROR', 0);
define('CODE_AJAX_SUCCESS', 1);
define('CODE_AJAX_ERROR_DATA', 2);
define('UPLOAD_MAX_FILE_SIZE', 2000000);
define('UNREFUNDED', 0);
define('REFUNDED', 1);
define('GUEST', -1);

define('PASSWORD', '********');

define('CHAT_ROOM_NAME_ONE_ON_ONE', 'チャンネル');

define('CATEGORY_DELETED', 'このカテゴリーは削除済です');

define('ADMIN_ROLE', 1);
define('SCHOOL_ROLE', 2);
define('TRAINER_ROLE', 3);

define('SCHOOL_STATUS_0', 0);
define('SCHOOL_STATUS_1', 1);
define('SCHOOL_STATUS_2', 2);
define('SCHOOL_STATUS_99', 99);

define('LESSON_STATUS_1', 1);
define('LESSON_STATUS_2', 2);
define('LESSON_STATUS_99', 99);

define('PLAYER_STATUS_0', 0);
define('PLAYER_STATUS_1', 1);
define('PLAYER_STATUS_2', 2);
define('PLAYER_STATUS_3', 3);
define('PLAYER_STATUS_99', 99);

define('AVATAR_SCHOOL_SAVE_TMP', $_SERVER['DOCUMENT_ROOT'].'/uploads/schools/tmp/');
define('AVATAR_SCHOOL_SAVE', $_SERVER['DOCUMENT_ROOT'].'/uploads/schools/');
define('AVATAR_SCHOOL_SAVE_TMP_JS', '/uploads/schools/tmp/');
define('AVATAR_SCHOOL_SAVE_JS', '/uploads/schools/');

define('AVATAR_LESSON_SAVE_TMP', $_SERVER['DOCUMENT_ROOT'].'/uploads/lessons/tmp/');
define('AVATAR_LESSON_SAVE', $_SERVER['DOCUMENT_ROOT'].'/uploads/lessons/');
define('AVATAR_LESSON_SAVE_TMP_JS', '/uploads/lessons/tmp/');
define('AVATAR_LESSON_SAVE_JS', '/uploads/lessons/');

define('AVATAR_TRAINER_SAVE_TMP', $_SERVER['DOCUMENT_ROOT'].'/uploads/trainers/tmp/');
define('AVATAR_TRAINER_SAVE', $_SERVER['DOCUMENT_ROOT'].'/uploads/trainers/');
define('AVATAR_TRAINER_SAVE_TMP_JS', '/uploads/trainers/tmp/');
define('AVATAR_TRAINER_SAVE_JS', '/uploads/trainers/');

define('CONTRACT_VALUE_0', 0);
define('CONTRACT_VALUE_1', 1);
define('CONTRACT_VALUE_2', 2);
define('CONTRACT_VALUE_3', 3);
define('CONTRACT_VALUE_4', 4);

define('ACCOUNT_TYPE_1', 1);
define('ACCOUNT_TYPE_VALUE_1', '普通');
define('ACCOUNT_TYPE_2', 2);
define('ACCOUNT_TYPE_VALUE_2', '当座');

define('REGISTER_COACH_0', 0);
define('REGISTER_COACH_VALUE_0', '許可しない');
define('REGISTER_COACH_1', 1);
define('REGISTER_COACH_VALUE_1', '許可する');

define('FIND_SCHOOL_0', 0);
define('FIND_SCHOOL_VALUE_0', '掲示しない');
define('FIND_SCHOOL_1', 1);
define('FIND_SCHOOL_VALUE_1', '掲示する');

define('LESSON_DATE_0', 0);
define('LESSON_DATE_VALUE_0', '1回');
define('LESSON_DATE_1', 1);
define('LESSON_DATE_VALUE_1', '毎週');
define('LESSON_DATE_2', 2);
define('LESSON_DATE_VALUE_2', 'レッスン詳細に掲載');

define('FREE_TRIAL_LESSON', '無料体験レッスン');

define('MONDAY', 0);
define('TUESDAY', 1);
define('WEDNESDAY', 2);
define('THURSDAY', 3);
define('FRIDAY', 4);
define('SATURDAY', 5);
define('SUNDAY', 6);
define('CELEBRATION', 7);

define('LIMIT_20', 20);
define('LIMIT_50', 50);
define('LIMIT_100', 100);

define('CHAT_ROOM_STATUS', 4);

define('LESSON_PLAYER_STATUS_0', 0);
define('LESSON_PLAYER_STATUS_1', 1);
define('LESSON_PLAYER_STATUS_2', 2);
define('LESSON_PLAYER_STATUS_3', 3);
define('LESSON_PLAYER_STATUS_4', 4);
define('LESSON_PLAYER_STATUS_99', 99);

define('PAYMENT_STATUS_0', 0);
define('PAYMENT_STATUS_1', 1);
define('PAYMENT_STATUS_2', 2);
define('PAYMENT_STATUS_99', 99);

define('ROLE_0', 0);
define('ROLE_1', 1);
define('ROLE_2', 2);

define('SUCCESS', 'success');
define('ERROR', 'error');

define('SETTLEMENT_0', 0);
define('SETTLEMENT_1', 1);
define('SETTLEMENT_2', 2);
define('SETTLEMENT_99', 99);

define('READ_MESSAGE', 0);
define('UNREAD_MESSAGE', 1);
define('READ_AND_ANSWER', 2);

define('ACTION_VALUE_0', 0);
define('ACTION_VALUE_1', 1);
define('ACTION_VALUE_2', 2);
define('ACTION_VALUE_3', 3);
define('ACTION_VALUE_4', 4);

define('DATE_DESC', 0);
define('DATE_ASC', 1);
define('PLAYER_ASC', 2);
define('PLAYER_DESC', 3);

define('LESSON_DATE_DESC', 1);
define('LESSON_DATE_ASC', 2);
define('LESSON_PRICE_DESC', 3);
define('LESSON_PRICE_ASC', 4);
define('NEARBY_AREA', 5);

define('ONETIME_PASSWORD_VALID', 30);

define('ADVICE_RETURN', 1);
define('ADVICE_FOREHAND', 2);
define('ADVICE_BACKHAND', 3);
define('ADVICE_VOLLEY', 4);
define('ADVICE_SMASH', 5);

define('TODAY', '今日');
define('YESTERDAY', '昨日');

define('DEFAULT_LEVEL', 0);

define('DEUCE_SIDE', 1);
define('DEUCE_SIDE_NAME', 'デュースサイド');
define('ADD_SIDE', 2);
define('ADD_SIDE_NAME', 'アドサイド');
define('NO_DATA_FOUND', 'データが見つかりません。');

define('ACTION_BUY_POINT', 'ポイント購入');

define('SERVE', 'Sv');
define('RETURN', 'Rt');
define('STROKE', 'St');
define('VOLLEY', 'Vo');
define('DROP', 'Dr');
define('SMASH', 'Sm');

define('NO_DATA_SHARE_FOUND', '試合解析情報を共有しているスクール／コーチはありません');

define('REGISTER_LESSON_0', 0);
define('REGISTER_LESSON_VALUE_0', '許可しない');
define('REGISTER_LESSON_1', 1);
define('REGISTER_LESSON_VALUE_1', '許可する');

define('SHARE_STATUS_0', 0);
define('SHARE_STATUS_1', 1);

define('SCHOOL_DATE_DESC', 1);
define('SCHOOL_DATE_ASC', 2);
define('SCHOOL_NEARBY_AREA', 3);

return [
    'school_status' => [
        SCHOOL_STATUS_0 => 'サービス前',
        SCHOOL_STATUS_1 => 'サービス中',
        SCHOOL_STATUS_2 => 'サービス終了',
        SCHOOL_STATUS_99 => '削除',
    ],
    'player_status' => [
        PLAYER_STATUS_0 => 'レッスン申込',
        PLAYER_STATUS_1 => 'レッスン前',
        PLAYER_STATUS_2 => 'レッスン中',
        PLAYER_STATUS_3 => 'レッスン終了',
        PLAYER_STATUS_99 => 'キャンセル',
    ],
    'lesson_status' => [
        LESSON_STATUS_1 => 'サービス中',
        LESSON_STATUS_2 => 'サービス終了',
        LESSON_STATUS_99 => '削除',
    ],

    'array_extension_file_avatar' => ['jpg', 'jpeg', 'png'],

    'list_contracts' => [
        CONTRACT_VALUE_0 => '無料',
        CONTRACT_VALUE_1 => 'ライト',
        CONTRACT_VALUE_2 => 'スタンダード①',
        CONTRACT_VALUE_3 => 'スタンダード②',
        CONTRACT_VALUE_4 => 'アドバンス',
    ],
    'limit_item' => [
        LIMIT_20 => '20件ずつ表示',
        LIMIT_50 => '50件ずつ表示',
        LIMIT_100 => '100件ずつ表示',
    ],

    'lesson_player_status' => [
        LESSON_PLAYER_STATUS_0 => 'レッスン申込',
        LESSON_PLAYER_STATUS_1 => 'レッスン前',
        LESSON_PLAYER_STATUS_2 => 'レッスン中',
        LESSON_PLAYER_STATUS_3 => 'レッスン終了',
        LESSON_PLAYER_STATUS_4 => 'キャンセル申請中',
        LESSON_PLAYER_STATUS_99 => 'キャンセル',
    ],
    'payment_status' => [
        PAYMENT_STATUS_0 => '未処理',
        PAYMENT_STATUS_1 => '処理中',
        PAYMENT_STATUS_2 => '支払済',
        PAYMENT_STATUS_99 => '削除',
    ],

    'admin_role' => [
        ROLE_0 => '閲覧者',
        ROLE_1 => '管理者',
        ROLE_2 => 'システム管理者',
    ],
    'settlement_status' => [
        SETTLEMENT_0 => 'カード決済済',
        SETTLEMENT_1 => 'ポイント消化',
        SETTLEMENT_2 => 'エラー',
        SETTLEMENT_99 => '削除',
    ],

    'day_of_week' => [
        MONDAY => '月',
        TUESDAY => '火',
        WEDNESDAY => '水',
        THURSDAY => '木',
        FRIDAY => '金',
        SATURDAY => '土',
        SUNDAY => '日',
    ],

    'lesson_date' => [
        LESSON_DATE_0 => '1回',
        LESSON_DATE_1 => '毎週',
        LESSON_DATE_2 => 'レッスン詳細に掲載',
    ],

    'list_price_contracts' => [
        CONTRACT_VALUE_0 => 0,
        CONTRACT_VALUE_1 => 3000,
        CONTRACT_VALUE_2 => 6000,
        CONTRACT_VALUE_3 => 6000,
        CONTRACT_VALUE_4 => 10000,
    ],

    'list_action_contracts' => [
        ACTION_VALUE_0 => 'スクール登録',
        ACTION_VALUE_1 => '試合解析管理機能',
        ACTION_VALUE_2 => '試合解析アドバイス',
        ACTION_VALUE_3 => 'オンラインレッスン',
        ACTION_VALUE_4 => 'ブログ掲載',
    ],

    'array_action_contracts' => [
        CONTRACT_VALUE_0 => [ACTION_VALUE_0],
        CONTRACT_VALUE_1 => [ACTION_VALUE_0, ACTION_VALUE_1],
        CONTRACT_VALUE_2 => [ACTION_VALUE_0, ACTION_VALUE_1, ACTION_VALUE_2, ACTION_VALUE_4],
        CONTRACT_VALUE_3 => [ACTION_VALUE_0, ACTION_VALUE_3, ACTION_VALUE_4],
        CONTRACT_VALUE_4 => [ACTION_VALUE_0, ACTION_VALUE_1, ACTION_VALUE_2, ACTION_VALUE_3, ACTION_VALUE_4],
    ],
    'sort_data' => [
        DATE_DESC => '日付降順',
        DATE_ASC => '日付昇順',
        PLAYER_ASC => 'プレーヤー名昇順',
        PLAYER_DESC => 'プレーヤー名降順',
    ],

    'sort_data_player_lesson' => [
        LESSON_DATE_DESC => '登録の新しい順',
        LESSON_DATE_ASC => '登録の古い順',
        LESSON_PRICE_DESC => '価格の高い順',
        LESSON_PRICE_ASC => '価格の安い順',
        NEARBY_AREA => '近くのエリア',
    ],

    'match_weather' => [
        1 => '晴れ',
        2 => '曇り',
        3 => '雨',
    ],

    'match_win_direction' => [
        1 => '東',
        2 => '西',
        3 => '南',
        4 => '北',
        5 => '北東',
        6 => '北西',
        7 => '南東',
        8 => '南西',
    ],

    'match_coat' => [
        1 => 'グラス',
        2 => 'クレー',
        3 => 'ハード',
        4 => 'オムニ',
        5 => 'カーペット',
    ],

    'match_format' => [
        1 => '1セットマッチ',
        3 => '3セットマッチ',
        5 => '5セットマッチ',
    ],

    'match_advantage' => [
        1 => 'デュースあり',
        2 => 'ノーアドバンテージ',
    ],

    'match_preferred_hand' => [
        1 => '右利き',
        2 => '左利き',
    ],

    'match_coat_selection' => [
        1 => '画面下',
        2 => '画面上',
    ],

    'match_change_coat' => [
        1 => 'する',
        2 => 'しない',
    ],

    'category_lesson' => [
        1 => 'ワンタイムレッスン',
        2 => 'ワンタイムレッスン（日付指定)',
    ],

    'match_serve_right' => [
        1 => '自分',
        2 => '相手',
    ],

    'match_default' => [
        'advantage' => 1,
        'change_coat' => 1,
        'points' => [0, 0],
        'tie_breaker_active' => (object) [],
        'current_set' => 0,
        'current_game' => 0,
        'player_sets_won' => [0, 0],
        'winner' => -1,
        'games' => [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]],
        'tie_breaker_score' => [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]],
        'latest_event' => ['level' => 0, 'text' => 'Match just started!'],
        'won_sets' => [],
        'first_f' => [],
        'games_per_set' => 6,
        'sets' => 3,
        'tie_breaker_points' => 7,
        'players' => ['自分', '相手'],
        'coat_selection' => 1,
        'serve_right' => 1,
        'serve_fault' => 0,
        'is_serve' => 1,
        'current_hit' => 1,
        'switch_site' => 0,
        'games_points' => (object) [0 => (object) [0 => [[0, 0]]]],
        'serve_course_breakdown' => (object) [0 => (object) ['deuce_side' => (object) ['1st' => (object) ['total' => 0, 'W' => (object) ['win' => 0, 'total' => 0], 'B' => (object) ['win' => 0, 'total' => 0], 'S' => (object) ['win' => 0, 'total' => 0]], '2nd' => (object) ['total' => 0, 'W' => (object) ['win' => 0, 'total' => 0], 'B' => (object) ['win' => 0, 'total' => 0], 'S' => (object) ['win' => 0, 'total' => 0]]], 'ad_side' => (object) ['1st' => (object) ['total' => 0, 'W' => (object) ['win' => 0, 'total' => 0], 'B' => (object) ['win' => 0, 'total' => 0], 'S' => (object) ['win' => 0, 'total' => 0]], '2nd' => (object) ['total' => 0, 'W' => (object) ['win' => 0, 'total' => 0], 'B' => (object) ['win' => 0, 'total' => 0], 'S' => (object) ['win' => 0, 'total' => 0]]]], 1 => (object) ['deuce_side' => (object) ['1st' => (object) ['total' => 0, 'W' => (object) ['win' => 0, 'total' => 0], 'B' => (object) ['win' => 0, 'total' => 0], 'S' => (object) ['win' => 0, 'total' => 0]], '2nd' => (object) ['total' => 0, 'W' => (object) ['win' => 0, 'total' => 0], 'B' => (object) ['win' => 0, 'total' => 0], 'S' => (object) ['win' => 0, 'total' => 0]]], 'ad_side' => (object) ['1st' => (object) ['total' => 0, 'W' => (object) ['win' => 0, 'total' => 0], 'B' => (object) ['win' => 0, 'total' => 0], 'S' => (object) ['win' => 0, 'total' => 0]], '2nd' => (object) ['total' => 0, 'W' => (object) ['win' => 0, 'total' => 0], 'B' => (object) ['win' => 0, 'total' => 0], 'S' => (object) ['win' => 0, 'total' => 0]]]]],
        'won_lose_breakdown' => (object) [0 => (object) ['serve' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'out_deuce_side' => 0, 'deuce_side' => 0, 'out_ad_side' => 0, 'ad_side' => 0, 'out' => 0]], 'return' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'volley' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'drop' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'smash' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'forehandStroke' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'backhandStroke' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]]], 1 => (object) ['serve' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'return' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'volley' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'drop' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'smash' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'forehandStroke' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]], 'backhandStroke' => (object) ['won' => 0, 'lose' => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]]]],
        'stats' => (object) ['total_1st' => 0, 'total_self_1st' => 0, 'total_self_2nd' => 0, 'total_rival_1st' => 0, 'total_rival_2nd' => 0, 0 => (object) ['ace' => 0, 'fault' => 0, 'self_1st' => (object) ['deuce_side' => (object) ['in' => 0, 'total' => 0], 'ad_side' => (object) ['in' => 0, 'total' => 0]], 'self_2nd' => (object) ['deuce_side' => (object) ['in' => 0, 'total' => 0], 'ad_side' => (object) ['in' => 0, 'total' => 0]], 'self_1st_return' => (object) ['deuce_side' => 0, 'ad_side' => 0], 'self_2nd_return' => (object) ['deuce_side' => 0, 'ad_side' => 0], 'rival_1st_return' => (object) ['deuce_side' => 0, 'ad_side' => 0], 'rival_2nd_return' => (object) ['deuce_side' => 0, 'ad_side' => 0], 'stroke' => (object) ['total' => 0, 'won' => 0], 'voley' => (object) ['total' => 0, 'won' => 0]], 1 => (object) ['ace' => 0, 'fault' => 0, 'self_1st' => (object) ['deuce_side' => (object) ['in' => 0, 'total' => 0], 'ad_side' => (object) ['in' => 0, 'total' => 0]], 'self_2nd' => (object) ['deuce_side' => (object) ['in' => 0, 'total' => 0], 'ad_side' => (object) ['in' => 0, 'total' => 0]], 'self_1st_return' => (object) ['deuce_side' => 0, 'ad_side' => 0], 'self_2nd_return' => (object) ['deuce_side' => 0, 'ad_side' => 0], 'rival_1st_return' => (object) ['deuce_side' => 0, 'ad_side' => 0], 'rival_2nd_return' => (object) ['deuce_side' => 0, 'ad_side' => 0], 'stroke' => (object) ['total' => 0, 'won' => 0], 'voley' => (object) ['total' => 0, 'won' => 0]]],
        'miss' => (object) [0 => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0], 1 => (object) ['net' => 0, 'deuce_side' => 0, 'ad_side' => 0, 'out' => 0]],
        'serve' => (object) [0 => (object) ['serve_1st' => (object) ['net' => 0, 'out_deuce_side' => 0, 'deuce_side' => 0, 'out_ad_side' => 0, 'ad_side' => 0, 'out' => 0], 'serve_2nd' => (object) ['net' => 0, 'out_deuce_side' => 0, 'deuce_side' => 0, 'out_ad_side' => 0, 'ad_side' => 0, 'out' => 0]], 1 => (object) ['serve_1st' => (object) ['net' => 0, 'out_deuce_side' => 0,  'deuce_side' => 0, 'out_ad_side' => 0,  'ad_side' => 0, 'out' => 0], 'serve_2nd' => (object) ['net' => 0, 'out_deuce_side' => 0, 'deuce_side' => 0, 'out_ad_side' => 0, 'ad_side' => 0, 'out' => 0]]],
    ],

    'match_note' => [
        'Sv' => 'サービス',
        'Rt' => 'リターン',
        'St' => 'ストローク',
        'Vo' => 'ボレー',
        'Dr' => 'ドロップ',
        'Sm' => 'スマッシュ',
    ],

    'advice_label' => [
        'advice_return' => 'リターン',
        'advice_fore_hand' => 'フォアハンド',
        'advice_back_hand' => 'バックハンド',
        'advice_smash' => 'スマッシュ',
        'advice_volley' => 'ボレー',
    ],

    'serve_label' => [
        'net_adv_content' => 'ネット',
        'overfault_adv_content' => 'オーバーフォルト',
        'deuce_adv_content' => 'サイドフォルト（デュース）',
        'add_side_content' => 'サイドフォルト（アドサイド）',
    ],

    'share_status' => [
        SHARE_STATUS_0 => '申請中',
        SHARE_STATUS_1 => '共有中',
    ],

    'sort_data_player_school' => [
        SCHOOL_DATE_DESC => '登録の新しい順',
        SCHOOL_DATE_ASC => '登録の古い順',
        SCHOOL_NEARBY_AREA => '近くのエリア',
    ],
];
