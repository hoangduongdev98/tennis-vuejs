<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Trainer;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('web')->prefix('trainer')->group(function () {
    Route::group(['namespace' => 'Trainer', 'middleware' => 'prevent-back-history'], function () {
        Route::get('/login', [Trainer\AuthController::class, 'getLogin'])->name('trainer.login');
        Route::post('/login', [Trainer\AuthController::class, 'postLogin']);
        Route::get('/logout', [Trainer\AuthController::class, 'logout'])->name('trainer.logout');
        // Route::get('/forgot-password', 'ForgotPasswordController@showLinkRequestForm')->name('trainer.forgot');
        // Route::post('/forgot-password', 'ForgotPasswordController@sendResetLinkEmail')->name('trainer.postForgot');
        // Route::get('/reset-password/{token}', 'ResetsPasswordController@showResetForm')->name('trainer.resetPassword');
        // Route::post('/reset-password', 'ResetsPasswordController@reset')->name('trainer.reset');
        Route::get('/', [Trainer\HomeController::class, 'index'])->name('trainer.index');
        Route::prefix('player')->group(function () {
            Route::any('/', 'PlayerController@index')->name('trainer.players');
            Route::get('/export/', 'PlayerController@playerExport')->name('trainer.playerExport');
            Route::get('/fetch_data', 'PlayerController@fetch_data')->name('trainer.data_player');
            Route::any('/search', 'PlayerController@index')->name('trainer.searchPlayer');
            Route::get('/player-page/{playerId}', 'PlayerController@playerPage')->name('trainer.playerPage');
            Route::get('/player-matches-page/{playerId}', 'PlayerController@playerMatchPage')->name('trainer.playerMatchesPage');
            Route::get('/share', 'PlayerController@listShare')->name('trainer.listShare');
            Route::any('/share/search', 'PlayerController@listShare')->name('trainer.searchSharedPlayer');
            Route::get('/share/export/', 'PlayerController@exportSharedPlayer')->name('trainer.playerSharedExport');
            Route::post('/share/accept', 'PlayerController@acceptShare')->name('trainer.acceptShare');
            Route::post('/share/delete', 'PlayerController@deleteShare')->name('trainer.deleteShare');
        });
        Route::prefix('lessons')->group(function () {
            Route::any('/', 'LessonController@index')->name('lesson.list');
            Route::get('/export', 'LessonController@lessonExport')->name('lesson.export');
            Route::any('/search', 'LessonController@index')->name('lesson.search');
            Route::get('/create', 'LessonController@createLesson')->name('lesson.create');
            Route::post('/create', 'LessonController@postCreateLesson')->name('lesson.postCreateLesson');
            Route::any('/getListPlayer', 'LessonController@getListPlayerPopUp')->name('lesson.getListPlayer');
            Route::get('/edit/{idLesson}', 'LessonController@editLesson')->name('lesson.edit_lesson');
            Route::post('/edit', 'LessonController@postEditLesson')->name('lesson.postEditLesson');
            Route::any('/edit-confirm/{idLesson}', 'LessonController@editConfirmLesson')->name('lesson.edit_lesson_confirm');
            Route::post('/process-save-edit-lesson', 'LessonController@saveEditLesson')->name('lesson.saveEditLesson');
            Route::get('/detail/{idLesson}', 'LessonController@detailLesson')->name('lesson.detail');
            Route::any('/delete-lesson/{idLesson}', 'LessonController@deleteLesson')->name('lesson.delete');
            Route::get('/delete/{idSetting}', 'LessonController@deleteSettlementAndFee')->name('lesson.deleteSettingSettlementAndFee');
            Route::any('/create-confirm', 'LessonController@createConfirmLesson')->name('lesson.create_lesson_confirm');
            Route::get('/sort-lesson', 'LessonController@sortLesson')->name('lesson.sort_lesson');
            Route::get('/sort-lesson-today', 'LessonController@sortLessonToday')->name('lesson.sort_lesson_today');
            Route::any('/categories-list', 'LessonController@sortAndPaginationCategories')->name('lesson.listCategory');
            Route::get('/categories/{idCategory}', 'CategoriesManagerController@getDataCategoryById')->name('lesson.getCategory');
            Route::post('/process-save-create', 'LessonController@saveCreateLesson')->name('lesson.saveCreateLesson');
            Route::post('/categories', 'CategoriesManagerController@postCreateCategories')->name('lesson.categoriesCreate');
            Route::post('/categories/edit/{idCategory}', 'CategoriesManagerController@edit')->name('lesson.editCategory');
            Route::get('delete-category/{idCategory}', 'CategoriesManagerController@deleteCategory')->name('lesson.deleteCategory');
            Route::post('/process-save-edit-category/{idCategory}', 'CategoriesManagerController@saveEditCategory')->name('lesson.saveEditCategory');
            Route::post('/categories/save', 'CategoriesManagerController@saveCreateCategories')->name('lesson.postCategoriesCreate');
            Route::post('/upload-avatar/{img}', 'LessonController@uploadAvatarLesson')->name('lesson.uploadAvatar');
            Route::any('/addOneToOne', 'LessonController@addNewChatOneToOne')->name('trainer.addOneToOne');
            Route::post('/rejectPlayer', 'LessonController@rejectPlayerJoinLesson')->name('trainer.rejectPlayer');
            Route::post('/cancelPlayer', 'LessonController@cancelPlayerInLesson')->name('trainer.cancelPlayer');
            Route::post('/cancelReject', 'LessonController@cancelReject')->name('trainer.cancelReject');
            Route::post('/completeLesson', 'LessonController@completeLesson')->name('trainer.completeLesson');
            Route::any('/sendMessage', 'LessonController@sendMessage')->name('trainer.sendMessage');

        });
        Route::prefix('settlement')->group(function () {
            Route::any('/', 'SettlementController@settlements')->name('trainer.settlements');   
            Route::get('/export', 'SettlementController@settlementExport')->name('trainer.settlementExport');
            Route::any('/search', 'SettlementController@settlements')->name('trainer.searchSettlement');
        });
        Route::prefix('trainer')->group(function () {
            Route::get('/edit', 'TrainerController@trainerEdit')->name('trainer.edit');
            Route::post('/edit', 'TrainerController@postEditTrainer')->name('trainer.postEditTrainer');
            Route::get('/edit-confirm/{key}', 'TrainerController@editConfirmTrainer')->name('trainer.editTrainerConfirm');
            Route::post('/process-save-edit', 'TrainerController@saveEditTrainer')->name('trainer.saveEditTrainer');
            Route::post('/upload-avatar/{img}', 'TrainerController@uploadAvatarTrainer')->name('trainer.uploadAvatar');
            Route::post('/get-address', 'TrainerController@getAddress')->name('trainer.getAddressTrainer');
            Route::get('delete/{idSetting}', 'TrainerController@deleteStructureFee')->name('trainer.deleteStructureFee');
        });
        Route::prefix('message')->group(function () {
            Route::get('/', 'MessagesController@index')->name('trainer.message');
            Route::any('/getMessage', 'MessagesController@getMessage')->name('trainer.getMessage');
            Route::any('/search', 'MessagesController@index')->name('trainer.message.search');
        });
        Route::prefix('match-analysis')->group(function () {
            Route::get('/', 'MatchController@index')->name('trainer.match');
            Route::post('/postAnalyst', 'MatchController@postAnalyst')->name('trainer.postAnalyst');
            Route::get('/confirmAnalyst/{key}', 'MatchController@confirmAnalyst')->name('trainer.confirmAnalyst');
            Route::post('/saveAnalyst', 'MatchController@saveAnalyst')->name('trainer.saveAnalyst');
        });
    });
});
