<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Auth\Passwords\CanResetPassword;

class Players extends Authenticatable
{
    use HasFactory, Notifiable, Sortable, CanResetPassword;

    protected $table = 'players';

    protected $fillable = [
        'name',
        'address',
        'phone',
        'email',
        'password',
        'point',
        'last_login',
    ];

    public $sortable = ['updated_at', 'updatedPlayer', 'playerLessonStatus', 'last_login'];

    public $timestamps = true;

    public function lesson()
    {
        return $this->belongsToMany(Lessons::class,'player_lessons','player_id','lesson_id')->withPivot(['status']);
    }
    public function player_lessons()
    {
        return $this->hasMany(PlayerLesson::class, 'player_id', 'id');
    }

    public function playerMatches() {
        return $this->hasMany(Matches::class, 'created_id', 'id');
    }

    public function playerChatParticipant() {
        return $this->hasMany(ChatRoomParticipants::class, 'member_id', 'id');
    }

    public function updatedPlayerSortable($query, $direction) {
        return $query->orderBy('updatedPlayer', $direction);
    }

    public function playerLessonStatusSortable($query, $direction) {
        return $query->orderBy('playerLessonStatus', $direction);
    }

    public function chat_room()
    {
        return $this->hasMany(ChatRooms::class, 'player_id', 'id');
    }
}
