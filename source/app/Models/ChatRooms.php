<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
/**
 * Description of Categories
 *
 */
class ChatRooms extends Model {
   
    use HasFactory, Notifiable;

    protected $table = 'chat_rooms';

    protected $fillable = ['player_id','lesson_id'];

    public $timestamps = true;

    public function player()
    {
        return $this->belongsTo(Players::class, 'player_id', 'id');
    }
}
