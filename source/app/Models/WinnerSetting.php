<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class WinnerSetting extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'winner_setting';

    protected $fillable = [
        'name',
        'id',
    ];
}
