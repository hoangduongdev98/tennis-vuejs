<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Images extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'images';

    protected $fillable = ['name', 'path', 'position', 'table_id', 'info_types'];

    public $timestamps = true;

}
