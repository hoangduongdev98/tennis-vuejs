<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonFlags extends Model
{
    use HasFactory;
    protected $table = 'lesson_flags';
    protected $fillable = ['id ','player_lesson_id','flag','date_register'];

    public $timestamps = true;
}
