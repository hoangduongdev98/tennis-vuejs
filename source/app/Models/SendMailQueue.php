<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class SendMailQueue extends Model
{
    protected $table = 'send_mail_queue';

    protected $fillable = [
        'id_mess',
        'flag',
    ];
}
