<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class Schools extends Model
{
    use HasFactory, Notifiable, Sortable;

    protected $table = 'schools';

    protected $fillable = [
        'contract_id',
        'name',
        'representative',
        'post_code',
        'prefecture_id',
        'address',
        'phone',
        'fax',
        'day_off',
        'note',
        'status',
        'license_date',
        'bank_name',
        'branch_bank',
        'account_type',
        'account_number',
        'account_holder',
        'levels',
        'ratio',
        'register_coach',
        'find_school',
        'register_lesson',
        'achievement',
        'page_url'
    ];

    public $sortable = ['contract_id', 'prefecture_id', 'status', 'updated_at'];

    public $timestamps = true;

    public function login_staff()
    {
        return $this->hasMany(Staffs::class, 'school_id','id');
    }

    public function school_contract()
    {
        return $this->belongsTo(Contracts::class, 'contract_id', 'id');
    }

    public function school_trainers()
    {
        return $this->hasMany(Trainers::class, 'school_id', 'id');
    }

    public function school_lesson()
    {
        return $this->hasMany(Lessons::class, 'table_id', 'id')->where('lessons.info_type', 1);
    }
    
    public function images()
    {
        return $this->hasMany(Images::class, 'table_id', 'id')->where('images.info_types', 1);
    }
    
    public function school_trainers_player()
    {
        return $this->hasMany(Trainers::class, 'school_id', 'id')
            ->where([
                'is_deleted' => UNDELETED,
                'status' => SCHOOL_STATUS_1,
                'schools.status' => SCHOOL_STATUS_1,
                'schools.find_school' => FIND_SCHOOL_1,
                'schools.is_deleted' => UNDELETED
            ]);
    }
}
