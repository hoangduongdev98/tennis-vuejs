<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\Models\PlayerLesson;
use Kyslik\ColumnSortable\Sortable;
use Auth;

class Settlements extends Model
{
    use HasFactory, Notifiable, Sortable;

    protected $table = 'settlements';

    protected $fillable = ['id', 'player_lesson_id ', 'payment_date', 'sett_price', 'status'];

    public $sortable = ['payment_date', 'sett_price', 'status'];

    public $timestamps = true;

    public function feeSortable($query, $direction)
    {
        return $query->orderBy('lessons.payment_fee', $direction);
    }

    public function settlement_player_lesson() {
        return $this->belongsTo(PlayerLesson::class, 'player_lesson_id', 'id');
    }

    public function settlement_payment() {
        return $this->hasMany(Payments::class, 'settlement_id', 'id');
    }

    public function lessonFeeSortable($query, $direction)
    {
        return $query->orderBy('lessons.payment_fee', $direction);
    }

}
