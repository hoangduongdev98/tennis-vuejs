<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\Mail\Admin\PasswordReset;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Kyslik\ColumnSortable\Sortable;

class AdminLogin extends Authenticatable
{
    use HasFactory, Notifiable, CanResetPassword, Sortable;

    protected $table = 'admin_login';

    protected $fillable = [
        'id', 'user_name', 'email', 'password', 'role', 'is_deleted',
    ];

    public $timestamps = true;

    public $sortable = ['role', 'updated_at'];

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->getEmailForPasswordReset())->send(new PasswordReset($token, $this));
    }

}
