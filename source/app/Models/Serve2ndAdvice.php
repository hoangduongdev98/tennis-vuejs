<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Serve2ndAdvice extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'serve_2nd_advice';

    protected $fillable = [
        'name',
        'id',
    ];
}
