<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class SharingAnalyses extends Model
{
    use HasFactory, Sortable;
    
    protected $table = 'sharing_analyses';
    
    public $sortable = ['id', 'status', 'updated_at', 'lastLogin'];

    protected $fillable = [
        'player_id',
        'school_id',
        'trainer_id',
        'status',
    ];
    
    public function player()
    {
        return $this->belongsTo(Players::class, 'player_id', 'id')->where('is_deleted', UNDELETED);
    }
    
    public function school()
    {
        return $this->belongsTo(Schools::class, 'school_id', 'id')->where('is_deleted', UNDELETED)->where('status', SCHOOL_STATUS_1);
    }
    
    public function trainer()
    {
        return $this->belongsTo(Trainers::class, 'trainer_id', 'id')->where('is_deleted', UNDELETED)->where('status', SCHOOL_STATUS_1);
    }
    
    public function lastLoginSortable($query, $direction) {
        return $query->orderBy('player_last_login', $direction);
    }
}
