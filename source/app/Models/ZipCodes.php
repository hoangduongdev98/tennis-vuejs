<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class ZipCodes extends Model
{
    use HasFactory, Notifiable;

    public $timestamps = false;
    protected $table = 'zip_codes';

    protected $fillable = [
        'zip_city_id',
        'zip_code_frt',
        'zip_code',
        'zip_pref_kana',
        'zip_city_kana',
        'zip_other_kana',
        'zip_pref',
        'zip_city',
        'zip_other',
        'zip_no1',
        'zip_no2',
        'zip_no3',
        'zip_no4',
        'zip_no5',
        'zip_no6',
    ];
}
