<?php

namespace App\Models;

use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;

class Lessons extends Model
{
    use HasFactory;
    use Notifiable;
    use Sortable;

    protected $table = 'lessons';

    protected $fillable = [
        'table_id',
        'info_type',
        'name',
        'category',
        'quantity_player',
        'payment_fee',
        'lesson_date',
        'times_study',
        'status',
        'lesson_detail',
        'levels',
    ];

    public $sortable = ['payment_fee', 'status', 'updated_at'];

    public $timestamps = true;

    public function updateLessonSortable($query, $direction)
    {
        return $query->orderBy('updated_at', $direction);
    }

    public function expireLessonSortable($query, $direction)
    {
        return $query->orderBy('register_expire_date', $direction);
    }

    public function lessonPointSortable($query, $direction)
    {
        return $query->orderBy('payment_fee', $direction);
    }

    public function lessonStatusSortable($query, $direction)
    {
        return $query->orderBy('status', $direction);
    }

    public function pointTodaySortable($query, $direction)
    {
        return $query->orderBy('payment_fee', $direction);
    }

    public function lesson_trainer()
    {
        return $this->belongsTo(Trainers::class, 'table_id', 'id');
    }

    public function lesson_school()
    {
        return $this->belongsTo(Schools::class, 'table_id', 'id');
    }

    public function player()
    {
        return $this->belongsToMany(Players::class, 'player_lessons', 'lesson_id', 'player_id');
    }

    public function lesson_player()
    {
        return $this->hasMany(PlayerLesson::class, 'lesson_id', 'id');
    }

    public function image()
    {
        return $this->hasOne(Images::class, 'table_id')
            ->where([
                'info_types' => INFO_TYPE_LESSON,
                'is_deleted' => UNDELETED,
            ]);
    }
    
    public function lesson_trainers()
    {
        return $this->belongsTo(Trainers::class, 'table_id', 'id')->where('is_deleted', UNDELETED)->where('status', SCHOOL_STATUS_1)->where('lessons.info_type', INFO_TYPE_TRAINER);
    }

    public function lesson_schools()
    {
        return $this->belongsTo(Schools::class, 'table_id', 'id')->where('is_deleted', UNDELETED)->where('status', SCHOOL_STATUS_1)->where('lessons.info_type', INFO_TYPE_SCHOOL);
    }
}
