<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\Models\Settlement;
use Kyslik\ColumnSortable\Sortable;

class Payments extends Model
{
    use HasFactory, Notifiable, Sortable;

    protected $table = 'payments';

    protected $fillable = ['id','settlement_id','info_type','status','updated_at','payment_date','payment_point'];

    public $timestamps = true;

    public $sortable = ['total', 'payment_date', 'payment_point', 'totalPayment', 'monthYear', 'paymentDate', 'paymentPoint'];

    public function settlement() {
        return $this->belongsTo(Settlement::class, 'settlement_id', 'id');
    }

    public function totalSortable($query, $direction) {
        return $query->orderBy('total', $direction);
    }

    public function totalPaymentSortable($query, $direction) {
        return $query->orderBy('totalPayment', $direction);
    }

    public function monthYearSortable($query, $direction) {
        return $query->orderBy('monthYear', $direction);
    }

    public function paymentDateSortable($query, $direction) {
        return $query->orderBy('paymentDate', $direction);
    }

    public function paymentPointSortable($query, $direction) {
        return $query->orderBy('paymentPoint', $direction);
    }

}
