<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class MasterLevels extends Model
{
    use HasFactory, Notifiable, Sortable;

    protected $table = 'master_levels';

    protected $fillable = [
        'name',
        'display_order',
    ];

    public $timestamps = true;

    public $sortable = ['display_order', 'updated_at'];

}
