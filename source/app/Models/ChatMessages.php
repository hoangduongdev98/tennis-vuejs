<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
/**
 * Description of Categories
 *
 */
class ChatMessages extends Model {
   
    use HasFactory, Notifiable;

    protected $table = 'chat_messages';

    protected $fillable = ['chat_room_id','message','from','to','info_type','unread_message'];

    public $timestamps = true;
}
