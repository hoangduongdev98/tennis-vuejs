<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class SettingSettlementFee extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'setting_settlement_fee';

    protected $fillable = [
        'point',
        'price',
        'token',
    ];

    public $timestamps = true;

}
