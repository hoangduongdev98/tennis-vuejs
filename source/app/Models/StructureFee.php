<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
/**
 * Description of StructreFee
 *
 */
class StructureFee extends Model {
   
    use HasFactory;
    protected $table = 'structure_fee';

    protected $fillable = ['id', 'name ', 'fee', 'table_id', 'info_type'];

    public $timestamps = true;
}
