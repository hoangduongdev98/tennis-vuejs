<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Mail\School\PasswordReset;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class Staffs extends Authenticatable
{
    use HasFactory, Notifiable, CanResetPassword, Sortable;

    protected $table = 'staffs';



    protected $fillable = [
        'school_id', 'school_manager', 'email', 'password', 'role',
    ];

    public $sortable = [
                        'role',
                        'updated_at',];

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public $timestamps = true;

    public function staff_schools()
    {
        return $this->belongsTo(Schools::class, 'school_id', 'id');
    }
    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->getEmailForPasswordReset())->send(new PasswordReset($token, $this));
    }
}
