<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Mail\Trainer\PasswordReset;
use Kyslik\ColumnSortable\Sortable;

class Trainers extends Authenticatable
{
    use HasFactory, Notifiable, Sortable;

    protected $table = 'trainers';

    protected $fillable = [
        'id',
        'email',
        'name',
        'address',
        'post_code',
        'password',
        'school_id',
        'phone',
        'fax',
        'status',
        'license_date',
        'bank_name',
        'branch_bank',
        'account_type',
        'account_number',
        'account_holder',
        'ratio',
        'levels',
    ];

    public $sortable = ['update',
                        'prefecture_id',
                        'status',
                        'updated_at',
                        'lesson_count',
                        'player_count',
                        'trainerStatus'];

    public $timestamps = true;

    public function trainer_school()
    {
        return $this->belongsTo(Schools::class, 'school_id', 'id');
    }

    public function trainer_lesson()
    {
        return $this->hasMany(Lessons::class, 'table_id', 'id')->where('lessons.info_type', 2);
    }

    public function scopeLogin($query)
    {
        return $query->where(['school_id'  => Auth::guard('schools')->user()->school_id,'trainers.is_deleted' => UNDELETED]);
    }

    public function updateSortable($query, $direction) {
        return $query->orderBy('update', $direction);
    }
    
    public function trainerStatusSortable($query, $direction) {
        return $query->orderBy('trainerStatus', $direction);
    }

    public function lessonCountSortable($query, $direction) {
        return $query->orderBy('lesson_count', $direction);
    }

    public function playerCountSortable($query, $direction) {
        return $query->orderBy('player_count', $direction);
    }

    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->getEmailForPasswordReset())->send(new PasswordReset($token, $this));
    }
    
    public function images()
    {
        return $this->hasMany(Images::class, 'table_id', 'id')->where('images.info_types', 2);
    }
}
