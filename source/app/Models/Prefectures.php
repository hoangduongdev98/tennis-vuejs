<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Prefectures extends Model
{
    public $timestamps = false;
    protected $table = 'prefectures';
    use HasFactory;
    protected $fillable = [
        'name',
        'kana',
    ];
}
