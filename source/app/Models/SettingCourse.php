<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class SettingCourse extends Model
{
    use HasFactory, Notifiable;
    protected $table = 'setting_course';

    protected $fillable = [
        'name',
        'id',
    ];
}
