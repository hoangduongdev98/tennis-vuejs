<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class PlayerLesson extends Model
{
    use HasFactory, Notifiable,Sortable;

    protected $table = 'player_lessons';

    protected $fillable = [
        'player_id',
        'lesson_id',
        'status',
    ];

    public $sortable = ['status','updated_at','created_at'];
    public $timestamps = true;

    public function player()
    {
        return $this->belongsTo(Players::class,'player_id','id');
    }
    public function lesson()
    {
        return $this->belongsTo(Lessons::class,'lesson_id','id');
    }

    public function payment()
    {
        return $this->hasMany(Payment::class, 'player_lesson_id', 'id');
    }

    public function player_lesson_settlement () {
        return $this->hasOne(Settlement::class, 'player_lesson_id');
    }

}
