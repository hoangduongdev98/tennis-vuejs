<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Matches extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'matches';

    protected $fillable = [
        'match_name',
        'wind_direction',
        'place',
        'weather',
        'coat',
        'match_format',
        'advantage',
        'opponent',
        'preferred_hand',
        'coat_selection',
        'change_coat',
        'serve_right',
        'match_detail',
        'created_id'
    ];

    public $timestamps = true;

    public function playerOfMatches() {
        $this->belongsTo(Player::class, 'created_id', 'id');
    }
    
}
