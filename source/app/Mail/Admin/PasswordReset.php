<?php
namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AdminLogin;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var AdminLogin
     */
    protected $adminLogin;

    /**
     * @var string
     */
    protected $token;

    /**
     * PasswordReset constructor.
     * @param  AdminLogin $adminLogin
     * @param  $token
     */
    public function __construct($token, $adminLogin)
    {
        $this->token = $token;
        $this->adminLogin  = $adminLogin;
    }


    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        $subject = '[tennis_mgt] password request';
        return $this->text('admin.auth.email_password_reset', [
            'adminLogin'  => $this->adminLogin,
            'token' => $this->token,
        ])->subject($subject);
    }
}
