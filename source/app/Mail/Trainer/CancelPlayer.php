<?php

namespace App\Mail\Trainer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class CancelPlayer extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $playerCancel;

    /**
     * cancel Player constructor.
     *
     * @param  $playerCancel
     */
    public function __construct($playerCancel)
    {
        $this->playerCancel = $playerCancel;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $trainer = Auth::guard('trainers')->user();
        $subject = '[ポケコーチ]'.$trainer->name.'様からレッスンのキャンセル申請が届いています。';

        return $this->text('trainer.email.cancel_player', $this->playerCancel)->subject($subject);
    }
}
