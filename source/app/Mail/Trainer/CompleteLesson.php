<?php

namespace App\Mail\Trainer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class CompleteLesson extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $playerComplete;

    /**
     * cancel Player constructor.
     *
     * @param  $playerComplete
     */
    public function __construct($playerComplete)
    {
        $this->playerComplete = $playerComplete;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $trainer = Auth::guard('trainers')->user();
        $subject = '[ポケコーチ]'.$trainer->name.'様からレッスンの完了申請が届いています。';

        return $this->text('trainer.email.complete_lesson', $this->playerComplete)->subject($subject);
    }
}
