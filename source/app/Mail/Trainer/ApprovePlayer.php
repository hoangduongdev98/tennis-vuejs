<?php

namespace App\Mail\Trainer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApprovePlayer extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $playerApprove;

    /**
     * Approve Player constructor.
     *
     * @param  $playerApprove
     */
    public function __construct($playerApprove)
    {
        $this->playerApprove = $playerApprove;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]レッスンの申し込みが確定いたしました。';

        return $this->text('trainer.email.approve_player', $this->playerApprove)->subject($subject);
    }
}
