<?php

namespace App\Mail\Trainer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMessage extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $sendMail;
    public $trainerName;

    /**
     * send message Player constructor.
     *
     * @param  $sendMail
     */
    public function __construct($sendMail, $trainerName)
    {
        $this->sendMail = $sendMail;
        $this->trainerName = $trainerName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]'.$this->trainerName.'様から新しいメッセージが届いています。';

        return $this->text('trainer.email.send_message ', $this->sendMail)->subject($subject);
    }
}
