<?php
namespace App\Mail\Trainer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Trainers;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $trainer;


    /**
     * @var string
     */
    protected $token;


    /**
     * PasswordReset constructor.
     * @param  Trainers $trainer
     * @param  $token
     */
    public function __construct($token, $trainer)
    {
        $this->token = $token;
        $this->trainer  = $trainer;
    }


    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        $subject = '[tennis_mgt] password request';
        return $this->text('trainer.email.password_reset', [
            'trainer'  => $this->trainer,
            'token' => $this->token,
        ])->subject($subject);
    }
}