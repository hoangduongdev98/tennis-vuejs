<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyToPlayer extends Mailable
{
    use Queueable, SerializesModels;

    
    public $info;


    /**
     * Approve Player constructor.
     * @param  $info
     */
    public function __construct($info)
    {
        $this->info = $info;
    }


    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]レッスンご予約のお知らせ';       
        return $this->text('layouts.email.notify_to_player', $this->info)->subject($subject);
    }
}