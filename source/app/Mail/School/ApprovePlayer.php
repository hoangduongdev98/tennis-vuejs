<?php
namespace App\Mail\School;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Trainers;

class ApprovePlayer extends Mailable
{
    use Queueable, SerializesModels;

    
    public $playerApprove;


    /**
     * Approve Player constructor.
     * @param  $playerApprove
     */
    public function __construct($playerApprove)
    {
        $this->playerApprove = $playerApprove;
    }


    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]レッスンの申し込みが確定いたしました。';       
        return $this->text('school.email.approve_player',$this->playerApprove)->subject($subject);
    }
}