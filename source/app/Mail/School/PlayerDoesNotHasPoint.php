<?php
namespace App\Mail\School;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Trainers;

class PlayerDoesNotHasPoint extends Mailable
{
    use Queueable, SerializesModels;

    
    public $player;


    /**
     * Approve Player constructor.
     * @param  $player
     */
    public function __construct($player)
    {
        $this->player = $player;
    }


    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]レッスンのご利用ポイントにつきまして';       
        return $this->text('school.email.player_lacks_points',$this->player)->subject($subject);
    }
}