<?php

namespace App\Mail\School;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class CompleteLesson extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $playerComplete;

    /**
     * cancel Player constructor.
     *
     * @param  $playerComplete
     */
    public function __construct($playerComplete)
    {
        $this->playerComplete = $playerComplete;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $staffs = Auth::guard('schools')->user();
        $school = get_school_by_staff($staffs->id);
        $subject = '[ポケコーチ]'.$school->name.'様からレッスンの完了申請が届いています。';

        return $this->text('school.email.complete_lesson', $this->playerComplete)->subject($subject);
    }
}
