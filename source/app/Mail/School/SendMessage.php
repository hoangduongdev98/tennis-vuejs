<?php

namespace App\Mail\School;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class SendMessage extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $sendMail;
    public $schoolName;

    /**
     * send message Player constructor.
     *
     * @param  $sendMail
     */
    public function __construct($sendMail, $schoolName)
    {
        $this->sendMail = $sendMail;
        $this->schoolName = $schoolName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $school = Auth::guard('schools')->user();
        $subject = '[ポケコーチ]'.$this->schoolName.'様から新しいメッセージが届いています。';

        return $this->text('school.email.send_message ', $this->sendMail)->subject($subject);
    }
}
