<?php

namespace App\Mail\School;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class CancelPlayer extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $cancelPlayer;

    /**
     * Approve Player constructor.
     *
     * @param  $cancelPlayer
     */
    public function __construct($cancelPlayer)
    {
        $this->cancelPlayer = $cancelPlayer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $staffs = Auth::guard('schools')->user();
        $school = get_school_by_staff($staffs->id);
        $subject = '[ポケコーチ]'.$school->name.'様からレッスンのキャンセル申請が届いています。';

        return $this->text('school.email.cancel_player', $this->cancelPlayer)->subject($subject);
    }
}
