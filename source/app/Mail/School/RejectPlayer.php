<?php

namespace App\Mail\School;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RejectPlayer extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $playerReject;

    /**
     * Approve Player constructor.
     *
     * @param  $playerReject
     */
    public function __construct($playerReject)
    {
        $this->playerReject = $playerReject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]レッスンの申し込みがキャンセルされました。';

        return $this->text('school.email.reject_player', $this->playerReject)->subject($subject);
    }
}
