<?php
namespace App\Mail\School;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Staffs;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $staff;


    /**
     * @var string
     */
    protected $token;


    /**
     * PasswordReset constructor.
     * @param  Staffs $staff
     * @param  $token
     */
    public function __construct($token, $staff)
    {
        $this->token = $token;
        $this->staff  = $staff;
    }


    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        $subject = '[tennis_mgt] password request';
        return $this->text('school.auth.password_reset', [
            'staff'  => $this->staff,
            'token' => $this->token,
        ])->subject($subject);
    }
}