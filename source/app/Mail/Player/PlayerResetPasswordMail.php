<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PlayerResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $playerResetPassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($playerResetPassword)
    {
        $this->playerResetPassword = $playerResetPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]ワンタイムパスワード';
        return $this->text('player.auth.mail_reset_password', $this->playerResetPassword)->subject($subject);
    }
}
