<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class SendMessageTrainer extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $sendMail;

    /**
     * Reject Player constructor.
     *
     * @param  $sendMail
     */
    public function __construct($sendMail)
    {
        $this->sendMail = $sendMail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $player = Auth::guard('players')->user();
        $subject = '[ポケコーチ]'.$player->name.'様から新しいメッセージが届いています。';

        return $this->text('player.trainer.send_message', $this->sendMail)->subject($subject);
    }
}
