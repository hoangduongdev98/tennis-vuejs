<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PlayerRegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    public $playerRegister;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($playerRegister)
    {
        $this->playerRegister = $playerRegister;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]ワンタイムパスワード';
        return $this->text('player.auth.mail_register', $this->playerRegister)->subject($subject);
    }
}
