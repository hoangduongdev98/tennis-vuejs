<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMessage extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $sendMail;
    public $playerName;

    /**
     * send message Player constructor.
     *
     * @param  $sendMail
     */
    public function __construct($sendMail, $playerName)
    {
        $this->sendMail = $sendMail;
        $this->playerName = $playerName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]'.$this->playerName.'様から新しいメッセージが届いています。';

        return $this->text('player.message.send_message', $this->sendMail)->subject($subject);
    }
}
