<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ResendLesson extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $resendLesson;

    /**
     * Reject Player constructor.
     *
     * @param  $resendLesson
     */
    public function __construct($resendLesson)
    {
        $this->resendLesson = $resendLesson;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $player = Auth::guard('players')->user();
        $subject = '[ポケコーチ]'.$player->name.'様からレッスン完了申請が差し戻されました。';

        return $this->text('player.message.resend', $this->resendLesson)->subject($subject);
    }
}
