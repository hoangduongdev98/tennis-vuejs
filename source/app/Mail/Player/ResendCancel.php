<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ResendCancel extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $resendCancel;

    /**
     * Reject Player constructor.
     *
     * @param  $resendCancel
     */
    public function __construct($resendCancel)
    {
        $this->resendCancel = $resendCancel;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $player = Auth::guard('players')->user();
        $subject = '[ポケコーチ]'.$player->name.' 様からレッスンキャンセル申請が差し戻されました。';

        return $this->text('player.message.resend_cancel', $this->resendCancel)->subject($subject);
    }
}
