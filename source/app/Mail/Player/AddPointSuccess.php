<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AddPointSuccess extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $addPoint;

    /**
     * Add point success
     *
     * @param  $addPoint
     */
    public function __construct($addPoint)
    {
        $this->addPoint = $addPoint;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'ポイント更新できました';

        return $this->text('player.epsilon.add_point_success', $this->addPoint)->subject($subject);
    }
}
