<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class CancelLesson extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $cancelLesson;

    /**
     * Reject Player constructor.
     *
     * @param  $cancelLesson
     */
    public function __construct($cancelLesson)
    {
        $this->cancelLesson = $cancelLesson;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $player = Auth::guard('players')->user();
        $subject = '[ポケコーチ]'.$player->name.' 様からレッスンキャンセル申請が差し戻されました。';

        return $this->text('player.lesson.cancelLesson', $this->cancelLesson)->subject($subject);
    }
}
