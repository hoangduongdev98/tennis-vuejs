<?php

namespace App\Mail\Player;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PlayerEditInfoMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    public $playerInfo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($playerInfo)
    {
        $this->playerInfo = $playerInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = '[ポケコーチ]ワンタイムパスワード';

        return $this->text('player.account.mail_edit_info', $this->playerInfo)->subject($subject);
    }
}
