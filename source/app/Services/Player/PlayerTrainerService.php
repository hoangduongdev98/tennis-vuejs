<?php

namespace App\Services\Player;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Player\PlayerTrainerRepository;
use App\Repositories\Player\PlayerLessonRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Trainers;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use App\Mail\Player\SendMessageTrainer;
use Illuminate\Support\Facades\Mail;

class PlayerTrainerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $playerTrainerRepository;
    protected $playerLessonRepository;
    protected $trainer;

    public function __construct (
        PlayerTrainerRepository $playerTrainerRepository,
        PlayerLessonRepository $playerLessonRepository,
        Trainers $trainer
    )
    {
        $this->playerTrainerRepository = $playerTrainerRepository;
        $this->playerLessonRepository = $playerLessonRepository;
        $this->trainer = $trainer;
    }

    /**
     * Get lists trainer for player
     *
     * @param array $search
     * @return Repository
     */
    public function listTrainers ($search = null)
    {
        $data = [];
        $data['count'] = 0;
        $listTrainer = $this->playerTrainerRepository->getListTrainer($search)->paginate(LIMIT_20);
        if (count($listTrainer)) {
            $data['count'] = $listTrainer->total();
            foreach ($listTrainer as $trainer) {
                $arrStructureFee = [];
                $arr = [];
                $arr['id'] = $trainer['id'];
                $arr['name'] = $trainer['name'];
                $arr['prefecture_id'] = $trainer['prefecture_id'];
                $arr['levels'] = $trainer['levels'];
                $arr['schoolName'] = $trainer['schoolName'];
                $arr['day_off'] = $trainer['day_off'];
                $arr['start_time'] = $trainer['start_time'];
                $arr['end_time'] = $trainer['end_time'];
                $arr['note'] = $trainer['note'];
                $arr['achievement'] = $trainer['achievement'];
                $arr['highlight'] = $trainer['highlight'];
                $dataStructureFee = $this->playerTrainerRepository->getStructureFeeOfTrainer($trainer['id']);
                $arr['structure_fee'] = $this->playerTrainerRepository->getArrayStructureFee($dataStructureFee);
                if ($image = $this->playerTrainerRepository->findImageForTrainerById($trainer['id'])) {
                    $arr['image_trainer'] = $image;
                } else {
                    $arr['image_trainer'] = [];
                }
                $chat_room_id = $this->playerTrainerRepository->getIdChatRoomOfTrainer($trainer['id']);
                $arr['chat_room_id'] = $chat_room_id;
                $data[] = $arr;
            }
        }
        return $data;
    }

    /**
     * Player register trainer
     *
     * @param int $trainerId
     * @param array $dataPost
     * @return \Illuminate\Http\Response
     */
    public function ajaxPlayerRegisterTrainer($trainerId, $dataPost) {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $sendMail = [];
        $dataPlayer = Auth::guard('players')->user();
        if ($this->playerTrainerRepository->registerTrainerById($trainerId, $dataPost, $dataPlayer['id'])) {
            $dataTrainer = $this->trainer->find($trainerId);
            $sendMail['trainer_name'] = $dataTrainer['name'];
            $sendMail['message'] = $dataPost['message'];
            $sendMail['player_name'] = $dataPlayer['name'];
            $sendMail['player_email'] = $dataPlayer['email'];
            Mail::to($dataTrainer['email'])->send(new SendMessageTrainer($sendMail));
            if (count(Mail::failures()) > 0) {
                $result['success'] = CODE_AJAX_ERROR;
                $result['msg'] = 'Error';
            } else {
                $result['success'] = CODE_AJAX_SUCCESS;
                $result['msg'] = 'Success';
            }
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Get array levels
     *
     * @return array $data
     */
    public function arrayMasterLevel () {
        $data = [];
        $dataLevels = $this->playerTrainerRepository->listLevels();
        if (count($dataLevels)) {
            foreach ($dataLevels as $value) {
                $data[$value['id']] = $value['name'];
            }
        }
        return $data;
    }

    /**
     * Get trainer by id
     *
     * @return Repository
     */
    public function getTrainerById ($trainerId) {
        return $this->playerTrainerRepository->getTrainerById($trainerId);
    }
}
