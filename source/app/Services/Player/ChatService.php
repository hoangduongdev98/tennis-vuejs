<?php

namespace App\Services\Player;

use App\Models\ChatMessages;
use App\Repositories\Player\ChatRoomRepository;
use App\Services\TennisMgtService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class ChatService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $chatRoomRepository;

    public function __construct(
        ChatRoomRepository $chatRoomRepository
    ) {
        $this->chatRoomRepository = $chatRoomRepository;
    }

    /**
     * get list chat history.
     *
     * @return array $history
     */
    public function getChatHistory()
    {
        $chatHistory = $this->chatRoomRepository->getChatHistory();

        return $chatHistory;
    }

    /**
     * Get message by participant id.
     *
     * @param $idParticipant
     *
     * @return array $arrMess
     */
    public function getMessageById($idParticipant)
    {
        $arrMess = [];
        $unreadMessage = $this->chatRoomRepository->getUnreadMessageById($idParticipant);
        $dataMess = $this->chatRoomRepository->getMessageById($idParticipant, $unreadMessage);
        foreach ($dataMess as $mess) {
            $arr = [];
            $timeMessage = date('Y/m/d', strtotime($mess->messageTime));
            $disabled = '';
            $time = Carbon::parse($mess->pl_updated_at);
            $limitTime = $time->addHours(72);
            $now = Carbon::now();
            if ($mess->roomType == INFO_TYPE_SCHOOL) {
                if ($mess->schoolStt == SCHOOL_STATUS_99 
                || $mess->schoolStt == SCHOOL_STATUS_2 
                || $mess->lessonStt == LESSON_STATUS_99 
                || $mess->lessonStt == LESSON_STATUS_2 
                || $limitTime->lt($now) && $mess->player_lesson_status == LESSON_PLAYER_STATUS_3 
                || $limitTime->lt($now) && $mess->pl_deleted == DELETED 
                || $limitTime->lt($now) && $mess->player_lesson_status == LESSON_PLAYER_STATUS_4) {
                    $disabled = 'disabled';
                }
            } elseif ($mess->roomType == INFO_TYPE_TRAINER) {
                if ($mess->trainerStt == SCHOOL_STATUS_99 
                || $mess->trainerSchStt == SCHOOL_STATUS_99 
                || $mess->lessonStt == LESSON_STATUS_99 
                || $mess->trainerStt == SCHOOL_STATUS_2 
                || $mess->trainerSchStt == SCHOOL_STATUS_2 
                || $mess->lessonStt == LESSON_STATUS_2 
                || $limitTime->lt($now) && $mess->player_lesson_status == LESSON_PLAYER_STATUS_3 
                || $limitTime->lt($now) && $mess->player_lesson_status == LESSON_PLAYER_STATUS_4 
                || $limitTime->lt($now) && $mess->pl_deleted == DELETED) {
                    $disabled = 'disabled';
                }
            }
            $oppositeId = ($mess->roomType == INFO_TYPE_SCHOOL) ? $mess->schoolId : $mess->trainerId;
            foreach ($dataMess as $m) {
                $timeMess = date('Y/m/d', strtotime($m->messageTime));
                if ($timeMessage == $timeMess) {
                    $message = [
                        'from' => $m->from,
                        'to' => $m->to,
                        'infoType' => $m->infoType,
                        'message' => $m->message,
                        'oppositeName' => ($m->roomType == INFO_TYPE_SCHOOL) ? $m->schoolName : $m->trainerName,
                        'time' => $m->messageTime,
                    ];
                    array_push($arr, $message);
                }
            }
            $arrMess[$timeMessage] = $arr;
            $arrMess['oppositeId'] = $oppositeId;
            $arrMess['disabled'] = $disabled;
        }

        return $arrMess;
    }

    /**
     * send message chat.
     *
     * @return Repository
     */
    public function sendMessageChat(array $dataPosts)
    {
        $chatRoom = $this->chatRoomRepository->getChatRoom(json_decode($dataPosts['participantId']));
        if (empty($chatRoom)) {
            return false;
        }
        if ($chatRoom->lessonStt == LESSON_STATUS_1) {
            $newMessage = new ChatMessages();
            $newMessage->chat_room_id = json_decode($dataPosts['participantId']);
            $newMessage->message = $dataPosts['message'];
            $newMessage->from = Auth::guard('players')->user()->id;
            $newMessage->to = json_decode($dataPosts['oppositeId']);
            $newMessage->info_type = INFO_TYPE_MESSAGE_PLAYER;
            $newMessage->unread_message = UNREAD_MESSAGE;

            return $this->chatRoomRepository->saveMessage($newMessage);
        } else {
            return false;
        }
    }
}
