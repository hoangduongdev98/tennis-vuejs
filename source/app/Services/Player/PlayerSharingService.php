<?php

namespace App\Services\Player;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Player\PlayerSharingRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use App\Models\SharingAnalyses;

class PlayerSharingService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $playerSharingRepository;
    protected $sharingAnalyses;

    public function __construct(
        PlayerSharingRepository $playerSharingRepository,
        SharingAnalyses $sharingAnalyses
    ) {
        $this->playerSharingRepository = $playerSharingRepository;
        $this->sharingAnalyses = $sharingAnalyses;
    }

    /**
     * Get lists lesson for player
     *
     * @param array $search
     * @return Repository
     */
    public function getListSharing ()
    {
        return $this->playerSharingRepository->getListSharing();
    }
    
    /**
     * Get school/coach for register share analyses
     * @param type $search
     * @return type
     */
    public function searchShare ($search = null)
    {
        return $this->playerSharingRepository->searchShare($search);
    }
    
    /**
     * register share
     * @param array $dataPosts
     * @return type
     */
    public function registerShare (array $dataPosts)
    {
        $data = $this->sharingAnalyses;
        $data->player_id = Auth::guard('players')->user()->id;
        if($dataPosts['shareType'] && $dataPosts['shareType'] == 1) {
            $data->school_id = $dataPosts['shareId'];
        } else {
            $data->trainer_id = $dataPosts['shareId'];
        }

        return $this->playerSharingRepository->saveShareAnalysis($data);
    }
    
    /**
     * delete share
     * @param type $data
     * @return type
     */
    public function deleteShare($data)
    {
        return $this->playerSharingRepository->deleteShare($data);
    }
}
