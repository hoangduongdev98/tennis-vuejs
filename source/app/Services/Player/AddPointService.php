<?php

namespace App\Services\Player;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Player\AddPointRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class AddPointService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $addPointRepository;

    public function __construct (AddPointRepository $addPointRepository)
    {
        $this->addPointRepository = $addPointRepository;
    }

    /**
     * Get list settlement and fee to add point
     *
     * @return Repository
     */
    public function feeList ()
    {
        return $this->addPointRepository->getAllFeeAddPoint();
    }
}
