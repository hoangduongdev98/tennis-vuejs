<?php

namespace App\Services\Player;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Player\PlayerSchoolRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Schools;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class PlayerSchoolService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $playerSchoolRepository;
    protected $schools;

    public function __construct (
        PlayerSchoolRepository $playerSchoolRepository,
        Schools $schools
    )
    {
        $this->playerSchoolRepository = $playerSchoolRepository;
        $this->schools = $schools;
    }

    /**
     * Get lists school for player
     *
     * @param array $search
     * @return Repository
     */
    public function listSchools ($search = null)
    {
        return $this->playerSchoolRepository->getListSchools($search);
    }

    /**
     * Get school by id
     *
     * @return Repository
     */
    public function getSchoolById ($schoolId) {
        return $this->playerSchoolRepository->getSchoolById($schoolId);
    }
    
    /**
     * Get array levels
     *
     * @return array $data
     */
    public function arrayMasterLevel () {
        $data = [];
        $dataLevels = $this->playerSchoolRepository->listLevels();
        if (count($dataLevels)) {
            foreach ($dataLevels as $value) {
                $data[$value['id']] = $value['name'];
            }
        }
        return $data;
    }
}
