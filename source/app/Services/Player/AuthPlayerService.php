<?php

namespace App\Services\Player;

use App\Models\Players;
use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Player\AuthPlayerRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class AuthPlayerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $authPlayerRepository;
    protected $player;

    public function __construct (AuthPlayerRepository $authPlayerRepository, Players $player)
    {
        $this->authPlayerRepository = $authPlayerRepository;
        $this->player = $player;
    }

    /**
     * Save register player
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function savePlayerRegister(array $dataPosts)
    {
        $newPlayer = $this->player;
        $newPlayer->name = $dataPosts['name'];
        $newPlayer->area = $dataPosts['area'];
        $newPlayer->email = $dataPosts['email'];
        $newPlayer->password = Hash::make($dataPosts['password']);
        $newPlayer->last_login = date("Y-m-d H:i:s");

        return $this->authPlayerRepository->saveCreatePlayer($newPlayer);
    }

    /**
     * Save register player
     *
     * @param array $dataPosts
     * @return Repository | bool
     */
    public function saveResetPassword(array $dataPosts)
    {
        return $this->authPlayerRepository->saveResetPasswordPlayer($dataPosts);
    }

    /**
     * Save edit informations for player
     *
     * @param array $dataPosts
     * @return Repository | bool
     */
    public function editInfoPlayer(array $dataPosts)
    {
        return $this->authPlayerRepository->saveEditInfoPlayer($dataPosts);
    }
    
    /**
     * Save time for player's last login
     *
     * @param array $params
     * @return Repository | bool
     */
    public function lastLogin(array $params)
    {
        return $this->authPlayerRepository->lastLogin($params);
    }
}
