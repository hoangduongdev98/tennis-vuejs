<?php

namespace App\Services\Player;

use App\Models\ChatMessages;
use App\Models\Lessons;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\Settlements;
use App\Repositories\Player\PlayerLessonRepository;
use App\Services\TennisMgtService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class PlayerLessonService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $playerLessonRepository;
    protected $playerLesson;
    protected $player;
    protected $lesson;

    public function __construct(PlayerLessonRepository $playerLessonRepository, Players $player, PlayerLesson $playerLesson, Lessons $lesson)
    {
        $this->playerLessonRepository = $playerLessonRepository;
        $this->player = $player;
        $this->playerLesson = $playerLesson;
        $this->lesson = $lesson;
    }

    /**
     * Get lists lesson for player.
     *
     * @param array $search
     *
     * @return Repository
     */
    public function listLessons($search = null)
    {
        return $this->playerLessonRepository->getListLesson($search);
    }

    /**
     * Get array levels.
     *
     * @return array $data
     */
    public function arrayMasterLevel()
    {
        $data = [];
        $dataLevels = $this->playerLessonRepository->listLevels();
        if (count($dataLevels)) {
            foreach ($dataLevels as $value) {
                $data[$value['id']] = $value['name'];
            }
        }

        return $data;
    }

    /**
     * Player register lesson.
     *
     * @param int $lessonId
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxPlayerRegisterLesson($lessonId)
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $dataPlayer = Auth::guard('players')->user();
        $result['point'] = $dataPlayer['point'];
        $lesson = $this->playerLessonRepository->getLessonById($lessonId);
        if ($lesson) {
            if ($dataPlayer['point'] < $lesson['payment_fee']) {
                $result['success'] = CODE_AJAX_ERROR;
                $result['point'] = CODE_AJAX_ERROR;
                $result['msg'] = 'Error';
            } else {
                if ($this->playerLessonRepository->registerLessonById($lessonId, $dataPlayer['id'])) {
                    $result['msg'] = Config::get('message.common_msg.register_lesson_success');
                } else {
                    $result['success'] = CODE_AJAX_ERROR;
                    $result['msg'] = 'Error';
                }
            }
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = Config::get('message.common_msg.not_isset_id');
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Get lists lesson history of player.
     *
     * @return Repository
     */
    public function getLessonHistory()
    {
        return $this->playerLessonRepository->getLessonHistory();
    }

    /**
     * find lesson by id.
     *
     * @return array $dataLesson
     */
    public function findLessonById(int $idLesson)
    {
        $dataLesson = $this->playerLessonRepository->getLessonById($idLesson);

        return $dataLesson;
    }

    /**
     * update lesson flag.
     *
     * @return repository
     */
    public function updateLessonFlag($dataPost)
    {
        $editFlag = $this->playerLessonRepository->getFlagByPlayerLessonId($dataPost['player_lesson_id']);
        $editFlag->flag = 1;
        $updatePlayerLesson = $this->playerLesson->find($dataPost['player_lesson_id']);
        $updatePlayerLesson->status = LESSON_PLAYER_STATUS_3;

        return $this->playerLessonRepository->saveUpdateLessonFlag($editFlag, $updatePlayerLesson);
    }

    /**
     * update status settlement status.
     *
     * @return repository
     */
    public function updateStatusSettlement($dataPost)
    {
        $updateSettle = $this->playerLessonRepository->getSettlementByPlayerLessonId($dataPost['player_lesson_id']);
        $updatePlayer = $this->playerLesson->find($dataPost['player_lesson_id']);
        $player = $this->player->find($dataPost['player_id']);
        $lesson = $this->lesson->find($dataPost['lesson_id']);
        $manage_id = '';
        if ($lesson->info_type == INFO_TYPE_SCHOOL) {
            $manage_id = $lesson->table_id;
        } elseif ($lesson->info_type == INFO_TYPE_TRAINER) {
            $manage_id = $lesson->table_id;
        }
        $payments = [];
        if ($updateSettle) {
            $payments = $this->playerLessonRepository->getPaymentBySettId($updateSettle->id);
            if ($updatePlayer->refunded == REFUNDED) {
                if ($player->point >= $lesson->payment_fee) {
                    $updateSettle->status = SETTLEMENT_1;
                    if (count($payments)) {
                        $payments = $this->updatePayment($payments, PAYMENT_STATUS_1);
                    }
                } else {
                    $updateSettle->status = SETTLEMENT_99;
                    if (count($payments)) {
                        $payments = $this->updatePayment($payments, PAYMENT_STATUS_99);
                    }
                }
            } else {
                $updateSettle->status = SETTLEMENT_1;
                if (count($payments)) {
                    $payments = $this->updatePayment($payments, PAYMENT_STATUS_1);
                }
            }
            $updateSettle->updated_id = $manage_id;
        } else {
            $updateSettle = new Settlements();
            $updateSettle->player_lesson_id = $updatePlayer->id;
            if (date('Y-m-d') < date('Y-m-d', strtotime($lesson['datetime_lesson'])) && ($lesson['category'] == LESSON_PLAYER_STATUS_2)) {
                $updateSettle->payment_date = $lesson['datetime_lesson'];
            } else {
                $updateSettle->payment_date = date('Y-m-d');
            }
            $updateSettle->sett_price = $lesson->payment_fee;
            $updateSettle->status = SETTLEMENT_1;
        }

        return $this->playerLessonRepository->updateStatusSettlement($updateSettle, $payments, $lesson, $manage_id);
    }

    /**
     * update payment.
     *
     * @return mixed
     */
    public function updatePayment($payments, $status)
    {
        foreach ($payments as $payment) {
            if ($payment->status != PAYMENT_STATUS_0 && $payment->status !== null) {
                $payment->status = $status;
            }
        }

        return $payments;
    }

    /**
     * update  flag.
     *
     * @param  $dataPost
     *
     * @return repository
     */
    public function updateFlag($dataPost, $cancel = false)
    {
        $updatePlayerLesson = $this->playerLesson->find($dataPost['player_lesson_id']);
        $updatePlayerLesson->status = LESSON_PLAYER_STATUS_2;
        $updatePlayerLesson->refund_point = null;
        $updatePlayerLesson->refund_rate = null;
        if (!$cancel) {
            $editFlag = $this->playerLessonRepository->getFlagByPlayerLessonId($dataPost['player_lesson_id']);
            $editFlag->delete();
        }

        return $this->playerLessonRepository->saveUpdateFlag($updatePlayerLesson);
    }

    /**
     * update player lesson.
     *
     * @param  $dataPost
     *
     * @return repository
     */
    public function updatePlayerLesson($dataPost)
    {
        $playerLesson = $this->playerLesson->find($dataPost['player_lesson_id']);
        if ($playerLesson) {
            $playerLesson->delete();
        }

        return true;
    }

    /**
     * update player lesson.
     *
     * @param  $dataPost
     *
     * @return repository
     */
    public function updatePointPlayer($dataPost)
    {
        $dataPlayer = Auth::guard('players')->user();
        $playerLesson = $this->playerLesson->find($dataPost['player_lesson_id']);
        $lesson = $this->lesson->find($playerLesson->lesson_id);
        $player = $this->player->find($dataPlayer->id);
        $player->point = $player->point + $playerLesson->refund_point;
        $playerLesson->is_deleted = DELETED;
        $playerLesson->refunded = REFUNDED;
        $playerLesson->status = LESSON_PLAYER_STATUS_99;

        return $this->playerLessonRepository->savePointPlayer($playerLesson, $player);
    }

    /**
     * update settlement cancel.
     *
     * @param  $dataPost
     *
     * @return repository
     */
    public function updateSettlementCancel($dataPost)
    {
        $dataPlayer = Auth::guard('players')->user();
        $updateSettle = $this->playerLessonRepository->getSettlementByPlayerLessonId($dataPost['player_lesson_id']);
        $updatePlayer = $this->playerLesson->find($dataPost['player_lesson_id']);
        $player = $this->player->find($dataPlayer->id);
        $lesson = $this->lesson->find($dataPost['lesson_id']);
        $payments = [];
        if ($updateSettle) {
            $payments = $this->playerLessonRepository->getPaymentBySettId($updateSettle->id);
            if ($updatePlayer->refunded == REFUNDED) {
                if ($player->point >= $lesson->payment_fee) {
                    $updateSettle->status = SETTLEMENT_1;
                    if (count($payments)) {
                        $payments = $this->updatePayment($payments, PAYMENT_STATUS_1);
                    }
                } else {
                    $updateSettle->status = SETTLEMENT_99;
                    if (count($payments)) {
                        $payments = $this->updatePayment($payments, PAYMENT_STATUS_99);
                    }
                }
            } else {
                $updateSettle->status = SETTLEMENT_1;
                if (count($payments)) {
                    $payments = $this->updatePayment($payments, PAYMENT_STATUS_1);
                }
            }
            $updateSettle->updated_id = $dataPost['manage_id'];
        } else {
            $updateSettle = new Settlements();
            $updateSettle->player_lesson_id = $updatePlayer->id;
            if (date('Y-m-d') < date('Y-m-d', strtotime($lesson['datetime_lesson'])) && ($lesson['category'] == LESSON_PLAYER_STATUS_2)) {
                $updateSettle->payment_date = $lesson['datetime_lesson'];
            } else {
                $updateSettle->payment_date = date('Y-m-d');
            }
            $updateSettle->sett_price = $lesson->payment_fee - $updatePlayer->refund_point;
            $updateSettle->status = SETTLEMENT_1;
        }

        return $this->playerLessonRepository->updateStatusSettlement($updateSettle, $payments, $lesson, $dataPost['manage_id']);
    }

    /**
     * send message chat from popup.
     *
     * @return Repository
     */
    public function sendMessagePopUp(array $dataPosts)
    {
        $dataPlayer = Auth::guard('players')->user();
        $newMessage = new ChatMessages();
        $newMessage->chat_room_id = $dataPosts['room_id'];
        $newMessage->message = $dataPosts['message_chat'];
        $newMessage->from = $dataPlayer->id;
        $newMessage->to = $dataPosts['manage_id'];
        $newMessage->info_type = INFO_TYPE_MESSAGE_PLAYER;
        $newMessage->unread_message = UNREAD_MESSAGE;

        return $this->playerLessonRepository->saveMessagePopUp($newMessage);
    }

    /**
     * Get email.
     *
     * @return mixed
     */
    public function getEmail($info_type, $manage_id)
    {
        if ($info_type == INFO_TYPE_TRAINER) {
            return $this->playerLessonRepository->getEmailByTrainerId($manage_id);
        } elseif ($info_type == INFO_TYPE_SCHOOL) {
            return $this->playerLessonRepository->getEmailBySchoolId($manage_id);
        }
    }

    /**
     * get all level.
     *
     * @return Repository
     */
    public function getAllLevel()
    {
        $arrLevel = $this->playerLessonRepository->getAllLevel();
        $arr = [];
        if (count($arrLevel)) {
            foreach ($arrLevel as $level) {
                $arr[$level['id']] = $level['name'];
            }
        }

        return $arr;
    }
}
