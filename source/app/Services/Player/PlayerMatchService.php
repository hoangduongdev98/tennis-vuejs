<?php

namespace App\Services\Player;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Player\PlayerMatchRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use App\Models\Matches;

class PlayerMatchService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $playerMatchRepository;
    protected $matches;

    public function __construct(
        PlayerMatchRepository $playerMatchRepository,
        Matches $matches
    ) {
        $this->playerMatchRepository = $playerMatchRepository;
        $this->matches = $matches;
    }

    /**
     * Get match list of player
     *
     * @param array $search
     * @return Repository
     */
    public function getMatchListHistory($search = null)
    {
        $matchListHistory = $this->playerMatchRepository->getMatchListHistory($search);

        return $matchListHistory;
    }

    /**
     * Get data match by id
     *
     * @param int $idMatch
     * @return \Illuminate\Http\Response
     */
    public function getMatchInfo(int $idMatch)
    {
        $match = $this->playerMatchRepository->getMatchInfo($idMatch);

        return $match;
    }

    /**
     * create match
     * 
     * @param array $dataPosts
     * @return type
     */
    public function createMatch(array $dataPosts, $userId, $userName)
    {
        $newMatch = new Matches($dataPosts);
        //get match config default data
        $match_default = Config::get('const.match_default');
        //set info config default data
        $match_default['players'][0] = $userName;
        $match_default['players'][1] = $newMatch->opponent;
        $match_default['sets'] = $newMatch->match_format;
        $match_default['advantage'] = (int)$newMatch->advantage;
        //set match detail default data
        $newMatch->match_detail = json_encode($match_default);
        $newMatch->created_id = $userId;

        return $this->playerMatchRepository->saveMatch($newMatch);
    }

    /**
     * create match
     * 
     * @param array $dataPosts
     * @return type
     */
    public function startMatch(array $dataPosts, $userId, $userName, $is_edit = false)
    {
        $match = $this->matches->find($dataPosts['id']);
        $match_detail = json_decode($match->match_detail);
        $match->fill($dataPosts);
        if (!$is_edit) {
            $match_detail->players[0] = $userName;
            $match_detail->players[1] = $match->opponent;
            $match_detail->sets = $match->match_format;
            $match_detail->serve_right = (int)$match->serve_right;
            $match_detail->coat_selection = (int)$match->coat_selection;
            $match_detail->current_hit = ($match->serve_right == 1) ? 1 : 2;
            $match_detail->advantage = (int)$match->advantage;
            $match_detail->change_coat = (int)$match->change_coat;
            $match_detail->games_points = (int)$match->serve_right == 1 ? (object)[0 => (object)[0 => [["0 F", "0"]]]] : (object)[0 => (object)[0 => [["0", "0 F"]]]];
            //set match detail default data
            $match->match_detail = json_encode($match_detail);
        } else {
            // when edit can not edit 2 below option match_format and advantage so unset it in case removing disable in front end
            unset($match->match_format);
            unset($match->advantage);
            $match_detail->players[0] = $userName;
            $match_detail->players[1] = $match->opponent;
        }
        $match->updated_id = $userId;

        return $this->playerMatchRepository->saveMatch($match);
    }

    /**
     * Delete match
     *
     * @param int $idMatch
     * @return bool
     */
    public function deleteMatch(int $idMatch)
    {
        $match = $this->matches->find($idMatch);
        $match->is_deleted = DELETED;
        $match->deleted_id = Auth::guard('players')->user()->id;

        return $this->playerMatchRepository->saveMatch($match);
    }

    /**
     * save match record
     *
     * @param int $idMatch
     * @return bool
     */
    public function saveRecord(array $dataPosts, int $idMatch)
    {
        $match = $this->matches->find($idMatch);
        $match->fill($dataPosts);

        return $this->playerMatchRepository->saveMatch($match);
    }

    /**
     * Get Trainer by playerId
     *
     * @param int $playerId
     * @return Repository
     */
    public function getTrainerByPlayerId($winnerMax, $loserMax, $missMax, $serve1Max, $serve2Max)
    {
        return $this->playerMatchRepository->getTrainerByPlayerId($winnerMax, $loserMax, $missMax, $serve1Max, $serve2Max);
    }
}
