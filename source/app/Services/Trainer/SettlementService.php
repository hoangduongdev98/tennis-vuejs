<?php

namespace App\Services\Trainer;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Trainer\SettlementRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;

class SettlementService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $settlementRepository;

    public function __construct(SettlementRepository $settlementRepository)
    {
        $this->settlementRepository = $settlementRepository;
    }

    /**
     * get list settlement
     * @param array $search
     * @return array $result
     */
    public function getListSettlement($search = null)
    {
       return $this->settlementRepository->getListSettlement($search);
    }
}
