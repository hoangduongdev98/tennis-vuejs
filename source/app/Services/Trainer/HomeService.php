<?php

namespace App\Services\Trainer;

use App\Services\TennisMgtService;
use App\Repositories\Trainer\LessonRepository;
use App\Repositories\Trainer\PlayerRepository;
use App\Repositories\Trainer\MatchesRepository;

class HomeService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $playerRepository;
    protected $lessonRepository;
    protected $matchesRepository;

    public function __construct (
        PlayerRepository $playerRepository,
        LessonRepository $lessonRepository,
        MatchesRepository $matchesRepository
    )
    {
        $this->playerRepository = $playerRepository;
        $this->lessonRepository = $lessonRepository;
        $this->matchesRepository = $matchesRepository;
    }
    /**
     * Count data lessons
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataLessons () {
        return $this->lessonRepository->countDataOfLessons();
    }

    /**
     * get Data in year
     *
     * @return \Illuminate\Http\Response
     */
    public function dataLessonsInAYear() {
        return $this->lessonRepository->getNumbersLessonOneYear();
    }

        /**
     * Count data matches
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataMatches () {
        return $this->matchesRepository->countDataOfMatches();
    }

        /**
     * Count data players
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataPlayers () {
        return $this->playerRepository->countDataOfPlayers();
    }


}
