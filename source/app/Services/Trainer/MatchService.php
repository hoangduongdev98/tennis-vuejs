<?php


namespace App\Services\Trainer;


use App\Services\TennisMgtService;
use App\Repositories\Trainer\MatchesRepository;
use App\Models\WinnerSetting;
use App\Models\MissSetting;
use App\Models\SettingCourse;
use App\Models\Serve1stAdvice;
use App\Models\Serve2ndAdvice;
use Illuminate\Support\Facades\Auth;

/**
 * Description of AuthService
 *
 */
class MatchService extends TennisMgtService
{

    protected $matchRepository;

    public function __construct(MatchesRepository $matchRepository)
    {
        $this->matchRepository = $matchRepository;
    }

    /**
     * Save match analysis
     * @param array $dataPosts
     * @return array $result
     */
    public function saveMatchAnalysis(array $dataPosts)
    {
        $result = true;
        $created = Auth::guard('trainers')->user();
        //insert advice winners setting
        $winnerSetting = $this->matchRepository->getWinnerSetting($created->id);
        if(!$winnerSetting){
            $winnerSetting = new WinnerSetting();
        }
        $winnerSetting->advice_return = $dataPosts['advice_return'];
        $winnerSetting->advice_fore_hand = $dataPosts['advice_fore_hand'];
        $winnerSetting->advice_back_hand = $dataPosts['advice_back_hand'];
        $winnerSetting->advice_smash = $dataPosts['advice_smash'];
        $winnerSetting->advice_volley = $dataPosts['advice_volley'];
        $winnerSetting->created_id = $created->id;
        if (!$this->matchRepository->saveWinnerSetting($winnerSetting)) {
            $result = false;
        }
        //insert advice miss setting
        $missSetting = $this->matchRepository->getMissSetting($created->id);
        if(!$missSetting){
            $missSetting = new MissSetting();
        }
        $missSetting->advice_return = $dataPosts['advice_miss_return'];
        $missSetting->advice_fore_hand = $dataPosts['advice_miss_forehand'];
        $missSetting->advice_back_hand = $dataPosts['advice_miss_backhand'];
        $missSetting->advice_smash = $dataPosts['advice_miss_smash'];
        $missSetting->advice_volley = $dataPosts['advice_miss_volley'];
        $missSetting->created_id = $created->id;
        if (!$this->matchRepository->saveMissSetting($missSetting)) {
            $result = false;
        }
        //insert setting course
        $newAdviceReturn = $this->matchRepository->getAdviceSetting($created->id,ADVICE_RETURN);
        if(!$newAdviceReturn){
            $newAdviceReturn = new SettingCourse();
        }
        $newAdviceReturn->net_advice = $dataPosts['advice_return_net'];
        $newAdviceReturn->deuce_advice = $dataPosts['advice_return_deuce'];
        $newAdviceReturn->ad_side_advice = $dataPosts['advice_return_outside'];
        $newAdviceReturn->over_advice = $dataPosts['advice_return_overout'];
        $newAdviceReturn->info_type = ADVICE_RETURN;
        $newAdviceReturn->created_id = $created->id;
        if (!$this->matchRepository->saveAdviceSetting($newAdviceReturn)) {
            $result = false;
        }
        //insert setting course
        $newAdviceForeHand = $this->matchRepository->getAdviceSetting($created->id,ADVICE_FOREHAND);
        if(!$newAdviceForeHand){
            $newAdviceForeHand = new SettingCourse();
        }
        $newAdviceForeHand->net_advice = $dataPosts['advice_forehand_net'];
        $newAdviceForeHand->deuce_advice = $dataPosts['advice_forehand_deuce'];
        $newAdviceForeHand->ad_side_advice = $dataPosts['advice_forehand_outside'];
        $newAdviceForeHand->over_advice = $dataPosts['advice_forehand_overout'];
        $newAdviceForeHand->info_type = ADVICE_FOREHAND;
        $newAdviceForeHand->created_id = $created->id;
        if (!$this->matchRepository->saveAdviceSetting($newAdviceForeHand)) {
            $result = false;
        }
        //insert setting course
        $newAdviceBackHand = $this->matchRepository->getAdviceSetting($created->id,ADVICE_BACKHAND);
        if(!$newAdviceBackHand){
            $newAdviceBackHand = new SettingCourse();
        }
        $newAdviceBackHand->net_advice = $dataPosts['advice_backhand_net'];
        $newAdviceBackHand->deuce_advice = $dataPosts['advice_backhand_deuce'];
        $newAdviceBackHand->ad_side_advice = $dataPosts['advice_backhand_outside'];
        $newAdviceBackHand->over_advice = $dataPosts['advice_backhand_overout'];
        $newAdviceBackHand->info_type = ADVICE_BACKHAND;
        $newAdviceBackHand->created_id = $created->id;
        if (!$this->matchRepository->saveAdviceSetting($newAdviceBackHand)) {
            $result = false;
        }
        //insert setting course
        $newAdviceVolley = $this->matchRepository->getAdviceSetting($created->id,ADVICE_VOLLEY);
        if(!$newAdviceVolley){
            $newAdviceVolley = new SettingCourse();
        }
        $newAdviceVolley->net_advice = $dataPosts['advice_volley_net'];
        $newAdviceVolley->deuce_advice = $dataPosts['advice_volley_deuce'];
        $newAdviceVolley->ad_side_advice = $dataPosts['advice_volley_outside'];
        $newAdviceVolley->over_advice = $dataPosts['advice_volley_overout'];
        $newAdviceVolley->info_type = ADVICE_VOLLEY;
        $newAdviceVolley->created_id = $created->id;
        if (!$this->matchRepository->saveAdviceSetting($newAdviceVolley)) {
            $result = false;
        }
         //insert setting course
        $newAdviceSmash = $this->matchRepository->getAdviceSetting($created->id,ADVICE_SMASH);
        if(!$newAdviceSmash){
            $newAdviceSmash = new SettingCourse();
        }
        $newAdviceSmash->net_advice = $dataPosts['advice_smash_net'];
        $newAdviceSmash->deuce_advice = $dataPosts['advice_smash_deuce'];
        $newAdviceSmash->ad_side_advice = $dataPosts['advice_smash_outside'];
        $newAdviceSmash->over_advice = $dataPosts['advice_smash_overout'];
        $newAdviceSmash->info_type = ADVICE_SMASH;
        $newAdviceSmash->created_id = $created->id;
        if (!$this->matchRepository->saveAdviceSetting($newAdviceSmash)) {
            $result = false;
        }

        //insert advice serve 1st
        $newServe1st = $this->matchRepository->getServe1st($created->id);
        if(!$newServe1st){
            $newServe1st = new Serve1stAdvice();
        }
        $newServe1st->net_adv_content = $dataPosts['serve_1_net'];
        $newServe1st->overfault_adv_content = $dataPosts['serve_1_overfault'];
        $newServe1st->deuce_adv_content = $dataPosts['serve_1_deuce'];
        $newServe1st->add_side_content = $dataPosts['serve_1_add_side'];
        $newServe1st->point_rate_in_serve_left_1 = $dataPosts['rate_more_1st'];
        $newServe1st->point_rate_adv_content_1 = $dataPosts['advice_rate_more_1st'];
        $newServe1st->point_rate_in_serve_right_1 = $dataPosts['rate_less_1_1st'];
        $newServe1st->point_rate_in_serve_left_2 = $dataPosts['rate_more_2_1st'];
        $newServe1st->point_rate_in_serve_right_2= $dataPosts['rate_less_2_1st'];
        $newServe1st->point_rate_adv_content_2 = $dataPosts['advice_more_and_less_1st'];
        $newServe1st->point_rate_adv_content_3 = $dataPosts['advice_rate_less_1st'];
        $newServe1st->created_id = $created->id;
        if (!$this->matchRepository->saveServe1st($newServe1st)) {
            $result = false;
        }

        //insert advice serve 2nd
        $newServe2nd = $this->matchRepository->getServe2nd($created->id);
        if(!$newServe2nd){
            $newServe2nd = new Serve2ndAdvice();
        }
        $newServe2nd->net_adv_content = $dataPosts['serve_2_net'];
        $newServe2nd->overfault_adv_content = $dataPosts['serve_2_overfault'];
        $newServe2nd->deuce_adv_content = $dataPosts['serve_2_deuce'];
        $newServe2nd->add_side_content = $dataPosts['serve_2_add_side'];
        $newServe2nd->point_rate_in_serve_left_1 = $dataPosts['rate_more_2nd'];
        $newServe2nd->point_rate_adv_content_1 = $dataPosts['advice_rate_more_2nd'];
        $newServe2nd->point_rate_in_serve_right_1 = $dataPosts['rate_less_1_2nd'];
        $newServe2nd->point_rate_in_serve_left_2 = $dataPosts['rate_more_2_2nd'];
        $newServe2nd->point_rate_in_serve_right_2= $dataPosts['rate_less_2_2nd'];
        $newServe2nd->point_rate_adv_content_2 = $dataPosts['advice_more_and_less_2nd'];
        $newServe2nd->point_rate_adv_content_3 = $dataPosts['advice_rate_less_2nd'];
        $newServe2nd->created_id = $created->id;
        if (!$this->matchRepository->saveServe2nd($newServe2nd)) {
            $result = false;
        }
        return $result;
    }
    /**
     * get winner setting
     * @param array $created_id
     * @return array 
     */
    public function getWinnerSetting($created_id){
        return $this->matchRepository->getWinnerSetting($created_id);
    }
      /**
     * get miss setting
     * @param array $created_id
     * @return array 
     */
    public function getMissSetting($created_id){
        return $this->matchRepository->getMissSetting($created_id);
    }
      /**
     * get advice return
     * @param array $created_id
     * @return array 
     */
    public function getAdviceReturn($created_id){
        return $this->matchRepository->getAdviceSetting($created_id,ADVICE_RETURN);
    }
     /**
     * get advice forehand
     * @param array $created_id
     * @return array 
     */
    public function getAdviceForeHand($created_id){
        return $this->matchRepository->getAdviceSetting($created_id,ADVICE_FOREHAND);
    }
    /**
     * get advice backhand
     * @param array $created_id
     * @return array 
     */
    public function getAdviceBackHand($created_id){
        return $this->matchRepository->getAdviceSetting($created_id,ADVICE_BACKHAND);
    }
    /**
     * get advice volley
     * @param array $created_id
     * @return array 
     */
    public function getAdviceVolley($created_id){
        return $this->matchRepository->getAdviceSetting($created_id,ADVICE_VOLLEY);
    }
    /**
     * get advice smash
     * @param array $created_id
     * @return array 
     */
    public function getAdviceSmash($created_id){
        return $this->matchRepository->getAdviceSetting($created_id,ADVICE_SMASH);
    }
      /**
     * get advice serve 1st
     * @param array $created_id
     * @return array 
     */
    public function getServe1st($created_id){
        return $this->matchRepository->getServe1st($created_id);
    }
      /**
     * get advice serve 2nd
     * @param $created_id
     * @return array 
     */
    public function getServe2nd($created_id){
        return $this->matchRepository->getServe2nd($created_id);
    }
}
