<?php

namespace App\Services\Trainer;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Trainer\PlayerLessonRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;

class PlayerLessonService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $playerLessonRepository;

    public function __construct(PlayerLessonRepository $playerLessonRepository)
    {
        $this->playerLessonRepository = $playerLessonRepository;
    }

    /**
     * get list player
     * @param array $search
     * @return array $result
     */
    public function getListPlayer($search = null)
    {
        return $this->playerLessonRepository->getListPlayer($search);
    }

}
