<?php

namespace App\Services\Trainer;

use App\Models\Images;
use App\Models\StructureFee;
use App\Models\Trainers;
use App\Repositories\Trainer\TrainerRepository;
use App\Services\TennisMgtService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class TrainerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $trainerRepository;
    protected $trainer;
    protected $fee;
    protected $image;

    public function __construct(
        TrainerRepository $trainerRepository,
        Trainers $trainer,
        StructureFee $fee,
        Images $image
    ) {
        $this->trainerRepository = $trainerRepository;
        $this->trainers = $trainer;
        $this->fee = $fee;
        $this->image = $image;
    }

    /**
     * get level name.
     *
     * @return array $arr
     */
    public function getLevelName()
    {
        $arr = [];
        $levels = $this->trainerRepository->getAllLevel();
        if (count($levels)) {
            foreach ($levels as $level) {
                $arr[$level['id']] = $level['name'];
            }
        }

        return $arr;
    }

    /**
     * get detail Trainer.
     *
     * @return array $dataTrainer
     */
    public function findTrainerById(int $trainer_id)
    {
        $dataTrainer = $this->trainerRepository->getTrainerById($trainer_id);
        $arrImage = $this->trainerRepository->findAllImageByTrainerId($trainer_id);
        if (count($arrImage)) {
            foreach ($arrImage as $img) {
                $dataTrainer['avatar'.$img['position']] = $img['name'];
            }
        }

        return $dataTrainer;
    }

    /**
     * get structure fee.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStructureFee(int $trainer_id)
    {
        return $this->trainerRepository->getStructureFee($trainer_id);
    }

    /**
     *  edit trainer.
     *
     * @return \Illuminate\Http\Response
     */
    public function editTrainer(int $trainer_id, array $dataPosts)
    {
        if ($trainer_id != $dataPosts['id']) {
            return false;
        }
        $arrImage = [];
        $updated_id = Auth::guard('trainers')->user()->id;
        $editTrainer = $this->trainers->find($trainer_id);
        if ($editTrainer) {
            $editTrainer->name = $dataPosts['name'];
            $editTrainer->post_code = trim(str_replace('-', '', $dataPosts['post_code']));
            $editTrainer->prefecture_id = $dataPosts['prefecture_id'];
            $editTrainer->address = $dataPosts['address'];
            $editTrainer->email = $dataPosts['email'];
            $editTrainer->phone = trim(str_replace('-', '', $dataPosts['phone']));
            $editTrainer->fax = trim(str_replace('-', '', $dataPosts['fax']));
            $editTrainer->bank_name = $dataPosts['bank_name'];
            $editTrainer->branch_bank = $dataPosts['branch_bank'];
            $editTrainer->account_type = $dataPosts['account_type'];
            $editTrainer->account_number = $dataPosts['account_number'];
            $editTrainer->account_holder = $dataPosts['account_holder'];
            if ($dataPosts['password'] == PASSWORD) {
                $editTrainer->password = $dataPosts['old_password'];
            } else {
                $editTrainer->password = Hash::make($dataPosts['password']);
            }
            $editTrainer->updated_id = $updated_id;
            if (!empty($dataPosts['avatar1'])) {
                $editImage1 = $this->trainerRepository->findImageByTrainer($trainer_id, 1);
                if (!$editImage1) {
                    $editImage1 = new Images();
                }
                $this->saveImageForTrainer($editImage1, $dataPosts['avatar1'], 1, null, $updated_id);
                array_push($arrImage, $editImage1);
            }
            if (!empty($dataPosts['avatar2'])) {
                $editImage2 = $this->trainerRepository->findImageByTrainer($trainer_id, 2);
                if (!$editImage2) {
                    $editImage2 = new Images();
                }
                $this->saveImageForTrainer($editImage2, $dataPosts['avatar2'], 2, null, $updated_id);
                array_push($arrImage, $editImage2);
            }
            if (!empty($dataPosts['avatar3'])) {
                $editImage3 = $this->trainerRepository->findImageByTrainer($trainer_id, 3);
                if (!$editImage3) {
                    $editImage3 = new Images();
                }
                $this->saveImageForTrainer($editImage3, $dataPosts['avatar3'], 3, null, $updated_id);
                array_push($arrImage, $editImage3);
            }
        }
        if (isset($dataPosts['level'])) {
            $editTrainer->levels = json_encode($dataPosts['level']);
        } else {
            $editTrainer->levels = null;
        }
        $editTrainer->highlight = $dataPosts['highlight'];
        $editTrainer->achievement = $dataPosts['achievement'];
        $editTrainer->updated_id = $updated_id;

        return $this->trainerRepository->saveEditTrainer($editTrainer, $arrImage);
    }

    public function saveImageForTrainer($saveImage, $name, $position, $created_id = null, $updated_id = null)
    {
        $saveImage->name = $name;
        $saveImage->path = AVATAR_TRAINER_SAVE_JS;
        $saveImage->position = $position;
        $saveImage->info_types = INFO_TYPE_TRAINER;
        if ($created_id) {
            $saveImage->created_id = $created_id;
        }
        if ($updated_id) {
            $saveImage->updated_id = $updated_id;
        }
    }

    public function deleteStructureFeeById($id)
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $dataSettlementFee = $this->trainerRepository->getStructureFeeById($id);
        if ($dataSettlementFee) {
            $dataSettlementFee->is_deleted = DELETED;
            $dataSettlementFee->deleted_id = Auth::guard('trainers')->user()->id;
            if ($dataSettlementFee->save()) {
                $result['msg'] = Config::get('message.common_msg.delete_success');
            } else {
                $result['success'] = CODE_AJAX_ERROR;
                $result['msg'] = Config::get('message.common_msg.delete_error');
            }
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = Config::get('message.common_msg.not_isset_id');
        }

        return response()->json(['result' => $result]);
    }
}
