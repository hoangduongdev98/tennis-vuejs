<?php


namespace App\Services\Trainer;


use App\Services\TennisMgtService;
use App\Repositories\Trainer\AuthRepository;
/**
 * Description of AuthService
 *
 */
class AuthService extends TennisMgtService{
    
    protected $authRepository;
    
    public function __construct(AuthRepository $authRepository) {
        $this->authRepository = $authRepository;
    }
    
    
    public function getStatusTrainer($data) {
        return $this->authRepository->getDataTrainer($data);
    }
}
