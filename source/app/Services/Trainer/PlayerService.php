<?php

namespace App\Services\Trainer;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Trainer\PlayerRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;

class PlayerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $playerRepository;

    public function __construct (PlayerRepository $playerRepository)
    {
        $this->playerRepository = $playerRepository;
    }

    /**
     * get list player
     * @param array $search
     * @return array $result
     */
    public function getListPlayer($search = null)
    {
        return $this->playerRepository->getListPlayer($search);
    }
    
    /**
     * Get account player by id
     *
     * @param $id
     * @return array $result
     */
    public function getAccountLogonByPlayerId($id)
    {
        return $this->playerRepository->getAccountLogonByPlayerId($id);
    }
    /**
     * Sort list player
     *
     * @param array $search
     * @return array $result
     */
    public function sortListPlayer($playerList, $sort, $direction)
    {
        return $this->playerRepository->sortListPlayer($playerList, $sort, $direction);
    }
    
    /**
     * Get list shared player
     *
     * @param array $search
     * @return array $result
     */
    public function getListSharedPlayer($search = null)
    {
        return $this->playerRepository->getListSharedPlayer($search);
    }
    
    /**
     * accept share
     * @param type $data
     * @return type
     */
    public function acceptShare($data)
    {
        return $this->playerRepository->acceptShare($data);
    }
    
    /**
     * delete share
     * @param type $data
     * @return type
     */
    public function deleteShare($data)
    {
        return $this->playerRepository->deleteShare($data);
    }
    
    /**
     * Get account player by id
     *
     * @param $id
     * @return array $result
     */
    public function getAccountSharedPlayer($trainerId, $playerId)
    {
        return $this->playerRepository->getAccountSharedPlayer($trainerId, $playerId);
    }
}
