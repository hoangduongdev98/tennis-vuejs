<?php

namespace App\Services\Trainer;

use App\Models\ChatMessages;
use App\Models\ChatRooms;
use App\Models\LessonFlags;
use App\Models\Lessons;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Repositories\Trainer\ChatRoomRepository;
use App\Repositories\Trainer\LessonRepository;
use App\Repositories\Trainer\PlayerRepository;
use App\Services\TennisMgtService;
use Illuminate\Support\Facades\Auth;

class LessonService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $lessonRepository;
    protected $playerRepository;
    protected $chatRoomRepository;
    protected $playerLesson;
    protected $player;
    protected $lesson;
    protected $lessonFlag;

    public function __construct(
        LessonRepository $lessonRepository,
        PlayerRepository $playerRepository,
        ChatRoomRepository $chatRoomRepository,
        PlayerLesson $playerLesson,
        Players $player,
        Lessons $lesson,
        LessonFlags $lessonFlag
    ) {
        $this->lessonRepository = $lessonRepository;
        $this->playerRepository = $playerRepository;
        $this->chatRoomRepository = $chatRoomRepository;
        $this->playerLesson = $playerLesson;
        $this->player = $player;
        $this->lesson = $lesson;
        $this->lessonFlag = $lessonFlag;
    }

    /**
     * get list lesson.
     *
     * @param array $search
     *
     * @return array $result
     */
    public function getListLesson($search = null)
    {
        $lessonList = $this->lessonRepository->getListLesson($search);

        return $lessonList;
    }

    public function getRegister($id)
    {
        return $this->lessonRepository->getRegister($id);
    }

    /**
     * get list lesson today.
     *
     * @return array $result
     */
    public function getListLessonToday()
    {
        $lessonList = $this->lessonRepository->getListLessonToday();

        return $lessonList;
    }

    /**
     * Count new player.
     *
     * @return Repository
     */
    public function countNewPlayer()
    {
        return  $this->lessonRepository->listLesson()->get();
    }

    /**
     * Count new player.
     *
     * @return Repository
     */
    public function getPlayerStatus()
    {
        return  $this->lessonRepository->getNewPlayer()->get();
    }

    /**
     * get all level.
     *
     * @return Repository
     */
    public function getAllLevel()
    {
        $arrLevel = $this->lessonRepository->getAllLevel();
        $arr = [];
        if (count($arrLevel)) {
            foreach ($arrLevel as $level) {
                $arr[$level['id']] = $level['name'];
            }
        }

        return $arr;
    }

    /**
     * Get lesson by id.
     *
     * @param $lessonId
     *
     * @return Repository
     */
    public function findLessonById($lessonId)
    {
        return $this->lessonRepository->getLessonById($lessonId);
    }

    /**
     * Delete lesson by id.
     *
     * @param $lessonId
     *
     * @return Repository
     */
    public function deleteLesson($lessonId)
    {
        $playerLesson = $this->lessonRepository->getPlayerLessonByLessonId($lessonId);
        if (count($playerLesson)) {
            return false;
        } else {
            return $this->lessonRepository->deleteLesson($lessonId, Auth::guard('trainers')->user()->id);
        }
    }

    /**
     * add message chat.
     *
     * @return Repository
     */
    public function addRoomChat(array $dataPosts)
    {
        $created_id = Auth::guard('trainers')->user();
        $newMessage = new ChatRooms();
        $newMessage->lesson_id = $dataPosts['lesson_id'];
        $newMessage->player_id = $dataPosts['member_id'];
        $newMessage->player_lesson_id = $dataPosts['player_lesson_id'];
        $newMessage->created_id = $created_id->id;

        return $this->lessonRepository->saveOneToOneChat($newMessage);
    }

    /**
     * send message chat from popup.
     *
     * @return Repository
     */
    public function sendMessagePopUp(array $dataPosts)
    {
        $trainer = Auth::guard('trainers')->user();
        $newMessage = new ChatMessages();
        if (isset($dataPosts['chat_participant_id'])) {
            $newMessage->chat_room_id = $dataPosts['chat_participant_id'];
        } else {
            $newMessage->chat_room_id = $dataPosts['room_id'];
        }
        $newMessage->message = $dataPosts['message_chat'];
        $newMessage->from = $trainer->id;
        $newMessage->to = $dataPosts['member_id'];
        $newMessage->info_type = INFO_TYPE_MESSAGE_TRAINER;
        $newMessage->unread_message = UNREAD_MESSAGE;

        return $this->lessonRepository->saveMessagePopUp($newMessage);
    }

    /**
     * Get player lesson by lesson_id.
     *
     * @param $lessonId
     *
     * @return Repository
     */
    public function getPlayerLessonByLessonId($lessonId)
    {
        $arr = [];
        $lesson = $this->findLessonById($lessonId);
        $playerLessons = $this->lessonRepository->getPlayerLessonById($lessonId)->get();
        if ($lesson['lesson_date'] == LESSON_DATE_1) {
            $lesson['times'] = json_decode($lesson['times_study']);
            foreach ($lesson['times'] as $index => $value) {
                $arr[$index + 1] = $value;
            }
        }

        return $playerLessons;
    }

    /**
     * cancel player lesson.
     *
     * @return Repository
     */
    public function cancelPlayerLesson(array $dataPosts)
    {
        $trainer = Auth::guard('trainers')->user();
        $lesson = $this->lessonRepository->getLessonById($dataPosts['lesson_id']);
        $player = $this->lessonRepository->getPlayerById($dataPosts['member_id']);
        $playerLesson = $this->lessonRepository->getPlayerLesson($dataPosts['player_lesson_id']);
        $settlement = $this->lessonRepository->getSettlementByPlayerLessonId($dataPosts['player_lesson_id']);
        $payments = [];
        if ($lesson->status == LESSON_STATUS_1) {
            if ($player->is_deleted == UNDELETED) {
                if ($playerLesson->status != PLAYER_STATUS_0) {
                    $playerLesson->status = PLAYER_STATUS_0;
                    $playerLesson->updated_id = $trainer->id;
                }
                if ($playerLesson->refunded == UNREFUNDED && $playerLesson->refunded !== null) {
                    $playerLesson->refund_rate = $dataPosts['percent'];
                    $playerLesson->refund_point = $lesson->payment_fee - (($lesson->payment_fee * $dataPosts['percent']) / 100);
                    $playerLesson->status = LESSON_PLAYER_STATUS_4;
                }
                if ($settlement) {
                    $payments = $this->lessonRepository->getPaymentBySettId($settlement->id);
                    $settlement->status = SETTLEMENT_99;
                    $settlement->updated_id = $trainer->id;
                    if (count($payments)) {
                        foreach ($payments as $payment) {
                            $payment->status = PAYMENT_STATUS_99;
                        }
                    }
                }

                return $this->lessonRepository->cancelPlayerLesson($playerLesson, $settlement, $player, $payments);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * cancel player lesson.
     *
     * @return Repository
     */
    public function cancelReject(array $dataPosts)
    {
        $trainer = Auth::guard('trainers')->user();
        $playerLesson = $this->lessonRepository->getPlayerLesson($dataPosts['player_lesson_id']);
        $playerLesson->status = LESSON_PLAYER_STATUS_0;
        $playerLesson->updated_id = $trainer->id;

        return $this->lessonRepository->cancelReject($playerLesson);
    }

    /**
     * get player lesson by id.
     *
     * @return repository
     */
    public function getPlayerLessonById(int $idPlayerLesson)
    {
        $arr = [];
        $arrPlayerLesson = [];
        $playerLesson = $this->lessonRepository->getPlayerLessonByIdPL($idPlayerLesson);
        $playerLesson['level'] = json_decode($playerLesson['levels']);
        if ($playerLesson['lesson_date'] == LESSON_DATE_1) {
            $playerLesson['times'] = json_decode($playerLesson['times_study']);
            foreach ($playerLesson['times'] as $index => $value) {
                $arr[$index + 1] = $value;
            }
            $playerLesson['classes'] = $this->getClass($arr, $playerLesson['start_time'], $playerLesson['end_time']);
            if (date('Y-m-d') < date('Y-m-d', strtotime($playerLesson['end_time']))) {
                $playerLesson['player_classes'] = $this->getClass($arr, $playerLesson['start_time'], date('Y-m-d'));
            } else {
                $playerLesson['player_classes'] = $this->getClass($arr, $playerLesson['start_time'], $playerLesson['end_time']);
            }
        } elseif ($playerLesson['lesson_date'] == LESSON_DATE_0 && $playerLesson['lesson_date'] !== null) {
            $playerLesson['classes'] = LESSON_DATE_1;
            $cnt = $this->getClass($arr, $playerLesson['start_time'], date('Y-m-d'));
            if ($cnt == LESSON_DATE_0) {
                $playerLesson['player_classes'] = LESSON_DATE_0;
            } else {
                $playerLesson['player_classes'] = LESSON_DATE_1;
            }
        } else {
            $playerLesson['classes'] = LESSON_DATE_0;
            $playerLesson['player_classes'] = LESSON_DATE_0;
        }
        array_push($arrPlayerLesson, $playerLesson);

        return $arrPlayerLesson;
    }

    /**
     * update status player lesson status.
     *
     * @return repository
     */
    public function updateStatusPlayerLesson(int $player_lesson_id, $approve = false)
    {
        $trainer = Auth::guard('trainers')->user();
        $now = date('Y/m/d H:i:s');
        $updatePlayer = $this->playerLesson->find($player_lesson_id);
        $player = $this->player->find($updatePlayer->player_id);
        $lesson = $this->lesson->find($updatePlayer->lesson_id);
        $updatePlayer->updated_id = $trainer->id;
        if ($approve) {
            if (date('Y-m-d') < date('Y-m-d', strtotime($lesson->datetime_lesson)) && ($lesson->category == LESSON_PLAYER_STATUS_2)) {
                $updatePlayer->status = LESSON_PLAYER_STATUS_1;
            } elseif (date('Y-m-d') >= date('Y-m-d', strtotime($lesson->datetime_lesson)) && date('Y-m-d') <= date('Y-m-d', strtotime($lesson->datetime_lesson)) && ($lesson->category == LESSON_PLAYER_STATUS_2) || $lesson->datetime_lesson == null) {
                $updatePlayer->status = LESSON_PLAYER_STATUS_2;
            } else {
                $updatePlayer->status = LESSON_PLAYER_STATUS_3;
            }
            if ($updatePlayer->refunded == REFUNDED) {
                if ($player->point >= $lesson->payment_fee) {
                    $player->point = (float) $player->point - (float) $lesson->payment_fee;
                    $updatePlayer->refunded = UNREFUNDED;
                } else {
                    return false;
                }
            }
            $updatePlayer->approve_date = $now;
        } else {
            if ($updatePlayer->refunded == UNREFUNDED && $updatePlayer->refunded !== null) {
                if ($lesson->status == LESSON_STATUS_1) {
                    $player->point = (float) $player->point + (float) $lesson->payment_fee;
                    $updatePlayer->refunded = REFUNDED;
                }
            }
            $updatePlayer->status = LESSON_PLAYER_STATUS_99;
        }

        return $this->lessonRepository->updateStatusPlayerLesson($updatePlayer, $player, $player_lesson_id);
    }

    /**
     * update lesson flag.
     *
     * @return repository
     */
    public function updateLessonFlag(int $player_lesson_id)
    {
        $newFlag = new LessonFlags();
        $newFlag->player_lesson_id = $player_lesson_id;

        return $this->lessonRepository->saveLessonFlag($newFlag);
    }

    /**
     * count status player lesson.
     *
     * @return Repository
     */
    public function countStatusPlayerLesson()
    {
        return $this->lessonRepository->countPlayerLessonApply();
    }

    /**
     * Get array player.
     *
     * @param $member_id
     *
     * @return mixed
     */
    public function getArrayPlayer($member_id)
    {
        $arrPlayer = [];
        foreach ($member_id as $member) {
            $player = $this->playerRepository->getPlayerByMemberId($member);
            array_push($arrPlayer, $player);
        }

        return $arrPlayer;
    }

    /**
     * Get email.
     *
     * @param $member_id
     *
     * @return mixed
     */
    public function getArrayEmail($member_id)
    {
        $arrEmail = [];
        foreach ($member_id as $member) {
            $email = $this->playerRepository->getEmailByMemberId($member);
            array_push($arrEmail, $email);
        }

        return $arrEmail;
    }

    /**
     * Get email.
     *
     * @param $member_id
     *
     * @return mixed
     */
    public function getEmail($member_id)
    {
        return $this->playerRepository->getEmailByMemberId($member_id);
    }

    /**
     * get class.
     *
     * @return Repository
     */
    public function getClass(array $arr, $startTime, $endTime)
    {
        $count = 0;
        foreach ($arr as $index => $value) {
            for ($i = strtotime($startTime); $i <= strtotime($endTime); $i = strtotime('+1 day', $i)) {
                if (date('N', $i) == $index) {
                    ++$count;
                }
            }
        }

        return $count;
    }

    /**
     * get school by id.
     *
     * @param $school_id
     *
     * @return Repository
     */
    public function getSchoolById($school_id)
    {
        return $this->playerRepository->getSchoolById($school_id);
    }

    public function getRoomById($player_lesson_id)
    {
        return $this->lessonRepository->getChatRoom($player_lesson_id);
    }
}
