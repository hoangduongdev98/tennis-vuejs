<?php

namespace App\Services\Trainer;

use App\Models\ChatMessages;
use App\Models\Lessons;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Repositories\Trainer\ChatRoomRepository;
use App\Services\TennisMgtService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class ChatService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $chatRoomRepository;
    protected $player;
    protected $lesson;
    protected $playerLesson;

    public function __construct(
        ChatRoomRepository $chatRoomRepository,
        Players $player,
        Lessons $lesson,
        PlayerLesson $playerLesson
    ) {
        $this->chatRoomRepository = $chatRoomRepository;
        $this->player = $player;
        $this->lesson = $lesson;
        $this->playerLesson = $playerLesson;
    }

    /**
     * send message chat.
     *
     * @return Repository
     */
    public function sendMessageChat(array $dataPosts)
    {
        $trainer = Auth::guard('trainers')->user();
        $arr_participant_id = $dataPosts['participant_id'];
        $dataMess = [];
        foreach ($arr_participant_id as $participant) {
            $playerId = $this->chatRoomRepository->getPlayerIdByParticipantId($participant);
            if ($playerId->lessStt == LESSON_PLAYER_STATUS_1) {
                $newMessage = new ChatMessages();
                $newMessage->chat_room_id = $participant;
                $newMessage->message = $dataPosts['message'];
                $newMessage->from = $trainer->id;
                $newMessage->to = $playerId->player_id;
                $newMessage->info_type = INFO_TYPE_MESSAGE_TRAINER;
                $newMessage->unread_message = UNREAD_MESSAGE;
                array_push($dataMess, $newMessage);
            }
        }

        return $this->chatRoomRepository->saveMessage($dataMess);
    }

    /**
     * get list Chat Participants.
     *
     * @param array $search
     *
     * @return array $result
     */
    public function getListChatParticipants($search = null)
    {
        $listParticipants = $this->chatRoomRepository->getListChatParticipants($search)->get();
        $arrParticipant = [];
        foreach ($listParticipants as $participant) {
            $participant['status'] = null;
            $latestMessageOfPlayer = $this->chatRoomRepository->getLatestMessage($participant->id, INFO_TYPE_MESSAGE_PLAYER)->first();
            $latestMessageOfTrainer = $this->chatRoomRepository->getLatestMessage($participant->id, INFO_TYPE_MESSAGE_TRAINER)->first();
            if ($latestMessageOfTrainer) {
                if ($latestMessageOfPlayer) {
                    if ($latestMessageOfPlayer->created_at >= $latestMessageOfTrainer->created_at) {
                        if ($latestMessageOfPlayer->unread_message == UNREAD_MESSAGE) {
                            $participant['status'] = UNREAD_MESSAGE;
                        } else {
                            $participant['status'] = READ_MESSAGE;
                        }
                    } else {
                        $participant['status'] = READ_AND_ANSWER;
                    }
                } else {
                    $participant['status'] = READ_AND_ANSWER;
                }
            } elseif ($latestMessageOfPlayer) {
                if ($latestMessageOfPlayer->unread_message == UNREAD_MESSAGE) {
                    $participant['status'] = UNREAD_MESSAGE;
                } else {
                    $participant['status'] = READ_MESSAGE;
                }
            }
            array_push($arrParticipant, $participant);
        }

        return $arrParticipant;
    }

    /**
     * delete chat room participant.
     *
     * @return Repository
     */
    public function getRoomByPlayerId($player_id)
    {
        return  $this->chatRoomRepository->getListChatParticipants($player_id);
    }

    /**
     * delete chat room participant.
     *
     * @return Repository
     */
    public function getAllLesson()
    {
        $arrLesson = $this->chatRoomRepository->getAllLesson();
        $arr = [];
        if (count($arrLesson)) {
            foreach ($arrLesson as $lesson) {
                $arr[$lesson['id']] = $lesson['name'];
            }
        }

        return $arr;
    }

    /**
     * delete chat room participant.
     *
     * @return Repository
     */
    public function getAllPlayer()
    {
        $arrPlayer = $this->chatRoomRepository->getAllPlayer();
        $arr = [];
        if (count($arrPlayer)) {
            foreach ($arrPlayer as $player) {
                $arr[$player['id']] = $player['name'];
            }
        }

        return $arr;
    }

    /**
     * delete chat room participant.
     *
     * @return Repository
     */
    public function addChatRoomById($id)
    {
        $roomChat = $this->ChatRooms->getChatRoomById($id);
        $now = date('Y/m/d H:i:s');
        $created_id = Auth::guard('trainers')->user();
        if ($roomChat) {
            $roomChat->updated_id = $created_id->id;
            $roomChat->updated_at = $now;
        }

        return $this->chatRoomRepository->saveRoom($roomChat);
    }

    /**
     * delete chat room participant.
     *
     * @return Repository
     */
    public function getListMessageByParticipantId($room_id)
    {
        $arrMess = [];
        $unreadMessage = $this->chatRoomRepository->getUnreadMessageByParticipantId($room_id, INFO_TYPE_MESSAGE_PLAYER)->get();
        $dataMess = $this->chatRoomRepository->getListMessageByParticipantId($room_id, $unreadMessage);
        if ($dataMess) {
            foreach ($dataMess as $mess) {
                $arr = [];
                $timeMessage = date('Y/m/d', strtotime($mess->message_time));
                foreach ($dataMess as $m) {
                    $timeMess = date('Y/m/d', strtotime($m->message_time));
                    $disabled = '';
                    $limitTime = $mess->updated_at->addHours(72);
                    $now = Carbon::now();
                    if ($mess->trainerStt == SCHOOL_STATUS_99
                    || $mess->trainerSchStt == SCHOOL_STATUS_99
                    || $mess->lessStt == LESSON_STATUS_99
                    || $mess->trainerStt == SCHOOL_STATUS_2
                    || $mess->trainerSchStt == SCHOOL_STATUS_2
                    || $mess->lessStt == LESSON_STATUS_2
                    || $limitTime->lt($now) && $mess->lessonStt == LESSON_PLAYER_STATUS_4
                    || $limitTime->lt($now) && $mess->is_deleted == DELETED
                    || $limitTime->lt($now) && $mess->lessonStt == LESSON_PLAYER_STATUS_3) {
                        $disabled = 'disabled';
                    }
                    if ($timeMessage == $timeMess) {
                        $message = [
                            'from' => $m->from,
                            'to' => $m->to,
                            'info_type' => $m->info_type,
                            'message' => $m->message,
                            'time' => $m->message_time,
                            'player_name' => $m->player_name,
                            'lesson_stt' => $m->lessonStt,
                        ];
                        array_push($arr, $message);
                    }
                }
                $arrMess[$timeMessage] = $arr;
                $arrMess['disabled'] = $disabled;
            }
        }

        return $arrMess;
    }

    /**
     * Get settlement.
     *
     * @param $player_lesson_id
     *
     * @return mixed
     */
    public function getSettlement($player_lesson_id)
    {
        return $this->chatRoomRepository->getSettlementByPlayerLessonId($player_lesson_id);
    }
}
