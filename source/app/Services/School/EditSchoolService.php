<?php



namespace App\Services\School;



use App\Services\TennisMgtService;
use App\Repositories\School\EditSchoolRepository;
use App\Models\StructureFee;
use Auth;
use Illuminate\Support\Facades\Config;
use App\Models\Schools;
use Illuminate\Support\Facades\Hash;
use App\Models\Images;
/**
 * Description of EditSchoolService
 *
 */
class EditSchoolService extends TennisMgtService 
{    
    /**
     * @var \App\Repositories\Repository
     */
    protected $editSchoolRepository, $school, $fee, $image;
            
    public function __construct(
        EditSchoolRepository $editSchoolRepository, 
        Schools $school, 
        StructureFee $fee, 
        Images $image
    ) {
        $this->editSchoolRepository = $editSchoolRepository;
        $this->school = $school;
        $this->fee = $fee;
        $this->image = $image;
    }        
    
    /**
     * Get level name
     *
     * @return array $arr
     */
    public function getLevelName() 
    {
        $arr = [];
        $levels = $this->editSchoolRepository->getAllLevel();
        if (count($levels)) {
            foreach ($levels as $level)
            {
                $arr[$level['id']] = $level['name'];
            }
        }        
        return $arr;        
    }
        
    /**
     * Get school by id
     *
     * @param int $idSchool
     * @return array $dataSchool
     */
    public function findSchoolById (int $idSchool) 
    {
        $dataSchool = $this->editSchoolRepository->getSchoolById($idSchool);
        $arrImage = $this->editSchoolRepository->findAllImageBySchoolId($idSchool);
        if (count($arrImage)) {
            foreach ($arrImage as $img) {
                $dataSchool['avatar'. $img['position']] = $img['name'];
            }
        }       
        return $dataSchool;
    }
    
    /**
     * Get first staff of school
     *
     * @param int $idSchool
     * @return Repository
     */
    public function getFirstStaffOfSchool (int $idSchool) 
    {
        return $this->editSchoolRepository->findStaffBySchoolId($idSchool);
    }
    
    /**
     * Get structure fee of school
     *
     * @param int $idSchool
     * @return Repository
     */
    public function getStructureFee(int $idSchool) 
    {
        return $this->editSchoolRepository->getStructureFee($idSchool);
    }
    
    /**
     * save edit school
     *
     * @param int $idSchool
     * @param array $dataPosts     
     * @return Repository
     */
    public function editSchool (int $idSchool, array $dataPosts) 
    {
        if ($idSchool != $dataPosts['schoolId']) {
            return false;
        }
        $arrImage = [];
        $now = date("Y-m-d H:i:s");
        $editSchool = $this->school->find($idSchool);
        $editSchool->name = $dataPosts['school_name'];
        $editSchool->representative = $dataPosts['representative'];
        $editSchool->post_code = trim(str_replace('-','',$dataPosts['post_code']));
        $editSchool->prefecture_id = $dataPosts['prefecture_id'];
        $editSchool->address = $dataPosts['address'];
        $editSchool->phone = trim(str_replace('-','',$dataPosts['phone']));
        $editSchool->fax = trim(str_replace('-','',$dataPosts['fax']));
        $editSchool->bank_name = $dataPosts['bank_name'];
        $editSchool->branch_bank = $dataPosts['branch_bank'];
        $editSchool->account_type = $dataPosts['account_type'];
        $editSchool->account_number = $dataPosts['account_number'];
        $editSchool->account_holder = $dataPosts['account_holder'];
        $editSchool->highlight = $dataPosts['highlight'];
        if (isset($dataPosts['day_off'])) {
            $editSchool->day_off = json_encode($dataPosts['day_off']);
        } else {
            $editSchool->day_off = NULL;
        }
        $editSchool->achievement = $dataPosts['achievement'];
        $editSchool->note = $dataPosts['note'];
        $editSchool->updated_id = $dataPosts['updated_id'];
        $editSchool->updated_at = $now;
        $editSchool->page_url = $dataPosts['page_url'];
        if ($editSchool) {
            $editStaff = $this->editSchoolRepository->findStaffBySchoolId($idSchool);
            if ($editStaff) {
                $editStaff->school_manager = $dataPosts['school_manager'];
                $editStaff->email = $dataPosts['email'];
                if ($dataPosts['password'] == PASSWORD) {
                    $editStaff->password = $dataPosts['old_password'];
                } else {
                    $editStaff->password = Hash::make($dataPosts['password']);
                }
                $editStaff->role = ROLE_1;
                $editStaff->updated_id = $dataPosts['updated_id'];
                $editStaff->updated_at = $now;
                //save image
                if ($editStaff) {
                    if (!empty($dataPosts['avatar1'])) {
                        $editImage1 = $this->editSchoolRepository->findImageBySchool($idSchool, 1);
                        if (!$editImage1) {
                            $editImage1 = new Images();
                        }
                        $this->saveImageForSchool($editImage1, $dataPosts['avatar1'], 1, null, $dataPosts['updated_id']);
                        array_push($arrImage, $editImage1);
                    }
                    if (!empty($dataPosts['avatar2'])) {
                        $editImage2 = $this->editSchoolRepository->findImageBySchool($idSchool, 2);
                        if (!$editImage2) {
                            $editImage2 = new Images();
                        }
                        $this->saveImageForSchool($editImage2, $dataPosts['avatar2'], 2, null, $dataPosts['updated_id']);
                        array_push($arrImage, $editImage2);
                    }
                    if (!empty($dataPosts['avatar3'])) {
                        $editImage3 = $this->editSchoolRepository->findImageBySchool($idSchool, 3);
                        if (!$editImage3) {
                            $editImage3 = new Images();
                        }
                        $this->saveImageForSchool($editImage3, $dataPosts['avatar3'], 3, null, $dataPosts['updated_id']);
                        array_push($arrImage, $editImage3);
                    }
                }
            }
        }
        return $this->editSchoolRepository->saveEditSchool($editSchool, $editStaff, $arrImage, $idSchool);
    }
    
    /**
     * Save image for school
     *
     * @param $saveImage
     * @param $name
     * @param int $position
     * @param $created_id
     * @param $updated_id
     * @return $saveImage
     */
    public function saveImageForSchool ($saveImage, $name, $position, $created_id = null, $updated_id = null) 
    {
        $saveImage->name = $name;
        $saveImage->path = AVATAR_SCHOOL_SAVE_JS;
        $saveImage->position = $position;
        $saveImage->info_types = INFO_TYPE_SCHOOL;
        if ($created_id) {
            $saveImage->created_id = $created_id;
        }
        if ($updated_id) {
            $saveImage->updated_id = $updated_id;
        }
    }
    
    
    
    /**
     * Delete Structure fee by id
     *
     * @param $id
     * @return json $result
     */
    public function deleteStructureFeeById($id, $schoolId) 
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $dataSettlementFee = $this->editSchoolRepository->getStructureFeeById($id);
        if ($dataSettlementFee) {
            $dataSettlementFee->is_deleted = DELETED;
            $dataSettlementFee->deleted_id = $schoolId;
            if ($dataSettlementFee->save()) {
                $result['msg'] = 'Delete success';
            } else {
                $result['success'] = CODE_AJAX_ERROR;
                $result['msg'] = Config::get('message.common_msg.delete_error');
            }
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = Config::get('message.common_msg.not_isset_id');
        }

        return response()->json(['result' => $result]);
    }
   
}
