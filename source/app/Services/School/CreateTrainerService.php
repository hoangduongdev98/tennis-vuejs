<?php

namespace App\Services\School;

use App\Repositories\School\CreateTrainerRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Trainers;
use Illuminate\Support\Facades\Hash;
use App\Services\TennisMgtService;

class CreateTrainerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $createTrainerRepository,$trainer;

    public function __construct(
        CreateTrainerRepository $createTrainerRepository,
        Trainers $trainer
    ) {
        $this->trainer = $trainer;
        $this->createTrainerRepository = $createTrainerRepository;
    }

        /**
     * Save create trainer
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function create(array $dataPosts)
    {
        $newTrainer = $this->trainer;
        $newTrainer->name = $dataPosts['trainer_name'];
        $newTrainer->school_id = $dataPosts['created_id'];
        $newTrainer->post_code = trim(str_replace('-','',$dataPosts['post_code']));
        $newTrainer->prefecture_id = $dataPosts['prefecture_id'];
        $newTrainer->address = $dataPosts['address'];
        if (!empty($dataPosts['phone'])) {
            $newTrainer->phone = trim(str_replace('-','',$dataPosts['phone']));
        }
        if (!empty($dataPosts['fax'])) {
            $newTrainer->fax = trim(str_replace('-','',$dataPosts['fax']));
        }
        $newTrainer->email = $dataPosts['email'];
        $newTrainer->password = Hash::make($dataPosts['password']);
        $newTrainer->license_date = date("Y-m-d", strtotime($dataPosts['license_date']));
        $newTrainer->status = $dataPosts['status'];
        if (!empty($dataPosts['bank_name'])) {
            $newTrainer->bank_name = $dataPosts['bank_name'];
        }
        if (!empty($dataPosts['branch_bank'])) {
            $newTrainer->branch_bank = $dataPosts['branch_bank'];
        }
        if (!empty($dataPosts['account_type'])) {
            $newTrainer->account_type = $dataPosts['account_type'];
        }
        if (!empty($dataPosts['account_number'])) {
            $newTrainer->account_number = $dataPosts['account_number'];
        }
        if (!empty($dataPosts['account_holder'])) {
            $newTrainer->account_holder = $dataPosts['account_holder'];
        }
        if (!empty($dataPosts['rate'])) {
            $newTrainer->ratio = $dataPosts['rate'];
        } else {
            $newTrainer->ratio = 0;
        }
        $newTrainer->created_id = $dataPosts['created_id'];
        return $this->createTrainerRepository->saveCreateTrainer($newTrainer);
    }
    
    public function findTrainerById (int $idTrainer) 
    {
        return $this->createTrainerRepository->getTrainerById($idTrainer);
    }
    
    /**
     * Save edit Trainer
     *
     * @param $idTrainer
     * @param array $dataPosts
     * @return Repository
     */
    public function editTrainer (int $idTrainer, array $dataPosts) 
    {       
        if ($idTrainer != $dataPosts['id']) {
            return false;
        }
        $now = date("Y-m-d H:i:s");
        $editTrainer = $this->trainer->find($idTrainer);
        $editTrainer->name = $dataPosts['trainer_name'];
        $editTrainer->post_code = trim(str_replace('-','',$dataPosts['post_code']));
        $editTrainer->prefecture_id = $dataPosts['prefecture_id'];
        $editTrainer->address = $dataPosts['address'];
        $editTrainer->phone = trim(str_replace('-','',$dataPosts['phone']));
        $editTrainer->fax = trim(str_replace('-','',$dataPosts['fax']));
        $editTrainer->email = $dataPosts['email'];
        if ($dataPosts['password'] == PASSWORD) {
            $editTrainer->password = $dataPosts['old_password'];
        } else {
            $editTrainer->password = Hash::make($dataPosts['password']);
        }
        $licenseDateTrainer = $dataPosts['license_date'];
        $editTrainer->license_date = date("Y-m-d", strtotime($licenseDateTrainer));
        $editTrainer->status = $dataPosts['status'];
        $editTrainer->bank_name = $dataPosts['bank_name'];
        $editTrainer->branch_bank = $dataPosts['branch_bank'];
        $editTrainer->account_type = $dataPosts['account_type'];
        $editTrainer->account_number = $dataPosts['account_number'];
        $editTrainer->account_holder = $dataPosts['account_holder'];
        $editTrainer->ratio = $dataPosts['rate'];
        $editTrainer->updated_id = $dataPosts['updated_id'];
        $editTrainer->updated_at = $now;
        
        return $this->createTrainerRepository->saveEditTrainer($editTrainer, $idTrainer);
    }
    
    /**
     * Delete trainer
     *
     * @param int $idTrainer
     * @return Repository
     */
    public function deleteTrainer (int $idTrainer, $schoolId) 
    {
        return $this->createTrainerRepository->deleteDataTrainer($idTrainer, $schoolId);
    }
    
    /**
     * get trainer by id
     *
     * @param int $idTrainer
     * @return Repository
     */
    public function getTrainerById (int $idTrainer) 
    {
        return $this->createTrainerRepository->getTrainerById($idTrainer);
    }
    
    /**
     * Get school by staff school_id
     *
     * @param int $id
     * @return Repository
     */
    public function getSchoolByStaffLogin(int $id) 
    {
        return $this->createTrainerRepository->getSchoolByStaffLogin($id);
    }
}
