<?php

namespace App\Services\School;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\School\AccountStaffRepository;
use App\Models\Staffs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountManagerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $accountStaffRepository, $staff;

    public function __construct (
        AccountStaffRepository $accountStaffRepository,
        Staffs $staff
    ) {
        $this->accountStaffRepository = $accountStaffRepository;
        $this->staff = $staff;
    }

    /**
     * save create account
     * @param array $dataPosts
     * @return Repository
     */
    public function createAccount(array $dataPosts)
    {
        $newAccount = $this->staff;
        $newAccount->school_id = $dataPosts['schoolId'];
        $newAccount->school_manager = $dataPosts['name'];
        $newAccount->email = $dataPosts['email'];
        $newAccount->password = Hash::make($dataPosts['password']);
        $newAccount->role = $dataPosts['role'];
        $newAccount->created_id = $dataPosts['created_id'];

        return $this->accountStaffRepository->saveCreateAccount($newAccount);
    }

    /**
     * get list account manager
     * @return Repository
     */
    public function getListAccountManager () {
        return $this->accountStaffRepository->getListAccountManagers();
    }

    /**
     * get data account by id
     * @param $request \Illuminate\Http\Request, $id
     * @return Repository
     */
    public function findAccountById(Request $request, $id) 
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $result['account'] = [];
        $account = $this->accountStaffRepository->getAccountById($id);
        if ($account) {
            if (isset($account['updated_at'])) {
                $account['time_updated'] = date('Y/m/d H:i:s', strtotime($account['updated_at']));
            }
            $result['account'] = $account;
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }

    /**
     * save create account
     * @param array $dataPosts
     * @return Repository
     */
    public function editAccount (int $idAccount, array $dataPosts) 
    {
        if ($idAccount != $dataPosts['id']) {
            return false;
        }
        $editAccount = $this->staff->find($idAccount);
        $editAccount->school_manager = $dataPosts['name'];
        $editAccount->email = $dataPosts['email'];
        if ($dataPosts['password'] == PASSWORD) {
            $editAccount->password = $dataPosts['old_password'];
        } else {
            $editAccount->password = Hash::make($dataPosts['password']);
        }
        $editAccount->role = $dataPosts['role'];
        $editAccount->updated_id = $dataPosts['updated_id'];
        return $this->accountStaffRepository->saveEditAccount($editAccount, $idAccount);
    }

    /**
     * delete account manager
     * @param int $idAccount
     * @return Repository
     */
    public function deleteAccountManager($idAccount, $schoolId) 
    {
        $result = true;
        $account = $this->accountStaffRepository->getAccountById($idAccount);
        if ($account) {
            $account->is_deleted = DELETED;
            $account->deleted_id = $schoolId;
            if (!$account->save()) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Get data account logon from admin site by school_id
     * @param $schoolId
     * @return Repository
     */
    public function getAccountLogonByShoolId($schoolId) 
    {
        return $this->accountStaffRepository->getAccountLogonByShoolId($schoolId);
    }
    
    /**
     * get data account by id
     * @param $id
     * @return Repository
     */
    public function getStaffAccountById($id) 
    {
        return $this->accountStaffRepository->getAccountById($id);
    }

}
