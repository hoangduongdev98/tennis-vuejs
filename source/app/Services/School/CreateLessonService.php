<?php

namespace App\Services\School;

use App\Models\ChatRooms;
use App\Models\Images;
use App\Models\Lessons;
use App\Repositories\School\CreateLessonRepository;
use App\Services\TennisMgtService;

/**
 * Description of CreateLessonService.
 */
class CreateLessonService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $createLessonRepository;
    protected $lesson;
    protected $image;
    protected $chatroom;

    public function __construct(
        CreateLessonRepository $createLessonRepository,
        Lessons $lesson,
        Images $image,
        ChatRooms $chatroom
    ) {
        $this->createLessonRepository = $createLessonRepository;
        $this->lesson = $lesson;
        $this->image = $image;
        $this->chatroom = $chatroom;
    }

    /**
     * Save create lesson.
     *
     * @return repository
     */
    public function createLesson(array $dataPosts)
    {
        $arrImage = [];
        $newLesson = $this->lesson;
        $newLesson->name = $dataPosts['name'];
        $newLesson->category = $dataPosts['category'];
        if (isset($dataPosts['date_lesson'])) {
            $dateTime = date('Y-m-d', strtotime($dataPosts['date_lesson']));
            $newLesson->datetime_lesson = $dateTime;
        }
        if (isset($dataPosts['register_expire_date'])) {
            $expireDate = date('Y-m-d', strtotime($dataPosts['register_expire_date']));
            $newLesson->register_expire_date = $expireDate;
        } else {
            $newLesson->register_expire_date = null;
        }
        $newLesson->quantity_player = $dataPosts['applicans'];
        $newLesson->payment_fee = $dataPosts['point'];
        $newLesson->table_id = $dataPosts['created_id'];
        $newLesson->info_type = INFO_TYPE_SCHOOL;
        $newLesson->status = LESSON_STATUS_1;
        $newLesson->created_id = $dataPosts['created_id'];
        if (isset($dataPosts['level'])) {
            $newLesson->levels = json_encode($dataPosts['level']);
        } else {
            $newLesson->levels = null;
        }
        if (isset($dataPosts['date_format']) && $dataPosts['date_format'] !== null) {
            $newLesson->lesson_date = $dataPosts['date_format'];
        } else {
            $newLesson->lesson_date = null;
        }
        if (!empty($dataPosts['detail'])) {
            $newLesson->lesson_detail = $dataPosts['detail'];
        }
        $time_study = [];
        if (isset($dataPosts['times_study']) || isset($dataPosts['lesson_start_time']) && isset($dataPosts['lesson_end_time'])) {
            if (isset($dataPosts['lesson_start_time']) && isset($dataPosts['lesson_end_time'])) {
                $time_study[99] = ['start' => $dataPosts['lesson_start_time']];
                $time_study[99] += ['end' => $dataPosts['lesson_end_time']];
            }
            $newLesson->times_study = json_encode($time_study, JSON_FORCE_OBJECT);
        } elseif (isset($dataPosts['date_format']) && $dataPosts['date_format'] == LESSON_DATE_2) {
            $newLesson->times_study = LESSON_DATE_VALUE_2;
        } else {
            $newLesson->times_study = null;
        }
        if ($newLesson) {
            if (!empty($dataPosts['avatar1'])) {
                $newImage1 = new Images();
                $this->saveImageForLesson($newImage1, $dataPosts['avatar1'], 1);
                $arrImage = $newImage1;
            }
        }

        return $this->createLessonRepository->saveCreateLesson($newLesson, $arrImage);
    }

    /**
     * Save image for lesson.
     *
     * @param $saveImage
     * @param $name
     * @param int $position
     * @param $created_id
     * @param $updated_id
     *
     * @return $saveImage
     */
    public function saveImageForLesson(
        $saveImage,
        $name,
        $position,
        $created_id = null,
        $updated_id = null
    ) {
        $saveImage->name = $name;
        $saveImage->path = AVATAR_LESSON_SAVE_JS;
        $saveImage->position = $position;
        $saveImage->info_types = INFO_TYPE_LESSON;
        if ($created_id) {
            $saveImage->created_id = $created_id;
        }
        if ($updated_id) {
            $saveImage->updated_id = $updated_id;
        }
    }

    /**
     * find lesson by id.
     *
     * @return array $dataLesson
     */
    public function findLessonById(int $idLesson)
    {
        $dataLesson = $this->createLessonRepository->getLessonById($idLesson);
        $arrImage = $this->createLessonRepository->findImageByLessonId($idLesson);
        if (count($arrImage)) {
            foreach ($arrImage as $img) {
                $dataLesson['avatar'.$img['position']] = $img['name'];
            }
        }

        return $dataLesson;
    }

    /**
     * Edit lesson by id.
     *
     * @return repository
     */
    public function editLesson(array $dataPosts, int $idLesson)
    {
        $arrImage = [];
        $now = date('Y-m-d H:i:s');
        $editLesson = $this->lesson->find($idLesson);
        $editLesson->name = $dataPosts['name'];
        $editLesson->category = $dataPosts['category'];
        $editLesson->quantity_player = $dataPosts['applicans'];
        if (isset($dataPosts['date_lesson'])) {
            $dateTime = date('Y-m-d', strtotime($dataPosts['date_lesson']));
            $editLesson->datetime_lesson = $dateTime;
        } elseif ($dataPosts['category'] == LESSON_DATE_1) {
            $editLesson->datetime_lesson = null;
        }
        if (isset($dataPosts['expire_date'])) {
            $expireDate = date('Y-m-d', strtotime($dataPosts['expire_date']));
            $editLesson->register_expire_date = $expireDate;
        } 
        $editLesson->updated_id = $dataPosts['updated_id'];
        if (isset($dataPosts['level'])) {
            $editLesson->levels = json_encode($dataPosts['level']);
        } else {
            $editLesson->levels = null;
        }
        if (isset($dataPosts['date_format']) && $dataPosts['date_format'] !== null) {
            $editLesson->lesson_date = $dataPosts['date_format'];
        } else {
            $editLesson->lesson_date = null;
        }
        $editLesson->lesson_detail = $dataPosts['detail'];
        $time_study = [];
        if (isset($dataPosts['lesson_start_time']) && isset($dataPosts['lesson_end_time'])) {
            if (isset($dataPosts['lesson_start_time']) && isset($dataPosts['lesson_end_time'])) {
                $time_study[99] = ['start' => $dataPosts['lesson_start_time']];
                $time_study[99] += ['end' => $dataPosts['lesson_end_time']];
            }
            $editLesson->times_study = json_encode($time_study, JSON_FORCE_OBJECT);
        } elseif ($dataPosts['category'] == LESSON_DATE_1) {
            $editLesson->times_study = LESSON_DATE_VALUE_2;
        }
        $editLesson->updated_at = $now;
        if ($editLesson) {
            if (!empty($dataPosts['avatar1'])) {
                $editImage1 = $this->createLessonRepository->findImageByLesson($idLesson, 1);
                if ($editImage1) {
                    $editImage1 = $this->image;
                } else {
                    $editImage1 = new Images();
                }
                $this->saveImageForLesson($editImage1, $dataPosts['avatar1'], 1, null, $dataPosts['updated_id']);
                array_push($arrImage, $editImage1);
            }
        }

        return $this->createLessonRepository->saveEditLesson($editLesson, $arrImage, $idLesson);
    }

    /**
     * delete lesson by id.
     *
     * @return repository
     */
    public function deleteLesson(int $idLesson, $schoolId)
    {
        $playerLesson = $this->createLessonRepository->getPlayerLessonByLessonId($idLesson);
        if (count($playerLesson)) {
            return false;
        } else {
            return $this->createLessonRepository->deleteDataLesson($idLesson, $schoolId);
        }
    }
}
