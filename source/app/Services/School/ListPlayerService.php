<?php


namespace App\Services\School;



use App\Services\TennisMgtService;
use App\Repositories\School\ListPlayerRepository;
/**
 * Description of ListPlayerService
 */
class ListPlayerService extends TennisMgtService 
{    
    /**
     * @var \App\Repositories\Repository
     */
    protected $listPlayerRepository;
            
    public function __construct (
        ListPlayerRepository $listPlayerRepository
    ) {
        $this->listPlayerRepository = $listPlayerRepository;
    }
    
    /**
     * Get list player
     *
     * @param array $search
     * @return array $result
     */
    public function getListPlayer($search = null)
    {
        return $this->listPlayerRepository->getListPlayer($search);
    }
    
    /**
     * Get account player by id
     *
     * @param $id
     * @return array $result
     */
    public function getAccountLogonByPlayerId($id)
    {
        return $this->listPlayerRepository->getAccountLogonByPlayerId($id);
    }
    
    /**
     * Sort list player
     *
     * @param array $search
     * @return array $result
     */
    public function sortListPlayer($playerList, $sort, $direction)
    {
        return $this->listPlayerRepository->sortListPlayer($playerList, $sort, $direction);
    }
    
    /**
     * get school
     *
     * @return array $result
     */
    public function getSchool()
    {
        return $this->listPlayerRepository->getSchool();
    }
    
    /**
     * Get list shared player
     *
     * @param array $search
     * @return array $result
     */
    public function getListSharedPlayer($search = null)
    {
        return $this->listPlayerRepository->getListSharedPlayer($search);
    }
    
    /**
     * accept share
     * @param type $data
     * @return type
     */
    public function acceptShare($data)
    {
        return $this->listPlayerRepository->acceptShare($data);
    }
    
    /**
     * delete share
     * @param type $data
     * @return type
     */
    public function deleteShare($data)
    {
        return $this->listPlayerRepository->deleteShare($data);
    }
    
    /**
     * Get account player by id
     *
     * @param $id
     * @return array $result
     */
    public function getAccountSharedPlayer($schoolId, $playerId)
    {
        return $this->listPlayerRepository->getAccountSharedPlayer($schoolId, $playerId);
    }
}
