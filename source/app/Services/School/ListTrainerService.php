<?php


namespace App\Services\School;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\School\ListTrainerReposirory;

/**
 * class ListTrainerService
 *
 */
class ListTrainerService extends TennisMgtService
{   
    /**
     * @var \App\Repositories\Repository
     */
    protected $listTrainerReposirory;
            
    public function __construct (
            ListTrainerReposirory $listTrainerReposirory
    ) {
        $this->listTrainerReposirory = $listTrainerReposirory;
    }
    
    /**
     * Get list trainer
     *
     * @param array $search
     * @return array $arrTrainer
     */
    public function getListTrainer($search = null) 
    {                   
        $trainerLesson = $this->listTrainerReposirory->getListTrainerHasLesson($search);
        $trainerNoLesson = $this->listTrainerReposirory->getListTrainerHasZeroLesson($search);
        if (count($trainerLesson->get())) {
            $arrTrainer = $trainerLesson->union($trainerNoLesson);
        } else {
            $arrTrainer = $trainerNoLesson;
        }
        return $arrTrainer->sortable(['update' => 'DESC']);
    }
    
    /**
     * Get list lesson of trainer
     *
     * @param int $id
     * @return Repository
     */
    public function getTrainerLesson(int $id) 
    {
        return $this->listTrainerReposirory->getTrainerLesson($id);
    }
    
    /**
     * Get account logon by trainerId
     *
     * @param int $id
     * @return Repository
     */
    public function getAccountLogonByTrainerId(int $id) 
    {
        return $this->listTrainerReposirory->getAccountLogonByTrainerId($id);
    }
    
    /**
     * Get school by staff school_id
     *
     * @param int $id
     * @return Repository
     */
    public function getSchoolByStaffLogin(int $id) 
    {
        return $this->listTrainerReposirory->getSchoolByStaffLogin($id);
    }
}
