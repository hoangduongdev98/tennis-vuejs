<?php


namespace App\Services\School;

use App\Services\TennisMgtService;
use Auth;
use App\Repositories\School\ListPaymentRepository;

/**
 * Description of ListPaymentService
 *
 */
class ListPaymentService extends TennisMgtService
{   
    /**
     * @var \App\Repositories\Repository
     */
    protected $listPaymentRepository;
    
    public function __construct(
        ListPaymentRepository $listPaymentRepository
    ) {
        $this->listPaymentRepository = $listPaymentRepository;
    }
    
    /**
     * Get list payment
     *
     * @param array $search
     * @return Repository
     */
    public function getListPayment($search = null)
    {                           
        return $result = $this->listPaymentRepository->getListPayment($search);  
    }
    
    /**
     * Get detail payment
     *
     * @param $id
     * @param $date
     * @return Repository
     */
    public function getDetailPaymentList($id, $date) 
    {   
        return $this->listPaymentRepository->getPaymentDetail($id, $date); 
    }
    
    /**
     * Get name trainer
     *
     * @param id
     * @return Repository
     */
    public function getNameTrainer($id) 
    {
        return $this->listPaymentRepository->getNameTrainer($id);
    }
    
    /**
     * Get all name trainer
     *
     * @return array $arr
     */
    public function getAllNameTrainer() 
    {
        $arrTrainer = $this->listPaymentRepository->getAllTrainers();
        $arr = [];
        if(count($arrTrainer)){
            foreach ($arrTrainer as $trainer) {
                $arr[$trainer['id']] = $trainer['name'];
            }
        }
        return $arr;
    }
       
    /**
     * Edit processing payment
     *
     * @param $dataPost
     * @return array $result
     */
    public function editProcessingPayment($dataPost) 
    {
        $result = true;
        if ($arrTrainerId = $dataPost['arr_id_trainer']) {
            $arrTrainerId = json_decode($arrTrainerId, true);
            foreach ($arrTrainerId as $trainerId => $arrTrainerTimes) {
                if ($arrTrainerTimes) {
                    foreach ($arrTrainerTimes as $trainerTime) {
                        $allTrainerPayments = $this->listPaymentRepository->getDataPaymentByTrainerId($trainerId, $trainerTime)->get();
                        if (count($allTrainerPayments)) {
                            foreach ($allTrainerPayments as $trainerPayment) {
                                if (!$this->listPaymentRepository->editPaymentStatus($trainerPayment['id'], $dataPost['status_approve'], $dataPost['schoolId'])) {
                                    $result = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
    
    /**
     * Delete payment of Trainer
     *
     * @param $trainerId
     * @param $monthYear
     * @return json $result
     */
    public function deletePaymentOfTrainer ($trainerId, $monthYear) 
    {
        return $this->listPaymentRepository->saveDeletePaymentOfSchoolOrTrainer($trainerId, $monthYear, INFO_TYPE_TRAINER, $schoolId);
    }
    
    public function getDetailPayment (int $paymentId) 
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $data = $this->listPaymentRepository->findPaymentById($paymentId);
        if ($data) {
            $result['payment'] = $data;
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }
    
    /**
     * edit payment
     *
     * @param $dataPost
     * @return Repository
     */
    public function editPayment ($dataPost) 
    {
        return $this->listPaymentRepository->saveEditPayment($dataPost, $dataPost['schoolId']);
    }
    
    /**
     * Get data payment by id
     *
     * @param int $paymentId
     * @return \Illuminate\Http\Response
     */
    public function getPaymentById ($paymentId) 
    {
        return $this->listPaymentRepository->findPaymentById($paymentId);
    }
    
    /**
     * Delete payment by id
     *
     * @param int $paymentId
     * @return bool
     */
    public function deletePaymentById (int $paymentId, $schoolId) 
    {
        return $this->listPaymentRepository->saveDeletePayment($paymentId, $schoolId);
    }
     
}
