<?php


namespace App\Services\School;


use App\Services\TennisMgtService;
use App\Repositories\School\AuthRepository;
/**
 * Description of AuthService
 *
 */
class AuthService extends TennisMgtService
{    
    /**
     * @var \App\Repositories\Repository
     */
    protected $authRepository;
    
    public function __construct(
        AuthRepository $authRepository
    ) {
        $this->authRepository = $authRepository;
    }
    
    /**
     * get status school by data
     *
     * @param array $data
     * @return Repository
     */
    public function getStatusSchool($data) 
    {
        return $this->authRepository->getSchoolByStaff($data);
    }
}
