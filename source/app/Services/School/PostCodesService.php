<?php

namespace App\Services\School;

use App\Services\TennisMgtService;
use Illuminate\Support\Facades\Config;
use App\Repositories\School\PostCodesRepository;

class PostCodesService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $postCodeRepository;

    public function __construct (
        PostCodesRepository $postCodeRepository
    ) {
        $this->postCodeRepository = $postCodeRepository;
    }

    /**
     * get address by post_code
     * @param $postCode
     * @return array json
     */
    public function getAddressByPostCode ($postCode) 
    {
        $prefectures_id = null;
        $prefectures = $this->postCodeRepository->getAllPrefectures();
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        //$postCode = str_replace('-', '', $postCode);
        $data = $this->postCodeRepository->findZipCodeByCode($postCode);
        if ($data) {
            foreach ($prefectures as $pref) {
                if ($data['zip_pref'] == $pref['name']) {
                    $prefectures_id = $pref['id'];
                }
            }
            $result['prefectures_id'] = $prefectures_id;
            $result['field'] = $data;
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = Config::get('message.common_msg.postcode_not_exists');
            $prefectures_id = null;
        }

        return response()->json(['result' => $result]);
    }
}
