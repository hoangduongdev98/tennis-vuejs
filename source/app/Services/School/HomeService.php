<?php

namespace App\Services\School;

use App\Services\TennisMgtService;
use App\Repositories\School\HomeRepository;

class HomeService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $homeRepository;

    public function __construct ( 
        HomeRepository $homeRepository
    ) {
        $this->homeRepository = $homeRepository;
    }

    /**
     * Count data trainers
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataTrainers () 
    {
        return $this->homeRepository->countDataOfTrainers();
    }

    /**
     * Count data players
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataPlayers () 
    {
        return $this->homeRepository->countDataOfPlayers();
    }

    /**
     * Count data lessons
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataLessons () 
    {
        return $this->homeRepository->countDataOfLessons();
    }

    /**
     * Count data matches
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataMatches () 
    {
        return $this->homeRepository->countDataOfMatches();
    }

    /**
     * get total lessons in a year
     *
     * @return \Illuminate\Http\Response
     */
    public function dataLessonsInAYear() 
    {
        return $this->homeRepository->getNumbersLessonOneYear();
    }
    
    /**
     * get total matches in a year
     *
     * @return \Illuminate\Http\Response
     */
    public function dataLessonPointInAYear() 
    {
        return $this->homeRepository->getPointOneYear();
    }

}
