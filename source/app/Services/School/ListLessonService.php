<?php

namespace App\Services\School;

use App\Models\ChatMessages;
use App\Models\ChatRooms;
use App\Models\LessonFlags;
use App\Models\Lessons;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\Settlements;
use App\Repositories\School\ListLessonRepository;
use App\Services\TennisMgtService;
use Illuminate\Support\Facades\Auth;

/**
 * class ListLessonService.
 */
class ListLessonService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $listLessonRepository;
    protected $lesson;
    protected $player;
    protected $playerLesson;

    public function __construct(
        ListLessonRepository $listLessonRepository,
        Players $player,
        Lessons $lesson,
        PlayerLesson $playerLesson
    ) {
        $this->listLessonRepository = $listLessonRepository;
        $this->lesson = $lesson;
        $this->player = $player;
        $this->playerLesson = $playerLesson;
    }

    /**
     * Get list lesson.
     *
     * @param array $search
     *
     * @return array $result
     */
    public function getListLesson($search = null)
    {
        return $this->listLessonRepository->getListLesson($search);
    }

    /**
     * Get all level.
     *
     * @return array $arr
     */
    public function getAllLevel()
    {
        $arrLevel = $this->listLessonRepository->getAllLevel();
        $arr = [];
        if (count($arrLevel)) {
            foreach ($arrLevel as $level) {
                $arr[$level['id']] = $level['name'];
            }
        }

        return $arr;
    }

    /**
     * Get player lesson by lesson_id.
     *
     * @param $lessonId
     *
     * @return Repository
     */
    public function getPlayerLessonByLessonId($lessonId)
    {
        $arr = [];
        $lesson = $this->findLessonById($lessonId);
        $playerLessons = $this->listLessonRepository->getPlayerLessonById($lessonId)->get();
        if ($lesson['lesson_date'] == LESSON_DATE_1) {
            $lesson['times'] = json_decode($lesson['times_study']);
            foreach ($lesson['times'] as $index => $value) {
                $arr[$index + 1] = $value;
            }
        }
        foreach ($playerLessons as $playerLesson) {
            if ($lesson['lesson_date'] == LESSON_DATE_1) {
                $playerLesson['classes'] = $this->getClass($arr, $lesson['start_time'], $lesson['end_time']);
                if ($playerLesson['pl_status'] != PLAYER_STATUS_99) {
                    if (date('Y-m-d') <= date('Y-m-d', strtotime($lesson['end_time']))) {
                        $playerLesson['player_classes'] = $this->getClass($arr, $lesson['start_time'], date('Y-m-d'));
                    } else {
                        $playerLesson['player_classes'] = $this->getClass($arr, $lesson['start_time'], $lesson['end_time']);
                    }
                } else {
                    $playerLesson['player_classes'] = LESSON_DATE_0;
                }
            } elseif ($lesson['lesson_date'] == LESSON_DATE_0 && $lesson['lesson_date'] !== null) {
                $playerLesson['classes'] = LESSON_DATE_1;
                $cnt = $this->getClass($arr, $lesson['start_time'], date('Y-m-d'));
                if ($cnt == LESSON_DATE_0) {
                    $playerLesson['player_classes'] = LESSON_DATE_0;
                } else {
                    $playerLesson['player_classes'] = LESSON_DATE_1;
                }
            } else {
                $playerLesson['classes'] = LESSON_DATE_0;
                $playerLesson['player_classes'] = LESSON_DATE_0;
            }
        }

        return $playerLessons;
    }

    /**
     * Get all class by startTime and endTime.
     *
     * @param $startTime
     * @param $endTime
     *
     * @return Repository
     */
    public function getClass(array $arr, $startTime, $endTime)
    {
        $count = 0;
        foreach ($arr as $index => $value) {
            for ($i = strtotime($startTime); $i <= strtotime($endTime); $i = strtotime('+1 day', $i)) {
                if (date('N', $i) == $index) {
                    ++$count;
                }
            }
        }

        return $count;
    }

    /**
     * Get lesson by id.
     *
     * @param $lessonId
     *
     * @return Repository
     */
    public function findLessonById($lessonId)
    {
        return $this->listLessonRepository->getLessonById($lessonId);
    }

    /**
     * send message chat from popup.
     *
     * @return Repository
     */
    public function sendMessagePopUp(array $dataPosts)
    {
        $schoolId = Auth::guard('schools')->user()->school_id;
        $newMessage = new ChatMessages();
        if (isset($dataPosts['chat_participant_id'])) {
            $newMessage->chat_room_id = $dataPosts['chat_participant_id'];
        } else {
            $newMessage->chat_room_id = $dataPosts['room_id'];
        }
        $newMessage->message = $dataPosts['message_chat'];
        $newMessage->from = $schoolId;
        $newMessage->to = $dataPosts['member_id'];
        $newMessage->info_type = INFO_TYPE_MESSAGE_SCHOOL;
        $newMessage->unread_message = UNREAD_MESSAGE;

        return $this->listLessonRepository->saveMessagePopUp($newMessage);
    }

    /**
     * update status player lesson status.
     *
     * @param $approve
     *
     * @return repository
     */
    public function updateStatusPlayerLesson(int $player_lesson_id, $schoolId, $approve = false)
    {
        $now = date('Y/m/d H:i:s');
        $updatePlayer = $this->playerLesson->find($player_lesson_id);
        $player = $this->player->find($updatePlayer->player_id);
        $lesson = $this->lesson->find($updatePlayer->lesson_id);
        $updatePlayer->updated_id = $schoolId;
        if ($approve) {
            if ($updatePlayer->refunded == REFUNDED) {
                if ($player->point >= $lesson->payment_fee) {
                    $player->point = (float)$player->point - (float)$lesson->payment_fee;
                    $updatePlayer->refunded = UNREFUNDED;
                } else {
                    return false;
                }
            }
            if (date('Y-m-d') < date('Y-m-d', strtotime($lesson->datetime_lesson)) && ($lesson->category == LESSON_PLAYER_STATUS_2)) {
                $updatePlayer->status = LESSON_PLAYER_STATUS_1;
            } elseif (date('Y-m-d') >= date('Y-m-d', strtotime($lesson->datetime_lesson)) && date('Y-m-d') <= date('Y-m-d', strtotime($lesson->datetime_lesson)) && ($lesson->category == LESSON_PLAYER_STATUS_2) || ($lesson->datetime_lesson == null) || $lesson->category) {
                $updatePlayer->status = LESSON_PLAYER_STATUS_2;
            } else {
                $updatePlayer->status = LESSON_PLAYER_STATUS_3;
            }
            $updatePlayer->approve_date = $now;
        } else {
            if ($updatePlayer->refunded == UNREFUNDED && $updatePlayer->refunded !== null) {
                if ($lesson->status == LESSON_STATUS_1 && $lesson->status !== null) {
                    $player->point = (float)$player->point +  (float)$lesson->payment_fee;
                    $updatePlayer->refunded = REFUNDED;
                }
            }
            $updatePlayer->status = LESSON_PLAYER_STATUS_99;
        }

        return $this->listLessonRepository->updateStatusPlayerLesson($updatePlayer, $player, $player_lesson_id);
    }


    /**
     * update lesson flag.
     *
     * @return repository
     */
    public function updateLessonFlag(int $player_lesson_id)
    {
        $newFlag = new LessonFlags();
        $newFlag->player_lesson_id = $player_lesson_id;

        return $this->listLessonRepository->saveLessonFlag($newFlag);
    }


    /**
     * Get email.
     *
     * @param $member_id
     *
     * @return mixed
     */
    public function getPlayer($member_id)
    {
        return $this->listLessonRepository->getPlayerByMemberId($member_id);
    }

    /**
     * Get school by id.
     *
     * @param $idSchool
     *
     * @return mixed
     */
    public function getSchoolById($idSchool)
    {
        return $this->listLessonRepository->getSchoolById($idSchool);
    }

    /**
     * add message chat.
     *
     * @return Repository
     */
    public function addRoomChat(array $dataPosts)
    {
        $schoolId = Auth::guard('schools')->user()->school_id;
        $chatRoomParticipant = new ChatRooms();
        $chatRoomParticipant->lesson_id = $dataPosts['lesson_id'];
        $chatRoomParticipant->player_id = $dataPosts['member_id'];
        $chatRoomParticipant->player_lesson_id = $dataPosts['player_lesson_id'];
        $chatRoomParticipant->created_id = $schoolId;

        return $this->listLessonRepository->saveOneToOneChat($chatRoomParticipant);
    }

    /**
     * cancel player lesson.
     *
     * @return Repository
     */
    public function cancelPlayerLesson(array $dataPosts)
    {
        $lesson = $this->listLessonRepository->getLessonById($dataPosts['lesson_id']);
        $player = $this->listLessonRepository->getPlayerById($dataPosts['member_id']);
        $playerLesson = $this->listLessonRepository->getPlayerLesson($dataPosts['player_lesson_id']);
        $settelement = $this->listLessonRepository->getSettlementByPlayerLessonId($dataPosts['player_lesson_id']);
        $payments = [];
        if ($lesson->status == LESSON_STATUS_1 || $lesson->status == SCHOOL_STATUS_1) {
            if ($player->is_deleted == UNDELETED) {
                if ($playerLesson->status != PLAYER_STATUS_0) {
                    if ($playerLesson->refunded == UNREFUNDED && $playerLesson->refunded !== null) {
                        $playerLesson->status = PLAYER_STATUS_0;
                        $playerLesson->updated_id = $dataPosts['schoolId'];
                        $playerLesson->refund_rate = $dataPosts['percent'];
                        $playerLesson->refund_point = $lesson->payment_fee - (($lesson->payment_fee * $dataPosts['percent']) / 100);
                        $playerLesson->status = LESSON_PLAYER_STATUS_4;
                    }
                }
                if ($lesson->status == LESSON_STATUS_1) {
                    if ($settelement) {
                        $payments = $this->listLessonRepository->getPaymentBySettId($settelement->id);
                        $settelement->status = SETTLEMENT_99;
                        $settelement->updated_id = $dataPosts['schoolId'];
                        if (count($payments)) {
                            foreach ($payments as $payment) {
                                $payment->status = PAYMENT_STATUS_99;
                            }
                        }
                    }
                }

                return $this->listLessonRepository->cancelPlayerLesson($playerLesson, $settelement, $player, $payments);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * cancel player lesson.
     *
     * @return Repository
     */
    public function cancelReject(array $dataPosts)
    {
        $schoolId = Auth::guard('schools')->user()->school_id;
        $playerLesson = $this->listLessonRepository->getPlayerLesson($dataPosts['player_lesson_id']);
        $playerLesson->status = LESSON_PLAYER_STATUS_0;
        $playerLesson->updated_id = $schoolId;

        return $this->listLessonRepository->cancelReject($playerLesson);
    }

    /**
     * get player lesson by id.
     *
     * @return repository
     */
    public function getPlayerLessonById(int $idPlayerLesson)
    {
        $arr = [];
        $arrPlayerLesson = [];
        $playerLesson = $this->listLessonRepository->getPlayerLessonByIdPL($idPlayerLesson);
        $playerLesson['level'] = json_decode($playerLesson['levels']);
        if ($playerLesson['lesson_date'] == LESSON_DATE_1) {
            $playerLesson['times'] = json_decode($playerLesson['times_study']);
            foreach ($playerLesson['times'] as $index => $value) {
                $arr[$index + 1] = $value;
            }
            $playerLesson['classes'] = $this->getClass($arr, $playerLesson['start_time'], $playerLesson['end_time']);
            if (date('Y-m-d') < date('Y-m-d', strtotime($playerLesson['end_time']))) {
                $playerLesson['player_classes'] = $this->getClass($arr, $playerLesson['start_time'], date('Y-m-d'));
            } else {
                $playerLesson['player_classes'] = $this->getClass($arr, $playerLesson['start_time'], $playerLesson['end_time']);
            }
        } elseif ($playerLesson['lesson_date'] == LESSON_DATE_0 && $playerLesson['lesson_date'] !== null) {
            $playerLesson['classes'] = LESSON_DATE_1;
            $cnt = $this->getClass($arr, $playerLesson['start_time'], date('Y-m-d'));
            if ($cnt == LESSON_DATE_0) {
                $playerLesson['player_classes'] = LESSON_DATE_0;
            } else {
                $playerLesson['player_classes'] = LESSON_DATE_1;
            }
        } else {
            $playerLesson['classes'] = LESSON_DATE_0;
            $playerLesson['player_classes'] = LESSON_DATE_0;
        }
        array_push($arrPlayerLesson, $playerLesson);

        return $arrPlayerLesson;
    }

    /**
     * Count new player.
     *
     * @param $search
     *
     * @return Repository
     */
    public function getPlayerStatus($search = null)
    {
        return  $this->listLessonRepository->getNewPlayer($search)->get();
    }
}
