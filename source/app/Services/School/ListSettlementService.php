<?php



namespace App\Services\School;

use App\Services\TennisMgtService;
use Auth;
use App\Repositories\School\ListSettlementRepository;
/**
 * Description of ListSettlementService
 */
class ListSettlementService extends TennisMgtService
{  
    /**
     * @var \App\Repositories\Repository
     */
    protected $listSettlementRepository;
    
    public function __construct(
        ListSettlementRepository $listSettlementRepository
    ) {
        $this->listSettlementRepository = $listSettlementRepository;
    }
    
    /**
     * Get list settlement
     *
     * @param array $search
     * @return Repository
     */
    public function getListSettlement($search = null) 
    {              
        return $this->listSettlementRepository->getListSettlement($search);
    }
    
    public function getAllNameTrainer() 
    {
        $arrTrainer = $this->listSettlementRepository->getTrainerName();
        $arr = [];
        if (count($arrTrainer)) {
            foreach ($arrTrainer as $trainer) {
                $arr[$trainer['id']] = $trainer['name'];
            }
        }
        return $arr;
    }
    
    /**
     * Get all name player
     *
     * @return array $arr
     */
    public function getAllNamePlayer() 
    {
        $arrPlayer= $this->listSettlementRepository->getPlayerName();
        $arr = [];
        if (count($arrPlayer)) {
            foreach ($arrPlayer as $player) {
                $arr[$player['id']] = $player['name'];
            }
        }
        return $arr;
    }
    
    /**
     * Get detail settlement
     *
     * @param int $settlementId
     * @return json $result
     */
    public function getDetailSettlement (int $settlementId) 
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $result['account'] = [];
        $data = $this->listSettlementRepository->getDataSettlementById($settlementId);
        if ($data) {
            $result['settlement'] = $data;
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }
    
    /**
     * Edit settlement
     *
     * @param $dataPost
     * @return json $result
     */
    public function editSettlement ($dataPost, $schoolId) 
    {
        return $this->listSettlementRepository->saveEditSettlement($dataPost, $schoolId);
    }
    
    /**
     * Get settlement by id
     *
     * @param int $settlementId
     * @return Repository
     */
    public function getSettlementById (int $settlementId) 
    {
        return $this->listSettlementRepository->getDataSettlementById($settlementId);
    }
    
    /**
     * Delete settlement by id
     *
     * @param int $settlementId
     * @return Repository
     */
    public function deleteSettlementById (int $settlementId) 
    {
        return $this->listSettlementRepository->saveDeleteSettlement($settlementId);
    }
    
}
