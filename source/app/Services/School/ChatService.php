<?php

namespace App\Services\School;

use App\Models\ChatMessages;
use App\Repositories\School\ChatRoomRepository;
use App\Services\TennisMgtService;
use Illuminate\Support\Carbon;

class ChatService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $chatRoomRepository;
    protected $chatRooms;
    protected $ChatMessages;

    public function __construct(
        ChatRoomRepository $chatRoomRepository
        ) {
        $this->chatRoomRepository = $chatRoomRepository;
    }

    /**
     * send message chat.
     *
     * @return Repository
     */
    public function sendMessageChat(array $dataPosts)
    {
        $arr_participant_id = $dataPosts['participant_id'];
        $dataMess = [];
        foreach ($arr_participant_id as $participant) {
            $playerId = $this->chatRoomRepository->getPlayerIdByParticipantId($participant);
            if ($playerId->lessStt == LESSON_PLAYER_STATUS_1) {
                $newMessage = new ChatMessages();
                $newMessage->chat_room_id = $participant;
                $newMessage->message = $dataPosts['message'];
                $newMessage->from = $dataPosts['schoolId'];
                $newMessage->to = $playerId->player_id;
                $newMessage->info_type = INFO_TYPE_MESSAGE_SCHOOL;
                $newMessage->unread_message = UNREAD_MESSAGE;
                array_push($dataMess, $newMessage);
            }
        }

        return $this->chatRoomRepository->saveMessage($dataMess);
    }

    /**
     * get list Chat Participants.
     *
     * @param array $search
     *
     * @return array $result
     */
    public function getListChatParticipants($search = null)
    {
        $listParticipants = $this->chatRoomRepository->getListChatParticipants($search)->get();
        $arrParticipant = [];
        foreach ($listParticipants as $participant) {
            $participant['status'] = null;
            $latestMesssageOfPlayer = $this->chatRoomRepository->getLatestMesssage($participant->id, INFO_TYPE_MESSAGE_PLAYER)->first();
            $latestMesssageOfSchool = $this->chatRoomRepository->getLatestMesssage($participant->id, INFO_TYPE_MESSAGE_SCHOOL)->first();
            if ($latestMesssageOfSchool) {
                if ($latestMesssageOfPlayer) {
                    if ($latestMesssageOfPlayer->created_at >= $latestMesssageOfSchool->created_at) {
                        if ($latestMesssageOfPlayer->unread_message == UNREAD_MESSAGE) {
                            $participant['status'] = UNREAD_MESSAGE;
                        } else {
                            $participant['status'] = READ_MESSAGE;
                        }
                    } else {
                        $participant['status'] = READ_AND_ANSWER;
                    }
                } else {
                    $participant['status'] = READ_AND_ANSWER;
                }
            }
            array_push($arrParticipant, $participant);
        }

        return $arrParticipant;
    }

    /**
     * Get all lesson.
     *
     * @return array $arr
     */
    public function getAllLesson()
    {
        $arrLesson = $this->chatRoomRepository->getAllLesson();
        $arr = [];
        if (count($arrLesson)) {
            foreach ($arrLesson as $lesson) {
                $arr[$lesson['id']] = $lesson['name'];
            }
        }

        return $arr;
    }

    /**
     * Get all player.
     *
     * @return array $arr
     */
    public function getAllPlayer()
    {
        $arrPlayer = $this->chatRoomRepository->getAllPlayer();
        $arr = [];
        if (count($arrPlayer)) {
            foreach ($arrPlayer as $player) {
                $arr[$player['id']] = $player['name'];
            }
        }

        return $arr;
    }

    /**
     * Get list Message by participant id.
     *
     * @param $participant_id
     *
     * @return array $arrMess
     */
    public function getListMessageByParticipantId($participant_id)
    {
        $arrMess = [];
        $unreadMessage = $this->chatRoomRepository->getUnreadMessageByParticipantId($participant_id, INFO_TYPE_MESSAGE_PLAYER)->get();
        $dataMess = $this->chatRoomRepository->getListMessageByParticipantId($participant_id, $unreadMessage);
        foreach ($dataMess as $mess) {
            $arr = [];
            $timeMessage = date('Y/m/d', strtotime($mess->message_time));
            $disabled = '';
            $time = Carbon::parse($mess->pl_updated_at);
            $limitTime = $time->addHours(72);
            $now = Carbon::now();
            if ($limitTime->lt($now) && $mess->lessonStt == LESSON_PLAYER_STATUS_3 
            || $mess->schoolStt == SCHOOL_STATUS_99 
            || $mess->schoolStt == SCHOOL_STATUS_2 
            || $mess->lessStt == LESSON_STATUS_99 
            || $limitTime->lt($now) && $mess->lessonStt == LESSON_PLAYER_STATUS_4 
            || $limitTime->lt($now) && $mess->is_deleted == DELETED 
            || $mess->lessStt == LESSON_STATUS_2) {
                $disabled = 'disabled';
            }
            foreach ($dataMess as $m) {
                $timeMess = date('Y/m/d', strtotime($m->message_time));
                if ($timeMessage == $timeMess) {
                    $message = [
                        'from' => $m->from,
                        'to' => $m->to,
                        'info_type' => $m->info_type,
                        'message' => $m->message,
                        'player_name' => $m->player_name,
                        'school_name' => $m->school_name,
                        'time' => $m->message_time,
                        'lesson_stt' => $m->lessonStt,
                    ];
                    array_push($arr, $message);
                }
            }
            $arrMess[$timeMessage] = $arr;
            $arrMess['disabled'] = $disabled;
        }

        return $arrMess;
    }

    /**
     * Get school by id.
     *
     * @param $schoolId
     *
     * @return Repository
     */
    public function getSchoolById($schoolId)
    {
        return $this->chatRoomRepository->getSchoolById($schoolId);
    }

    /**
     * Get array player.
     *
     * @param $member_id
     *
     * @return mixed
     */
    public function getArrayPlayer($member_id)
    {
        $arrPlayer = [];
        foreach ($member_id as $member) {
            $player = $this->chatRoomRepository->getPlayerByMemberId($member);
            array_push($arrPlayer, $player);
        }

        return $arrPlayer;
    }

    /**
     * Get chat room by id.
     *
     * @param $chatRoomId
     *
     * @return Repository
     */
    public function getChatRoomById($chatRoomId)
    {
        return $this->chatRoomRepository->getChatRoomById($chatRoomId);
    }
}
