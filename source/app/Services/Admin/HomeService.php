<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Admin\HomeRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;

class HomeService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminHomeRepository;

    public function __construct (HomeRepository $adminHomeRepository)
    {
        $this->adminHomeRepository = $adminHomeRepository;
    }

    /**
     * Count data schools
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataSchools () {
        return $this->adminHomeRepository->countDataOfSchools();
    }

    /**
     * Count data trainers
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataTrainers () {
        return $this->adminHomeRepository->countDataOfTrainers();
    }

    /**
     * Count data players
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataPlayers () {
        return $this->adminHomeRepository->countDataOfPlayers();
    }

    /**
     * Count data lessons
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataLessons () {
        return $this->adminHomeRepository->countDataOfLessons();
    }

    /**
     * Count data matches
     *
     * @return \Illuminate\Http\Response
     */
    public function countDataMatches () {
        return $this->adminHomeRepository->countDataOfMatches();
    }

    /**
     * Get array count lesson in a year
     *
     * @return \Illuminate\Http\Response
     */
    public function dataLessonsInAYear() {
        return $this->adminHomeRepository->getNumbersLessonOneYear();
    }

    /**
     * Get array sum contract fee of school in a year
     *
     * @return \Illuminate\Http\Response
     */
    public function dataFeeSchoolContractInAYear() {
        return $this->adminHomeRepository->getContractFeeSchoolOneYear();
    }

    /**
     * Get array sum contract fee of school in a year
     *
     * @return \Illuminate\Http\Response
     */
    public function dataLessonPointInAYear() {
        return $this->adminHomeRepository->getPointOneYear();
    }
}
