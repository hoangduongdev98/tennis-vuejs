<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Admin\AdminSettingRepository;
use App\Models\SettingSettlementFee;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\MasterLevels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;

class SettingsMasterManagerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminSettingRepository, $level, $settingSettlementFee;

    public function __construct (AdminSettingRepository $adminSettingRepository, MasterLevels $level, SettingSettlementFee $settingSettlementFee)
    {
        $this->adminSettingRepository = $adminSettingRepository;
        $this->level = $level;
        $this->settingSettlementFee = $settingSettlementFee;
    }

    /**
     * Save create master level
     *
     * @param array $dataPosts
     * @return bool
     */
    public function createMasterLevel(array $dataPosts)
    {
        $newLevel = $this->level;
        $newLevel->name = $dataPosts['name'];
        $newLevel->display_order = $dataPosts['order'];
        $newLevel->created_id = Auth::guard('admin')->user()->id;

        return $this->adminSettingRepository->saveCreateMasterLevels($newLevel);
    }

    /**
     * Get lists master level
     *
     * @return repository
     */
    public function getListMasterLevel() {
        return $this->adminSettingRepository->getListMasterLevels();
    }

    /**
     * Find master level by id
     *
     * @param int $idMasterLevel
     * @return \Illuminate\Http\Response
     */
    public function dataMasterLevelById ($idMasterLevel){
        return $this->adminSettingRepository->getMasterLevelById($idMasterLevel);
    }

    /**
     * Ajax get data master level by id
     *
     * @param Illuminate\Http\Request $request
     * @param int $id
     * @return bool
     */
    public function findMasterLevelById(Request $request, $id) {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $result['level'] = [];
        $dataLevel = $this->adminSettingRepository->getMasterLevelById($id);
        if ($dataLevel) {
            if (isset($dataLevel['updated_at'])) {
                $dataLevel['time_updated'] = date('Y/m/d H:i:s', strtotime($dataLevel['updated_at']));
            }
            $result['level'] = $dataLevel;
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Save edit master level
     *
     * @param int $idMasterLevel
     * @param array $dataPosts
     * @return bool
     */
    public function editMasterLevel (int $idMasterLevel, array $dataPosts) {
        $now = date("Y-m-d H:i:s");
        $editMasterLevel = $this->level->find($idMasterLevel);
        $editMasterLevel->name = $dataPosts['name'];
        $editMasterLevel->display_order = $dataPosts['order'];
        $editMasterLevel->updated_id = Auth::guard('admin')->user()->id;
        $editMasterLevel->updated_at = $now;
        return $this->adminSettingRepository->saveEditMasterLevels($editMasterLevel, $idMasterLevel);
    }

    /**
     * Delete master level
     *
     * @param $idMasterLevel
     * @return bool
     */
    public function deleteMasterLevel(int $idMasterLevel) {
        return $this->adminSettingRepository->deleteDataMasterLevel($idMasterLevel, Auth::guard('admin')->user()->id);
    }

    /**
     * Get all data settlement fee related settings
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllSettingSettlementFee() {
        return $this->adminSettingRepository->getSettingSettlementFee();
    }

    /**
     * Save create settlement fee related settings
     *
     * @param array $dataPosts
     * @return bool
     */
    public function saveSettingSettlementAndFee (array $dataPosts) {
        $result = true;
        $now = date("Y-m-d H:i:s");
        $dataPosts['data_setting'] = json_decode($dataPosts['data_setting'], true);
        if (count($dataPosts['data_setting'])) {
            foreach ($dataPosts['data_setting'] as $setting) {
                $setting = (array) $setting;
                $id = $setting['id_setting'];
                if ($id) {
                    $dataSetting = $this->settingSettlementFee->find($id);
                    $dataSetting->updated_id = Auth::guard('admin')->user()->id;
                    $dataSetting->updated_at = $now;
                } else {
                    $dataSetting = new SettingSettlementFee();
                    $dataSetting->created_id = Auth::guard('admin')->user()->id;
                }
                $dataSetting->point = $setting['setting_point'];
                $dataSetting->price = $setting['setting_price'];
                if (!$this->adminSettingRepository->saveSettlementFee($dataSetting)) {
                    $result = false;
                }
            }
        }
        return $result;
    }

    /**
     * Delete settlement fee related settings
     *
     * @param int $id
     * @return bool
     */
    public function deleteSettlementAndFeeById($id) {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $dataSettlementFee = $this->adminSettingRepository->getSettlementFeeById($id);
        if ($dataSettlementFee) {
            $dataSettlementFee->is_deleted = DELETED;
            $dataSettlementFee->deleted_id = Auth::guard('admin')->user()->id;
            if ($dataSettlementFee->save()) {
                $result['msg'] = 'Delete success';
            } else {
                $result['success'] = CODE_AJAX_ERROR;
                $result['msg'] = Config::get('message.common_msg.delete_error');
            }
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = Config::get('message.common_msg.not_isset_id');
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Get max time of updated at
     *
     * @return bool
     */
    public function getTimeUpdatedAt() {
        return $this->adminSettingRepository->getNewUpdatedAtSettlementFee();
    }
}
