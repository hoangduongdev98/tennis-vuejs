<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Admin\AdminAccountRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\AdminLogin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;

class AccountManagerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminAccountRepository, $account;

    public function __construct (AdminAccountRepository $adminAccountRepository, AdminLogin $account)
    {
        $this->adminAccountRepository = $adminAccountRepository;
        $this->account = $account;
    }

    /**
     * Save create account
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createAccount(array $dataPosts)
    {
        $newAccount = $this->account;
        $newAccount->fill($dataPosts);
        $newAccount->user_name = $dataPosts['name'];
        $newAccount->password = Hash::make($dataPosts['password']);
        $newAccount->created_id = Auth::guard('admin')->user()->id;

        return $this->adminAccountRepository->saveCreateAccount($newAccount);
    }

    /**
     * Get list account manager
     *
     * @return Repository
     */
    public function getListAccountManager () {
        return $this->adminAccountRepository->getListAccountManagers();
    }

    /**
     * Get data account by id
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return json
     */
    public function findAccountById($id) {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $result['account'] = [];
        $account = $this->adminAccountRepository->getAccountById($id);
        if ($account) {
            if (isset($account['updated_at'])) {
                $account['time_updated'] = date('Y/m/d H:i:s', strtotime($account['updated_at']));
            }
            $result['account'] = $account;
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Save create account
     *
     * @param int $idAccount
     * @param array $dataPosts
     * @return bool
     */
    public function editAccount (int $idAccount, array $dataPosts) {
        if ($idAccount != $dataPosts['id']) {
            return false;
        }
        $editAccount = $this->account->find($idAccount);
        $editAccount->fill($dataPosts);
        $editAccount->user_name = $dataPosts['name'];
        if ($dataPosts['password'] == PASSWORD) {
            $editAccount->password = $dataPosts['old_password'];
        } else {
            $editAccount->password = Hash::make($dataPosts['password']);
        }
        $editAccount->updated_id = Auth::guard('admin')->user()->id;

        return $this->adminAccountRepository->saveEditAccount($editAccount, $idAccount);
    }

    /**
     * Delete account manager
     *
     * @param int $idAccount
     * @return bool
     */
    public function deleteAccountManager($idAccount) {
        $result = true;
        $account = $this->adminAccountRepository->getAccountById($idAccount);
        if ($account) {
            $account->is_deleted = DELETED;
            $account->deleted_id = Auth::guard('admin')->user()->id;
            if (!$account->save()) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Get data account manager by id
     *
     * @param int $idAccount
     * @return \Illuminate\Http\Response
     */
    public function dataAccountById($idAccount){
        return $this->adminAccountRepository->getAccountById($idAccount);
    }
}
