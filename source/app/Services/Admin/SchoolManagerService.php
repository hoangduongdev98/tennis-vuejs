<?php

namespace App\Services\Admin;

use App\Models\Schools;
use App\Models\Staffs;
use App\Models\Images;
use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Admin\AdminSchoolRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class SchoolManagerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminSchoolRepository;
    protected $school, $staff;

    public function __construct(
        AdminSchoolRepository $adminSchoolRepository,
        Schools $school,
        Staffs $staff,
        Images $image
    )
    {
        $this->adminSchoolRepository = $adminSchoolRepository;
        $this->school = $school;
        $this->staff = $staff;
        $this->image = $image;
    }

    /**
     * Save create school
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createSchool(array $dataPosts)
    {   
        $arrImage = [];
        $created_id = Auth::guard('admin')->user()->id;
        $newSchool = $this->school;
        $newSchool->fill($dataPosts);
        $newSchool->contract_id = $dataPosts['plan'];
        $newSchool->name = $dataPosts['school_name'];
        $newSchool->post_code = trim(str_replace('-','',$dataPosts['post_code']));
        if (!empty($dataPosts['phone'])) {
            $newSchool->phone = trim(str_replace('-','',$dataPosts['phone']));
        }
        if (!empty($dataPosts['fax'])) {
            $newSchool->fax = trim(str_replace('-','',$dataPosts['fax']));
        }
        $newSchool->license_date = date("Y-m-d", strtotime($dataPosts['license_date']));
        $newSchool->created_id = $created_id;

        if ($newSchool) {
            $newStaff = $this->staff;
            $newStaff->fill($dataPosts);
            $newStaff->password = Hash::make($dataPosts['password']);
            $newStaff->login_type = INFO_TYPE_SCHOOL;
            $newStaff->role = ROLE_1;
            $newStaff->created_id = $created_id;
            if ($newStaff) {
                if (!empty($dataPosts['avatarSchool1'])) {
                    $newImage1 = new Images();
                    $this->saveImageForSchool($newImage1, $dataPosts['avatarSchool1'], 1, $created_id);
                    array_push($arrImage, $newImage1);
                }
                if (!empty($dataPosts['avatarSchool2'])) {
                    $newImage2 = new Images();
                    $this->saveImageForSchool($newImage2, $dataPosts['avatarSchool2'], 2, $created_id);
                    array_push($arrImage, $newImage2);
                }
                if (!empty($dataPosts['avatarSchool3'])) {
                    $newImage3 = new Images();
                    $this->saveImageForSchool($newImage3, $dataPosts['avatarSchool3'], 3, $created_id);
                    array_push($arrImage, $newImage3);
                }
            }
        }

        return $this->adminSchoolRepository->saveCreateSchool($newSchool, $newStaff, $arrImage);
    }

    /**
     * Save image for school
     *
     * @param $saveImage
     * @param $name
     * @param int $position
     * @param $created_id
     * @param $updated_id
     * @return $saveImage
     */
    public function saveImageForSchool ($saveImage, $name, $position, $created_id = null, $updated_id = null) {
        $saveImage->name = $name;
        $saveImage->path = AVATAR_SCHOOL_SAVE_JS;
        $saveImage->position = $position;
        $saveImage->info_types = INFO_TYPE_SCHOOL;
        if ($created_id) {
            $saveImage->created_id = $created_id;
        }
        if ($updated_id) {
            $saveImage->updated_id = $updated_id;
        }
    }

    /**
     * Get data school by id
     *
     * @param int $idSchool
     * @return \Illuminate\Http\Response
     */
    public function findSchoolById (int $idSchool) {
        $dataSchool = $this->adminSchoolRepository->getSchoolById($idSchool);
        $arrImage = $this->adminSchoolRepository->findAllImageBySchoolId($idSchool);
        if (count($arrImage)) {
            foreach ($arrImage as $img) {
                $dataSchool['avatarSchool'. $img['position']] = $img['name'];
            }
        }
        return $dataSchool;
    }

    /**
     * Get data of first staff
     *
     * @param int $idSchool
     * @return Repository
     */
    public function getFirstStaffOfSchool (int $idSchool) {
        return $this->adminSchoolRepository->findStaffBySchoolId($idSchool);
    }

    /**
     * Save edit school
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function editSchool (int $idSchool, array $dataPosts) {
        if ($idSchool != $dataPosts['schoolId']) {
            return false;
        }
        $now = date("Y-m-d H:i:s");
        $arrImage = [];
        $updated_id = Auth::guard('admin')->user()->id;
        $editSchool = $this->school->find($idSchool);
        $editSchool->fill($dataPosts);
        $editSchool->contract_id = $dataPosts['plan'];
        $editSchool->name = $dataPosts['school_name'];
        $editSchool->post_code = trim(str_replace('-','',$dataPosts['post_code']));
        if (!empty($dataPosts['phone'])) {
            $editSchool->phone = trim(str_replace('-','',$dataPosts['phone']));
        }
        if (!empty($dataPosts['fax'])) {
            $editSchool->fax = trim(str_replace('-','',$dataPosts['fax']));
        }
        $editSchool->license_date = date("Y-m-d", strtotime($dataPosts['license_date']));
        $editSchool->updated_id = $updated_id;
        $editSchool->updated_at = $now;
        if ($editSchool) {
            $editStaff = $this->adminSchoolRepository->findStaffBySchoolId($idSchool);
            if ($editStaff) {
                $editStaff->fill($dataPosts);
                if ($dataPosts['password'] == PASSWORD) {
                    $editStaff->password = $dataPosts['old_password'];
                } else {
                    $editStaff->password = Hash::make($dataPosts['password']);
                }
                $editStaff->login_type = INFO_TYPE_SCHOOL;
                $editStaff->role = ROLE_1;
                $editStaff->updated_id = $updated_id;
                $editStaff->updated_at = $now;
                //save image
                if ($editStaff) {
                    if (!empty($dataPosts['avatarSchool1'])) {
                        $editImage1 = $this->adminSchoolRepository->findImageBySchool($idSchool, 1);
                        if (!$editImage1) {
                            $editImage1 = new Images();
                        }
                        $this->saveImageForSchool($editImage1, $dataPosts['avatarSchool1'], 1, null, $updated_id);
                        array_push($arrImage, $editImage1);
                    }
                    if (!empty($dataPosts['avatarSchool2'])) {
                        $editImage2 = $this->adminSchoolRepository->findImageBySchool($idSchool, 2);
                        if (!$editImage2) {
                            $editImage2 = new Images();
                        }
                        $this->saveImageForSchool($editImage2, $dataPosts['avatarSchool2'], 2, null, $updated_id);
                        array_push($arrImage, $editImage2);
                    }
                    if (!empty($dataPosts['avatarSchool3'])) {
                        $editImage3 = $this->adminSchoolRepository->findImageBySchool($idSchool, 3);
                        if (!$editImage3) {
                            $editImage3 = new Images();
                        }
                        $this->saveImageForSchool($editImage3, $dataPosts['avatarSchool3'], 3, null, $updated_id);
                        array_push($arrImage, $editImage3);
                    }
                }
            }
        }

        return $this->adminSchoolRepository->saveEditSchool($editSchool, $editStaff, $arrImage, $idSchool);
    }

    /**
     * Get list school
     *
     * @param array $search
     * @return array $result
     */
    public function getListSchool ($search = null) {
        return $this->adminSchoolRepository->getListSchool($search);
    }

    /**
     * Delete school
     *
     * @param int $idSchool
     * @return bool
     */
    public function deleteSchool (int $idSchool) {
        return $this->adminSchoolRepository->deleteDataSchool($idSchool, Auth::guard('admin')->user()->id);
    }

    /**
     * Get lists school
     *
     * @return array $arr
     */
    public function getAllNameSchools() {
        $arr = [];
        $arrSchool = $this->adminSchoolRepository->getAllSchools();
        if (count($arrSchool)) {
            foreach ($arrSchool as $school) {
                $arr[$school['id']] = $school['name'];
            }
        }
        return $arr;
    }

}
