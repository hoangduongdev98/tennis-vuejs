<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Admin\AdminTrainerRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;

class TrainerManagerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminTrainerRepository;

    public function __construct (AdminTrainerRepository $adminTrainerRepository)
    {
        $this->adminTrainerRepository = $adminTrainerRepository;
    }

    /**
     * Get list trainers
     *
     * @param array $search
     * @return \Illuminate\Http\Response
     */
    public function getListTrainer ($search = null) {
        return $this->adminTrainerRepository->getListTrainers($search);
    }

    /**
     * Get lists trainer
     *
     * @return array $arr
     */
    public function getAllNameTrainers() {
        $arr = [];
        $arrTrainer = $this->adminTrainerRepository->getAllTrainers();
        if (count($arrTrainer)) {
            foreach ($arrTrainer as $trainer) {
                $arr[$trainer['id']] = $trainer['name'];
            }
        }
        return $arr;
    }

    /**
     * Find trainer by id
     *
     * @param int $trainerId
     * @return \Illuminate\Http\Response
     */
    public function findTrainerById ($trainerId) {
        return $this->adminTrainerRepository->getTrainerById($trainerId);
    }

    /**
     * Get trainer account logon from admin site by trainer_id
     *
     * @param $trainerId
     * @return Repository
     */
    public function getAccountLogonByTrainerId($trainerId) {
        return $this->adminTrainerRepository->getAccountLogonByTrainerId($trainerId);
    }
}
