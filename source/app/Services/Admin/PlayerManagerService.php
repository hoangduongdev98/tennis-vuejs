<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Admin\AdminPlayerRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;

class PlayerManagerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminPlayerRepository;

    public function __construct (AdminPlayerRepository $adminPlayerRepository)
    {
        $this->adminPlayerRepository = $adminPlayerRepository;
    }

    /**
     * Get lists player
     *
     * @param array $search
     * @return array $result
     */
    public function getListPlayer ($search = null) {
        return $this->adminPlayerRepository->getListPlayers($search);
    }

    /**
     * Get detail player
     *
     * @param int $playerId
     * @return \Illuminate\Http\Response
     */
    public function getDetailPlayerById ($playerId) {
        return $this->adminPlayerRepository->getDetailPlayers($playerId);
    }

    /**
     * Get detail player no lesson
     *
     * @param int $playerId
     * @return \Illuminate\Http\Response
     */
    public function getDetailPlayerNoLessonById ($playerId) {
        return $this->adminPlayerRepository->detailPlayerHasNoLesson($playerId);
    }

    /**
     * Get name players
     *
     * @return array $arr
     */
    public function getAllNamePlayers() {
        $arr = [];
        $arrPlayer = $this->adminPlayerRepository->getAllPlayers();
        if (count($arrPlayer)) {
            foreach ($arrPlayer as $player) {
                $arr[$player['id']] = $player['name'];
            }
        }
        return $arr;
    }

    /**
     * Get player account logon from admin site by player id
     *
     * @param $playerId
     * @return Repository
     */
    public function getAccountLogonByPlayerId($playerId) {
        return $this->adminPlayerRepository->getAccountLogonByPlayerId($playerId);
    }
}
