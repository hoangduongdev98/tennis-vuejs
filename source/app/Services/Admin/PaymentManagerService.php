<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Services\TennisMgtService;
use App\Repositories\Admin\AdminPaymentRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class PaymentManagerService extends TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminPaymentRepository;

    public function __construct (AdminPaymentRepository $adminPaymentRepository)
    {
        $this->adminPaymentRepository = $adminPaymentRepository;
    }

    /**
     * Get list school
     *
     * @param array $searchT
     * @return array $result
     */
    public function getListSettlements ($search = null) {
        return $this->adminPaymentRepository->getListSettlements($search)->sortable(['payment_date' => 'DESC'])->orderBy('id', 'desc');
    }

    /**
     * Get data settlement by id
     *
     * @param int $settlementId
     * @return AdminPaymentRepository
     */
    public function getDetailSettlement (int $settlementId) {
        $result = [];
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $result['account'] = [];
        $data = $this->adminPaymentRepository->getDataSettlementById($settlementId);
        if ($data) {
            $result['settlement'] = $data;
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Edit settlement by id
     *
     * @param int $settlementId
     * @return AdminPaymentRepository
     */
    public function editSettlement ($dataPost) {
        return $this->adminPaymentRepository->saveEditSettlement($dataPost, Auth::guard('admin')->user()->id);
    }

    /**
     * Find settlement by id
     *
     * @param int $settlementId
     * @return \Illuminate\Http\Response
     */
    public function getSettlementById (int $settlementId) {
        return $this->adminPaymentRepository->getDataSettlementById($settlementId);
    }

    /**
     * Delete settlement by id
     *
     * @param int $settlementId
     * @return \Illuminate\Http\Response
     */
    public function deleteSettlementById (int $settlementId) {
        return $this->adminPaymentRepository->saveDeleteSettlement($settlementId);
    }

    /**
     * Edit processing payment
     *
     * @param array $dataPost
     * @return bool
     */
    public function editAprrovePayment($dataPost) {
        $result = true;
        $dataSearch = [];
        if ($arrSchoolId = $dataPost['arr_id_school']) {
            $arrSchoolId = json_decode($arrSchoolId, true);
            foreach ($arrSchoolId as $schoolId => $arrSchoolTimes) {
                if ($arrSchoolTimes) {
                    foreach ($arrSchoolTimes as $schoolTime) {
                        $allSchoolPayments = $this->adminPaymentRepository->getDataPaymentBySchoolId($schoolId, $schoolTime, INFO_TYPE_SCHOOL)->get();
                        if (count($allSchoolPayments)) {
                            foreach ($allSchoolPayments as $schoolPayment) {
                                if (!$this->adminPaymentRepository->editPaymentStatus($schoolPayment['id'], $dataPost['status_approve'], Auth::guard('admin')->user()->id)) {
                                    $result = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get data payment by school id
     *
     * @param int $schoolId
     * @param string $monthYear
     * @return \Illuminate\Http\Response
     */
    public function getPaymentDetailBySchool (int $schoolId, $monthYear = null) {
        return $this->adminPaymentRepository->getDataPaymentBySchoolId($schoolId, $monthYear, INFO_TYPE_SCHOOL);
    }

    /**
     * Get data payment by trainer id
     *
     * @param int $trainerlId
     * @param string $monthYear
     * @return \Illuminate\Http\Response
     */
    public function getPaymentDetailByTrainer (int $trainerlId, $monthYear) {
        return $this->adminPaymentRepository->getDataPaymentByTrainerId($trainerlId, $monthYear);
    }

    /**
     * Get list payments
     *
     * @param array $search
     * @return array $result
     */
    public function listPayments ($search = null) {
        return $this->adminPaymentRepository->getAllDataPayments($search);
    }

    /**
     * Delete payment by id
     *
     * @param int $paymentId
     * @return bool
     */
    public function deletePaymentById (int $paymentId) {
        return $this->adminPaymentRepository->saveDeletePayment($paymentId, Auth::guard('admin')->user()->id);
    }

    /**
     * Get data payment by id
     *
     * @param int $paymentId
     * @return \Illuminate\Http\Response
     */
    public function getPaymentById (int $paymentId) {
        return $this->adminPaymentRepository->findPaymentById($paymentId);
    }

    /**
     * Get data payment by id
     *
     * @param int $paymentId
     * @return \Illuminate\Http\Response
     */
    public function getDetailPayment (int $paymentId) {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $data = $this->adminPaymentRepository->findPaymentById($paymentId);
        if ($data) {
            $result['payment'] = $data;
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Edit payment by id
     *
     * @param int $paymentId
     * @return bool
     */
    public function editPayment ($dataPost) {
        return $this->adminPaymentRepository->saveEditPayment($dataPost, Auth::guard('admin')->user()->id);
    }

    /**
     * Delete payment of the school by month, year
     *
     * @param int $schoolId
     * @param $monthYear
     * @return bool
     */
    public function deletePaymentOfSchool ($schoolId, $monthYear) {
        return $this->adminPaymentRepository->saveDeletePaymentOfSchoolOrTrainer($schoolId, $monthYear, INFO_TYPE_SCHOOL, Auth::guard('admin')->user()->id);
    }

    /**
     * Delete payment of the trainer by month, year
     *
     * @param int $trainerId
     * @param $monthYear
     * @param type delete
     * @return bool
     */
    public function deletePaymentOfTrainer ($trainerId, $monthYear) {
        return $this->adminPaymentRepository->saveDeletePaymentOfSchoolOrTrainer($trainerId, $monthYear, INFO_TYPE_TRAINER, Auth::guard('admin')->user()->id);
    }
    
}
