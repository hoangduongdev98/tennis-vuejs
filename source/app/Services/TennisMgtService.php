<?php

namespace App\Services;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Config;

class TennisMgtService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $tennisMgtRepository;
    protected $school, $staff;

    public function __construct(
        TennisMgtRepository $tennisMgtRepository
    )
    {
        $this->tennisMgtRepository = $tennisMgtRepository;
    }

    /**
     * Get all prefectures
     *
     * @return array data
     */
    public function getAllPrefectures() {
        $arr = [];
        $prefectures = $this->tennisMgtRepository->listPrefectures();
        foreach ($prefectures as $pref) {
            $arr[$pref['id']] = $pref['name'];
        }
        return $arr;
    }
    
    /**
     * Upload img
     *
     * @param $request,
     * @param $img
     * @return json $result
     */
    public function uploadAvatar($request, $img, $store)
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        if ($request->isMethod('post')) {
            $data = $request->all();
            $dirTmp = $store;
            if (!is_dir($dirTmp)) {
                mkdir($dirTmp, 0755, true);
            }
            if ($request->file($img) != null) {
                $file = $request->file($img);
                $originalName = $file->getClientOriginalName();
                $ext = strtolower(pathinfo($originalName, PATHINFO_EXTENSION));
                if (isset($request->validator) && $request->validator->fails()) {
                    $result['success'] = CODE_AJAX_ERROR;
                    $result['msg'] = $request->validator->errors()->first();              
                } elseif (!in_array($ext, Config::get('const.array_extension_file_avatar'))) {
                    $result['success'] = CODE_AJAX_ERROR;
                    $result['msg'] = Config::get('message.common_msg.upload_avatar_file_type');
                } else {                   
                    do {
                        $nameFile = date('YmdHis') . "_" . rand(10000, 99999) . "." . $ext; // create random name for file
                    } while (file_exists($dirTmp . $nameFile));
                    if ($nameFile) {    
                        $file->move($dirTmp, $nameFile);
                        $result['success'] = CODE_AJAX_SUCCESS;
                        $result['fileName'] = $nameFile;
                    } else {
                        $result['success'] = CODE_AJAX_ERROR;
                        $result['msg'] = sprintf(__(Config::get('message.common_msg.upload_max_file_size')), '2mb');
                    }
                }
            }
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }
    
    /**
     * Get all levels
     *
     * @return array data
     */
    public function getAllLevels() {
        $arr = [];
        $levels = $this->tennisMgtRepository->listLevels();
        foreach ($levels as $level) {
            $arr[$level['id']] = $level['name'];
        }
        return $arr;
    }
}
