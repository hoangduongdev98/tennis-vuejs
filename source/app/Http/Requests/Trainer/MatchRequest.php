<?php

namespace App\Http\Requests\Trainer;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

class MatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public $validator = null;

    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'advice_return' => 'max:1000',
            'advice_fore_hand' => 'max:1000',
            'advice_back_hand' => 'max:1000',
            'advice_volley' => 'max:1000',
            'advice_smash' => 'max:1000',
            'advice_miss_return' => 'required_with:advice_return_net|required_with:advice_return_deuce|required_with:advice_return_outside|required_with:advice_return_overout|max:1000',
            'advice_return_net' => 'max:1000',
            'advice_return_deuce' => 'max:1000',
            'advice_return_outside' => 'max:1000',
            'advice_return_overout' => 'max:1000',
            'advice_miss_forehand' => 'required_with:advice_forehand_net|required_with:advice_forehand_deuce|required_with:advice_forehand_outside|required_with:advice_forehand_overout|max:1000',
            'advice_forehand_net' => 'max:1000',
            'advice_forehand_deuce' => 'max:1000',
            'advice_forehand_outside' => 'max:1000',
            'advice_forehand_overout' => 'max:1000',
            'advice_miss_backhand' => 'required_with:advice_backhand_net|required_with:advice_backhand_deuce|required_with:advice_backhand_outside|required_with:advice_backhand_overout|max:1000',
            'advice_backhand_net' => 'max:1000',
            'advice_backhand_deuce' => 'max:1000',
            'advice_backhand_outside' => 'max:1000',
            'advice_backhand_overout' => 'max:1000',
            'advice_miss_volley' => 'required_with:advice_volley_net|required_with:advice_volley_deuce|required_with:advice_volley_outside|required_with:advice_volley_overout|max:1000',
            'advice_volley_net' => 'max:1000',
            'advice_volley_deuce' => 'max:1000',
            'advice_volley_outside' => 'max:1000',
            'advice_volley_overout' => 'max:1000',
            'advice_miss_smash' => 'required_with:advice_smash_net|required_with:advice_smash_deuce|required_with:advice_smash_outside|required_with:advice_smash_overout|max:1000',
            'advice_smash_net' => 'max:1000',
            'advice_smash_deuce' => 'max:1000',
            'advice_smash_outside' => 'max:1000',
            'advice_smash_overout' => 'max:1000',
            'serve_1_net' => 'max:1000',
            'serve_1_overfault' => 'max:1000',
            'serve_1_deuce' => 'max:1000',
            'serve_1_add_side' => 'max:1000',
            'advice_rate_more_1st' => 'max:1000',
            'advice_more_and_less_1st' => 'max:1000',
            'advice_rate_less_1st' => 'max:1000',
            'serve_2_net' => 'max:1000',
            'serve_2_overfault' => 'max:1000',
            'serve_2_deuce' => 'max:1000',
            'serve_2_add_side' => 'max:1000',
            'advice_rate_more_2nd' => 'max:1000',
            'advice_more_and_less_2nd' => 'max:1000',
            'advice_rate_less_2nd' => 'max:1000',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'advice_return.max' => '内容を1000文字以内で入力してください。',
            'advice_fore_hand.max' => '内容を1000文字以内で入力してください。',
            'advice_back_hand.max' => '内容を1000文字以内で入力してください。',
            'advice_volley.max' => '内容を1000文字以内で入力してください。',
            'advice_smash.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_return.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_return.required_with' => 'リターン'.Config::get('message.validation_common_msg.input_err'),
            'advice_return_net.max' => '内容を1000文字以内で入力してください。',
            'advice_return_deuce.max' => '内容を1000文字以内で入力してください。',
            'advice_return_outside.max' => '内容を1000文字以内で入力してください。',
            'advice_return_overout.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_forehand.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_forehand.required_with' => 'フォアハンド'.Config::get('message.validation_common_msg.input_err'),
            'advice_forehand_net.max' => '内容を1000文字以内で入力してください。',
            'advice_forehand_deuce.max' => '内容を1000文字以内で入力してください。',
            'advice_forehand_outside.max' => '内容を1000文字以内で入力してください。',
            'advice_forehand_overout.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_backhand.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_backhand.required_with' => 'バックハンド'.Config::get('message.validation_common_msg.input_err'),
            'advice_backhand_net.max' => '内容を1000文字以内で入力してください。',
            'advice_backhand_deuce.max' => '内容を1000文字以内で入力してください。',
            'advice_backhand_outside.max' => '内容を1000文字以内で入力してください。',
            'advice_backhand_overout.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_volley.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_volley.required_with' => 'ボレー'.Config::get('message.validation_common_msg.input_err'),
            'advice_volley_net.max' => '内容を1000文字以内で入力してください。',
            'advice_volley_deuce.max' => '内容を1000文字以内で入力してください。',
            'advice_volley_outside.max' => '内容を1000文字以内で入力してください。',
            'advice_volley_overout.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_smash.max' => '内容を1000文字以内で入力してください。',
            'advice_miss_smash.required_with' => 'スマッシュ'.Config::get('message.validation_common_msg.input_err'),
            'advice_smash_net.max' => '内容を1000文字以内で入力してください。',
            'advice_smash_deuce.max' => '内容を1000文字以内で入力してください。',
            'advice_smash_outside.max' => '内容を1000文字以内で入力してください。',
            'advice_smash_overout.max' => '内容を1000文字以内で入力してください。',
            'serve_1_net.max' => '内容を1000文字以内で入力してください。',
            'serve_1_overfault.max' => '内容を1000文字以内で入力してください。',
            'serve_1_deuce.max' => '内容を1000文字以内で入力してください。',
            'serve_1_add_side.max' => '内容を1000文字以内で入力してください。',
            'advice_rate_more_1st.max' => '内容を1000文字以内で入力してください。',
            'advice_rate_more_1st.max' => '内容を1000文字以内で入力してください。',
            'advice_more_and_less_1st.max' => '内容を1000文字以内で入力してください。',
            'advice_rate_less_1st.max' => '内容を1000文字以内で入力してください。',
            'serve_2_net.max' => '内容を1000文字以内で入力してください。',
            'serve_2_overfault.max' => '内容を1000文字以内で入力してください。',
            'serve_2_deuce.max' => '内容を1000文字以内で入力してください。',
            'serve_2_add_side.max' => '内容を1000文字以内で入力してください。',
            'advice_rate_more_2nd.max' => '内容を1000文字以内で入力してください。',
            'advice_more_and_less_2nd.max' => '内容を1000文字以内で入力してください。',
            'advice_rate_less_2nd.max' => '内容を1000文字以内で入力してください。',
        ];
    }
}
