<?php

namespace App\Http\Requests\Player;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class PlayerRegisterRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:30',
            'area' => 'required|exists:prefectures,id',
            'email' => 'required|check_email_format|email:rfc,dns|check_unique:players,email|max:256',
            'email_confirm' => 'required|check_email_format|same:email|max:256',
            'password' => 'required|check_password_format|max:64|min:8',
            'password_confirm' => 'required|same:password',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'ユーザー名'. Config::get('message.validation_common_msg.input_err'),
            'name.max'      => 'ユーザー名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "30"),
            'area.required' => 'エリア'. Config::get('message.validation_common_msg.input_err'),
            'area.exists' => Config::get('message.validation_common_msg.data_value_err'),
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => 'メールアドレス'. Config::get('message.validation_common_msg.email_format_err'),
            'email.email' => 'メールアドレス'. Config::get('message.validation_common_msg.email_format_err'),
            'email.check_unique' => 'メールアドレス'. Config::get('message.validation_common_msg.unique_err'),
            'email.max'      => 'メールアドレス' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "256"),
            'email_confirm.required' => sprintf(__(Config::get('message.validation_common_msg.input_confirm_err')),'メールアドレス（確認用）','メールアドレス'),
            'email_confirm.check_email_format' => 'メールアドレス（確認用）'. Config::get('message.validation_common_msg.email_format_err'),
            'email_confirm.same' => sprintf(__(Config::get('message.validation_common_msg.input_confirm_err')),'メールアドレス（確認用）','メールアドレス'),
            'email_confirm.max'      => 'メールアドレス（確認用）' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "256"),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.check_password_format' => 'パスワード'. Config::get('message.validation_common_msg.password_error'),
            'password.max'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "64"),
            'password.min'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "64"),
            'password_confirm.required' => 'パスワード（確認用）'. Config::get('message.validation_common_msg.input_err'),
            'password_confirm.same' => sprintf(__(Config::get('message.validation_common_msg.input_confirm_err')),'パスワード（確認用）','パスワード'),
        ];
    }
}
