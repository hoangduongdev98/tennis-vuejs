<?php

namespace App\Http\Requests\Player;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class PlayerMatchRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'match_name' => 'sometimes|required|max:30',
            'weather' => 'nullable|numeric|between:1,3',
            'wind_direction' => 'nullable|numeric|between:1,8',
            'place' => 'sometimes|required|max:50',
            'coat' => 'sometimes|required|numeric|between:1,5',
            'match_format' => 'sometimes|required|in:1,3,5',
            'advantage' => 'sometimes|required',
            'opponent' => 'sometimes|required|max:30',
            'preferred_hand' => 'sometimes|required',
            'coat_selection' => 'sometimes|required',
            'change_coat' => 'sometimes|required',
            'serve_right' => 'sometimes|required'
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'match_name.required' => '試合名'. Config::get('message.validation_common_msg.input_err'),
            'match_name.max' => '試合名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "30"),
            'weather.numeric' => '天候' . Config::get('message.validation_common_msg.select_err'),
            'weather.between' => '天候' . Config::get('message.validation_common_msg.select_err'),
            'wind_direction.numeric' => '風向き' . Config::get('message.validation_common_msg.select_err'),
            'wind_direction.between' => '風向き' . Config::get('message.validation_common_msg.select_err'),
            'place.required' => '場所' . Config::get('message.validation_common_msg.input_err'),
            'place.max' => '場所' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "50"),
            'coat.required' => 'コート' . Config::get('message.validation_common_msg.select_err'),
            'match_format.required' => '試合形式' . Config::get('message.validation_common_msg.select_err'),
            'advantage.required' => 'アドバンテージ' . Config::get('message.validation_common_msg.input_err'),
            'opponent.required' => '対戦相手' . Config::get('message.validation_common_msg.input_err'),
            'opponent.max' => '対戦相手' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "30"),
            'preferred_hand.required' => '利き手' . Config::get('message.validation_common_msg.input_err'),
            'coat_selection.required' => 'コート選択' . Config::get('message.validation_common_msg.input_err'),
            'change_coat.required' => 'チェンジコート' . Config::get('message.validation_common_msg.input_err'),
            'serve_right.required' => 'サーブ権' . Config::get('message.validation_common_msg.input_err'),
            'weather.between' => Config::get('message.validation_common_msg.data_value_err'),
            'wind_direction.between' => Config::get('message.validation_common_msg.data_value_err'),
            'coat.between' => Config::get('message.validation_common_msg.data_value_err'),
            'match_format.in' => Config::get('message.validation_common_msg.data_value_err')
        ];
    }
}
