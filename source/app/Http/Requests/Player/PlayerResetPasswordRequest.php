<?php

namespace App\Http\Requests\Player;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class PlayerResetPasswordRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|check_email_format|max:256|not_exists:players,email',
            'password' => 'required|check_password_format|max:64|min:8',
            'password_confirm' => 'required|same:password',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.max'      => 'メールアドレス' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "256"),
            'email.not_exists' => 'メールアドレス'. Config::get('message.validation_common_msg.no_exists_err'),
            'password.required' => '新しいパスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.check_password_format' => '新しいパスワード'. Config::get('message.validation_common_msg.password_error'),
            'password.max'      => '新しいパスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "64"),
            'password.min'      => '新しいパスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "64"),
            'password_confirm.required' => '新しいパスワード（確認用）'. Config::get('message.validation_common_msg.input_err'),
            'password_confirm.same' => sprintf(__(Config::get('message.validation_common_msg.input_confirm_err')),'新しいパスワード（確認用）','新しいパスワード'),
        ];
    }
}
