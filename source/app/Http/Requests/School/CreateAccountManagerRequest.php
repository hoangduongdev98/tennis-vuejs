<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class CreateAccountManagerRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'email' => 'required|check_email_format|check_unique:staffs,email|max:256|email:rfc,dns',
            'password' => 'required|check_password_format|between:8,128',
            'role' => 'required',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'アカウント名'. Config::get('message.validation_common_msg.input_err'),
            'name.max'      => 'アカウント名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "20"),
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.check_unique' => 'メールアドレス'. Config::get('message.validation_common_msg.unique_err'),
            'email.max'      => 'メールアドレス' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "256"),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.check_password_format' => 'パスワード'. Config::get('message.validation_common_msg.password_format_err'),
            'password.between'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'role.required' => '権限'. Config::get('message.validation_common_msg.select_err'),
            'email.email' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
        ];
    }

}
