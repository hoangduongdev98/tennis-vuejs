<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class SettingStructureFeeRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($dataPost)
    {
        if (!empty($dataPost['data_setting'])) {
            $data_setting = json_decode($dataPost['data_setting'], true);
            foreach ($data_setting as $index => $setting) {
                if($setting['setting_name'] != null) {
                    $rules['setting_price_' . $index] = 'required';
                    $rules['setting_name_' . $index] = 'max:200';
                } else if ($setting['setting_price'] != null) {
                     $rules['setting_name_' . $index] = 'required';
                     $rules['setting_price_' . $index] = 'nullable|numeric|min:0|check_money';  
                }
                $rules['id_setting'] = 'nullable';
            }
        } else {
            $rules['data_setting'] = 'nullable';
        }
        return $rules;
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages ($dataPost = null)
    {
        if (!empty($dataPost['data_setting'])) {
            $data_setting = json_decode($dataPost['data_setting'], true);
            foreach ($data_setting as $index => $setting) {
                $messages['setting_name_' . $index . '.required'] = '名前と金額 ' . Config::get('message.validation_common_msg.input_err');
                $messages['setting_name_' . $index . '.max'] ='名前と金額'. sprintf(__(Config::get('message.validation_common_msg.max_length_input_number')), "200");
                $messages['setting_price_' . $index . '.required'] = '名前と金額 ' . Config::get('message.validation_common_msg.input_err');
                $messages['setting_price_' . $index . '.numeric'] = '名前と金額 ' . Config::get('message.validation_common_msg.number_err');
                $messages['setting_price_' . $index . '.check_money'] =  Config::get('message.validation_common_msg.check_price');
                $messages['setting_price_' . $index . '.min'] = sprintf(__(Config::get('message.validation_common_msg.value_min_0')), "名前と金額 ");
            }
        } else {
            $messages['data_setting.nullable'] = '';
        }
        return $messages;
    }

}
