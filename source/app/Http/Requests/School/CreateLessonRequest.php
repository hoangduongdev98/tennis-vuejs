<?php

namespace App\Http\Requests\School;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;

/**
 * Description of CreateLessonRequest.
 */
class CreateLessonRequest extends FormRequest
{
    public $validator = null;

    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    public function rules()
    {
        $rules = [
                'name' => 'required|max:50',
                'category' => 'required',
                'register_expire_date' => 'nullable|date_format:Y/m/d',
                'applicans' => 'required|numeric|between:0,99|integer',
                'point' => 'required|max:20|check_money',
                'detail' => 'nullable|max:3000',
                'date_lesson' => 'nullable|required_with:lesson_start_time,!=,null|required_with:lesson_end_time,!=,null',
                'lesson_start_time' => 'nullable|required_with:lesson_end_time,!=,null|required_with:date_lesson,!=,null|date_format:H:i',
                'lesson_end_time' => 'nullable|required_with:lesson_start_time,!=,null|required_with:date_lesson,!=,null|date_format:H:i',
            ];
        $idLesson = $this->request->get('id');
        if ($idLesson !== null) {
            unset($rules['point']);
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'レッスン名'.Config::get('message.validation_common_msg.input_err'),
            'name.max' => 'レッスン名'.sprintf(__(Config::get('message.validation_common_msg.max_length_err')), '50'),
            'category.required' => 'レッスンカテゴリー'.Config::get('message.validation_common_msg.select_err'),
            'category.exists' => Config::get('message.validation_common_msg.data_value_err'),
            'register_expire_date.date_format' => '掲載終了日'.Config::get('message.validation_common_msg.format_err'),
            'register_expire_date.required' => '掲載終了日'.Config::get('message.validation_common_msg.input_err'),
            'date_lesson.required' => '日付・時間入力'.Config::get('message.validation_common_msg.input_err'),
            'lesson_start_time.required' => '日付・時間入力'.Config::get('message.validation_common_msg.input_err'),
            'lesson_end_time.required' => '日付・時間入力'.Config::get('message.validation_common_msg.input_err'),
            'date_lesson.required_with' => '日付・時間入力'.Config::get('message.validation_common_msg.input_err'),
            'lesson_start_time.required_with' => '日付・時間入力'.Config::get('message.validation_common_msg.input_err'),
            'lesson_end_time.required_with' => '日付・時間入力'.Config::get('message.validation_common_msg.input_err'),
            'lesson_start_time.date_format' => '日付・時間入力'.Config::get('message.validation_common_msg.format_err'),
            'lesson_end_time.date_format' => '日付・時間入力'.Config::get('message.validation_common_msg.format_err'),
            'applicans.required' => '募集人数'.Config::get('message.validation_common_msg.select_err'),
            'applicans.numeric' => '募集人数'.Config::get('message.validation_common_msg.number_err'),
            'applicans.between' => Config::get('message.validation_common_msg.data_value_err'),
            'applicans.integer' => Config::get('message.validation_common_msg.data_value_err'),
            'point.required' => 'ポイント単価'.Config::get('message.validation_common_msg.input_err'),
            'point.check_money' => Config::get('message.validation_common_msg.check_point'),
            'point.max' => 'ポイント単価'.sprintf(__(Config::get('message.validation_common_msg.max_length_err')), '20'),
            'detail.max' => 'レッスンの詳細'.sprintf(__(Config::get('message.validation_common_msg.max_length_err')), '3000'),
            'detail.required' => 'レッスンの詳細'.Config::get('message.validation_common_msg.input_err'),
        ];
    }
}
