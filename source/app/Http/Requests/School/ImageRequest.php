<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class ImageRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar1' => 'nullable|image|mimes:jpeg,jpg,png|mimetypes:image/jpeg,image/png|max:2048',
            'avatar2' => 'nullable|image|mimes:jpeg,jpg,png|mimetypes:image/jpeg,image/png|max:2048',
            'avatar3' => 'nullable|image|mimes:jpeg,jpg,png|mimetypes:image/jpeg,image/png|max:2048',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'avatar1.image' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar1.mimes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar1.mimetypes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar1.max'      => sprintf(__(Config::get('message.common_msg.upload_max_file_size')), "2mb"),
            'avatar2.image' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar2.mimes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar2.mimetypes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar2.max'      => sprintf(__(Config::get('message.common_msg.upload_max_file_size')), "2mb"),
            'avatar3.image' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar3.mimes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar3.mimetypes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatar3.max'      => sprintf(__(Config::get('message.common_msg.upload_max_file_size')), "2mb"),
        ];
    }

}

