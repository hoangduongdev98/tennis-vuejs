<?php


namespace App\Http\Requests\School;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
/**
 * Description of CreateTrainer
 *
 */
class CreateTrainerRequest extends FormRequest
{
    
    public $validator = null;
    
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }
    
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'trainer_name' => 'required|max:50',
            'post_code' => 'required|check_post_code_format|max:8',
            'address' => 'required|max:200',
            'email' => 'required|check_email_format|check_unique:trainers,email|max:256|email:rfc,dns',
            'password' => 'required|check_password_format|between:8,128',
            'license_date' => 'required|date_format:Y/m/d',
            'status' => 'required',
            'phone' => 'check_tel_and_fax',
            'fax' => 'check_tel_and_fax', 
            'account_holder' => 'nullable|required_with:account_number,!=,null|required_with:bank_name,!=,null|required_with:branch_bank,!=,null|max:100',
            'account_number' => 'nullable|digits:7|required_with:account_holder,!=,null|required_with:bank_name,!=,null|required_with:branch_bank,!=,null',
            'bank_name' => 'nullable|required_with:account_holder,!=,null|required_with:account_number,!=,null|required_with:branch_bank,!=,null|max:30',
            'branch_bank' => 'nullable|required_with:account_holder,!=,null|required_with:account_number,!=,null|required_with:bank_name,!=,null|max:30',
            'rate' => 'nullable|max:100|numeric|min:0|check_ratio',            
        ];
    }
    
    public function messages()
    {
        return [
            'trainer_name.required' => 'コーチ名'. Config::get('message.validation_common_msg.input_err'),
            'trainer_name.max' => 'コーチ名'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')),'50'),
            'post_code.required' => '郵便番号'. Config::get('message.validation_common_msg.input_err'),
            'post_code.max' => '郵便番号'. sprintf(__(Config::get('message.validation_common_msg.max_length_number')),'7'),
            'post_code.check_post_code_format' => '郵便番号'.Config::get('message.validation_common_msg.post_code_err'),
            'address.required' => '住所'. Config::get('message.validation_common_msg.input_err'),
            'address.max' => '住所'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')),'200'),
            'license_date.required' => 'サービス開始日'. Config::get('message.validation_common_msg.input_err'),
            'license_date.date_format' => 'サービス開始日'. Config::get('message.validation_common_msg.format_err'),
            'status.required' => 'ステータス'. Config::get('message.validation_common_msg.select_required'),
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.check_unique' => 'メールアドレス'. Config::get('message.validation_common_msg.unique_err'),
            'email.max' => 'メールアドレス'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')),'256'),
            'email.email' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.between'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')),'8','128'),
            'password.check_password_format' => 'パスワード'. Config::get('message.validation_common_msg.password_format_err'),
            'rate.numeric' => Config::get('message.validation_common_msg.number_err'),
            'rate.check_ratio' => Config::get('message.validation_common_msg.check_ratio'),
            'rate.min' => sprintf(__(Config::get('message.validation_common_msg.value_min_0')), "コミッション料率"),
            'rate.max' => sprintf(__(Config::get('message.validation_common_msg.value_max')), "コミッション料率", "100"),
            'phone.check_tel_and_fax' => '電話番号を' . sprintf(__(Config::get('message.validation_common_msg.digits_between_error')), "10", '11'),
            'fax.check_tel_and_fax' => 'ＦＡＸを' . sprintf(__(Config::get('message.validation_common_msg.digits_between_error')), "10", '11'),
            'account_holder.max' => '名義'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "100"),
            'account_holder.required_with' => '名義'. Config::get('message.validation_common_msg.input_err'),
            'account_number.digits' => '口座番号'. sprintf(__(Config::get('message.validation_common_msg.max_length_input_number')), "7"),
            'account_number.required_with' => '口座番号'. Config::get('message.validation_common_msg.input_err'),
            'bank_name.max' => '銀行名'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "30"),
            'bank_name.required_with' => '銀行名'. Config::get('message.validation_common_msg.input_err'),
            'branch_bank.max' => '支店名'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "30"),
            'branch_bank.required_with' => '支店名'. Config::get('message.validation_common_msg.input_err')
        ];
    }
}
