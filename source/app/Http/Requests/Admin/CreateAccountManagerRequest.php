<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class CreateAccountManagerRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'email' => 'required|check_email_format|email:rfc,dns|check_unique:admin_login,email',
            'password' => 'required|check_password_format|max:128|min:8',
            'role' => 'required',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'アカウント名'. Config::get('message.validation_common_msg.input_err'),
            'name.max'      => 'アカウント名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "20"),
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.email' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.check_unique' => 'メールアドレス'. Config::get('message.validation_common_msg.unique_err'),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.check_password_format' => 'パスワード'. Config::get('message.validation_common_msg.password_error'),
            'password.max'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'password.min'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'role.required' => '権限'. Config::get('message.validation_common_msg.select_err'),
        ];
    }

}
