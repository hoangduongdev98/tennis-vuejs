<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;

class AuthForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|check_email_format|not_exists:admin_login,email',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.not_exists' => 'メールアドレス'. Config::get('message.validation_common_msg.no_exists_err'),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password_confirm.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password_confirm.same' => sprintf(__(Config::get('message.validation_common_msg.input_confirm_err')),'確認パスワード','パスワード'),


        ];
    }
}
