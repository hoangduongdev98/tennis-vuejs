<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class CreateSchoolRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_name' => 'required|max:100',
            'representative' => 'required|max:20',
            'address' => 'required|max:200',
            'post_code' => 'required|check_post_code_format',
            'school_manager' => 'required|max:20',
            'email' => 'required|check_email_format|email:rfc,dns|check_unique:staffs,email',
            'password' => 'required|check_password_format|max:128|min:8',
            'license_date' => 'required|date|date_format:Y/m/d',
            'status' => 'required',
            'plan' => 'required',
            'ratio' => 'nullable|numeric|min:0|max:100|check_ratio',
            'phone' => 'check_tel_and_fax',
            'fax' => 'check_tel_and_fax',
            'account_type' => 'nullable|required_with:account_holder,!=,null|required_with:account_number,!=,null|required_with:bank_name,!=,null|required_with:branch_bank,!=,null',
            'account_holder' => 'nullable|required_with:account_number,!=,null|required_with:bank_name,!=,null|required_with:branch_bank,!=,null|max:100',
            'account_number' => 'nullable|digits:7|required_with:account_holder,!=,null|required_with:bank_name,!=,null|required_with:branch_bank,!=,null',
            'bank_name' => 'nullable|required_with:account_holder,!=,null|required_with:account_number,!=,null|required_with:branch_bank,!=,null|max:30',
            'branch_bank' => 'nullable|required_with:account_holder,!=,null|required_with:account_number,!=,null|required_with:bank_name,!=,null|max:30',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'school_name.required' => 'スクール名'. Config::get('message.validation_common_msg.input_err'),
            'school_name.max'      => 'スクール名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "100"),
            'representative.required' => '代表者名'. Config::get('message.validation_common_msg.input_err'),
            'representative.max'      => '代表者名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "20"),
            'address.required' => '住所'. Config::get('message.validation_common_msg.input_err'),
            'address.max'      => '住所' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "200"),
            'post_code.required' => '郵便番号'. Config::get('message.validation_common_msg.input_err'),
            'post_code.check_post_code_format' => '郵便番号'.Config::get('message.validation_common_msg.post_code_err'),
            'school_manager.required' => '担当者名'. Config::get('message.validation_common_msg.input_err'),
            'school_manager.max'      => '担当者名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "20"),
            'email.required' => '担当者メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => '担当者メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.email' => '担当者メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.check_unique' => '担当者メールアドレス'. Config::get('message.validation_common_msg.unique_err'),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.check_password_format' => 'パスワード'. Config::get('message.validation_common_msg.password_format_err'),
            'password.max'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'password.min'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'license_date.required' => 'サービス開始日'. Config::get('message.validation_common_msg.input_err'),
            'license_date.date' => 'サービス開始日'. Config::get('message.validation_common_msg.format_err'),
            'license_date.date_format' => 'サービス開始日'. Config::get('message.validation_common_msg.format_err'),
            'status.required' => 'ステータス'. Config::get('message.validation_common_msg.select_err'),
            'plan.required' => '契約プラン'. Config::get('message.validation_common_msg.select_err'),
            'ratio.numeric' => Config::get('message.validation_common_msg.number_err'),
            'ratio.min' => sprintf(__(Config::get('message.validation_common_msg.value_min_0')), "コミッション料率"),
            'ratio.max' => sprintf(__(Config::get('message.validation_common_msg.value_max')), "コミッション料率", "100"),
            'ratio.check_ratio' =>  Config::get('message.validation_common_msg.check_ratio'),
            'phone.check_tel_and_fax' => '電話番号を' . sprintf(__(Config::get('message.validation_common_msg.digits_between_error')), "10", '11'),
            'fax.check_tel_and_fax' => 'ＦＡＸを' . sprintf(__(Config::get('message.validation_common_msg.digits_between_error')), "10", '11'),
            'account_type.required_with' => '口座の種類'. Config::get('message.validation_common_msg.input_err'),
            'account_holder.max' => '名義'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "100"),
            'account_holder.required_with' => '名義'. Config::get('message.validation_common_msg.input_err'),
            'account_number.digits' => '口座番号'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "7"),
            'account_number.required_with' => '口座番号'. Config::get('message.validation_common_msg.input_err'),
            'bank_name.max' => '銀行名'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "30"),
            'bank_name.required_with' => '銀行名'. Config::get('message.validation_common_msg.input_err'),
            'branch_bank.max' => '支店名'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "30"),
            'branch_bank.required_with' => '支店名'. Config::get('message.validation_common_msg.input_err'),

        ];
    }

}
