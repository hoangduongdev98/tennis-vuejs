<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class CreateMasterLevelRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'order' => 'nullable|numeric|max:99',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'レベル名'. Config::get('message.validation_common_msg.input_err'),
            'name.max' => 'レベル名'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "50"),
            'order.numeric' => Config::get('message.validation_common_msg.number_err'),
            'order.max' => '表示順'. sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "2"),
        ];
    }

}
