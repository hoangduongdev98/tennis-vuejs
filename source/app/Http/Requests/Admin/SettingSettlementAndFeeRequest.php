<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class SettingSettlementAndFeeRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($dataPost)
    {
        if (!empty($dataPost['data_setting'])) {
            $data_setting = json_decode($dataPost['data_setting'], true);
            foreach ($data_setting as $index => $setting) {
                $rules['setting_point_' . $index] = 'required|numeric|min:0|check_money';
                $rules['setting_price_' . $index] = 'required|numeric|min:0|check_money';
            }
        } else {
            $rules['data_setting'] = 'required';
        }
        return $rules;
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages ($dataPost = null)
    {
        if (!empty($dataPost['data_setting'])) {
            $data_setting = json_decode($dataPost['data_setting'], true);
            foreach ($data_setting as $index => $setting) {
                $messages['setting_point_' . $index . '.required'] = 'ポイントと金額' . Config::get('message.validation_common_msg.input_err');
                $messages['setting_point_' . $index . '.check_money'] =  Config::get('message.validation_common_msg.check_point');
                $messages['setting_point_' . $index . '.numeric'] = 'ポイントと金額' . Config::get('message.validation_common_msg.number_err');
                $messages['setting_point_' . $index . '.min'] = sprintf(__(Config::get('message.validation_common_msg.value_min_0')), "ポイントや金額");
                $messages['setting_price_' . $index . '.required'] = 'ポイントと金額' . Config::get('message.validation_common_msg.input_err');
                $messages['setting_price_' . $index . '.check_money'] = Config::get('message.validation_common_msg.check_price');
                $messages['setting_price_' . $index . '.numeric'] = 'ポイントと金額' . Config::get('message.validation_common_msg.number_err');
                $messages['setting_price_' . $index . '.min'] = sprintf(__(Config::get('message.validation_common_msg.value_min_0')), "ポイントや金額");
            }
        } else {
            $messages['data_setting.required'] = '決済・手数料関係設定つ登録をする必要があります。';
        }
        return $messages;
    }

}
