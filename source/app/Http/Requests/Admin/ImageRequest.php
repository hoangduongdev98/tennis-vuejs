<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class ImageRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatarSchool1' => 'nullable|image|mimes:jpeg,jpg,png|mimetypes:image/jpeg,image/png|max:2048',
            'avatarSchool2' => 'nullable|image|mimes:jpeg,jpg,png|mimetypes:image/jpeg,image/png|max:2048',
            'avatarSchool3' => 'nullable|image|mimes:jpeg,jpg,png|mimetypes:image/jpeg,image/png|max:2048',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'avatarSchool1.image' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool1.mimes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool1.mimetypes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool1.max'      => sprintf(__(Config::get('message.common_msg.upload_max_file_size')), "2mb"),
            'avatarSchool2.image' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool2.mimes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool2.mimetypes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool2.max'      => sprintf(__(Config::get('message.common_msg.upload_max_file_size')), "2mb"),
            'avatarSchool3.image' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool3.mimes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool3.mimetypes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'avatarSchool3.max'      => sprintf(__(Config::get('message.common_msg.upload_max_file_size')), "2mb"),
        ];
    }

}

