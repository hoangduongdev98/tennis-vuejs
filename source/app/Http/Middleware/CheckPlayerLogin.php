<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

class CheckPlayerLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::guard('players')->check()) {
            $infoPlayer = Auth::guard('players')->user();
            if ($infoPlayer['is_deleted'] == DELETED) {
                Auth::guard('players')->logout();
                return redirect()->route('player.index');
            }
            return $next($request);
        } else {
            return redirect()->route('player.login');
        }
    }
}
