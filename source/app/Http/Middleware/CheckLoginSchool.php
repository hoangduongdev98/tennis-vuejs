<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class CheckLoginSchool
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::guard('schools')->check()) {
            $infoAdmin = Auth::guard('schools')->user();
            $school = get_school_by_staff($infoAdmin->id);
            if ($infoAdmin['is_deleted'] == DELETED || $school['is_deleted'] == DELETED || $school['status'] != SCHOOL_STATUS_1) {
                Auth::guard('schools')->logout();
                return redirect(url()->previous())->with(ERROR, config('message.common_msg.login_err'));
            }
            return $next($request);
        } else {
            return redirect()->route('school.login');

        }

    }
}

