<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Services\Trainer\PlayerLessonService;
use App\Services\Trainer\PlayerService;
use Carbon\Carbon;
use App\CSVDefine\SetHeader;

class PlayerController extends Controller
{

    /**
     * @var \App\Repositories\PlayerRepository
     */
    protected $playerService, $setHeader;

    public function __construct(PlayerService $playerService, SetHeader $setHeader)
    {
        $this->playerService = $playerService;
        $this->setHeader = $setHeader;
        $this->middleware('CheckLoginTrainer');
    }


    /**
     * get list booking.
     *
     * @param $request
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $params = $request->all();
            $playerList = $this->playerService->getListPlayer($params);

            return response()->json($playerList);
        }
        $status = Config::get('const.lesson_player_status');

        return view("trainer.player.list", compact('status'));
    }

    /**
     * Export CSV Player
     *
     * @return \Illuminate\Http\Response
     */
    public function playerExport(Request $request)
    {
        $dataSearch = $request->all();
        $playerList = $this->playerService->getListPlayer($dataSearch);
        $fileName = "PlayerList_" . Carbon::now()->format('Ymdhis') . '.csv';
        $headers = $this->setHeader->SetHeaderCSV($fileName);
        $columns = array('最終アクセス日', 'プレーヤー名', '受講しているレッスン', '試合解析登録数', 'ステータス');
        $callback = function () use ($playerList, $columns) {
            $file = fopen('php://output', 'w');
            fputs($file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
            fputcsv($file, $columns);
            foreach ($playerList as $player) {
                $lessonName = '';
                $status = '';
                $date=date_create($player->last_login);
                $lastLogin= date_format($date,"Y/m/d H:i:s");
                if (isset($player['lessonName'])) {
                    for ($i = 0; $i < count($player['lessonName']); $i++) {
                        $lessonName .= $player['lessonName'][$i] . "\n";
                        $status .= Config::get('const.lesson_player_status.' . $player['plStatus'][$i]) . "\n";
                    }
                } else {
                    $status = '問い合わせ';
                }
                $line = [
                    $lastLogin,
                    $player->name,
                    $lessonName,
                    $player->totalMatches,
                    $status,
                ];
                fputcsv($file, $line);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
    /**
     * Go to player page
     *
     * @param $playerId
     * @return \Illuminate\View\View
     */
    public function playerPage(Request $request, $playerId)
    {
        $trainer = Auth::guard('trainers')->user();
        $playerAccount = $this->playerService->getAccountLogonByPlayerId($playerId);
        if (!$playerAccount) {
            return redirect()->route('trainer.players')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        if (Auth::guard('players')->check()) {
            Auth::guard('players')->logout();
        }
        if (session()->has('name_admin_login')) {
            $request->session()->forget('name_admin_login');
        }
        if (session()->has('name_school_login')) {
            $request->session()->forget('name_school_login');
        }
        $request->session()->put('name_trainer_login', $trainer->name);
        $request->session()->put('type_login.id_login', $trainer->id);
        Auth::guard('players')->login($playerAccount);
        return redirect()->route('player.index');
    }
    
    /**
     * Match analysis shared player
     * @param Request $request
     * @return type
     */
    public function listShare(Request $request)
    {
        $school = getSchoolByTrainerId(Auth::guard('trainers')->user()->id);
        if ($school && ($school->contract_id == 0 || $school->contract_id == 3)) {
            return redirect()->route('trainer.index');
        }
        $limitPlayer = LIMIT_20;
        $shareStatus = Config::get('const.share_status');
        $dataSearch = [
            'search_startDate' => null,
            'search_endDate' => null,
            'search_name' => null,
            'search_status' => null
        ];
        try {
            if ($request->isMethod('post')) {
                $dataSearch = $request->all();
                $request->session()->put('search_shared_player', $dataSearch);
            } elseif (strpos($request->getRequestUri(), "search") !== false) {
                $dataSearch = $request->session()->get('search_shared_player');
            } else {
                $request->session()->forget('search_shared_player');
            }
            if ($request->query('limit')) {
                if (array_key_exists($request->query('limit'), Config::get('const.limit_item'))) {
                    $limitPlayer = $request->query('limit');
                }
            }
            $listShare = $this->playerService->getListSharedPlayer($dataSearch)->paginate($limitPlayer);
        } catch (\Kyslik\ColumnSortable\Exceptions\ColumnSortableException $e) {
            dd($e);
        }
        
        return view("trainer.player.shared_player_list", compact("listShare", "dataSearch", "shareStatus"));
    }
    
    /**
     * Export csv players shared lists
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function exportSharedPlayer(Request $request)
    {
        $dataSearch = $request->all();
        $listPlayers = $this->playerService->getListSharedPlayer($dataSearch);
        $fileName = "PlayerList_" . Carbon::now()->format('Ymd') . '.csv';
        $headers = $this->setHeader->SetHeaderCSV($fileName);
        $columns = array('最終アクセス日', 'プレーヤー名', '試合解析登録数', 'ステータス');
        $callback = function() use($listPlayers, $columns) {
            $file = fopen('php://output', 'w');
            fputs($file, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
            fputcsv($file, $columns);
            foreach ($listPlayers as $player) {
                $line = [
                        $player->player_last_login,
                        $player->player_name,
                        $player->total_matches,
                        Config::get('const.share_status')[$player->status],
                ];
                fputcsv($file, $line);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
    
    /**
     * update status share
     * @param Request $request
     */
    public function acceptShare(Request $request)
    {
        $dataPosts = $request->all();
        $data = $this->playerService->acceptShare($dataPosts);
        if ($data) {
            $msg = '解析情報' . Config::get('message.common_msg.update_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.edit_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');

        return redirect()->route('trainer.listShare')->with($result, $msg);
    }
    
    /**
     * delete share
     * @param Request $request
     * @return type
     */
    public function deleteShare (Request $request)
    {
        $dataPosts = $request->all();
        $data = $this->playerService->deleteShare($dataPosts);
        if ($data) {
            $msg = '解析情報' . Config::get('message.common_msg.delete_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.delete_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');

        return redirect()->route('trainer.listShare')->with($result, $msg);
    }
    
    /**
     * Go to player page
     *
     * @param $playerId
     * @return \Illuminate\View\View
     */
    public function playerMatchPage(Request $request, $playerId)
    {
        $trainer = Auth::guard('trainers')->user();
        $playerAccount = $this->playerService->getAccountSharedPlayer($trainer->id, $playerId);
        if (!$playerAccount) {
            return redirect()->route('trainer.players')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        if (Auth::guard('players')->check()) {
            Auth::guard('players')->logout();
        }
        if (session()->has('name_admin_login')) {
            $request->session()->forget('name_admin_login');
        }
        if (session()->has('name_school_login')) {
            $request->session()->forget('name_school_login');
        }
        $request->session()->put('name_trainer_login', $trainer->name);
        $request->session()->put('type_login.id_login', $trainer->id);
        Auth::guard('players')->login($playerAccount);
        return redirect()->route('player.matchListHistory');
    }
}
