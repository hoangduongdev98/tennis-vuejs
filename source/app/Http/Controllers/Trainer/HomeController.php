<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\Trainer\HomeService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var \App\Services\Service
     */
    protected $homeService;

    public function __construct(HomeService $homeService)
    {
        $this->homeService = $homeService;
        $this->middleware('CheckLoginTrainer');
    }

    /**
     * Show dashboard
     * 
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $countLessons = $this->homeService->countDataLessons();
        $countMatches = $this->homeService->countDataMatches();
        $countPlayers = $this->homeService->countDataPlayers();
        $dataLessonsForChart = $this->homeService->dataLessonsInAYear();
        return view('trainer.index', compact(
            'countLessons',
            'dataLessonsForChart',
            'countPlayers',
            'countMatches',
        ));
    }

    public function guard()
    {
        return Auth::guard('trainers');
    }
}
