<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Trainer\MatchRequest;
use App\Services\Trainer\MatchService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class MatchController extends Controller
{
    protected $matchService;

    public function __construct(MatchService $matchService)
    {
        $this->middleware('CheckLoginTrainer');
        $this->matchService = $matchService;
    }

    public function index(Request $request)
    {
        $created = Auth::guard('trainers')->user();
        $school = getSchoolByTrainerId($created->id);
        if ($school->contract_id == 0 || $school->contract_id == 1 || $school->contract_id == 3) {
            return redirect()->route('trainer.index');
        }
        $winnerSetting = $this->matchService->getWinnerSetting($created->id);
        $missSetting = $this->matchService->getMissSetting($created->id);
        $adviceReturn = $this->matchService->getAdviceReturn($created->id);
        $adviceForehand = $this->matchService->getAdviceForeHand($created->id);
        $adviceBackhand = $this->matchService->getAdviceBackHand($created->id);
        $adviceVolley = $this->matchService->getAdviceVolley($created->id);
        $adviceSmash = $this->matchService->getAdviceSmash($created->id);
        $serve1stSetting = $this->matchService->getServe1st($created->id);
        $serve2ndSetting = $this->matchService->getServe2nd($created->id);
        if ($key = $request->input('key')) {
            $data = $request->session()->get('confirm_data'.$key);
            if ($data) {
                return view('trainer.match-analysis.match_analysis', compact('data'));
            }
        }

        return view('trainer.match-analysis.match_analysis', compact('winnerSetting', 'missSetting', 'adviceReturn', 'adviceForehand', 'adviceBackhand', 'adviceVolley', 'adviceSmash', 'serve1stSetting', 'serve2ndSetting'));
    }

    /**
     * Post analyst.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postAnalyst(MatchRequest $request)
    {
        $dataPost = $request->all();
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->with(['data' => $dataPost])->withErrors($request->validator);
        } else {
            (isset($dataPost['key']) && $dataPost['key']) ? $key = $dataPost['key'] : $key = Str::random(32);
            $request->session()->put('confirm_data'.$key, $dataPost);

            return redirect()->route('trainer.confirmAnalyst', ['key' => $key]);
        }
    }

    /**
     * Show view confirm create analyst.
     *
     * @param $key
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmAnalyst(Request $request, $key)
    {
        $dataPost = $request->session()->get('confirm_data'.$key);
        if ($dataPost) {
            return view('trainer.match-analysis.match_analysis_confirm', compact('dataPost', 'key'));
        } else {
            return redirect()->route('trainer.match', compact('dataPost'));
        }
    }

    /**
     * Save create lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveAnalyst(Request $request)
    {
        $msg = '';
        $result = SUCCESS;
        $key = $request->input('key');
        $dataPosts = $request->session()->get('confirm_data'.$key);
        if (!$dataPosts) {
            return redirect()->route('trainer.index')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        $createLesson = $this->matchService->saveMatchAnalysis($dataPosts);
        if ($createLesson) {
            $msg = 'Match'.Config::get('message.common_msg.insert_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.create_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');

        return redirect()->route('trainer.match')->with($result, $msg);
    }
}
