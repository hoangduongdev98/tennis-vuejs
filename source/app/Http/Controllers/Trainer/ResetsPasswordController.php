<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Trainer;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;


/**
 * Description of ResetsPasswordController
 *
 * @author ADMIN
 */
class ResetsPasswordController extends Controller
{

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     * @var string
     */
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest:trainers');
    }
    /**
     * Create a new controller instance.
     * @return void
     */
    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->email;
        // if($this->guard()->check()) {
        //     $email = $this->guard()->user()->email;
        // }

        return view('trainer.auth.reset_password', [
            'token' => $token,
            'email' => $email
        ]);
    }

    protected function guard()
    {
        return Auth::guard('trainers');
    }

    protected function rules()
    {
        return [
            'email' => 'required',
            'token'    => 'required',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password',
        ];
    }

    /**
     * Get the password reset validation error messages.
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [
            'email.required' => 'トークン' . Config::get('message.validation_common_msg.input_err'),
            'token.required' => 'トークン' . Config::get('message.validation_common_msg.input_err'),
            'password.required' => 'パスワード' . Config::get('message.validation_common_msg.input_err'),
            'password.min'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.min_length_err')), '8'),
            'confirm_password.required' => 'パスワードを認証する' . Config::get('message.validation_common_msg.input_err'),
            'confirm_password.same' => 'パスワードが同じである必要があることを確認する',
        ];
    }

    protected function credentials(Request $request)
    {
        return $request->only(
            'email',
            'password',
            'confirm_password',
            'token'
        );
    }


    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse(Request $request, $response)
    {
        //        $this->guard()->strict(true);
        return redirect($this->redirectPath())
            ->with('status', trans($response));
    }


    /**
     * Get the broker to be used during password reset.
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('trainers');
    }
}
