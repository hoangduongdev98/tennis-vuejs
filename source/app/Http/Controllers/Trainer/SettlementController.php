<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Services\Trainer\SettlementService;
use Carbon\Carbon;
use App\CSVDefine\SetHeader;

class SettlementController extends Controller
{

    /**
     * @var \App\Service\SettlementService
     */
    protected $settlementService, $setHeader;

    public function __construct(SettlementService $settlementService, SetHeader $setHeader)
    {
        $this->settlementService = $settlementService;
        $this->setHeader = $setHeader;
        $this->middleware('CheckLoginTrainer');
        $this->middleware(function ($request, $next) {
            $school = getSchool(Auth::guard('trainers')->user()->school_id);
            if ($school && $school['register_lesson'] == REGISTER_LESSON_0 && $school['register_lesson'] !== null) {
                return redirect()->route('trainer.index');
            }
            return $next($request);
        });
    }

    /**
     * Show list settlements
     *
     * @return \Illuminate\Http\Response
     */
    public function settlements(Request $request)
    {
        if ($request->ajax()) {
            $params = $request->all();
            $settlementList = $this->settlementService->getListSettlement($params);

            return response()->json($settlementList);
        }

        return view("trainer.settlement.settlements");
    }

    /**
     * Export CSV Settlement
     *
     * @return \Illuminate\Http\Response
     */
    public function settlementExport(Request $request)
    {
        $dataSearch = $request->all();
        $settlementList = $this->settlementService->getListSettlement($dataSearch)->get();
        $fileName = "SettlementList" . Carbon::now()->format('Ymdhis') . '.csv';
        $headers = $this->setHeader->SetHeaderCSV($fileName);
        $columns = array('決済日', '明細', '', '支払金額');
        $callback = function () use ($settlementList, $columns) {
            $file = fopen('php://output', 'w');
            fputs($file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
            fputcsv($file, $columns);
            foreach ($settlementList as $settlements) {
                $line = [
                    date("Y/n/j", strtotime($settlements->payment_date)),
                    $settlements->player_name,
                    $settlements->lesson_name,
                    formatMoneyNumber($settlements->fee),
                ];
                fputcsv($file, $line);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
