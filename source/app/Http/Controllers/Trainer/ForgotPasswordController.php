<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;

/**
 * Description of ForgotPasswordController
 *
 * @author ADMIN
 */
class ForgotPasswordController extends Controller
{

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        return view('trainer.auth.forgot_password');
    }


    /**
     * Validate the email for the given request.
     * 
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'check_email_format', 'exists:staffs,email,is_deleted,0'],
        ], [
            'email.required' => 'メールアドレス' . Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => 'メールアドレス' . Config::get('message.validation_common_msg.format_err'),
            'email.exists' => 'メールアドレスがデータベースに存在しません. 正しく完全な電子メールアドレスを使用して再試行してください',
        ]);
    }


    /**
     * @return PasswordBroker
     */
    protected function broker()
    {
        return Password::broker('trainers');
    }
}
