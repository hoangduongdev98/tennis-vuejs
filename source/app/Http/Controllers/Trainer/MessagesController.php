<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use App\Services\Trainer\ChatService;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    protected $chatService;

    public function __construct(ChatService $chatService)
    {
        $this->chatService = $chatService;
        $this->middleware('CheckLoginTrainer');
    }

    /**
     * Show messages chat.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dataSearch = [
            'search_sort' => null,
            'search_lesson' => null,
            'search_player' => null,
            'search_status' => null,
            'search_playerName' => null,
        ];
        $member_id = $request->query('member_id');
        if ($member_id) {
            $dataSearch['search_player'] = $member_id;
        }
        if ($lesson_id = $request->query('lesson_id')) {
            $dataSearch['search_lesson'] = $lesson_id;
        }
        $arrLesson = $this->chatService->getAllLesson();
        $arrPlayer = $this->chatService->getAllPlayer();
        if ($request->isMethod('post')) {
            $dataSearch = $request->all();
            $request->session()->put('search_message', $dataSearch);
        } elseif (strpos($request->getRequestUri(), 'search') !== false) {
            $dataSearch = $request->session()->get('search_message');
        } else {
            $request->session()->forget('search_message');
        }
        $chatList = $this->chatService->getListChatParticipants($dataSearch);

        return view('trainer.message.message', compact('chatList', 'arrLesson', 'arrPlayer', 'dataSearch'));
    }

    /**
     * Get messages chat.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMessage(Request $request)
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $data = $request->all();
        $messages = collect($this->chatService->getListMessageByParticipantId($data['participant_id']));
        $result['disabled'] = $messages['disabled'];
        $messages->forget('disabled');
        if ($messages) {
            $result['data'] = $messages;
            $result['msg'] = 'Success';
        } else {
            $result['msg'] = 'Error';
            $result['success'] = CODE_AJAX_ERROR;
        }
        return response()->json([
            'result' => $result,
        ]);
    }
}
