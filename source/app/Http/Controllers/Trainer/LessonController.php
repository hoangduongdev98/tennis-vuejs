<?php

namespace App\Http\Controllers\Trainer;

use App\CSVDefine\SetHeader;
use App\Http\Controllers\Controller;
use App\Http\Requests\Trainer\CreateLessonRequest;
use App\Http\Requests\Trainer\ImageRequest;
use App\Mail\Trainer\ApprovePlayer;
use App\Mail\Trainer\CancelPlayer;
use App\Mail\Trainer\CompleteLesson;
use App\Mail\Trainer\PlayerLackPoint;
use App\Mail\Trainer\RejectPlayer;
use App\Mail\Trainer\SendMessage;
use App\Services\TennisMgtService;
use App\Services\Trainer\ChatService;
use App\Services\Trainer\CreateLessonService;
use App\Services\Trainer\LessonService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class LessonController extends Controller
{
    /**
     * @var \App\Repositories\PlayerRepository
     */
    protected $lessonService;
    protected $setHeader;
    protected $createLessonService;
    protected $tennisMgtService;

    public function __construct(
        LessonService $lessonService,
        CreateLessonService $createLessonService,
        SetHeader $setHeader,
        ChatService $chatService,
        TennisMgtService $tennisMgtService
    ) {
        $this->lessonService = $lessonService;
        $this->setHeader = $setHeader;
        $this->createLessonService = $createLessonService;
        $this->chatService = $chatService;
        $this->tennisMgtService = $tennisMgtService;
        $arrLevel = $this->lessonService->getAllLevel();
        $this->m_list = [
            'arrLevel' => $arrLevel,
        ];
        view()->composer('*', function ($view) {
            $view->with('m_list', $this->m_list);
        });

        $this->middleware('CheckLoginTrainer');
        $this->middleware(function ($request, $next) {
            $school = getSchool(Auth::guard('trainers')->user()->school_id);
            if ($school && $school['register_lesson'] == REGISTER_LESSON_0 && $school['register_lesson'] !== null) {
                return redirect()->route('trainer.index');
            }

            return $next($request);
        });
    }

    /**
     * Show list lessons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dataSearch = $request->all();
            $lessonList = $this->lessonService->getListLesson($dataSearch);
            $count = '';
            foreach ($lessonList['data'] as $lesson) {
                $lesson->count_pl += $this->lessonService->getRegister($lesson->id);
            }

            return response()->json($lessonList);
        }
        $lessonToday = $this->lessonService->getListLessonToday();
        $arrLevel = $this->lessonService->getAllLevel();
        $status = Config::get('const.lesson_status');

        return view('trainer.lesson.list', compact('status', 'arrLevel','lessonToday'));
    }

    /**
     * Export csv lists lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function lessonExport(Request $request)
    {
        $dataSearch = $request->all();
        $listLessons = $this->lessonService->getListLesson($dataSearch)->get();
        $fileName = 'LessonList_'.Carbon::now()->format('Ymd').'.csv';
        $headers = $this->setHeader->SetHeaderCSV($fileName);
        $columns = ['期間', 'レッスン名', 'レッスン日', '受講者数（募集数）', '@ポイント', 'ステータス'];
        $callback = function () use ($listLessons, $columns) {
            $file = fopen('php://output', 'w');
            fputs($file, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            fputcsv($file, $columns);
            foreach ($listLessons as $lesson) {
                $times = [];
                $html = '';
                $total = '';
                $dateLesson = date('Y/n/j', strtotime($lesson->start_time))."\n".date('Y/n/j', strtotime($lesson->end_time));
                if ($lesson['lesson_date'] != LESSON_DATE_2) {
                    $times = json_decode($lesson['times_study'], true);
                    if ($lesson['lesson_date'] == LESSON_DATE_0) {
                        $html .= LESSON_DATE_VALUE_0."\n";
                    } else {
                        $html .= LESSON_DATE_VALUE_1."\n";
                    }
                    foreach ($times as $time => $display) {
                        $html .= Config::get('const.day_of_week.'.$time).'('.$display['start'].')'."\n";
                    }
                } else {
                    $html = LESSON_DATE_VALUE_2;
                }
                if ($lesson->quantity_player > 0) {
                    $total .= $lesson->lesson_player_count.'('.$lesson->quantity_player.')';
                } else {
                    $total = '制限なし';
                }
                $line = [
                    $dateLesson,
                    $lesson->name,
                    $html,
                    $total,
                    formatMoneyNumber($lesson->payment_fee),
                    Config::get('const.lesson_status.'.$lesson->status),
                ];
                fputcsv($file, $line);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    /**
     * Create lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function createLesson(Request $request)
    {
        $arrLevel = $this->lessonService->getAllLevel();
        $arrNameCategories = Config::get('const.category_lesson');

        return view('trainer.lesson.create', compact('arrLevel', 'arrNameCategories'));
    }

    /**
     * Post create lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreateLesson(Request $request)
    {
        $dataPost = $request->all();
        // $msg = [];
        // $expireDate = $request->register_expire_date;
        // $startLesson = $request->lesson_start_time;
        // $endLesson = $request->lesson_end_time;
        // $dateLesson = $request->date_lesson;
        // $now = date('Y/m/d');
        // $timeNow = date('H:i');
        // if ($request->has('date_lesson') && $dateLesson !== null) {
        //     if ($dateLesson && $dateLesson < $now) {
        //         $msg['date_lesson'] = Config::get('message.validation_common_msg.date_lesson_err');
        //     }
        // }
        // if ($request->has('register_expire_date') && $expireDate !== null) {
        //     if ($expireDate && $expireDate < $now) {
        //         $msg['register_expire_date'] = Config::get('message.validation_common_msg.last_register_before_today');
        //     }
        // }
        // if ($request->has('lesson_start_time') && $request->lesson_start_time !== null && $request->has('lesson_end_time') && $request->lesson_end_time !== null) {
        //     if ($startLesson != null && $endLesson != null && date('H:i', strtotime($endLesson)) <= date('H:i', strtotime($startLesson))) {
        //         $msg['time'] = Config::get('message.validation_common_msg.check_time_start_end');
        //     }
        // }
        // if ($request->has('lesson_start_time') && $dateLesson && $dateLesson < $now || $dateLesson && $dateLesson == $now && $startLesson && $startLesson < $timeNow) {
        //     $msg['start_time'] = Config::get('message.validation_common_msg.check_time_begin');
        // }
        $validate = new CreateLessonRequest();
        $validator = Validator::make($dataPost, $validate->rules(), $validate->messages());
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'status' => CODE_AJAX_ERROR]);
        }
        // if (isset($request->validator) && $request->validator->fails() || count($msg)) {
        //     return response()->json(['errors' => $request->validator, 'status' => CODE_AJAX_ERROR, 'msg' => $msg]);
        // }
        return response()->json(['status' => CODE_AJAX_SUCCESS]);
    }

    /**
     * Show view confirm create lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function createConfirmLesson(Request $request)
    {
        $arrLevel = $this->lessonService->getAllLevel();
        $arrNameCategories = Config::get('const.category_lesson');
        if ($dataPost) {
            return view('trainer.lesson.create_confirm_lesson', compact('dataPost', 'arrNameCategories', 'key', 'arrLevel'));
        } else {
            return redirect()->route('lesson.create', compact('arrLevel', 'arrNameCategories', 'dataPost'));
        }
    }

    /**
     * Save create lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveCreateLesson(Request $request)
    {
        $msg = '';
        $result = SUCCESS;
        $dataPost = $request->all();
        if (!$dataPost) {
            return redirect()->route('lesson.list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        $createLesson = $this->createLessonService->createLesson($dataPost);
        if ($createLesson) {
            $msg = 'レッスン情報'.Config::get('message.common_msg.insert_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.create_error');
            $result = ERROR;
        }

        return response()->json(['status' => CODE_AJAX_SUCCESS]);
    }

    /**
     * Save image for lesson via ajax.
     *
     * @param \Illuminate\Http\Request $request
     * @param $img
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadAvatarLesson(ImageRequest $request, $img)
    {
        return $this->tennisMgtService->uploadAvatar($request, $img, AVATAR_LESSON_SAVE_TMP);
    }

    /**
     * Get detail lesson.
     *
     * @param $idLesson
     *
     * @return \Illuminate\View\View
     */
    public function detailLesson(Request $request, $idLesson)
    {
        $trainer = Auth::guard('trainers')->user();
        $lesson = $this->lessonService->findLessonById($idLesson);
        $arrCategories = Config::get('const.category_lesson');

        if ($lesson->id != null && $lesson->table_id == $trainer->id) {
            return view('trainer.lesson.detail', compact('lesson','arrCategories'));
        }
    }

    /**
     * Delete detail lesson.
     *
     * @param $idLesson
     *
     * @return \Illuminate\View\View
     */
    public function deleteLesson($idLesson)
    {

        $status = 200;
        $msg = '';
        $idLesson = (int) $idLesson;
        $lesson = $this->lessonService->findLessonById($idLesson);
        if (!$lesson) {
            $status = STATUS_FAILED;

            return response()->json([
                'msg' => Config::get('message.validation_common_msg.no_exists_err'),
                'status' => $status,
            ]);
        }
        $delete = $this->lessonService->deleteLesson($idLesson);
        if ($delete) {
            $msg = Config::get('message.common_msg.deleted_success');
        } else {
            $msg = Config::get('message.common_msg.delete_error');
            $status = 203;
        }

        return response()->json(['msg' => $msg, 'status' => $status]);
    }

    public function editLesson(Request $request, $idLesson)
    {
        $trainer = Auth::guard('trainers')->user();
        $arrLevel = $this->lessonService->getAllLevel();
        $arrNameCategories = Config::get('const.category_lesson');
        $lesson = [];
        $dataLesson = $this->createLessonService->findLessonById($idLesson);
        if ($key = $request->input('key')) {
            $lesson = $request->session()->get('confirm_data'.$key);
        }
        if (empty($lesson)) {
            $lesson = $this->createLessonService->findLessonById($idLesson);
            if (!$lesson || $lesson['table_id'] != $trainer->id) {
                return redirect()->route('lesson.list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
            } else {
                $lesson['level'] = json_decode($lesson['levels'], true);
                if ($lesson['lesson_date'] != LESSON_DATE_2) {
                    if ($lesson['times_study']) {
                        $lesson['times_study'] = json_decode($lesson['times_study'], true);
                        if (isset($lesson['times_study'][99])) {
                            $lesson['lesson_start_time'] = $lesson['times_study'][99]['start'];
                            $lesson['lesson_end_time'] = $lesson['times_study'][99]['end'];
                        }
                    } else {
                        $lesson['times_study'] = [];
                    }
                } else {
                    $lesson['times_study'] = [];
                }
                if ($lesson['register_expire_date']) {
                    $lesson['register_expire_date'] = date('Y/m/d', strtotime($lesson['register_expire_date']));
                }
                $lesson['applicants'] = $lesson['quantity_player'];
                $lesson['point'] = $lesson['payment_fee'];
                $lesson['date_format'] = $lesson['lesson_date'];
                $lesson['category'] = $lesson['category'];
                if ($lesson['lesson_date'] == LESSON_DATE_0 && $lesson['datetime_lesson']) {
                    $lesson['date_lesson'] = date('Y/m/d', strtotime($lesson['datetime_lesson']));
                }
            }
        }
        $trainerLogin = Auth::guard('trainers')->user();

        return view('trainer.lesson.edit', compact('arrLevel', 'lesson', 'dataLesson', 'arrNameCategories', 'idLesson', 'trainerLogin'));
    }

    /**
     * Edit lesson.
     *
     * @param \Illuminate\Http\Request $request
     * @param $idLesson
     *
     * @return Illuminate\Support\Facades\Route route
     */
    public function postEditLesson(CreateLessonRequest $request)
    {
        $dataPost = $request->all();
        $msg = [];
        $expireDate = $request->register_expire_date;
        $dateLesson = $request->date_lesson;
        $exDate = $request->expire_date;
        $startLesson = $request->lesson_start_time;
        $endLesson = $request->lesson_end_time;
        $lesson = $this->createLessonService->findLessonById($dataPost['id']);
        $now = date('Y/m/d');
        $timeNow = date('H:i');
        if ($request->has('date_lesson') && $dateLesson && $dateLesson < $now) {
            $msg['date_lesson'] = Config::get('message.validation_common_msg.date_lesson_err');
        }
        if ($request->has('register_expire_date') && $expireDate !== null) {
            if ($expireDate && $expireDate < $now) {
                $msg['register_expire_date'] = Config::get('message.validation_common_msg.last_register_before_today');
            }
        }
        if ($request->has('expire_date') && $exDate !== null) {
            if ($exDate && $exDate < $now) {
                $msg['expire_date'] = Config::get('message.validation_common_msg.last_register_before_today');
            }
        }
        if ($request->has('lesson_start_time') && $request->lesson_start_time !== null && $request->has('lesson_end_time') && $request->lesson_end_time !== null) {
            if ($startLesson != null && $endLesson != null && date('H:i', strtotime($endLesson)) <= date('H:i', strtotime($startLesson))) {
                $msg['time'] = Config::get('message.validation_common_msg.check_time_start_end');
            }
        }
        if ($request->has('lesson_start_time') && $dateLesson && $dateLesson < $now || $dateLesson && $dateLesson == $now && $startLesson && $startLesson < $timeNow) {
            $msg['start_time'] = Config::get('message.validation_common_msg.check_time_begin');
        }
        if (isset($request->validator) && $request->validator->fails() || count($msg)) {
            return redirect()->back()->with(['lesson' => $dataPost, 'msg' => $msg])->withErrors($request->validator);
        } else {
            if ($lesson['register_expire_date']) {
                $lesson['register_expire_date'] = date('Y/m/d', strtotime($lesson['register_expire_date']));
            }
            $dataPost['register_expire_date'] = $lesson['register_expire_date'];
            (isset($dataPost['key']) && !empty($dataPost['key'])) ? $key = $dataPost['key'] : $key = Str::random(32);
            $request->session()->put('confirm_data'.$key, $dataPost);

            return true;
        }
    }

    /**
     * Show view confirm edit lesson.
     *
     * @param $key
     * @param $idLesson
     *
     * @return \Illuminate\Http\Response
     */
    public function editConfirmLesson(Request $request, $key, $idLesson)
    {
        $arrLevel = $this->lessonService->getAllLevel();
        $arrNameCategories = Config::get('const.category_lesson');
        $dataPost = $request->session()->get('confirm_data'.$key);
        $lesson = $this->createLessonService->findLessonById($idLesson);
        if ($lesson['register_expire_date']) {
            $lesson['register_expire_date'] = date('Y/m/d', strtotime($lesson['register_expire_date']));
        }
        if ($dataPost) {
            return view('trainer.lesson.edit_confirm_lesson', compact('dataPost', 'arrNameCategories', 'key', 'arrLevel', 'idLesson', 'lesson'));
        } else {
            return redirect()->route('lesson.edit_lesson', compact('arrLevel', 'idLesson', 'arrNameCategories', 'lesson'));
        }
    }

    /**
     * Save edit lesson.
     *
     * @param $idLesson
     *
     * @return \Illuminate\Http\Response
     */
    public function saveEditLesson(Request $request)
    {
        $msg = '';
        $result = SUCCESS;
        $dataPost = $request->all();
        $editLesson = $this->createLessonService->editLesson($dataPost, $dataPost['id']);
        if ($editLesson) {
            $msg = 'レッスン情報'.Config::get('message.common_msg.update_success');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.edit_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');

        return redirect()->route('lesson.list')->with($result, $msg);
    }

    /**
     * get Player Pop up.
     *
     * @return \Illuminate\Http\Response
     */
    public function getListPlayerPopUp(Request $request)
    {
        $result = [];
        $result['success'] = CODE_AJAX_SUCCESS;
        if ($request->ajax()) {
            $lesson_id = $request->get('lesson_id');
            if ($lesson_id) {
                $result['totalPlayercnt'] = $this->getTotalPlayerJoin($lesson_id, false);
                $result['totalNewcnt'] = $this->getTotalPlayerJoin($lesson_id);
                $result['playerLesson']  = $this->lessonService->getPlayerLessonByLessonId($lesson_id)->paginate(5);
                $result['lesson'] = $this->lessonService->findLessonById($lesson_id);
            } else {
                $result['result'] = 'Data fail';
            }
        } else {
            $result['result'] = CODE_AJAX_ERROR;
        }

        return response()->json(['result' => $result]);
    }

    /**
     * Add new one on one chat.
     *
     * @return \Illuminate\Http\Response
     */
    public function addNewChatOneToOne(Request $request)
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $dataPost = $request->all();
        $dataPost['room_id'] = $this->lessonService->getRoomById($dataPost['player_lesson_id']);
        $trainer = Auth::guard('trainers')->user();
        $arrLevel = $this->lessonService->getAllLevel();
        $player_email = $this->lessonService->getEmail($dataPost['member_id']);
        if ($this->lessonService->updateStatusPlayerLesson($dataPost['player_lesson_id'], true)) {
            if ($dataParticipant = $this->lessonService->addRoomChat($dataPost)) {
                $dataPost['chat_participant_id'] = $dataParticipant;
                if ($dataPost['message_chat'] != null) {
                    $this->lessonService->sendMessagePopUp($dataPost);
                }
            } else {
                $result['msg'] = 'Error';
                $result['success'] = CODE_AJAX_ERROR;
            }
            $school = getSchool($trainer->school_id);
            $playerLesson = $this->lessonService->getPlayerLessonById($dataPost['player_lesson_id']);
            if ($player_email && $result['success'] == CODE_AJAX_SUCCESS) {
                foreach ($playerLesson as $player) {
                    $playerApprove = [
                        'email' => $player_email,
                        'arrLevel' => $arrLevel,
                        'trainer_name' => $trainer->name,
                        'school_name' => $school->name,
                        'trainer_address' => $trainer->address,
                        'trainer_email' => $trainer->email,
                        'trainer_phone' => $trainer->phone,
                        'trainer_fax' => $trainer->fax,
                        'date_register' => $player->created_at,
                        'lesson_name' => $player->lessonName,
                        'player_name' => $player->playerName,
                        'player_class' => $player->player_classes,
                        'classes' => $player->classes,
                        'level' => $player->level,
                        'comment' => $dataPost['message_chat'],
                    ];
                    Mail::to($player_email)->send(new ApprovePlayer($playerApprove));
                }
                if (count(Mail::failures()) > 0) {
                    $result['msg'] = Config::get('message.common_msg.send_mail_error');
                    $result['success'] = CODE_AJAX_ERROR;
                } else {
                    $result['msg'] = Config::get('message.common_msg.send_maill_success');
                }
            } else {
                $result['msg'] = Config::get('message.common_msg.mail_not_found');
                $result['success'] = CODE_AJAX_ERROR;
            }
        } else {
            $playerLesson = $this->lessonService->getPlayerLessonById($dataPost['player_lesson_id']);
            if ($player_email) {
                foreach ($playerLesson as $player) {
                    $playerLackPoints = [
                        'email' => $player_email,
                        'arrLevel' => $arrLevel,
                        'trainer_name' => $trainer->name,
                        'trainer_address' => $trainer->address,
                        'trainer_email' => $trainer->email,
                        'trainer_phone' => $trainer->phone,
                        'trainer_fax' => $trainer->fax,
                        'date_register' => $player->created_at,
                        'lesson_name' => $player->lessonName,
                        'player_name' => $player->playerName,
                        'player_class' => $player->player_classes,
                        'classes' => $player->classes,
                        'level' => $player->level,
                        'comment' => $dataPost['message_chat'],
                    ];
                    Mail::to($player_email)->send(new PlayerLackPoint($playerLackPoints));
                }
                $result['msg'] = Config::get('message.common_msg.not_enough_points');
                $result['success'] = CODE_AJAX_ERROR;
            }
        }

        return response()->json([
            'result' => $result,
        ]);
    }

    /**
     * Reject Player join lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function rejectPlayerJoinLesson(Request $request)
    {
        $result = [];
        $dataPost = $request->all();
        $trainer = Auth::guard('trainers')->user();
        $result['success'] = CODE_AJAX_SUCCESS;
        $player_email = $this->lessonService->getEmail($dataPost['member_id']);
        $arrLevel = $this->lessonService->getAllLevel();
        if ($request->ajax()) {
            if ($this->lessonService->updateStatusPlayerLesson($dataPost['player_lesson_id'])) {
                $result['msg'] = 'Email'.Config::get('message.common_msg.update_msg');
                $result['totalPlayercnt'] = $this->getTotalPlayerJoin($dataPost['lesson_id'], false);
                $result['totalNewcnt'] = $this->getTotalPlayerJoin($dataPost['lesson_id']);
                $allPlayerLesson = $this->lessonService->getPlayerLessonByLessonId($dataPost['lesson_id']);
                $result['lesson'] = $this->lessonService->findLessonById($dataPost['lesson_id']);
                foreach ($allPlayerLesson as $playerLesson) {
                    if ($playerLesson['pl_id'] == $dataPost['player_lesson_id']) {
                        $result['player'] = $playerLesson;
                    }
                }
            } else {
                $result['msg'] = 'Email'.Config::get('message.common_msg.edit_error');
                $result['success'] = CODE_AJAX_ERROR;
            }
            $school = getSchool($trainer->school_id);
            $playerLesson = $this->lessonService->getPlayerLessonById($dataPost['player_lesson_id']);
            if ($player_email && $result['success'] == CODE_AJAX_SUCCESS) {
                foreach ($playerLesson as $player) {
                    $playerReject = [
                        'email' => $player_email,
                        'arrLevel' => $arrLevel,
                        'trainer_name' => $trainer->name,
                        'school_name' => $school->name,
                        'trainer_address' => $trainer->address,
                        'trainer_email' => $trainer->email,
                        'trainer_phone' => $trainer->phone,
                        'trainer_fax' => $trainer->fax,
                        'date_register' => $player->created_at,
                        'lesson_name' => $player->lessonName,
                        'player_name' => $player->playerName,
                        'player_class' => $player->player_classes,
                        'classes' => $player->classes,
                        'level' => $player->level,
                        'comment' => $dataPost['message_chat'],
                    ];
                    Mail::to($player_email)->send(new RejectPlayer($playerReject));
                }
                if (count(Mail::failures()) > 0) {
                    $result['msg'] = Config::get('message.common_msg.send_mail_error');
                    $result['success'] = CODE_AJAX_ERROR;
                } else {
                    $result['msg'] = Config::get('message.common_msg.send_maill_success');
                }
            } else {
                $result['msg'] = Config::get('message.common_msg.mail_not_found');
                $result['success'] = CODE_AJAX_ERROR;
            }
        }

        return response()->json([
            'result' => $result,
        ]);
    }

    /**
     * Cancel Player join lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelPlayerInLesson(Request $request)
    {
        $result = [];
        $dataPost = $request->all();
        $trainer = Auth::guard('trainers')->user();
        $result['success'] = CODE_AJAX_SUCCESS;
        $arrLevel = $this->lessonService->getAllLevel();
        $player_email = $this->lessonService->getEmail($dataPost['member_id']);
        if ($request->ajax()) {
            if ($this->lessonService->cancelPlayerLesson($dataPost)) {
                if ($dataPost['message_chat'] != null) {
                    $this->lessonService->sendMessagePopUp($dataPost);
                }
                $result['msg'] = 'Success';
                $result['totalPlayercnt'] = $this->getTotalPlayerJoin($dataPost['lesson_id'], false);
                $result['totalNewcnt'] = $this->getTotalPlayerJoin($dataPost['lesson_id']);
                $result['lesson'] = $this->lessonService->findLessonById($dataPost['lesson_id']);
            } else {
                $result['msg'] = 'Error';
                $result['success'] = CODE_AJAX_ERROR;
            }
            $school = getSchool($trainer->school_id);
            $playerLesson = $this->lessonService->getPlayerLessonById($dataPost['player_lesson_id']);
            if ($player_email && $result['success'] == CODE_AJAX_SUCCESS) {
                foreach ($playerLesson as $player) {
                    $playerCancel = [
                        'email' => $player_email,
                        'arrLevel' => $arrLevel,
                        'trainer_name' => $trainer->name,
                        'school_name' => $school->name,
                        'trainer_address' => $trainer->address,
                        'trainer_email' => $trainer->email,
                        'trainer_phone' => $trainer->phone,
                        'trainer_fax' => $trainer->fax,
                        'date_register' => $player->created_at,
                        'lesson_name' => $player->lessonName,
                        'player_name' => $player->playerName,
                        'player_class' => $player->player_classes,
                        'classes' => $player->classes,
                        'level' => $player->level,
                        'comment' => $dataPost['message_chat'],
                    ];
                    Mail::to($player_email)->send(new CancelPlayer($playerCancel));
                }
                if (count(Mail::failures()) > 0) {
                    $result['msg'] = Config::get('message.common_msg.send_mail_error');
                    $result['success'] = CODE_AJAX_ERROR;
                } else {
                    $result['msg'] = Config::get('message.common_msg.send_maill_success');
                }
            } else {
                $result['msg'] = 'ERROR';
                $result['success'] = CODE_AJAX_ERROR;
            }
        }

        return response()->json([
            'result' => $result,
        ]);
    }

    /**
     * Cancel reject Player join lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelReject(Request $request)
    {
        $result = [];
        $dataPost = $request->all();
        $result['success'] = CODE_AJAX_SUCCESS;
        if ($request->ajax()) {
            if ($this->lessonService->cancelReject($dataPost)) {
                $result['msg'] = 'Success';
            } else {
                $result['msg'] = 'Error';
                $result['success'] = CODE_AJAX_ERROR;
            }
        }

        return response()->json([
            'result' => $result,
        ]);
    }

    /**
     * complete lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function completeLesson(Request $request)
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $dataPost = $request->all();
        $dataPost['room_id'] = $this->lessonService->getRoomById($dataPost['player_lesson_id']);
        $arrLevel = $this->lessonService->getAllLevel();
        $trainer = Auth::guard('trainers')->user();
        $player_email = $this->lessonService->getEmail($dataPost['member_id']);
        if ($this->lessonService->updateLessonFlag($dataPost['player_lesson_id'])) {
            $school = getSchool($trainer->school_id);
            $playerLesson = $this->lessonService->getPlayerLessonById($dataPost['player_lesson_id']);
            if ($dataPost['message_chat'] != null) {
                $this->lessonService->sendMessagePopUp($dataPost);
            }
            if ($player_email && $result['success'] == CODE_AJAX_SUCCESS) {
                foreach ($playerLesson as $player) {
                    $playerComplete = [
                            'email' => $player_email,
                            'arrLevel' => $arrLevel,
                            'trainer_name' => $trainer->name,
                            'school_name' => $school->name,
                            'trainer_address' => $trainer->address,
                            'trainer_email' => $trainer->email,
                            'trainer_phone' => $trainer->phone,
                            'trainer_fax' => $trainer->fax,
                            'date_register' => $player->created_at,
                            'lesson_name' => $player->lessonName,
                            'player_name' => $player->playerName,
                            'player_class' => $player->player_classes,
                            'classes' => $player->classes,
                            'level' => $player->level,
                            'comment' => $dataPost['message_chat'],
                        ];
                    Mail::to($player_email)->send(new CompleteLesson($playerComplete));
                }
                if (count(Mail::failures()) > 0) {
                    $result['msg'] = Config::get('message.common_msg.send_mail_error');
                    $result['success'] = CODE_AJAX_ERROR;
                } else {
                    $result['msg'] = Config::get('message.common_msg.send_maill_success');
                }
            } else {
                $result['msg'] = 'ERROR';
                $result['success'] = CODE_AJAX_ERROR;
            }
        }

        return response()->json([
            'result' => $result,
        ]);
    }

    /**
     * Send message ajax.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendMessage(Request $request)
    {
        $now = date('Y/m/d H:i:s');
        $result['result'] = CODE_AJAX_SUCCESS;
        $result['now'] = $now;
        $dataPost = $request->all();
        if ($dataMessage = $this->chatService->sendMessageChat($dataPost)) {
            $result['msg'] = Config::get('message.common_msg.send_maill_success');
            if (isset($dataMessage['data'])) {
                $result['data'] = $dataMessage['data'];
            }
        } else {
            $result['result'] = CODE_AJAX_ERROR;
        }

        return response()->json([
            'result' => $result,
        ]);
    }

    /**
     * Sorting and paginate ajax.
     *
     * @return \Illuminate\Http\Response
     */
    public function sortingAndPaginateAjax(Request $request)
    {
        $result = [];
        $result['success'] = CODE_AJAX_SUCCESS;

        if ($request->ajax()) {
            $lesson_id = $request->get('lesson_id');
            $arrLessonPlayer = $this->lessonService->getPlayerLessonByLessonId($lesson_id);
            if ($request->isMethod('get')) {
                $sort_by = $request->get('sortby');
                $sort_type = $request->get('sorttype');
                $arrLessonPlayer = $arrLessonPlayer->orderBy($sort_by, $sort_type);
                $arrLessonPlayer = $arrLessonPlayer->paginate(5);
                $result['data'] = $arrLessonPlayer;
            }
        } else {
            $result['result'] = CODE_AJAX_ERROR;
        }

        return response()->json([
            'result' => $result,
        ]);
    }

    /**
     * count player lesson.
     *
     * @param $lesson_id
     * @param $new
     *
     * @return \Illuminate\Http\Response
     */
    public function getTotalPlayerJoin($lesson_id, $new = true)
    {
        $newPlayer = $this->lessonService->getPlayerStatus();
        $count = 0;
        foreach ($newPlayer as $less) {
            $status_player = $less->player_lessons_status;
            $id = $less->lessonId;
            if ($id == $lesson_id) {
                if ($new == true) {
                    if ($status_player == LESSON_PLAYER_STATUS_0 && $status_player !== null) {
                        ++$count;
                    }
                } else {
                    if ($status_player == LESSON_PLAYER_STATUS_1 || $status_player == LESSON_PLAYER_STATUS_2 || $status_player == LESSON_PLAYER_STATUS_3) {
                        ++$count;
                    }
                }
            }
        }

        return $count;
    }
}
