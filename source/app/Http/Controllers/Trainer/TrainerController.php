<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Trainer\TrainerRequest;
use App\Services\Trainer\TrainerService;
use App\Models\StructureFee;
use Illuminate\Support\Facades\Config;
use App\Services\TennisMgtService;
use App\Http\Requests\Trainer\ImageRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Services\Trainer\PostCodesService;
use App\Http\Requests\Trainer\SettingStructureFeeRequest;
use Illuminate\Support\Facades\Auth;

class TrainerController extends Controller
{
    protected $trainerService;
    protected $postCodesService;
    protected $tennisMgtService;

    public function __construct(TrainerService $trainerService, PostCodesService $postCodesService, TennisMgtService $tennisMgtService)
    {
        $this->trainerService = $trainerService;
        $this->postCodesService = $postCodesService;
        $this->tennisMgtService = $tennisMgtService;
        $this->middleware('CheckLoginTrainer');
    }
    /**
     * Show view edit trainer
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function trainerEdit(Request $request)
    {
        $trainerLogin = Auth::guard('trainers')->user();
        $arrLevel = $this->trainerService->getLevelName();
        $structures = $this->trainerService->getStructureFee($trainerLogin->id);
        $trainer = [];
        if ($key = $request->input('key')) {
            $trainer = $request->session()->get('confirm_data' . $key);
        }
        if (empty($trainer)) {
            $trainer = $this->trainerService->findTrainerById($trainerLogin->id);
            if (!$trainer) {
                return redirect()->route('trainer.index')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
            } else {
                $trainer['level'] = json_decode($trainer['levels'], true);
                $trainer['day_off'] = json_decode($trainer['day_off'], true);
            }
        }
        return view("trainer.trainer.edit_trainer", compact('arrLevel', 'trainer', 'trainerLogin', 'structures'));
    }
    /**
     * Edit trainer
     *
     * @param \Illuminate\Http\Request $request
     * @param $idSchool
     * @return Illuminate\Support\Facades\Route route
     */
    public function postEditTrainer(TrainerRequest $request)
    {
        $dataPost = $request->all();
        $validateRequest = new SettingStructureFeeRequest();
        $newDataPost = $this->newDataPost($dataPost);
        $validator = Validator::make($newDataPost, $validateRequest->rules($dataPost), $validateRequest->messages($dataPost));
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->with(['trainer' => $dataPost])->withErrors($request->validator);
        } else if ($validator->fails()) {
            return redirect()->back()->with(['trainer' => $dataPost])->withErrors($validator);
        } else {
            (isset($dataPost['key']) && !empty($dataPost['key'])) ? $key = $dataPost['key'] : $key = Str::random(32);
            $request->session()->put('confirm_data' . $key, $dataPost);
            return redirect()->route('trainer.editTrainerConfirm', ['key' => $key]);
        }
    }

    /**
     * Show view confirm edit trainer
     *
     * @param \Illuminate\Http\Request $request
     * @param $key
     * @return \Illuminate\Http\Response
     */
    public function editConfirmTrainer(Request $request, $key)
    {
        $trainerLogin = Auth::guard('trainers')->user();
        $data = $request->session()->get('confirm_data' . $key);
        $trainer = $this->trainerService->findTrainerById($trainerLogin->id);
        $arrLevel = $this->trainerService->getLevelName();
        if ($data) {
            return view('trainer.trainer.edit_confirm', compact('data', 'key', 'trainer', 'arrLevel'));
        } else {
            return redirect()->route('trainer.edit_trainer', ['trainer_id' => $trainerLogin->id]);
        }
    }

    /**
     * Save edit trainer
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveEditTrainer(Request $request)
    {
        $trainerLogin = Auth::guard('trainers')->user();
        $key = $request->input('key');
        $dataPosts = $request->session()->get('confirm_data' . $key);
        $editTrainer = $this->trainerService->editTrainer($trainerLogin->id, $dataPosts);
        if ($editTrainer) {
            $msg = 'コーチ情報' . Config::get('message.common_msg.update_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.edit_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');

        return redirect()->route('trainer.edit')->with($result, $msg);
    }

    /**
     * New data post
     * @param \Illuminate\Http\Request $request
     * @param $dataPost
     * @return \Illuminate\Http\Response
     */
    public function newDataPost($dataPost)
    {
        $newDataPost = [];
        if (!empty($dataPost['data_setting'])) {
            $data_setting = json_decode($dataPost['data_setting']);
            foreach ($data_setting as $index => $setting) {
                $newDataPost['setting_name_' . $index] = $setting->setting_name;
                $newDataPost['setting_price_' . $index] = $setting->setting_price;
            }
        }
        return $newDataPost;
    }

    public function getAddress(Request $request)
    {
        $data = $request->all();
        $postCode = $data['zip_code'];
        return $this->postCodesService->getAddressByPostCode($postCode);
    }

    public function uploadAvatarTrainer(ImageRequest $request, $img)
    {
        return $this->tennisMgtService->uploadAvatar($request, $img, AVATAR_TRAINER_SAVE_TMP);
    }

    public function deleteStructureFee($idSetting)
    {
        return $this->trainerService->deleteStructureFeeById($idSetting);
    }
}
