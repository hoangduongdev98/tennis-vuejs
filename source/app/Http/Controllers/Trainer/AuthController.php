<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Trainer\AuthLoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
// use App\Services\Trainer\AuthService;


class AuthController extends Controller
{
    // protected $authService;

    public function __construct()
    {
        // $this->authService = $authService;
        $this->middleware('guest:trainers')->except('logout');
    }

    public function getLogin()
    {
        return view("trainer.auth.login");
    }

    public function postLogin(AuthLoginRequest $request)
    {
        $data = [
            'email' => $request->email,
            'password' =>  $request->password,
            'is_deleted' => 0,
            'status' => 1,
        ];
        //check data login
        // $dataTrainer = $this->authService->getStatusTrainer($data);
        // if ($dataTrainer && $dataTrainer['school_status'] == SCHOOL_STATUS_1 && $dataTrainer['school_status'] !== null) {
            if (Auth::guard('trainers')->attempt($data)) {
                if (session()->has('name_admin_login')) {
                    $request->session()->forget('name_admin_login');
                }
                if (session()->has('name_school_login')) {
                    $request->session()->forget('name_school_login');
                }
                return redirect()->route('trainer.index');
            } else {
                return redirect()->back()->withInput()->with('ERROR', Config::get('message.common_msg.login_err'));
            }
        // } else {
        //     return redirect()->back()->withInput()->with(ERROR, Config::get('message.common_msg.login_err'));
        // }
    }

    /**
     * Logout
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View login
     */
    public function logout(Request $request)
    {
        Auth::guard('trainers')->logout();
        $request->session()->forget('name_admin_login');
        $request->session()->forget('name_school_login');
        return redirect()->route('trainer.login');
    }
}
