<?php

namespace App\Providers;

use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*
         * Paginate a standard Laravel Collection.
         *
         * @param int $perPage
         * @param int $total
         * @param int $page
         * @param string $pageName
         * @return array
         */
        Collection::macro('paginate', function ($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);

            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // add rule
        Validator::extend('exists_value_select', function ($attribute, $value, $parameters, $validator) {
            $postData = $validator->getData();
            $checkArr = $postData[$parameters['0']];
            if ($value == null || array_key_exists($value, $checkArr)) {
                return true;
            }

            return false;
        });

        Validator::extend('check_tel_and_fax', function ($attribute, $value, $parameters, $validator) {
            if ($value == '') {
                return true;
            } else {
                if (preg_match('/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/', $value) || preg_match("/^0\d{9,10}$/", $value)) {
                    return true;
                } else {
                    return false;
                }
            }

            return true;
        });

        Validator::extend('check_email_format', function ($attribute, $value, $parameters, $validator) {
            return $value && preg_match("/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/", $value);
        });

        Validator::extend('check_unique', function ($attribute, $value, $parameters, $validator) {
            $postData = $validator->getData();
            $table = $parameters['0'];
            $field = $parameters['1'];

            $id = null;
            if (isset($postData['id'])) {
                $id = $postData['id'];
            }

            $data = DB::table($table)
                ->where([
                    $table.'.'.$field => $value,
                    $table.'.is_deleted' => UNDELETED,
                ]);
            $isColExist = Schema::hasColumn($table, 'status');
            if ($isColExist) {
                $data = $data
                    ->where($table.'.status', '!=', SCHOOL_STATUS_99)
                    ->where($table.'.status', '!=', SCHOOL_STATUS_2);
            }
            $isColExist = Schema::hasColumn($table, 'school_id');
            if ($isColExist) {
                $data = $data
                    ->leftJoin('schools', 'schools.id', '=', $table.'.school_id')
                    ->where('schools.status', '!=', SCHOOL_STATUS_99)
                    ->where('schools.status', '!=', SCHOOL_STATUS_2);
            }
            if ($id != null) {
                $data = $data->where($table.'.id', '!=', $id);
            }

            $data = $data->first();
            if ($data) {
                return false;
            }

            return true;
        });

        Validator::extend('not_exists', function ($attribute, $value, $parameters) {
            $data = DB::table($parameters['0'])
                ->where([
                    $parameters['1'] => $value,
                    $parameters['0'].'.is_deleted' => UNDELETED,
                ])
                ->first();
            if ($data) {
                return true;
            }

            return false;
        });

        Validator::extend('check_post_code_format', function ($attribute, $value, $parameters, $validator) {
            return $value && preg_match("/^\d{3}-\d{4}$/", $value);
        });

        Validator::extend('check_password_format', function ($attribute, $value, $parameters, $validator) {
            $postData = $validator->getData();

            if (isset($postData['id']) && $postData['id']) {
                if ($value == PASSWORD) {
                    return true;
                } else {
                    return preg_match('/^[a-zA-Z0-9]+$/u', $value);
                }
            } else {
                return preg_match('/^[a-zA-Z0-9]+$/u', $value);
            }
        });

        Validator::extend('without_spaces', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });

        Validator::extend('check_money', function ($attr, $value) {
            if ($value % 100 == 0) {
                return true;
            }

            return false;
        });
        Validator::extend('check_ratio', function ($attr, $value) {
            if ($value % 10 == 0) {
                return true;
            }

            return false;
        });
        Validator::extend('check_max_price', function ($attr, $value) {
            if(strlen($value) > 6){
                return false;
            }
            return true;
        });

        // Using view composer to set following variables globally
        view()->composer('*', function ($view) {
            $view->with('arrSchoolStatus', Config::get('const.school_status'));
            $view->with('arrListContract', Config::get('const.list_contracts'));
            $view->with('arrPlayerStatus', Config::get('const.player_status'));
            $view->with('arrLessonStatus', Config::get('const.lesson_status'));
            $view->with('arrLimitItem', Config::get('const.limit_item'));
            $view->with('arrLessonPlayerStatus', Config::get('const.lesson_player_status'));
            $view->with('arrAdminRole', Config::get('const.admin_role'));
            $view->with('arrPaymentStatus', Config::get('const.payment_status'));
            $view->with('arrSettlementStatus', Config::get('const.settlement_status'));
            $view->with('arrDayOfWeek', Config::get('const.day_of_week'));
            $view->with('lessonDate', Config::get('const.lesson_date'));
            $view->with('sort_data', Config::get('const.sort_data'));
            $view->with('arrSortPlayerLesson', Config::get('const.sort_data_player_lesson'));
            $view->with('arrMatchNote', Config::get('const.match_note'));
            // if you need to access in controller and views:
            //Config::set('something', $something);
        });

        Paginator::useBootstrap();
    }
}
