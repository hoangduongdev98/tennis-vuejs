<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/*
 * Format phone number
 * @param $number
 * @return $result
 */
if (!function_exists('formatDataPhoneAndFax')) {
    function formatDataPhoneAndFax($number)
    {
        $result = '';
        if (preg_match('/^(\d{3})(\d{4})(\d{4})$/', $number, $matches)) {
            $result = $matches[1].'-'.$matches[2].'-'.$matches[3];
        }

        return $result;
    }
}

/*
 * Format post code number
 * @param $number
 * @return $result
 */
if (!function_exists('formatPostcodeNumber')) {
    function formatPostcodeNumber($number)
    {
        $result = '';
        if (preg_match('/^(\d{3})(\d{4})$/', $number, $matches)) {
            $result = $matches[1].'-'.$matches[2];
        }

        return $result;
    }
}

/*
 * Get name cretor of lesson
 * @param $lesonId
 * @return $name
 */
if (!function_exists('getLessonCreator')) {
    function getLessonCreator($lesonId, $type = null)
    {
        $name = '';
        $lesson = App\Models\Lessons::find($lesonId);
        if ($lesson) {
            if ($lesson->info_type == INFO_TYPE_SCHOOL) {
                $school = App\Models\Schools::where('schools.id', $lesson->table_id)->first();
                if ($school) {
                    if ($type) {
                        return $school->id;
                    }
                    $name = $school->name;
                }
            } elseif ($lesson->info_type == INFO_TYPE_TRAINER) {
                $trainer = App\Models\Trainers::where('trainers.id', $lesson->table_id)->first();
                if ($trainer) {
                    if ($type) {
                        return $trainer->id;
                    }
                    $name = $trainer->name;
                }
            }
        }

        return $name;
    }
}

/*
 * Get name school by lesson id
 * @param $lesonId
 * @return $name
 */
if (!function_exists('getSchoolNameViaLesson')) {
    function getSchoolNameViaLesson($lesonId, $type = null)
    {
        $name = '';
        $lesson = App\Models\Lessons::find($lesonId);
        if ($lesson) {
            if ($lesson->info_type == INFO_TYPE_TRAINER) {
                $trainer = App\Models\Trainers::where(['trainers.id' => $lesson->table_id, 'trainers.is_deleted' => UNDELETED])->first();
                if ($trainer) {
                    $school = App\Models\Schools::where(['schools.id' => $trainer->school_id, 'schools.is_deleted' => UNDELETED])->first();
                    if ($school) {
                        if ($type) {
                            return $school->id;
                        }
                        $name = $school->name;
                    }
                }
            }
        }

        return $name;
    }
}

/*
 * Format money
 * @param $money
 * @return $money
 */
if (!function_exists('formatMoneyNumber')) {
    function formatMoneyNumber($money)
    {
        return number_format((float) $money, 0);
    }
}

/*
 * Check current controller
 * @param $controller
 * @return $active
 */
if (!function_exists('getActiveCurrentController')) {
    function getActiveCurrentController(...$controllers)
    {
        $active = '';
        foreach ($controllers as $controller) {
            if (class_basename(Route::current()->controller) == $controller) {
                $active = 'active';
            }
        }

        return $active;
    }
}

/*
 * Check current route
 * @param array $routes
 * @return $active
 */
if (!function_exists('getActiveCurrentRoute')) {
    function getActiveCurrentRoute(...$routes)
    {
        $active = '';
        foreach ($routes as $route) {
            if (Route::current()->getName() == $route) {
                $active = 'active';
            }
        }

        return $active;
    }
}

/*
 * Get current DateTime
 *
 * @return dateTime
 */
if (!function_exists('getCurrentDateTime')) {
    function getCurrentDateTime()
    {
        date_default_timezone_set('Asia/Tokyo');

        return date('Y-m-d h:i:sa');
    }
}

/*
 * Format datetime
 * @param $date
 * @param $format
 * @return $date
 */
if (!function_exists('formatDate')) {
    function formatDate($date, $format = 'Y/m/d H:i:s')
    {
        if (empty($date)) {
            return '';
        }
        try {
            if (!empty($date->year)) {
                $date = $date->format($format);
            } else {
                $dateCv = new \DateTime($date);
                $date = $dateCv->format($format);
            }
        } catch (\Exception $e) {
            return '';
        }

        return $date;
    }
}

/*
 * Get room_id  by lesson id
 * @param $lesonId
 * @return $room_id
 */
if (!function_exists('findChatRoomByLessonId')) {
    function findChatRoomByLessonId($lessonId)
    {
        $room_id = null;
        $chatRoom = App\Models\ChatRooms::where(['lesson_id' => $lessonId, 'is_deleted' => UNDELETED])->first();
        if ($chatRoom) {
            $room_id = $chatRoom->id;
        }

        return $room_id;
    }
}

/*
 * Get name image by auth school id
 * @param $lesonId
 * @return $room_id
 */
if (!function_exists('getImageSchool')) {
    function getImageSchool($staffSchoolId)
    {
        $name = '';
        $image = App\Models\Images::where(['table_id' => $staffSchoolId, 'is_deleted' => UNDELETED, 'info_types' => INFO_TYPE_SCHOOL])->first();
        if ($image) {
            $name = $image->name;
        }

        return $name;
    }
}
/*
 * Get name image by auth school id
 * @param $lesonId
 * @return $name
 */
if (!function_exists('getImageTrainer')) {
    function getImageTrainer($trainerId)
    {
        $name = '';
        $image = App\Models\Images::where(['table_id' => $trainerId, 'is_deleted' => UNDELETED, 'info_types' => INFO_TYPE_TRAINER])->first();
        if ($image) {
            $name = $image->name;
        }

        return $name;
    }
}
/*
 * Format datetime
 * @param $date
 * @return $date
 */
if (!function_exists('formatChatDate')) {
    function formatChatDate($date)
    {
        setlocale(LC_TIME, 'ja_JP.utf8');
        $dateCv = '';

        $dateCarbon = Carbon::parse($date);
        if ($dateCarbon->isToday()) {
            $dateCv = TODAY;
        } elseif ($dateCarbon->isYesterday()) {
            $dateCv = YESTERDAY;
        } elseif ($dateCarbon->isCurrentWeek()) {
            $dateCv = strftime('%A', strtotime($date));
        } else {
            $dateCv = strftime('%Y年%#m月%e日(%a)', strtotime($date));
        }

        return $dateCv;
    }
}

/*
 * Format datetime
 * @param $date
 * @return $date
 */
if (!function_exists('formatLessonDate')) {
    function formatLessonDate($date)
    {
        setlocale(LC_TIME, 'ja_JP.utf8');
        $dateCv = '';
        if ($date) {
            $dateCv = strftime('%a', strtotime($date));
        }

        return $dateCv;
    }
}

/*
 * Return whether previous route name is equal to a given route name.
 *
 * @param string $routeName
 * @return boolean
 */
if (!function_exists('is_previous_route')) {
    function is_previous_route(string $routeName): bool
    {
        $previousRequest = app('request')->create(URL::previous());
        try {
            $previousRouteName = app('router')->getRoutes()->match($previousRequest)->getName();
        } catch (\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $exception) {
            // Exception is thrown if no mathing route found.
            // This will happen for example when comming from outside of this app.
            return false;
        }

        return $previousRouteName === $routeName;
    }
}

/*
 * Return school of staff.
 *
 * @param $id
 * @return $school
 */
if (!function_exists('get_school_by_staff')) {
    function get_school_by_staff($id)
    {
        $staff = App\Models\Staffs::where('id', $id)->first();
        $school = App\Models\Schools::where('id', $staff->school_id)->first();

        return $school;
    }
}

/*
 * Return player lesson by lesson id
 *
 * @param $id
 * @return $lesson
 */
if (!function_exists('getPlByLessonId')) {
    function getPlByLessonId($lessonId, $playerId)
    {
        $pl = DB::table('player_lessons')
        ->where('player_lessons.lesson_id', '=', $lessonId)
        ->where('player_lessons.player_id', '=', $playerId)
        ->orderBy('player_lessons.updated_at', 'desc')
        ->first();

        return $pl;
    }
}

/*
 * Return player lesson by lesson id
 *
 * @param $id
 * @return $lesson
 */
if (!function_exists('checkFlag')) {
    function checkFlag($id)
    {
        $pl = DB::table('lesson_flags')
        ->where('lesson_flags.player_lesson_id', '=', $id)
        ->first();

        return $pl;
    }
}

/*
 * Return player lesson by lesson id
 *
 * @param $id
 * @return $lesson
 */
if (!function_exists('getPlayerLesson')) {
    function getPlayerLesson($id)
    {
        $pl = DB::table('player_lessons')
        ->where('player_lessons.id', '=', $id)
        ->first();

        return $pl;
    }
}

/*
 * Return school of trainer.
 *
 * @param $trainerId
 * @return $school
 */
if (!function_exists('getSchoolByTrainerId')) {
    function getSchoolByTrainerId($trainerId)
    {
        $trainer = App\Models\Trainers::where('id', $trainerId)->first();
        $school = App\Models\Schools::where('id', $trainer->school_id)->first();

        return $school;
    }
}

/*
 * Check if the date is a valid date
 *
 * @param mixed $date
 * @param string $format
 * @return boolean
 */
if (!function_exists('checkValidDate')) {
    function checkValidDate($date, $format = 'Y/m/d')
    {
        $d = DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) === $date;
    }
}

/*
 * Check if player send message
 *
 * @param $playerId
 * @return $countReadMessages
 */
if (!function_exists('checkPlayerSendMessage')) {
    function checkPlayerSendMessage($playerId, $infoTypeLesson, $lessonTableId)
    {
        $chatHistory = DB::table('chat_rooms')
            ->leftJoin('players', 'chat_rooms.player_id', '=', 'players.id')
            ->leftJoin('player_lessons', 'player_lessons.player_id', '=', 'players.id')
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
            ->leftJoin('chat_messages as chat1', 'chat_rooms.id', '=', 'chat1.chat_room_id')
            ->leftJoin('chat_messages as chat2', function ($join) {
                $join->on('chat1.chat_room_id', '=', 'chat2.chat_room_id')->whereRaw(DB::raw('chat1.created_at < chat2.created_at'));
            })
            ->select(
                'chat_rooms.*',
                'schools.name as schoolName',
                'trainers.name as trainerName',
                'chat1.unread_message as chatStatus',
                'chat1.from as chatFrom',
                'chat1.info_type as lastType',
                'chat1.message as lastMess'
            )
            ->whereNull('chat2.chat_room_id')
            ->where([
                'lessons.info_type' => $infoTypeLesson,
                'lessons.id' => $lessonTableId,
                'chat_rooms.player_id' => $playerId,
                'chat1.info_type' => INFO_TYPE_MESSAGE_PLAYER,
                'chat1.unread_message' => UNREAD_MESSAGE,
                'chat_rooms.is_deleted' => UNDELETED,
            ])->distinct('chat_rooms.id')->count();

        return $chatHistory;
    }
}

/*
 * count player register lesson
 *
 * @param $lessonId
 * @return $countReadMessages
 */
if (!function_exists('countPlayerRegisterLesson')) {
    function countPlayerRegisterLesson($lessonId)
    {
        $countPlayer = App\Models\PlayerLesson::where('lesson_id', $lessonId)
            ->where('is_deleted', '==', UNDELETED)
            ->count();

        return $countPlayer;
    }
}

/*
 * get chat room by player id, lesson id
 *
 * @param $playerId
 * @param $lessonId
 * @return $countReadMessages
 */
if (!function_exists('getChatRoom')) {
    function getChatRoom($player_lesson_id)
    {
        if ($player_lesson_id) {
            $chatRoom = App\Models\ChatRooms::where([
                'player_lesson_id' => $player_lesson_id,
            ])
                ->first();
            if ($chatRoom) {
                return $chatRoom->id;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}

/*
 * count new match
 *
 * @param $playerId
 * @return $countMatches
 */
if (!function_exists('countMatches')) {
    function countMatches($playerId)
    {
        $countMatches = App\Models\Matches::where('created_id', $playerId)
            ->whereDate('created_at', date('Y-m-d'))
            ->count();

        return $countMatches;
    }
}

/*
 * Get advice by trainerId
 *
 * @param int $trainerId
 * @return string $adviceWinner
 */
if (!function_exists('getAdviceByTrainer')) {
    function getAdviceByTrainer($trainerId)
    {
        return DB::table('winner_setting')->where('created_id', '=', $trainerId)->first();
    }
}

/*
 * Get trainer by id
 *
 * @param int $trainerId
 * @return trainer
 */
if (!function_exists('getTrainer')) {
    function getTrainer($trainerId)
    {
        return App\Models\Trainers::find($trainerId);
    }
}

/*
 * Get school by id
 *
 * @param int $schoolId
 * @return school
 */
if (!function_exists('getSchool')) {
    function getSchool($schoolId)
    {
        return App\Models\Schools::find($schoolId);
    }
}

/*
 * replace Detail by text japanese
 *
 * @param $string
 * @return $string
 */
if (!function_exists('replaceDetail')) {
    function replaceDetail($point)
    {
        foreach ($point as $key => $string) {
            $detail = substr($string, -3, -1);
            $details = explode(' ', $string);
            $arrNoteDetail = Config::get('const.match_note');
            if (array_key_exists($detail, $arrNoteDetail)) {
                if ($details[0] == '0') {
                    $string = $details[1];
                }
                $string = str_replace($detail, '('.$arrNoteDetail[$detail], $string);
                $string = $string.')';
            }
            $point[$key] = $string;
        }

        return $point;
    }
}

/*
 * add class for string
 *
 * @param $string
 * @return $string
 */
if (!function_exists('addClass')) {
    function addClass($string)
    {
        $class = '';
        $details = explode(' ', $string);
        if ($details[0] == 'WON') {
            $class = 'text-primary font-weight-bold';
        }

        return $class;
    }
}

/*
 * get scores
 *
 * @param $match_detail
 * @return $score
 */
if (!function_exists('getScore')) {
    function getScore($match_detail)
    {
        $score = '';
        $sets = $match_detail->sets;
        $games = $match_detail->games;
        $tiebreak = $match_detail->tie_breaker_score;
        $tieActive = $match_detail->tie_breaker_active;
        for ($i = 0; $i < $sets; ++$i) {
            $tie1 = '';
            $tie2 = '';
            if (isset($tieActive->$i)) {
                $tie1 = '<sup>'.$tiebreak[$i][0].'</sup>';
                $tie2 = '<sup>'.$tiebreak[$i][1].'</sup>';
            }
            if ($games[$i][0] != 0 || $games[$i][1] != 0) {
                if ($games[$i][0] > $games[$i][1]) {
                    $score .= '〇 '.$games[$i][0].$tie1.' - '.$games[$i][1].$tie2.', ';
                } else {
                    $score .= '● '.$games[$i][0].$tie1.' - '.$games[$i][1].$tie2.', ';
                }
            }
        }
        if (substr($score, -2) == ', ') {
            $score = substr($score, 0, -2);
        }
        echo $score;
    }
}

/*
 * get advice label
 *
 * @param $value
 * @return $advice
 */
if (!function_exists('getAdviceLabel')) {
    function getAdviceLabel($value)
    {
        $advice = Config::get('const.advice_label');

        return $advice[$value];
    }
}

/*
 * get advice label
 *
 * @param $value
 * @return $advice
 */
if (!function_exists('getServeLabel')) {
    function getServeLabel($value)
    {
        $advice = Config::get('const.serve_label');

        return $advice[$value];
    }
}

/*
 * get serve first
 *
 * @param $match_detail,$set,$game
 * @return $serveFirst
 */
if (!function_exists('checkServeFirst')) {
    function checkServeFirst($points)
    {
        if (str_contains($points[0][0], 'F')) {
            return true;
        }

        return false;
    }
}

/*
 * get new register lesson
 *
 * @param $lessonId
 * @return $count
 */
if (!function_exists('getRegister')) {
    function getRegister($lessonId)
    {
        $count = App\Models\PlayerLesson::where([
            'lesson_id' => $lessonId,
            'status' => LESSON_PLAYER_STATUS_0,
        ])->count();

        return $count;
    }
}

/**
 * Get url to link.
 *
 * @param $string
 *
 * @return $link
 */
function makeUrlToLink($string)
{
    if (stripos(strtolower($string), '<a href="') !== false) {
        return $string;
    }

    // The Regular Expression filter
    $reg_pattern = "/(((http|https|ftp|ftps)\:\/\/)|(www\.))[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\:[0-9]+)?(\/\S*)?/";
    // make the urls to hyperlinks
    return preg_replace($reg_pattern, '<a href="$0" target="_blank" rel="noopener noreferrer">$0</a>', $string);
}

/**
 * Get status of player_lesson after user register.
 *
 * @param int $lessonId
 *
 * @return $status
 */
function getStatusLessonAfterRegister($lessonId, $playerId)
{
    $status = null;
    $data = App\Models\PlayerLesson::where([
        'lesson_id' => $lessonId,
        'player_id' => $playerId,
        'is_deleted' => UNDELETED,
    ])->first();
    if ($data) {
        $status = $data->status;
    }

    return $status;
}

/**
 * Count number player has register lesson by id.
 *
 * @param int $lessonId
 *
 * @return mixed
 */
function countPlayerHasRegisterLesson($lessonId, $quantity)
{
    $count = App\Models\PlayerLesson::where([
            'lesson_id' => $lessonId,
            'is_deleted' => UNDELETED,
        ])
        ->where('status', '!=', LESSON_PLAYER_STATUS_99)
        ->count();
    $data = 0;
    $data = $quantity - $count;

    return $data;
}
