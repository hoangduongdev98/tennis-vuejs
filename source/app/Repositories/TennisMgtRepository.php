<?php
namespace App\Repositories;

use App\Models\Prefectures;
use App\Models\MasterLevels;
use Illuminate\Support\Facades\DB;

class TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $prefs;
    
    /**
     * @var MasterLevels $level
     */
    protected $level;

    public function __construct(
        Prefectures $prefs,
        MasterLevels $level
    ) {
        $this->prefs = $prefs;
        $this->level = $level;
    }
    /**
     * Get list prefectures
     *
     * @return mixed
     */
    public function listPrefectures () {
        return $this->prefs->all();
    }
    
    /**
     * Get list levels
     *
     * @return mixed
     */
    public function listLevels () {
        return $this->level->orderBy('display_order', 'asc')->get();
    }
}
