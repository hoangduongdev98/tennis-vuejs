<?php

namespace App\Repositories\Trainer;

use App\Models\Schools;
use App\Models\PlayerLesson;
use Illuminate\Support\Collection;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Auth;
use Exception;

class PlayerLessonRepository extends TennisMgtRepository {

    protected $playerLesson;

    public function __construct(PlayerLesson $playerLesson) {
        $this->playerLesson = $playerLesson;
    }

    /**
     * Get list player
     * @return mixed
     */
    public function getListPlayer ($search) {
        $trainer = Auth::guard('trainers')->user();
        $playerLesson = $this->playerLesson
                        ->leftJoin('players','players.id','=','player_lessons.player_id')
                        ->leftJoin('lessons','lessons.id','=','player_lessons.lesson_id')
                        ->leftJoin('trainers','trainers.id','=','lessons.table_id')
                        ->leftJoin('settlements','settlements.player_lesson_id','=','player_lessons.id')
                ->select(
                    'player_lessons.updated_at',
                    'lessons.info_type',
                    'players.updated_at as updated_at_player',
                    'players.name as player_name',
                    'lessons.name as lesson_name',
                    'lessons.quantity_player',
                    'player_lessons.status',
                    'settlements.payment_date',
                    'lessons.payment_fee',
                    'players.id as player_id',
                    'lessons.id as lesson_id'
                )
                ->where([
                    'players.is_deleted' => UNDELETED,
                    'lessons.info_type' => INFO_TYPE_TRAINER,
                    'trainers.id' => $trainer->id,
                ]);
        if (!empty($search)) {
            if ($search['search_startDate'] && $search['search_endDate']) {
                $startDate = date('Y-m-d',  strtotime($search['search_startDate']));
                $endDate = date('Y-m-d',  strtotime($search['search_endDate']));
                $playerLesson->whereBetween('player_lessons.updated_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
            if ($namePlayer = $search['search_name']) {
                $playerLesson->where('players.name', 'LIKE', "%{$namePlayer}%");
            }
            if ($nameLesson = $search['search_lessonName']) {
                $playerLesson->where('lessons.name', 'LIKE', "%{$nameLesson}%");
            }
            $statusPlayer = $search['search_status'];
            if ($statusPlayer !== null) {
                $playerLesson->where('player_lessons.status', '=', $statusPlayer);
            }
        }
        return $playerLesson->sortable();
    }
}
