<?php

namespace App\Repositories\Trainer;


use Illuminate\Support\Collection;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use App\Models\Images;
use App\Models\Trainers;
use App\Models\StructureFee;
use App\Models\MasterLevels;
use Illuminate\Support\Facades\DB;
use Exception;

class TrainerRepository extends TennisMgtRepository
{
    protected $level;
    protected $trainer;
    protected $image;
    protected $fee;
    public function __construct(
        MasterLevels $level,
        Images $image,
        StructureFee $fee,
        Trainers $trainer
    ) {
        $this->level = $level;
        $this->trainer = $trainer;
        $this->image = $image;
        $this->fee = $fee;
    }

    public function getAllLevel()
    {
        $list = $this->level->select(
            'master_levels.id',
            'master_levels.name')
            ->where('master_levels.is_deleted',UNDELETED)
            ->orderBy('display_order','asc')  
            ->orderBy('updated_at','desc')             
            ->get();
    return $list;
    }

    public function getTrainerById($id)
    {
        return $this->trainer
            ->leftJoin('schools', 'schools.id', '=', 'trainers.school_id')
            ->select(
                'trainers.*',
                'schools.name as school_name',
            )
            ->where([
                'trainers.id' => $id,
                'trainers.is_deleted' => UNDELETED,
            ])
            ->first();
    }

    public function findAllImageByTrainerId(int $trainer_id)
    {
        return $this->image->where([
            'table_id' => $trainer_id,
            'info_types' => INFO_TYPE_TRAINER,
            'is_deleted' => UNDELETED
        ])->get();
    }

    public function getStructureFee(int $trainer_id)
    {
        return $this->fee->where([
            'table_id' => $trainer_id,
            'info_type' => INFO_TYPE_TRAINER,
            'is_deleted' => UNDELETED
        ])->get();
    }

    public function saveEditTrainer(Trainers $trainer, $arrImage) {
        $return = true;
        DB::beginTransaction();
        try {
            if ($trainer->save()) {
                if (!empty($arrImage)) {
                    foreach ($arrImage as $image) {
                        $dirTmp = AVATAR_TRAINER_SAVE_TMP . $image['name'];
                        $dirSave = AVATAR_TRAINER_SAVE . $image['name'];
                        if (file_exists($dirTmp)) {
                            @rename($dirTmp, $dirSave);
                        }
                        $image->table_id = $trainer->id;
                        if (!$image->save()) {
                            $return = false;
                        }
                    }
                }
            } else {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }

    public function findImageByTrainer(int $trainer_id, $position)
    {
        return $this->image->where([
            'position' => $position,
            'table_id' => $trainer_id,
            'info_types' => INFO_TYPE_TRAINER,
            'is_deleted' => UNDELETED
        ])->first();
    }

    public function saveStructureFee(StructureFee $fee)
    {
       
        $result = true;
        DB::beginTransaction();
        try {
            if (!$fee->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $result = false;
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $result;
    }

    public function getStructureFeeById(int $id)
    {
        return $this->fee->where(['structure_fee.id' => $id, 'structure_fee.is_deleted' => UNDELETED])->first();
    }
}
