<?php

namespace App\Repositories\Trainer;

use App\Models\ChatRooms;
use App\Models\Matches;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\Schools;
use App\Models\SharingAnalyses;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlayerRepository extends TennisMgtRepository
{
    protected $player;
    protected $school;
    protected $chatRoom;
    protected $match;
    protected $player_lesson;

    public function __construct(
        Players $player,
        Schools $school,
        PlayerLesson $player_lesson,
        ChatRooms $chatRoom,
        Matches $match,
        SharingAnalyses $sharingAnalyses
    ) {
        $this->player = $player;
        $this->school = $school;
        $this->player_lesson = $player_lesson;
        $this->chatRoom = $chatRoom;
        $this->match = $match;
        $this->sharingAnalyses = $sharingAnalyses;
    }

    /**
     * Get list players has lesson belong to trainer.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function getListPlayer($search = null)
    {
        $playerList = $this->player->all();
        $playerLessonList = $this->getPlayerLesson();
        $chatRooms = $this->getChatRoom();
        $arrEmpty = [];
        if (!empty($search)) {
            $playerLessonList = $this->searchPlayer($playerLessonList, $search);
            if ($search['lessonName'] != null) {
                $chatRooms = $chatRooms->whereIn('chat_rooms.id', $arrEmpty);
            }
        }
        $playerLessonList = $playerLessonList->get();
        // $chatRooms = $chatRooms->get();
        foreach ($playerList as $player) {
            $arrLesson = [];
            $arrLessonId = [];
            $arrStatus = [];
            // $arrChatRoom = [];
            $arrLessonType = [];
            if (count($playerLessonList)) {
                foreach ($playerLessonList as $playerLesson) {
                    if ($player->id == $playerLesson->playerID) {
                        array_push($arrLesson, $playerLesson['lessonName']);
                        $player['lessonName'] = $arrLesson;
                        array_push($arrStatus, $playerLesson['plStatus']);
                        $player['plStatus'] = $arrStatus;
                        array_push($arrLessonId, $playerLesson['lessonId']);
                        $player['lessonId'] = $arrLessonId;
                        array_push($arrLessonType, $playerLesson['lessonType']);
                        $player['lessonType'] = $arrLessonType;
                    }
                }
            }
            // if (count($chatRooms)) {
            //     foreach ($chatRooms as $chatRoom) {
            //         if ($player->id == $chatRoom->player_id) {
            //             array_push($arrChatRoom, $chatRoom);
            //             $player['chatRoom'] = $arrChatRoom;
            //             array_push($arrStatus, 4);
            //             $player['plStatus'] = $arrStatus;
            //         }
            //     }
            // }
            $player['totalMatches'] = $this->countMatchesByPlayerId($player['id']);
        }
        $playerList = $playerList->whereNotNull('plStatus');
        $limit = isset($params['limit']) ? (int) $params['limit'] : PAGINATE_LIMIT;
        $offset = isset($params['page']) ? ($params['page'] * $limit) - $limit : '';
        $total = count($playerList);
        $last_page = (int) ceil($total / $limit);

        return ['data' => $playerList, 'total' => $total, 'last_page' => $last_page];
    }

    /**
     * Get data players after search.
     *
     * @param $data
     * @param array $search
     *
     * @return mixed
     */
    public function searchPlayer($data, $search)
    {
        if (!empty($search)) {
            $startDate = $search['started_at'] ? $search['started_at'] : '';
            $endDate = $search['ended_at'] ? $search['ended_at'] : '';
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d', strtotime($startDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d', strtotime($endDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($startDate && $endDate) {
                $data = $data->whereBetween('players.updated_at', [$startDate.' 00:00:00', $endDate.' 23:59:59']);
            } elseif ($startDate && !$endDate) {
                $startDate = date('Y-m-d', strtotime($search['started_at']));
                $data = $data->where('players.updated_at', '>=', $startDate.' 00:00:00');
            } elseif (!$startDate && $endDate) {
                $endDate = date('Y-m-d', strtotime($search['ended_at']));
                $data = $data->where('players.updated_at', '<=', $endDate.' 23:59:59');
            }
            if ($namePlayer = $search['name']) {
                $data = $data->where('players.name', 'LIKE', "%{$namePlayer}%");
            }
            if ($nameLesson = $search['lessonName']) {
                $data = $data->where('lessons.name', 'LIKE', "%{$nameLesson}%");
            }
            $statusPlayer = $search['userStatus'];
            if ($statusPlayer !== null) {
                $data = $data->where('player_lessons.status', '=', $statusPlayer);
            }
        }

        return $data;
    }

    /**
     * Get data player lesson.
     *
     * @return mixed
     */
    public function getPlayerLesson()
    {
        $trainer = Auth::guard('trainers')->user();

        return $this->player_lesson
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->leftJoin('players', 'players.id', '=', 'player_lessons.player_id')
            ->where(function ($query) use ($trainer) {
                $query->where([
                    'lessons.info_type' => INFO_TYPE_TRAINER,
                    'trainers.id' => $trainer->id,
                ]);
            })
            ->select(
                'player_lessons.player_id as playerID',
                'lessons.name as lessonName',
                'player_lessons.id as plId',
                'player_lessons.status as plStatus',
                'lessons.id as lessonId',
                'trainers.name as trainerName',
                'lessons.info_type as lessonType',
                'lessons.table_id as authorId',
                'players.name as playerName'
            )
            ->orderBy('player_lessons.created_at', 'desc');
    }

    /**
     * Get data chat room.
     *
     * @return mixed
     */
    public function getChatRoom()
    {
        $trainer = Auth::guard('trainers')->user();

        return $this->chatRoom
            ->leftJoin('trainers', 'trainers.id', '=', 'chat_rooms.table_id')
            ->leftJoin('players', 'players.id', '=', 'chat_rooms.player_id')
            ->where(function ($query) use ($trainer) {
                $query->where([
                    'chat_rooms.info_type' => INFO_TYPE_TRAINER,
                    'chat_rooms.table_id' => $trainer->id,
                ]);
            })
            ->select(
                'chat_rooms.*',
                'trainers.name as trainerName',
                'chat_rooms.info_type as roomType'
            );
    }

    /**
     * Count data of players.
     *
     * @return array $data
     */
    public function countDataOfPlayers()
    {
        $data = [];
        $playerOfMonth = $this->getDataOfPlayer()->whereMonth('players.created_at', date('m'))->whereYear('players.created_at', date('Y'));
        $data['player_registration'] = $playerOfMonth->count();
        $data['player_old_register'] = $this->getDataOfPlayer()->whereNotIn('players.id', $playerOfMonth->pluck('players.id'))->count();

        return $data;
    }

    /**
     * Get data chat after search.
     *
     * @param $data
     * @param array $search
     *
     * @return mixed
     */
    public function searchChatRoom($data, $search)
    {
        if (!empty($search)) {
            $startDate = $search['started_at'];
            $endDate = $search['ended_at'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d', strtotime($startDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d', strtotime($endDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($startDate && $endDate) {
                $data = $data->whereBetween('players.updated_at', [$startDate.' 00:00:00', $endDate.' 23:59:59']);
            } elseif ($startDate && !$endDate) {
                $startDate = date('Y-m-d', strtotime($search['started_at']));
                $data = $data->where('players.updated_at', '>=', $startDate.' 00:00:00');
            } elseif (!$startDate && $endDate) {
                $endDate = date('Y-m-d', strtotime($search['ended_at']));
                $data = $data->where('players.updated_at', '<=', $endDate.' 23:59:59');
            }
            if ($namePlayer = $search['name']) {
                $data = $data->where('players.name', 'LIKE', "%{$namePlayer}%");
            }
        }

        return $data;
    }

    /**
     * Sort list player.
     *
     * @param array $playerList
     * @param $sort
     * @param $direction
     *
     * @return mixed
     */
    public function sortListPlayer($playerList, $sort, $direction)
    {
        if (isset($sort) && isset($direction) && count($playerList)) {
            if ($sort == 'playerLesson') {
                if (strtolower($direction) == 'desc') {
                    $playerList = $playerList->sortByDesc(function ($status) {
                        return $status['plStatus'][0];
                    });
                } else {
                    $playerList = $playerList->sortBy(function ($status) {
                        return $status['plStatus'][0];
                    });
                }
            } else {
                if (strtolower($direction) == 'desc') {
                    $playerList = $playerList->sortByDesc($sort);
                } else {
                    $playerList = $playerList->sortBy($sort);
                }
            }
        }

        return $playerList->values();
    }

    /**
     * Get count matches by player id.
     *
     * @param $playerId
     *
     * @return mixed
     */
    public function countMatchesByPlayerId(int $playerId)
    {
        return $this->match->where([
            'matches.created_id' => $playerId,
            'matches.is_deleted' => UNDELETED,
        ])->count();
    }

    /**
     * Get data of player.
     *
     * @return mixed
     */
    public function getDataOfPlayer()
    {
        $trainer = Auth::guard('trainers')->user();
        $players = $this->player
            ->leftJoin('player_lessons', 'players.id', '=', 'player_lessons.player_id')
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->where(function ($query) use ($trainer) {
                $query->where(['lessons.info_type' => INFO_TYPE_TRAINER, 'trainers.id' => $trainer->id]);
            })
            ->select('players.*')
            ->distinct('players.id');

        return $players;
    }

    /**
     * Get email by member_id.
     *
     * @param $member_id
     *
     * @return email
     */
    public function getEmailByMemberId($member_id)
    {
        $dataMember = $this->player->find($member_id);
        $email = '';
        if ($dataMember) {
            $email = $dataMember['email'];
        }

        return $email;
    }

    /**
     * Get email by member_id.
     *
     * @param $member_id
     *
     * @return @mix
     */
    public function getPlayerByMemberId($member_id)
    {
        return  $this->player->find($member_id);
    }

    /**
     * Get school by id.
     *
     * @param $school_id
     *
     * @return array
     */
    public function getSchoolById($school_id)
    {
        return $this->school
            ->where([
                'schools.id' => $school_id,
            ])
            ->first();
    }

    /**
     * Get data account player to login from trainer site by player id.
     *
     * @param $playerId
     *
     * @return mixed
     */
    public function getAccountLogonByPlayerId(int $playerId)
    {
        return $this->player->where([
            'players.id' => $playerId,
            'players.is_deleted' => UNDELETED,
        ])->first();
    }

    /**
     * list shared player.
     */
    public function getListSharedPlayer($search = null)
    {
        $trainer = Auth::guard('trainers')->user();
        $data = SharingAnalyses::leftJoin('players', 'sharing_analyses.player_id', '=', 'players.id')
            ->leftJoin('matches', function ($join) {
                $join->on('sharing_analyses.player_id', '=', 'matches.created_id')->where('matches.is_deleted', UNDELETED);
            })
            ->select(
                'sharing_analyses.*',
                'players.name as player_name',
                'players.last_login as player_last_login',
                DB::raw('COUNT(matches.id) as total_matches')
            )
            ->where([
                'sharing_analyses.trainer_id' => $trainer->id,
            ])
            ->groupBy('sharing_analyses.player_id')
            ->sortable([
                'updated_at' => 'DESC',
                'id' => 'DESC',
            ]);
        if (!empty($search)) {
            $data = $this->searchSharedPlayer($data, $search);
        }

        return $data->get();
    }

    /**
     * Get data shared player after search.
     *
     * @param $data
     *
     * @return mixed
     */
    public function searchSharedPlayer($data, array $search)
    {
        if (!empty($search)) {
            $startDate = $search['started_at'];
            $endDate = $search['ended_at'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d', strtotime($startDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d', strtotime($endDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($startDate && $endDate) {
                $data = $data->whereBetween('players.last_login', [$startDate.' 00:00:00', $endDate.' 23:59:59']);
            } elseif ($startDate && !$endDate) {
                $startDate = date('Y-m-d', strtotime($search['started_at']));
                $data = $data->where('players.last_login', '>=', $startDate.' 00:00:00');
            } elseif (!$startDate && $endDate) {
                $endDate = date('Y-m-d', strtotime($search['ended_at']));
                $data = $data->where('players.last_login', '<=', $endDate.' 23:59:59');
            }
            if ($namePlayer = $search['name']) {
                $data = $data->where('players.name', 'LIKE', "%{$namePlayer}%");
            }
            $statusPlayer = $search['userStatus'];
            if ($statusPlayer !== null) {
                $data = $data->where('sharing_analyses.status', '=', $statusPlayer);
            }
        }

        return $data;
    }

    /**
     * update status share.
     *
     * @param type $data
     *
     * @return type
     */
    public function acceptShare($data)
    {
        return SharingAnalyses::where('id', $data['id'])
                ->update(['status' => 1]);
    }

    /**
     * delete share.
     *
     * @param type $data
     *
     * @return type
     */
    public function deleteShare($data)
    {
        if (SharingAnalyses::where('id', $data['id'])->exists()) {
            return SharingAnalyses::destroy($data['id']);
        } else {
            return false;
        }
    }

    /**
     * get account share.
     *
     * @return type
     */
    public function getAccountSharedPlayer($trainerId, $playerId)
    {
        if (SharingAnalyses::where('trainer_id', $trainerId)->where('player_id', $playerId)->exists()) { // check account exits in share table
            return $this->player->where([
                    'players.id' => $playerId,
                    'players.is_deleted' => UNDELETED,
                ])->first();
        } else {
            return false;
        }
    }
}
