<?php

namespace App\Repositories\Trainer;

use App\Models\WinnerSetting;
use App\Models\MissSetting;
use App\Models\SettingCourse;
use App\Models\Matches;
use App\Models\Serve1stAdvice;
use App\Models\Serve2ndAdvice;
use Illuminate\Support\Collection;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Models\SharingAnalyses;

class MatchesRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $match;
    protected $winnerSetting;
    protected $missSetting;
    protected $settingCourse;
    protected $serve1stAdvice;
    protected $serve2ndAdvice;

    public function __construct(
        Matches $match,
        WinnerSetting $winnerSetting,
        MissSetting $missSetting,
        SettingCourse $settingCourse,
        Serve1stAdvice $serve1stAdvice,
        Serve2ndAdvice $serve2ndAdvice,
        SharingAnalyses $sharingAnalyses
    ) {
        $this->match = $match;
        $this->winnerSetting = $winnerSetting;
        $this->missSetting = $missSetting;
        $this->settingCourse = $settingCourse;
        $this->serve1stAdvice = $serve1stAdvice;
        $this->serve2ndAdvice = $serve2ndAdvice;
        $this->sharingAnalyses = $sharingAnalyses;
    }

    /**
     * Count data match;
     *
     * @return array $data
     */
    public function countDataOfMatches()
    {
        $dataShare = SharingAnalyses::where('sharing_analyses.trainer_id', Auth::guard('trainers')->user()->id)->where('sharing_analyses.status', SHARE_STATUS_1)->get();
        $arrPlayerId = [];
        foreach($dataShare as $player) {
            $arrPlayerId[] = $player->player_id;
        }
        $dataSchool = getSchool(Auth::guard('trainers')->user()->school_id);
        $data = [];
        $data['matches_registration'] = 0;
        $dataMatchesOfMonthPlayer = $this->getDataOfMatchesPLayer($arrPlayerId)->whereMonth('matches.created_at', date('m'))->whereYear('matches.created_at', date('Y'))->where('matches.is_deleted', UNDELETED);
        if($dataSchool->contract_id == CONTRACT_VALUE_1 || $dataSchool->contract_id == CONTRACT_VALUE_2 || $dataSchool->contract_id == CONTRACT_VALUE_4)
        {
            $data['matches_registration'] = $dataMatchesOfMonthPlayer->count();
        }
        $dataOfPlayer = $this->getDataOfMatchesPLayer($arrPlayerId)->whereNotIn('matches.id', $dataMatchesOfMonthPlayer->pluck('matches.id'))->where('matches.is_deleted', UNDELETED);
        $data['matches_old_register'] = $dataOfPlayer->count();
        return $data;
    }

    public function getDataOfMatchesPLayer($arrPlayerId)
    {
        $matches = $this->match
                ->whereIn('created_id', $arrPlayerId)
                ->select('matches.*')
                ->distinct('matches.id');                    
        return $matches;
    }
    /**
     * Save winner setting
     *
     * @param entity $winnerSetting
     * @return bool
     */
    public function saveWinnerSetting(WinnerSetting $winnerSetting)
    {
        $return = true;
        DB::beginTransaction();
        try {
            if (!$winnerSetting->save()) {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }
    /**
     * get winner setting
     *
     * @param entity $created_id
     * @return bool
     */
    public function getWinnerSetting($created_id)
    {
        return  $this->winnerSetting->where('created_id', '=', $created_id)->first();
    }
    /**
     * Save miss setting
     *
     * @param entity $missSetting
     * @return bool
     */
    public function saveMissSetting(MissSetting $missSetting)
    {
        $return = true;
        DB::beginTransaction();
        try {
            if (!$missSetting->save()) {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }
    /**
     * get miss setting
     *
     * @param entity $created_id
     * @return bool
     */
    public function getMissSetting($created_id)
    {
        return  $this->missSetting->where('created_id', '=', $created_id)->first();
    }
    /**
     * Save advice setting course
     *
     * @param entity $settingCourse
     * @return bool
     */
    public function saveAdviceSetting(SettingCourse $settingCourse)
    {
        $return = true;
        DB::beginTransaction();
        try {
            if (!$settingCourse->save()) {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }
    /**
     * get advice setting course
     *
     * @param entity $created_id
     * @return bool
     */
    public function getAdviceSetting($created_id, $info_type)
    {
        return  $this->settingCourse
            ->where([
                'created_id' => $created_id,
                'info_type' => $info_type
            ])
            ->first();
    }
    /**
     * Save advice serve 1st
     *
     * @param entity $serve1stAdvice
     * @return bool
     */
    public function saveServe1st(Serve1stAdvice $serve1stAdvice)
    {
        $return = true;
        DB::beginTransaction();
        try {
            if (!$serve1stAdvice->save()) {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }
    /**
     * get advice serve 1st
     *
     * @param entity $created_id
     * @return bool
     */
    public function getServe1st($created_id)
    {
        return  $this->serve1stAdvice->where('created_id', '=', $created_id)->first();
    }
    /**
     * Save advice serve 2nd
     *
     * @param entity $serve2ndAdvice
     * @return bool
     */
    public function saveServe2nd(Serve2ndAdvice $serve2ndAdvice)
    {
        $return = true;
        DB::beginTransaction();
        try {
            if (!$serve2ndAdvice->save()) {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }
    /**
     * get advice serve 2nd
     *
     * @param entity $created_id
     * @return bool
     */
    public function getServe2nd($created_id)
    {
        return  $this->serve2ndAdvice->where('created_id', '=', $created_id)->first();
    }
}
