<?php

namespace App\Repositories\Trainer;

use App\Models\ChatMessages;
use App\Models\ChatRooms;
use App\Models\Lessons;
use App\Models\Payments;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\SendMailQueue;
use App\Models\Settlements;
use App\Repositories\TennisMgtRepository;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatRoomRepository extends TennisMgtRepository
{
    protected $chatRooms;
    protected $lesson;
    protected $player;
    protected $chatMessage;
    protected $settlement;
    protected $playerLesson;
    protected $payment;
    protected $sendMailQueue;

    public function __construct(
        ChatRooms $chatRooms,
        Lessons $lesson,
        Players $player,
        ChatMessages $chatMessage,
        Settlements $settlement,
        PlayerLesson $playerLesson,
        Payments $payment,
        SendMailQueue $sendMailQueue
        ) {
        $this->chatRooms = $chatRooms;
        $this->lesson = $lesson;
        $this->player = $player;
        $this->chatMessage = $chatMessage;
        $this->settlement = $settlement;
        $this->playerLesson = $playerLesson;
        $this->payment = $payment;
        $this->sendMailQueue = $sendMailQueue;
    }

    /**
     * save player to chat room.
     *
     * @return $result
     */
    public function saveMessage(array $chatMessage)
    {
        DB::beginTransaction();
        $result = [];
        $result['error'] = true;
        try {
            if ($chatMessage) {
                foreach ($chatMessage as $chat) {
                    if ($chat->save()) {
                        $this->sendMailQueue->create([
                            'id_mess' => $chat->id,
                            'flag' => 0,
                        ]);
                    } else {
                        $result['error'] = false;
                    }
                    $result['error'] ? DB::commit() : DB::rollback();
                }
                if (count($chatMessage) == 1) {
                    $result['data'] = $chatMessage[0];
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
            $result['error'] = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $result;
    }

    /**
     * Get list chat by participant.
     *
     * @param array $search
     *
     * @return $chatList
     */
    public function getListChatParticipants($search = null)
    {
        $trainer = Auth::guard('trainers')->user();
        $chatList = $this->chatRooms
            ->leftJoin('players', 'players.id', '=', 'chat_rooms.player_id')
            ->leftJoin('lessons', 'lessons.id', '=', 'chat_rooms.lesson_id')
            ->leftJoin('player_lessons', 'player_lessons.id', '=', 'chat_rooms.player_lesson_id')
            ->leftJoin('chat_messages as chat1', 'chat_rooms.id', '=', 'chat1.chat_room_id')
            ->leftJoin('chat_messages as chat2', function ($join) {
                $join->on('chat1.chat_room_id', '=', 'chat2.chat_room_id')->whereRaw(DB::raw('chat1.created_at < chat2.created_at'));
            })
            ->select(
            'chat_rooms.*',
            'players.name as player_name',
            'players.id as player_id',
            'lessons.info_type as info_type',
            'player_lessons.status as lessonStt',
            'player_lessons.updated_at as pl_updated_at',
            'player_lessons.id as pl_id',
            'player_lessons.is_deleted as pl_deleted',
            'lessons.name as lessonName',
            'chat1.created_at as lastChatTime',
        )
            ->whereNull('chat2.chat_room_id')
            ->where('lessons.status', '<>', LESSON_STATUS_99)
            ->where([
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'chat_rooms.is_deleted' => UNDELETED,
                'lessons.table_id' => $trainer->id,
        ]);
        if (!empty($search)) {
            $chatList = $this->searchListRoom($chatList, $search);
        }

        return $chatList->distinct('players.id');
    }

    /**
     * Get data Chat Participants after search.
     *
     * @param $data
     * @param $search
     *
     * @return mixed
     */
    public function searchListRoom($data, $search)
    {
        if ($search['search_sort'] !== null) {
            if ($search['search_sort'] == DATE_DESC) {
                $data = $data->orderBy('chat_rooms.updated_at', 'DESC');
            } elseif ($search['search_sort'] == DATE_ASC) {
                $data = $data->orderBy('chat_rooms.updated_at', 'ASC');
            } elseif ($search['search_sort'] == PLAYER_DESC) {
                $data = $data->orderBy('players.name', 'DESC');
            } else {
                $data = $data->orderBy('players.name', 'ASC');
            }
        }
        if ($playerName = $search['search_playerName']) {
            $data = $data->where('players.name', 'LIKE', "%{$playerName}%");
        }
        $playerId = $search['search_player'];
        if ($playerId !== null) {
            $data = $data->where('chat_rooms.player_id', '=', $playerId);
        }
        $statusLesson = $search['search_status'];
        if ($statusLesson !== null) {
            $data = $data->where('player_lessons.status', '=', $statusLesson);
        }
        $lessonId = $search['search_lesson'];
        if ($lessonId !== null) {
            $data = $data->where('lessons.id', '=', $lessonId);
        }

        return $data;
    }

    /**
     * Get all lesson.
     *
     * @return $lesson
     */
    public function getAllLesson()
    {
        $trainer = Auth::guard('trainers')->user();
        $lesson = $this->lesson
            ->select(
            'lessons.id',
            'lessons.name'
        )
            ->where([
            'lessons.info_type' => INFO_TYPE_TRAINER,
            'lessons.is_deleted' => UNDELETED,
        ])
        ->where('lessons.status', '<>', LESSON_STATUS_99);
        if ($trainer) {
            $lesson = $lesson->where('lessons.table_id', '=', $trainer->id);
        }
        $lesson = $lesson->orderBy('lessons.id')
            ->get();

        return $lesson;
    }

    /**
     * Get all player.
     *
     * @return $player
     */
    public function getAllPlayer()
    {
        $trainer = Auth::guard('trainers')->user();
        $player = $this->chatRooms
            ->leftJoin('players', 'players.id', '=', 'chat_rooms.player_id')
            ->leftJoin('lessons', 'lessons.id', '=', 'chat_rooms.lesson_id')
            ->select(
            'players.id',
            'players.name'
        )->where('lessons.table_id', '=', $trainer->id)
            ->orderBy('players.id')
            ->get();

        return $player;
    }

    /**
     * Get list master level.
     *
     * @return mixed
     */
    public function getAllLevel()
    {
        $levels = $this->level
            ->select(
            'master_levels.id',
            'master_levels.name',
            'master_levels.display_order'
        )
            ->get();

        return $levels;
    }

    /**
     * Get chat room by id.
     *
     * @param $id
     *
     * @return $result
     */
    public function getChatRoomById($id)
    {
        $trainerQuery = $this->trainer
            ->leftJoin('schools', 'schools.id', '=', 'trainers.school_id')
            ->select(
            'trainers.id',
            'trainers.name',
            'trainers.status',
            'schools.status as schoolStatus'
        );
        $chatRoom = $this->chatRooms
            ->leftJoin('players', 'players.id', '=', 'chat_rooms.player_id')
            ->leftJoin('player_lessons', 'player_lessons.id', '=', 'chat_rooms.player_lesson_id')
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
            ->leftJoinSub(
            $trainerQuery,
            'trainer',
            function ($join) {
                $join->on('trainer.id', '=', 'lessons.table_id');
            }
        )
            ->where(function ($query) {
                $query->where(function ($q1) {
                    $q1
                        ->where('schools.status', '!=', SCHOOL_STATUS_99)
                        ->where('schools.status', '!=', SCHOOL_STATUS_2);
                }
                )
                    ->orWhere(function ($q2) {
                        $q2
                    ->where('trainer.schoolStatus', '!=', SCHOOL_STATUS_2)
                    ->where('trainer.status', '!=', SCHOOL_STATUS_2)
                    ->where('trainer.schoolStatus', '!=', SCHOOL_STATUS_99)
                    ->where('trainer.status', '!=', SCHOOL_STATUS_99);
                    }
            );
            })
            ->where([
            'chat_rooms.id' => $id,
            'lessons.info_type ' => INFO_TYPE_TRAINER,
            'chat_rooms.is_deleted' => UNDELETED,
        ])
            ->first();

        return $chatRoom;
    }

    /**
     * Get list message by room_id.
     *
     * @param $room_id
     *
     * @return $result
     */
    public function getListMessageByParticipantId($room_id, $unreadMessages = null)
    {
        if (count($unreadMessages)) {
            foreach ($unreadMessages as $unread) {
                $editMessage = $this->chatMessage->find($unread->id);
                $editMessage->unread_message = READ_MESSAGE;
                $editMessage->save();
            }
        }
        $result = $this->chatMessage
            ->leftJoin('chat_rooms', 'chat_rooms.id', '=', 'chat_messages.chat_room_id')
            ->leftJoin('lessons', 'lessons.id', '=', 'chat_rooms.lesson_id')
            ->leftJoin('players', 'players.id', '=', 'chat_rooms.player_id')
            ->leftJoin('player_lessons', 'player_lessons.id', '=', 'chat_rooms.player_lesson_id')
            ->where([
            'chat_messages.chat_room_id' => $room_id,
        ])
            ->select(
            'chat_messages.created_at as message_time',
            'chat_messages.from',
            'chat_messages.message',
            'chat_messages.to',
            'chat_messages.info_type',
            'players.name as player_name',
            'player_lessons.status as lessonStt',
            'player_lessons.is_deleted',
            'lessons.status as lessStt',
            'player_lessons.updated_at',
            'lessons.id as lesson_id',
            'players.id as player_id',
        )
            ->orderBy('chat_messages.created_at')
            ->get();

        return $result;
    }

    /**
     * save chat room participant.
     *
     * @param entity $chatRoom
     *
     * @return bool
     */
    public function saveChatRoomParticipant(ChatRooms $chatRoom = null)
    {
        if ($chatRoom) {
            $error = true;
            DB::beginTransaction();
            try {
                if (!$chatRoom->save()) {
                    $error = false;
                }
                $error ? DB::commit() : DB::rollback();
            } catch (Exception $e) {
                $error = false;
                DB::beginTransaction();
                DB::rollback();
            }

            return $error;
        }
    }

    /**
     * Get player id by participant id.
     *
     * @return mixed
     */
    public function getPlayerIdByParticipantId($participantId)
    {
        return $this->chatRooms
            ->leftJoin('lessons', 'lessons.id', '=', 'chat_rooms.lesson_id')
            ->leftJoin('player_lessons', 'player_lessons.id', '=', 'chat_rooms.player_lesson_id')
            ->select(
                'chat_rooms.*',
                'player_lessons.status as lessonStt',
                'lessons.status as lessStt',
            )
            ->where(['chat_rooms.id' => $participantId])->first();
    }

    /**
     * Get unread message by participant id and info type.
     *
     * @param $roomId
     * @param $type
     *
     * @return $unread
     */
    public function getUnreadMessageByParticipantId($roomId, $type)
    {
        $unread = $this->chatMessage
            ->where([
            'chat_room_id' => $roomId,
            'unread_message' => UNREAD_MESSAGE,
            'is_deleted' => UNDELETED,
            'info_type' => $type,
        ])
            ->orderBy('created_at', 'DESC');

        return $unread;
    }

    /**
     * Get latest message  by participant id and info type.
     *
     * @param $chatRoomId
     * @param $type
     *
     * @return mixed
     */
    public function getLatestMessage($chatRoomId, $type)
    {
        $latest = $this->chatMessage
            ->where([
            'chat_room_id' => $chatRoomId,
            'info_type' => $type,
            'is_deleted' => UNDELETED,
        ])
            ->orderBy('id', 'DESC')
            ->orderBy('created_at', 'DESC');

        return $latest;
    }
}
