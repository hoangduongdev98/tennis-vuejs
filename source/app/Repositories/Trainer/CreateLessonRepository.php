<?php



namespace App\Repositories\Trainer;


use App\Repositories\TennisMgtRepository;
use App\Models\Lessons;
use App\Models\Images;
use App\Models\PlayerLesson;
use App\Models\Settlements;
use Illuminate\Support\Facades\DB;
use App\Models\ChatRooms;
use Exception;
use Illuminate\Support\Facades\Auth;

/**
 * Description of CreateLessonRepository
 *

 */
class CreateLessonRepository extends TennisMgtRepository
{

    protected $lesson, $image, $chatroom, $playerLesson, $settlement;

    public function __construct(
        Lessons $lesson,
        PlayerLesson $playerLesson,
        Images $image,
        ChatRooms $chatroom,
        Settlements $settlement
    ) {
        $this->lesson = $lesson;
        $this->image = $image;
        $this->chatroom = $chatroom;
        $this->playerLesson = $playerLesson;
        $this->settlement;
    }

    public function saveCreateLesson(Lessons $lesson, $newImage)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($lesson->save()) {
                if (!empty($newImage)) {
                    $dirTmp = AVATAR_LESSON_SAVE_TMP . $newImage['name'];
                    $dirSave = AVATAR_LESSON_SAVE . $newImage['name'];
                    if (file_exists($dirTmp)) {
                        @rename($dirTmp, $dirSave);
                    }
                    $newImage->table_id = $lesson->id;
                    if (!$newImage->save()) {
                        $error = false;
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }
        return $error;
    }

    public function getLessonById($idLesson)
    {
        $trainer = Auth::guard('trainers')->user();
        return $this->lesson
            ->where([
                'lessons.id' => $idLesson,
                'lessons.is_deleted' => UNDELETED,
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'lessons.table_id' => $trainer->id,
            ])
            ->first();
    }

    public function findImageByLessonId(int $idLesson)
    {
        return $this->image->where([
            'table_id' => $idLesson,
            'info_types' => INFO_TYPE_LESSON,
            'is_deleted' => UNDELETED
        ])->get();
    }

    public function findImageByLesson(int $idLesson, $position)
    {
        return $this->image->where([
            'position' => $position,
            'table_id' => $idLesson,
            'info_types' => INFO_TYPE_LESSON,
            'is_deleted' => UNDELETED
        ])->first();
    }

    public function saveEditLesson(Lessons $lesson, $arrImage, $idLesson)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($lesson->save()) {
                if (!empty($arrImage)) {
                    foreach ($arrImage as $image) {
                        $dirTmp = AVATAR_LESSON_SAVE_TMP . $image['name'];
                        $dirSave = AVATAR_LESSON_SAVE . $image['name'];
                        if (file_exists($dirTmp)) {
                            @rename($dirTmp, $dirSave);
                        }
                        $image->table_id = $lesson->id;
                        if (!$image->save()) {
                            $error = false;
                        }
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }
        return $error;
    }

    public function deleteDataLesson(int $idLesson, $deleted_id = null)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            $dataLesson = $this->lesson
                ->where(['lessons.id' => $idLesson])
                ->where('lessons.status', '!=', LESSON_STATUS_99)
                ->get();
            if (!$this->deleteLessonAndPaymentOfTrainer($dataLesson, $deleted_id)) {
                $delete = false;
            }
            $deleteLesson = $this->lesson->find($idLesson);
            $deleteLesson->status = LESSON_STATUS_99;
            if (!$deleteLesson->save()) {
                $delete = false;
            }
            $delete ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $delete = false;
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $delete;
    }

    public function deleteTrainerOfOtherTable($datas, $tableName, $deleted_id = null)
    {
        $result = true;
        if (count($datas)) {
            foreach ($datas as $value) {
                $table = $tableName->find($value['id']);
                $table->status = LESSON_STATUS_99;
                $table->deleted_id = $deleted_id;
                if (!$table->save()) {
                    $result = false;
                }
            }
        }
        return $result;
    }

    public function deleteLessonAndPaymentOfTrainer($dataLessons, $deleted_id = null)
    {
        $result = true;
        if (count($dataLessons)) {
            foreach ($dataLessons as $lesson) {
                $dataPlayerLessons = $this->playerLesson
                    ->where('player_lessons.lesson_id', $lesson->id)
                    ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_99)
                    ->get();
                if (count($dataPlayerLessons)) {
                    foreach ($dataPlayerLessons as $playerLessons) {
                        $dataSettlement = $this->settlement
                            ->where('settlements.player_lesson_id', $playerLessons->id)
                            ->where('settlements.status', '!=', SETTLEMENT_99)
                            ->get();
                        if (count($dataSettlement)) {
                            foreach ($dataSettlement as $settlement) {
                                $dataPayment = $this->payment
                                    ->where('payments.settlement_id', $settlement->id)
                                    ->where('payments.status', '!=', PAYMENT_STATUS_99)
                                    ->get();
                                if (!$this->deleteTrainerOfOtherTable($dataPayment, $this->payment, $deleted_id)) {
                                    $result = false;
                                }
                            }
                        }
                        if (!$this->deleteTrainerOfOtherTable($dataSettlement, $this->settlement, $deleted_id)) {
                            $result = false;
                        }
                    }
                }
                if (!$this->deleteTrainerOfOtherTable($dataPlayerLessons, $this->playerLesson, $deleted_id)) {
                    $result = false;
                }
            }
        }
        return $result;
    }
}
