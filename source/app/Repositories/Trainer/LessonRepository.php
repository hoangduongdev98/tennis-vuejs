<?php

namespace App\Repositories\Trainer;

use App\Models\ChatMessages;
use App\Models\ChatRooms;
use App\Models\Images;
use App\Models\LessonFlags;
use App\Models\Lessons;
use App\Models\MasterLevels;
use App\Models\Payments;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\SettingSettlementFee;
use App\Models\Settlements;
use App\Repositories\TennisMgtRepository;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LessonRepository extends TennisMgtRepository
{
    protected $lesson;
    protected $level;
    protected $player;
    protected $settingSettlementFee;
    protected $image;
    protected $playLesson;
    protected $settlement;
    protected $payment;
    protected $chatRooms;
    protected $lessonFlag;

    public function __construct(
        Lessons $lesson,
        MasterLevels $level,
        SettingSettlementFee $settingSettlementFee,
        Images $image,
        Players $player,
        PlayerLesson $playLesson,
        Settlements $settlement,
        Payments $payment,
        ChatRooms $chatRooms,
        LessonFlags $lessonFlag
    ) {
        $this->lesson = $lesson;
        $this->level = $level;
        $this->image = $image;
        $this->settingSettlementFee = $settingSettlementFee;
        $this->player = $player;
        $this->playLesson = $playLesson;
        $this->settlement = $settlement;
        $this->payment = $payment;
        $this->chatRooms = $chatRooms;
        $this->lessonFlag = $lessonFlag;
    }

    /**
     * Get lesson.
     *
     * @return mixed
     */
    public function listLesson()
    {
        $trainer = Auth::guard('trainers')->user();
        $lesson = $this->lesson
            ->leftJoin('player_lessons', 'lesson_id', '=', 'lessons.id')
            ->select(
                'lessons.*',
                'lessons.register_expire_date as expireDate',
                'lessons.payment_fee as feeLesson',
            )
            ->where([
                'lessons.is_deleted' => UNDELETED,
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'lessons.table_id' => $trainer->id,
            ]);

        return $lesson;
    }

    /**
     * Get list lesson.
     *
     * @return mixed
     */
    public function getListLesson($search = null)
    {
        $list = $this->listLesson()
            ->with(['lesson_trainer', 'lesson_player'])
            ->withCount(['lesson_player' => function ($query) {
                $query->whereNotNull('status')
                    ->where(function ($q) {
                        $q->where('status', '=', LESSON_PLAYER_STATUS_1)
                            ->orWhere('status', '=', LESSON_PLAYER_STATUS_2)
                            ->orWhere('status', '=', LESSON_PLAYER_STATUS_3)
                            ->orWhere('is_deleted', '=', UNDELETED);
                    });
            }]);
        if (!empty($search)) {
            $list = $this->searchLesson($list, $search);
        }
        $limit = isset($params['limit']) ? (int) $params['limit'] : PAGINATE_LIMIT;
        $offset = isset($params['page']) ? ($params['page'] * $limit) - $limit : '';
        $total = $list->count();
        $last_page = (int) ceil($total / $limit);
        $list->orderBy('updated_at', 'DESC');

        return ['data' => $list->get(), 'total' => $total, 'last_page' => $last_page];
    }

    public function getRegister($lessonId)
    {
        $count = $this->playLesson::where([
            'lesson_id' => $lessonId,
            'status' => LESSON_PLAYER_STATUS_0,
        ])->count();

        return $count;
    }

    /**
     * Get list lesson has no player.
     *
     * @return mixed
     */
    public function getListLessonHasNoPlayer($search = null)
    {
        $trainer = Auth::guard('trainers')->user();
        $list = $this->lesson->select(
            'lessons.*',
            DB::raw('0 as lesson_player_count'),
            'lessons.register_expire_date as expireDate',
            'lessons.payment_fee as feeLesson',
        )
            ->where([
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'lessons.table_id' => $trainer->id,
            ])
            ->whereNotIn('lessons.id', $this->getListLesson()->pluck('lessons.id'));
        if (!empty($search)) {
            $list = $this->searchLesson($list, $search);
        }

        return $list;
    }

    /**
     * Get status player.
     *
     * @return mixed
     */
    public function getNewPlayer()
    {
        $list = $this->playLesson
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->select('player_lessons.status as player_lessons_status', 'lessons.id as lessonId')
            ->where([
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'lessons.table_id' => Auth::guard('trainers')->user()->id,
            ]);

        return $list;
    }

    /**
     * Get data lesson after search.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function searchLesson($lesson, $search)
    {
        $arr = [];
        if (!empty($search)) {
            $startDate = $search['started_at'];
            $endDate = $search['ended_at'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d', strtotime($startDate));
                } else {
                    return $lesson->where('lessons.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d', strtotime($endDate));
                } else {
                    return $lesson->where('lessons.id', null);
                }
            }
            if ($startDate && $endDate) {
                if ($startDate < $endDate) {
                    $lesson = $lesson->whereDate('lessons.datetime_lesson', '>=', $startDate)
                        ->whereDate('lessons.datetime_lesson', '<=', $endDate);
                } else {
                    $lesson = $lesson->whereIn('lessons.id', $arr);
                }
            }
            if ($startDate && !$endDate) {
                $startDate = date('Y-m-d', strtotime($search['search_startDate']));
                $lesson->whereDate('lessons.datetime_lesson', '>=', $startDate);
            }
            if (!$startDate && $endDate) {
                $endDate = date('Y-m-d', strtotime($search['search_endDate']));
                $lesson->whereDate('lessons.datetime_lesson', '<=', $endDate);
            }
            if ($nameLesson = $search['name']) {
                $lesson->where('lessons.name', 'LIKE', "%{$nameLesson}%");
            }
            if ($levelLesson = $search['level']) {
                $level = "\"$levelLesson\"";
                $lesson->where('lessons.levels', 'LIKE', "%{$levelLesson}%");
            }
            $statusLesson = $search['userStatus'];
            if ($statusLesson !== null) {
                $lesson->where('lessons.status', '=', $statusLesson);
            }
        }

        return $lesson;
    }

    /**
     * Get list lesson today.
     *
     * @return mixed
     */
    public function getListLessonToday()
    {
        $trainer = Auth::guard('trainers')->user();
        $now = date('Y-m-d');
        $lesson = $this->lesson
            ->leftJoin('player_lessons', 'lesson_id', '=', 'lessons.id')
            ->select(
                'lessons.*',
                'lessons.register_expire_date as expireDate',
                'lessons.payment_fee as fee_lesson',
            )
            ->withCount(['lesson_player' => function ($query) {
                $query->whereNotNull('status')
                    ->where(function ($q) {
                        $q->where('status', '=', LESSON_PLAYER_STATUS_1)
                            ->orWhere('status', '=', LESSON_PLAYER_STATUS_2)
                            ->orWhere('status', '=', LESSON_PLAYER_STATUS_3);
                    });
            }])
            ->where([
                'lessons.is_deleted' => UNDELETED,
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'lessons.table_id' => $trainer->id,
                'lessons.status' => LESSON_STATUS_1,
            ])
            ->where('lessons.lesson_date', '<>', LESSON_DATE_2)
            ->where('lessons.datetime_lesson', '=', $now);
        $limit = isset($params['limit']) ? (int) $params['limit'] : PAGINATE_LIMIT;
        $offset = isset($params['page']) ? ($params['page'] * $limit) - $limit : '';
        $total = $lesson->count();
        $last_page = (int) ceil($total / $limit);
        $lesson->orderBy('updated_at', 'DESC');

        return ['data' => $lesson->get(), 'total' => $total, 'last_page' => $last_page];
    }

    /**
     * Get list lesson today has no player.
     *
     * @return mixed
     */
    public function getListLessonTodayHasNoPlayer()
    {
        $trainer = Auth::guard('trainers')->user();
        $now = date('Y-m-d');
        $list = $this->lesson
        ->leftJoin('player_lessons', 'lesson_id', '=', 'lessons.id')
        ->select(
            'lessons.*',
            'lessons.register_expire_date as expireDate',
            'lessons.payment_fee as fee_lesson',
            DB::raw('0 as lesson_player_count'),
        )
            ->where([
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'lessons.table_id' => $trainer->id,
            ])
            ->where('lessons.lesson_date', '<>', LESSON_DATE_2)
            ->where('lessons.datetime_lesson', '=', $now)
            ->whereNotIn('lessons.id', $this->getListLessonToday()->pluck('lessons.id'));

        return $list;
    }

    /**
     * Get all level.
     *
     * @return $levels
     */
    public function getAllLevel()
    {
        $list = $this->level->select(
            'master_levels.id',
            'master_levels.name'
        )
            ->where('master_levels.is_deleted', UNDELETED)
            ->orderBy('display_order', 'asc')
            ->get();

        return $list;
    }

    /**
     * Get data settlement fee related settings by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function getSettlementFeeById($id)
    {
        return $this->settingSettlementFee->where(['setting_settlement_fee.id' => $id, 'setting_settlement_fee.is_deleted' => UNDELETED])->first();
    }

    /**
     * Delete lesson by id.
     *
     * @return mixed
     */
    public function deleteLesson(int $idLesson, $deleted_id = null)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            $dataLesson = $this->lesson
                ->where(['lessons.id' => $idLesson])
                ->where('lessons.status', '!=', LESSON_STATUS_99)
                ->get();
            if (!$this->deleteLessonAndPaymentOfTrainer($dataLesson, $deleted_id)) {
                $delete = false;
            }
            $deleteLesson = $this->lesson->find($idLesson);
            $deleteLesson->status = LESSON_STATUS_99;
            if (!$deleteLesson->save()) {
                $delete = false;
            }
            $delete ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $delete = false;
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $delete;
    }

    /**
     * Delete lesson and payment of Trainer.
     *
     * @return mixed
     */
    public function deleteLessonAndPaymentOfTrainer($dataLessons, $deleted_id = null)
    {
        $result = true;
        if (count($dataLessons)) {
            foreach ($dataLessons as $lesson) {
                $dataPlayerLessons = $this->playLesson
                    ->where('player_lessons.lesson_id', $lesson->id)
                    ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_99)
                    ->get();
                if (count($dataPlayerLessons)) {
                    foreach ($dataPlayerLessons as $playerLessons) {
                        $dataSettlement = $this->settlement
                            ->where('settlements.player_lesson_id', $playerLessons->id)
                            ->where('settlements.status', '!=', SETTLEMENT_99)
                            ->get();
                        if (count($dataSettlement)) {
                            foreach ($dataSettlement as $settlement) {
                                $dataPayment = $this->payment
                                    ->where('payments.settlement_id', $settlement->id)
                                    ->where('payments.status', '!=', PAYMENT_STATUS_99)
                                    ->get();
                                if (!$this->deleteOtherTableLesson($dataPayment, $this->payment, $deleted_id)) {
                                    $result = false;
                                }
                            }
                        }
                        if (!$this->deleteOtherTableLesson($dataSettlement, $this->settlement, $deleted_id)) {
                            $result = false;
                        }
                    }
                }
                if (!$this->deleteOtherTableLesson($dataPlayerLessons, $this->playLesson, $deleted_id)) {
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * save delete data lesson of others table.
     *
     * @param array $data
     * @param $tableName
     *
     * @return bool
     */
    public function deleteOtherTableLesson($data, $tableName, $deleteId = null)
    {
        $result = true;
        if (count($data)) {
            foreach ($data as $value) {
                $table = $tableName->find($value['id']);
                $table->status = LESSON_STATUS_99;
                $table->deleted_id = $deleteId;
                $table->updated_id = $deleteId;
                if (!$table->save()) {
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * Count data lessons;.
     *
     * @return array $data
     */
    public function countDataOfLessons()
    {
        $data = [];
        $lessonOfMonth = $this->listLessonOfTrainer()->whereMonth('lessons.created_at', date('m'))->whereYear('lessons.created_at', date('Y'));
        $data['lesson_registration'] = $lessonOfMonth->count();
        $data['lesson_old_register'] = $this->listLessonOfTrainer()->whereNotIn('lessons.id', $lessonOfMonth->pluck('lessons.id'))->count();

        return $data;
    }

    public function listLessonOfTrainer()
    {
        $login = Auth::guard('trainers')->user();

        return $this->lesson
            ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->where(function ($query) use ($login) {
                $query->where([
                    'lessons.info_type' => INFO_TYPE_TRAINER,
                    'trainers.id' => $login->id,
                ]);
            });
    }

    public function getNumbersLessonOneYear()
    {
        $login = Auth::guard('trainers')->user();
        $data = [];
        for ($i = 1; $i < 13; ++$i) {
            $lessons = $this->lesson
                ->join('trainers', 'trainers.id', '=', 'lessons.table_id')
                ->where([
                    'lessons.is_deleted' => UNDELETED,
                ])
                ->whereMonth('lessons.created_at', $i)
                ->whereYear('lessons.created_at', date('Y'))
                ->where(function ($query) use ($login) {
                    $query->where([
                        'lessons.info_type' => INFO_TYPE_TRAINER,
                        'trainers.id' => $login->id,
                    ])
                        ->orWhere(function ($q) use ($login) {
                            $q->where([
                                'lessons.info_type' => INFO_TYPE_TRAINER,
                                'trainers.id' => $login->id,
                            ]);
                        });
                })
                ->count();
            array_push($data, $lessons);
        }

        return $data;
    }

    public function getPlayerLessonById($lesson_id)
    {
        $trainer = Auth::guard('trainers')->user();
        $dataPlayer = $this->playLesson
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('players', 'players.id', '=', 'player_lessons.player_id')
            ->leftJoin('chat_rooms', 'chat_rooms.player_id', '=', 'players.id')
            ->leftJoin('lesson_flags', 'lesson_flags.player_lesson_id', '=', 'player_lessons.id')
            ->where([
                'player_lessons.lesson_id' => $lesson_id,
                'player_lessons.is_deleted' => UNDELETED,
                'lessons.table_id' => $trainer->id,
                'lessons.info_type' => INFO_TYPE_TRAINER,
            ])
            ->select(
                'lessons.name as lesson_name',
                'players.name as player_name',
                'lessons.quantity_player',
                'lessons.levels as level_id',
                'player_lessons.created_at',
                'player_lessons.updated_at',
                'lessons.id as lesson_id',
                'players.id as player_id',
                'player_lessons.status as pl_status',
                'player_lessons.id as pl_id',
                'player_lessons.refund_rate',
                'player_lessons.refund_point',
                'player_lessons.approve_date',
                'lesson_flags.flag as pl_flag'
            );

        return $dataPlayer->distinct('chat_rooms.player_id')->sortable(['created_at' => 'DESC']);
    }

    /**
     * Get settlement by player_lesson_id.
     *
     * @param $playerLessonId
     *
     * @return mixed
     */
    public function updateStatusPlayerLesson(PlayerLesson $playerLesson, Players $player = null, int $playerLessonId)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($playerLesson->save()) {
                if ($player) {
                    $player->save();
                    if (!$player->save()) {
                        $error = false;
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * Cancel player lesson.
     *
     * @return bool
     */
    public function cancelPlayerLesson(PlayerLesson $playerLesson, Settlements $settlement = null, Players $player, $payments = null)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($player->attributesToArray() != null) {
                $player->save();
                if ($playerLesson->save()) {
                    if ($settlement) {
                        if ($settlement->save()) {
                            if (count($payments)) {
                                foreach ($payments as $payment) {
                                    $payment->save();
                                    if (!$payment->save()) {
                                        $error = false;
                                    }
                                }
                            }
                        } else {
                            $error = false;
                        }
                    }
                } else {
                    $error = false;
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * Get payment by settlement id.
     *
     * @param $settlementId
     *
     * @return mixed
     */
    public function getPaymentBySettId($settlementId)
    {
        return $this->payment
            ->where([
                'payments.settlement_id' => $settlementId,
            ])
            ->get();
    }

    /**
     * Get settlement by player_lesson_id.
     *
     * @param $playerLessonId
     *
     * @return mixed
     */
    public function getSettlementByPlayerLessonId($playerLessonId)
    {
        return $this->settlement
            ->where([
                'settlements.player_lesson_id' => $playerLessonId,
            ])
            ->first();
    }

    /**
     * Get player lesson.
     *
     * @param $playerLessonId
     *
     * @return mixed
     */
    public function getPlayerLesson($playerLessonId)
    {
        return $this->playLesson
            ->where([
                'player_lessons.id' => $playerLessonId,
            ])
            ->first();
    }

    /**
     * update status by player_lesson_id.
     *
     * @param $playerLessonId
     *
     * @return mixed
     */
    public function rejectPlayerLesson(PlayerLesson $playerLesson, Players $player = null, int $playerLessonId)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($playerLesson->save()) {
                if ($player) {
                    $player->save();
                    if (!$player->save()) {
                        $error = false;
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * cancel reject.
     *
     * @return mixed
     */
    public function cancelReject(PlayerLesson $playerLesson)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$playerLesson->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * Get lesson by id.
     *
     * @param $lessonId
     *
     * @return mixed
     */
    public function getLessonById($lessonId)
    {
        $lesson = $this->lesson
            ->with('image')
            ->select(
                'lessons.*',
            )
            ->where([
                'lessons.id' => $lessonId,
                'lessons.is_deleted' => UNDELETED,
            ])->first();

        return $lesson;
    }

    /**
     * Get lesson by id.
     *
     * @return mixed
     */
    public function getPlayerById($playerId)
    {
        $player = $this->player
            ->where([
                'players.id' => $playerId,
                'players.is_deleted' => UNDELETED,
            ])->first();

        return $player;
    }

    /**
     * get player lesson by lesson id.
     *
     * @return repository
     */
    public function getPlayerLessonByLessonId(int $idLesson)
    {
        return $this->playLesson
            ->where('player_lessons.lesson_id', $idLesson)
            ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_99)
            ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_3)
            ->get();
    }

    /**
     * save message in popup.
     *
     * @param entity $chatMessage
     *
     * @return bool
     */
    public function saveMessagePopUp(ChatMessages $chatMessage)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$chatMessage->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * save message in popup.
     *
     * @param entity $lessonFlag
     *
     * @return bool
     */
    public function saveLessonFlag(LessonFlags $lessonFlag)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$lessonFlag->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * save player to chat room.
     *
     * @param entity $chatRooms
     *
     * @return bool
     */
    public function saveOneToOneChat($chatRooms)
    {
        $chat_room_id = null;
        $error = true;
        DB::beginTransaction();
        try {
            if ($chatRooms->save()) {
                $chat_room_id = $chatRooms->id;
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $chat_room_id;
    }

    /**
     * get player lesson by player lesson id.
     *
     * @param $idPlayerLesson
     *
     * @return array $arr
     */
    public function getPlayerLessonByIdPL($idPlayerLesson)
    {
        return $this->playLesson
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('players', 'players.id', '=', 'player_lessons.player_id')
            ->select(
                'lessons.name as lessonName',
                'players.name as playerName',
                'player_lessons.created_at',
                'player_lessons.approve_date',
                'lessons.levels',
                'lessons.register_expire_date',
                'lessons.lesson_date',
                'lessons.times_study'
            )
            ->where('player_lessons.id', $idPlayerLesson)
            ->first();
    }

    /**
     * Get chat room participant by player_id and chat room id.
     *
     * @return mixed
     */
    public function getChatRoomParticipant($room_id)
    {
        return $this->playLesson
            ->where([
                'id' => $room_id,
            ])
            ->first();
    }

    public function getChatRoom($player_lesson_id)
    {
        $chatRoom = $this->chatRooms->where([
            'player_lesson_id' => $player_lesson_id,
        ])->first();
        if ($chatRoom) {
            return $chatRoom->id;
        } else {
            return null;
        }
    }
}
