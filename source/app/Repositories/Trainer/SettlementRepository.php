<?php

namespace App\Repositories\Trainer;

use App\Models\Settlements;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Auth;

class SettlementRepository extends TennisMgtRepository
{
    protected $settlement;

    public function __construct(Settlements $settlement)
    {
        $this->settlement = $settlement;
    }

    /**
     * Get list settlement.
     *
     * @return mixed
     */
    public function getListSettlement($search)
    {
        $trainer = Auth::guard('trainers')->user();
        $settle = $this->settlement
        ->leftJoin('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
        ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
        ->leftJoin('players', 'players.id', '=', 'player_lessons.player_id')
        ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
        ->select(
            'settlements.payment_date',
            'settlements.sett_price as price',
            'players.name as player_name',
            'lessons.name as lesson_name',
            'lessons.category as lessonCategory',
            'lessons.payment_fee as fee',
            'lessons.status as lessonStatus',
            'players.id as player_id'
         )
         ->where([
            'settlements.is_deleted' => UNDELETED,
            'lessons.info_type' => INFO_TYPE_TRAINER,
            'trainers.id' => $trainer->id,
         ])
         ->where('settlements.status', '!=', SETTLEMENT_99);
        //  if(!empty($search))
        //  {
        //      $settle = $this->searchSettlement($settle, $search);
        //  }
        $limit = isset($params['limit']) ? (int) $params['limit'] : PAGINATE_LIMIT;
        $offset = isset($params['page']) ? ($params['page'] * $limit) - $limit : '';
        $total = $settle->count();
        $last_page = (int) ceil($total / $limit);
        $settle->orderBy('settlements.updated_at', 'DESC');

        return ['data' => $settle->get(), 'total' => $total, 'last_page' => $last_page];
    }

    public function searchSettlement($settle, $search = null)
    {
        if (!empty($search)) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d', strtotime($startDate));
                } else {
                    return $settle->where('settlements.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d', strtotime($endDate));
                } else {
                    return $settle->where('settlements.id', null);
                }
            }
            if ($startDate && $endDate) {
                $settle = $settle->whereBetween('settlements.payment_date', [$startDate.' 00:00:00', $endDate.' 23:59:59']);
            } elseif ($startDate && !$endDate) {
                $startDate = date('Y-m-d', strtotime($search['search_startDate']));
                $settle = $settle->where('settlements.payment_date', '>=', $startDate.' 00:00:00');
            } elseif (!$startDate && $endDate) {
                $endDate = date('Y-m-d', strtotime($search['search_endDate']));
                $settle = $settle->where('settlements.payment_date', '<=', $endDate.' 23:59:59');
            }
            if ($namePlayer = $search['search_name']) {
                $settle->where('players.name', 'LIKE', "%{$namePlayer}%");
            }
        }

        return $settle;
    }
}
