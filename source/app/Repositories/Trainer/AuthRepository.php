<?php



namespace App\Repositories\Trainer;


use App\Repositories\TennisMgtRepository;
use App\Models\Trainers;
/**
 * Description of AuthRepository
 *
 */
class AuthRepository extends TennisMgtRepository{

    protected $trainer;

    public function __construct( Trainers $trainer ) {
            $this->trainer = $trainer;
    }

    /**
     * Get data trainer
     *
     * @param $data
     * @return mixed
     */
    public function getDataTrainer($data) {

            $trainer = $this->trainer
                    ->leftJoin('schools','schools.id','=','trainers.school_id')
                    ->where([
                            'trainers.email' => $data['email'],
                            'trainers.is_deleted' => UNDELETED,
                            'trainers.status' => $data['status'],
                            'schools.status' => $data['status']])
                    ->select(
                    [
                        'trainers.status',
                        'trainers.license_date',
                        'trainers.email',
                        'schools.license_date as school_date',
                        'schools.status as school_status',
                    ])
                    ->first();

        return $trainer;
    }

}
