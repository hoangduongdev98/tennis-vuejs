<?php
namespace App\Repositories\Player;

use App\Models\Lessons;
use App\Models\PlayerLesson;
use App\Models\Trainers;
use App\Models\ChatRooms;
use App\Models\ChatMessages;
use App\Models\StructureFee;
use App\Models\MasterLevels;
use App\Models\Images;
use App\Models\Settlements;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;

class PlayerTrainerRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $trainer;
    protected $structureFee;
    protected $lesson;
    protected $level;
    protected $image;
    protected $chatRooms;
    protected $chatMessage;

    public function __construct(
        Trainers $trainer,
        StructureFee $structureFee,
        MasterLevels $level,
        Images $image,
        ChatRooms $chatRooms,
        ChatMessages $chatMessage
    )
    {
        $this->trainer = $trainer;
        $this->structureFee = $structureFee;
        $this->level = $level;
        $this->image = $image;
        $this->chatRooms = $chatRooms;
        $this->chatMessage = $chatMessage;
    }

    /**
     * Get list trainer
     *
     * @param array $search
     * @return mixed
     *
     */
    public function getListTrainer ($search = null) {
        $lists = $this->trainer
                ->join('schools', 'schools.id', '=', 'trainers.school_id')
                ->select(
                    'trainers.id',
                    'trainers.name',
                    'trainers.prefecture_id',
                    'trainers.levels',
                    'schools.name as schoolName',
                    'trainers.day_off',
                    'trainers.start_time',
                    'trainers.end_time',
                    'trainers.note',
                    'trainers.achievement',
                    'trainers.highlight'
                )
                ->where('trainers.status', '!=', SCHOOL_STATUS_0)
                ->where('trainers.status', '!=', SCHOOL_STATUS_99)
                ->where('trainers.status', '!=', SCHOOL_STATUS_2)
                ->where('schools.status', '!=', SCHOOL_STATUS_2)
                ->where('schools.status', '!=', SCHOOL_STATUS_99)
                ->where([
                    'schools.find_school' => FIND_SCHOOL_1,
                    'schools.is_deleted' => UNDELETED,
                    'trainers.is_deleted' => UNDELETED,
                ]);
        if (!empty($search)) {
            $lists = $this->searchTrainer($lists, $search);
        }
        $lists = $lists->get();
        return $lists;
    }

    /**
     * Get data trainer for player after search
     *
     * @param $data
     * @param array $search
     * @return mixed
     */
    public function searchTrainer ($data, array $search) {
        if (!empty($search)) {
            $listTrainer = $data->get();
            //search_prefectures
            if ($idPrefecture = $search['search_prefectures']) {
                $data = $data->where('trainers.prefecture_id', '=', $idPrefecture);
            }

            if ($searchLevel = $search['search_level']) {
                $arrId = [];
                if (count($listTrainer)) {
                    foreach ($listTrainer as $trainers) {
                        if ($trainers['levels']) {
                            $arrLevel = json_decode($trainers['levels'], true);
                            if (in_array($searchLevel, $arrLevel)) {
                                array_push($arrId, $trainers['id']);
                            }
                        }
                    }
                }
                $data = $data->whereIn('trainers.id', $arrId);
            }

            $startPrice = isset($search['search_startPrice']) && $search['search_startPrice']!==null ? $search['search_startPrice'] : null;
            $endPrice = isset($search['search_endPrice']) && $search['search_endPrice']!==null  ? $search['search_endPrice'] : null;
            $arrTrainerId = $arrStructureFee = [];
            if ($startPrice!==null && $endPrice!==null && $startPrice > $endPrice) {
                $data = $data->whereIn('trainers.id', $arrTrainerId);
            } else {
                if (count($listTrainer)) {
                    foreach ($listTrainer as $trainer) {
                        $dataStructureFee = $this->getStructureFeeOfTrainer($trainer['id']);
                        if ($trainerFee = $this->getArrayStructureFee($dataStructureFee)) {
                            $arrStructureFee[$trainer['id']] = $trainerFee;
                        }
                    }
                    if (!empty($arrStructureFee)) {
                        foreach ($arrStructureFee as $key => $value) {
                            if ($startPrice !== null && $endPrice !== null) {
                                if (is_array($value)) {
                                    if (($value[0] <= $endPrice) && ($value[1] >= $startPrice)) {
                                        array_push($arrTrainerId, $key);
                                    }
                                } else {
                                    if ($this->searchOnRange($value, $startPrice, $endPrice)) {
                                        array_push($arrTrainerId, $key);
                                    }
                                }
                            } elseif ($startPrice !== null && $endPrice === null) {
                                if (is_array($value)) {
                                    if ($value[1] >= $startPrice) {
                                        array_push($arrTrainerId, $key);
                                    }
                                } else {
                                    if ($value >= $startPrice) {
                                        array_push($arrTrainerId, $key);
                                    }
                                }
                            } elseif ($startPrice === null && $endPrice !== null) {
                                if (is_array($value)) {
                                    if ($value[0] <= $endPrice) {
                                        array_push($arrTrainerId, $key);
                                    }
                                } else {
                                    if ($value <= $endPrice) {
                                        array_push($arrTrainerId, $key);
                                    }
                                }
                            }
                        }
                    }
                }
                if ($startPrice!==null || $endPrice!==null) {
                    $data = $data->whereIn('trainers.id', $arrTrainerId);
                }
            }

            if ($filter = $search['filter']) {
                if ($filter == LESSON_DATE_DESC) {
                    $data = $data->orderBy('trainers.created_at', 'DESC');
                } elseif ($filter == LESSON_DATE_ASC) {
                    $data = $data->orderBy('trainers.created_at', 'ASC');
                } elseif ($filter == LESSON_PRICE_DESC) {
                    $arrTrainerId = $this->sortDataByStructurefee($listTrainer, 'desc');
                    if (!empty($arrTrainerId)) {
                        $ids_ordered = implode(',', $arrTrainerId);
                        $data = $data->whereIn('trainers.id', $arrTrainerId)->orderByRaw(DB::raw("FIELD(trainers.id, $ids_ordered)"));
                    }
                } elseif ($filter == LESSON_PRICE_ASC) {
                    $arrTrainerId = $this->sortDataByStructurefee($listTrainer, 'asc');
                    if (!empty($arrTrainerId)) {
                        $ids_ordered = implode(',', $arrTrainerId);
                        $data = $data->whereIn('trainers.id', $arrTrainerId)->orderByRaw(DB::raw("FIELD(trainers.id, $ids_ordered)"));
                    }
                } elseif ($filter == NEARBY_AREA) {
                    $arrIdTrainerSameArea = $arrIdTrainerDiffArea = [];
                    $dataPlayerLogin = Auth::guard('players')->user();
                    $idArea = $dataPlayerLogin->area;
                    if (count($listTrainer)) {
                        foreach ($listTrainer as $trainer) {
                            if ($trainer['prefecture_id'] == $idArea) {
                                array_push($arrIdTrainerSameArea, $trainer['id']);
                            } else {
                                array_push($arrIdTrainerDiffArea, $trainer['id']);
                            }
                        }
                    }
                    if ($arrIdTrainerSameArea) {
                        $arrIdTrainer = array_merge($arrIdTrainerSameArea, $arrIdTrainerDiffArea);
                    } else {
                        $arrIdTrainer = $arrIdTrainerDiffArea;
                    }
                    $ids_ordered = implode(',', $arrIdTrainer);
                    $data = $data->whereIn('trainers.id', $arrIdTrainer)->orderByRaw(DB::raw("FIELD(trainers.id, $ids_ordered)"));
                }
            }
        }
        return $data;
    }

    /**
     * Sort data trainer by structure fee
     *
     * @param array $listTrainer
     * @param string $type
     * @return array $trainerId
     *
     */
    public function sortDataByStructurefee ($listTrainer, $type) {
        $trainerId = [];
        $arrTrainerStructures = $arrTemp = [];
        $arrTrainerNoFee = $arrTrainerHasFee = [];
        if (count($listTrainer)) {
            foreach ($listTrainer as $trainer) {
                $dataStructure = $this->getStructureFeeOfTrainer($trainer['id']);
                $arrStructures = $this->getArrayStructureFee($dataStructure);
                if (!empty($arrStructures)) {
                    $arrTrainerStructures[$trainer['id']] = $arrStructures;
                } else {
                    array_push($arrTrainerNoFee, $trainer['id']);
                }
            }
            if (!empty($arrTrainerStructures)) {
                $type == 'desc' ? arsort($arrTrainerStructures) : asort($arrTrainerStructures);
                foreach ($arrTrainerStructures as $key => $value) {
                    if (is_array($value)) {
                        $temp = $value[0];
                    } else {
                        $temp = $value;
                    }
                    $arrTemp[$key] = $temp;
                }
            }
            if ($arrTemp) {
                if ($type == 'asc') {
                    asort($arrTemp);
                    $arrTrainerHasFee = array_keys($arrTemp);
                    !empty($arrTrainerNoFee) ? $trainerId = array_merge($arrTrainerNoFee, $arrTrainerHasFee) : $trainerId = $arrTrainerHasFee;
                }
                if ($type == 'desc') {
                    arsort($arrTemp);
                    $arrTrainerHasFee = array_keys($arrTemp);
                    !empty($arrTrainerHasFee) ? $trainerId = array_merge($arrTrainerHasFee, $arrTrainerNoFee) : $trainerId = $arrTrainerNoFee;
                }
            }
        }
        return $trainerId;
    }

    /**
     * Player register trainer by trainer id
     *
     * @param int $trainerId
     * @param array $dataPost
     * @param int $loginId
     * @return bool
     */
    public function registerTrainerById($trainerId, $dataPost, $loginId) {
        $return = true;
        $message = $dataPost['message'];
        DB::beginTransaction();
        try {
            // Create chat_rooms
            $chatRoom = $this->chatRooms
                        ->where([
                            'chat_rooms.player_id' => $loginId
                        ])
                        ->first();
            if (!$chatRoom) {
                $chatRoom = new ChatRooms();
                $chatRoom->table_id = $trainerId;
                $chatRoom->info_type = INFO_TYPE_TRAINER;
                $chatRoom->player_id = $loginId;
            } else {
                $chatRoom->is_deleted = UNDELETED;
            }
            if ($chatRoom->save()) {
                $newChat = new ChatMessages();
                $newChat->chat_room_id = $chatRoom->id;
                $newChat->message = $message;
                $newChat->from = $loginId;
                $newChat->to = $trainerId;
                $newChat->info_type = INFO_TYPE_MESSAGE_PLAYER;
                $newChat->unread_message = UNREAD_MESSAGE;
                if (!$newChat->save()) {
                    $return = false;
                }
            } else {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }

    /**
     * Get list master levels
     *
     * @return mixed
     *
     */
    public function listLevels () {
        return $this->level->where('master_levels.is_deleted', UNDELETED)->get();
    }

    /**
     * Get structure fee of trainer
     *
     * @return mixed
     */
    public function getStructureFeeOfTrainer ($trainerId) {
        return $this->structureFee->where([
                    'structure_fee.table_id' => $trainerId,
                    'structure_fee.info_type' => INFO_TYPE_TRAINER,
                    'structure_fee.is_deleted' => UNDELETED
                ])->get();
    }

    /**
     * Get image of trainer by id trainer
     *
     * @param int $lessonId
     * @return mixed
     *
     */
    public function findImageForTrainerById ($trainerId) {
        return $this->image->where([
                    'images.table_id' => $trainerId,
                    'images.info_types' => INFO_TYPE_TRAINER
                ])
                ->get();
    }

    /**
     * Get array min, max structure fee of trainer
     *
     * @param array $dataStructureFee
     * @return array $arrStructureFee
     *
     */
    public function getArrayStructureFee ($dataStructureFee) {
        $arrStructureFee = [];
        if (count($dataStructureFee)) {
            foreach ($dataStructureFee as $fee) {
                array_push($arrStructureFee, $fee['fee']);
            }
        }
        if (count($arrStructureFee) > 1) {
            $arrStructureFee = [min($arrStructureFee), max($arrStructureFee)];
        } elseif (count($arrStructureFee) == 1) {
            $arrStructureFee = $arrStructureFee[0];
        } else {
            $arrStructureFee = null;
        }
        return $arrStructureFee;
    }

    /**
     * Get trainer by id
     *
     * @param $trainerId
     * @return mixed
     */
    public function getTrainerById ($trainerId) {
        return $this->trainer
                ->leftJoin('schools','schools.id','=','trainers.school_id')
                ->where([
                    'trainers.id' => $trainerId
                ])
                ->select('trainers.*','schools.name as schoolName')
                ->first();
    }

    /**
     * Get id chat room has request
     *
     * @return int $chatRoomId
     */
    public function getIdChatRoomOfTrainer ($trainerId) {
        $chatRoomId = null;
        $dataChatRoom = $this->chatRooms
                ->where([
                    'chat_rooms.player_id' => Auth::guard('players')->user()->id
                ])
                ->first();
        if ($dataChatRoom) {
            $chatRoomId = $dataChatRoom->id;
        }
        return $chatRoomId;
    }

    /**
     * Check a number in range or not
     *
     * @param int $number
     * @param int $min
     * @param int $max
     * @return bool
     */
    public function searchOnRange($number, $min, $max) {
        if (($number >= $min) && ($number <= $max)) {
            $result = true;
        } else {
            $result = false;
        }
        return $result;
    }
}
