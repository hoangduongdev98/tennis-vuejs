<?php

namespace App\Repositories\Player;

use App\Models\ChatMessages;
use App\Models\Images;
use App\Models\LessonFlags;
use App\Models\Lessons;
use App\Models\MasterLevels;
use App\Models\Payments;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\Schools;
use App\Models\Settlements;
use App\Models\Trainers;
use App\Repositories\TennisMgtRepository;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlayerLessonRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $lesson;
    protected $playerLesson;
    protected $player;
    protected $level;
    protected $image;
    protected $trainer;
    protected $lesson_flag;
    protected $chatMessage;
    protected $settlement;
    protected $payment;

    public function __construct(
        Lessons $lesson,
        PlayerLesson $playerLesson,
        LessonFlags $lesson_flag,
        Players $player,
        MasterLevels $level,
        Images $image,
        Settlements $settlement,
        Payments $payment,
        Trainers $trainer,
        Schools $school,
        ChatMessages $chatMessages
    ) {
        $this->lesson = $lesson;
        $this->playerLesson = $playerLesson;
        $this->player = $player;
        $this->level = $level;
        $this->image = $image;
        $this->settlement = $settlement;
        $this->payment = $payment;
        $this->trainer = $trainer;
        $this->school = $school;
        $this->lesson_flag = $lesson_flag;
        $this->chatMessages = $chatMessages;
    }

    /**
     * Get list lesson.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function getListLesson($search = null)
    {
        $now = date('Y-m-d');
        $data = $this->lesson->with(['lesson_school:*', 'lesson_trainer.trainer_school:*'])
                ->select('lessons.*')
                ->where([
                    'lessons.is_deleted' => UNDELETED,
                    'lessons.status' => LESSON_STATUS_1,
                ])
                ->where(function ($query) {
                    $query->whereHas('lesson_schools', function ($query2) {
                        $query2->where('register_lesson', REGISTER_LESSON_1);
                    });
                    $query->orWhereHas('lesson_trainers.trainer_school', function ($query3) {
                        $query3->where('register_lesson', REGISTER_LESSON_1);
                        $query3->where('is_deleted', UNDELETED);
                        $query3->where('status', SCHOOL_STATUS_1);
                    });
                })
                ->orderBy('id', 'DESC')
                ->orderBy('updated_at', 'DESC');
            $data->where(function($q) use ($now) {
                $q->where('lessons.datetime_lesson', '>=', $now)
                    ->orWhereNull('lessons.datetime_lesson');
            });
            $data->where(function($q1) use ($now) {
                $q1->where('lessons.register_expire_date', '>=', $now)
                    ->orWhereNull('lessons.register_expire_date');
            });
        if (!empty($search)) {
            $data = $this->searchLesson($data, $search);
        }

        return $data->get();
    }

    /**
     * Get array id lesson has register by player.
     *
     * @return array $arr
     */
    public function getArrayIdLessonHasRegister()
    {
        $arr = [];
        $dataPlayerLesson = $this->playerLesson->where([
            'player_lessons.player_id' => Auth::guard('players')->user()->id,
            'player_lessons.is_deleted' => UNDELETED,
        ])->get();
        if (count($dataPlayerLesson)) {
            foreach ($dataPlayerLesson as $dataLesson) {
                array_push($arr, $dataLesson->lesson_id);
            }
        }

        return $arr;
    }

    /**
     * Get array id lesson was not searched by school.
     *
     * @return array $arrId
     */
    public function getArrayIdLessonNoFindBySchool()
    {
        $arrId = [];
        $dataLessonSchools = $this->lesson->join('schools', 'schools.id', '=', 'lessons.table_id')
            ->select('lessons.id')
            ->where([
                'lessons.info_type' => INFO_TYPE_SCHOOL,
                'schools.register_lesson' => REGISTER_LESSON_0,
                'lessons.is_deleted' => UNDELETED,
            ])
            ->whereNotIn('lessons.status', [LESSON_STATUS_2, LESSON_STATUS_99])
            ->get();
        if (count($dataLessonSchools)) {
            foreach ($dataLessonSchools as $dataLesson) {
                array_push($arrId, $dataLesson->id);
            }
        }

        return $arrId;
    }

    /**
     * Get status of player_lesson after user register.
     *
     * @param int $lessonId
     *
     * @return $status
     */
    public function getStatusLessonAfterRegister($lessonId)
    {
        $status = null;
        $dataPlayerLesson = $this->playerLesson->where([
            'player_lessons.lesson_id' => $lessonId,
            'player_lessons.player_id' => Auth::guard('players')->user()->id,
            'player_lessons.is_deleted' => UNDELETED,
        ])->first();
        if ($dataPlayerLesson) {
            $status = $dataPlayerLesson->status;
        }

        return $status;
    }

    /**
     * Get array id lesson was created by trainer in school not searched.
     *
     * @return array $arrId
     */
    public function arrNoFindLessonOfTrainers()
    {
        $arrId = [];
        $dataLessonTrainers = $this->lesson
            ->join('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->join('schools', 'schools.id', '=', 'trainers.school_id')
            ->select('lessons.id')
            ->where([
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'schools.register_lesson' => REGISTER_LESSON_0,
                'lessons.is_deleted' => UNDELETED,
            ])
            ->whereNotIn('lessons.status', [LESSON_STATUS_2, LESSON_STATUS_99])
            ->get();
        if (count($dataLessonTrainers)) {
            foreach ($dataLessonTrainers as $dataLesson) {
                array_push($arrId, $dataLesson->id);
            }
        }

        return $arrId;
    }

    /**
     * Get data lesson for player after search.
     *
     * @param $data
     *
     * @return mixed
     */
    public function searchLesson($data, array $search)
    {
        if (!empty($search)) {
            if ($searchCategory = $search['search_category']) {
                $data = $data->where('lessons.category', $searchCategory);
            }
            if ($searchLevel = $search['search_level']) {
                $arrId = [];
                $listLesson = $data->get();
                if (count($listLesson)) {
                    foreach ($listLesson as $lessons) {
                        if ($lessons['levels']) {
                            $arrLevel = json_decode($lessons['levels'], true);
                            if (in_array($searchLevel, $arrLevel)) {
                                array_push($arrId, $lessons['id']);
                            }
                        }
                    }
                }
                $data = $data->whereIn('lessons.id', $arrId);
            }
            if (isset($search['search_freeWord'])) {
                $freeWord = $search['search_freeWord'];
                $data = $data->where(function ($query) use ($freeWord) {
                    $query->where('lessons.name', 'LIKE', "%$freeWord%")
                        ->orWhere('lessons.lesson_detail', 'LIKE', "%$freeWord%")
                        ->orWhereHas('lesson_school', function ($query2) use ($freeWord) {
                            $query2->where('name', 'LIKE', "%$freeWord%");
                        })
                        ->orWhereHas('lesson_trainer', function ($query3) use ($freeWord) {
                            $query3->where('name', 'LIKE', "%$freeWord%");
                        });
                });
            }
            $minPrice = $search['search_minPrice'];
            $maxPrice = $search['search_maxPrice'];
            if ($minPrice && $maxPrice) {
                $data = $data->whereBetween('lessons.payment_fee', [$minPrice, $maxPrice]);
            } elseif ($minPrice && !$maxPrice) {
                $data = $data->where('lessons.payment_fee', '>=', $minPrice);
            } elseif (!$minPrice && $maxPrice) {
                $data = $data->where('lessons.payment_fee', '<=', $maxPrice);
            }
            if ($filter = $search['filter']) {
                if ($filter == LESSON_DATE_DESC) {
                    $data = $data->reorder('lessons.updated_at', 'DESC')->orderBy('lessons.id', 'DESC');
                } elseif ($filter == LESSON_DATE_ASC) {
                    $data = $data->reorder('lessons.updated_at', 'ASC')->orderBy('lessons.id', 'ASC');
                } elseif ($filter == LESSON_PRICE_DESC) {
                    $data = $data->reorder('lessons.payment_fee', 'DESC')->orderBy('lessons.id', 'DESC');
                } elseif ($filter == LESSON_PRICE_ASC) {
                    $data = $data->reorder('lessons.payment_fee', 'ASC')->orderBy('lessons.id', 'DESC');
                } elseif ($filter == NEARBY_AREA) {
                    $arrIdLessonSameArea = $arrIdLessonDiffArea = [];
                    $userLogin = Auth::guard('players')->user();
                    $areaPlayer = $userLogin['area'];
                    $listLessons = $data->get();
                    if (count($listLessons)) {
                        foreach ($listLessons as $lesson) {
                            $idAreaCreatorLesson = null;
                            if ($lesson['info_type'] == INFO_TYPE_TRAINER) {
                                $trainerId = getLessonCreator($lesson['id'], INFO_TYPE_TRAINER);
                                if ($trainerId) {
                                    $dataTrainer = Trainers::find($trainerId);
                                    $idAreaCreatorLesson = $dataTrainer->prefecture_id;
                                }
                            } elseif ($lesson['info_type'] == INFO_TYPE_SCHOOL) {
                                $schoolId = getLessonCreator($lesson['id'], INFO_TYPE_SCHOOL);
                                if ($schoolId) {
                                    $dataSchool = Schools::find($schoolId);
                                    $idAreaCreatorLesson = $dataSchool->prefecture_id;
                                }
                            }
                            if ($idAreaCreatorLesson == $areaPlayer) {
                                array_push($arrIdLessonSameArea, $lesson['id']);
                            } else {
                                array_push($arrIdLessonDiffArea, $lesson['id']);
                            }
                        }
                    }
                    if ($arrIdLessonSameArea) {
                        $arrIdLesson = array_merge($arrIdLessonSameArea, $arrIdLessonDiffArea);
                    } else {
                        $arrIdLesson = $arrIdLessonDiffArea;
                    }
                    if (!empty($arrIdLesson)) {
                        $ids_ordered = implode(',', $arrIdLesson);
                        $data = $data->whereIn('lessons.id', $arrIdLesson);
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Get list master levels.
     *
     * @return mixed
     */
    public function listLevels()
    {
        return $this->level->where('master_levels.is_deleted', UNDELETED)->get();
    }

    /**
     * Get all level.
     *
     * @return $levels
     */
    public function getAllLevel()
    {
        $list = $this->level->select(
            'master_levels.id',
            'master_levels.name'
        )
            ->where('master_levels.is_deleted', UNDELETED)
            ->orderBy('display_order', 'asc')
            ->get();

        return $list;
    }

    /**
     * Count number player has register lesson by id.
     *
     * @param int $lessonId
     *
     * @return mixed
     */
    public function countPlayerHasRegisterLesson($lessonId)
    {
        return $this->playerLesson
            ->where([
                'player_lessons.lesson_id' => $lessonId,
                'player_lessons.is_deleted' => UNDELETED,
            ])
            ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_99)
            ->count();
    }

    /**
     * Get image of lesson by id.
     *
     * @param int $lessonId
     *
     * @return mixed
     */
    public function findImageForLessonById($lessonId)
    {
        return $this->image->where([
            'images.table_id' => $lessonId,
            'images.info_types' => INFO_TYPE_LESSON,
        ])
            ->first();
    }

    /**
     * Player register lesson by lesson id.
     *
     * @param int $lessonId
     * @param int $loginId
     *
     * @return bool
     */
    public function registerLessonById($lessonId, $loginId)
    {
        $return = true;
        DB::beginTransaction();
        try {
            $dataPlayerLesson = $this->playerLesson->where([
                'player_lessons.lesson_id' => $lessonId,
                'player_lessons.player_id' => $loginId,
                'player_lessons.is_deleted' => UNDELETED,
            ])->first();
            if ($dataPlayerLesson) {
                $dataPlayerLesson->status = LESSON_PLAYER_STATUS_0;
                if (!$dataPlayerLesson->save()) {
                    $return = false;
                }
            } else {
                $createPlayerLesson = new PlayerLesson();
                $createPlayerLesson->player_id = $loginId;
                $createPlayerLesson->lesson_id = $lessonId;
                $createPlayerLesson->refunded = REFUNDED;
                $createPlayerLesson->status = LESSON_PLAYER_STATUS_0;
                if (!$createPlayerLesson->save()) {
                    $return = false;
                }
            }
            $return ? DB::commit() : DB::rollback();

            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);

            return false;
        }
    }

    /**
     * Get data lesson by id.
     *
     * @param int $lessonId
     *
     * @return mixed
     */
    public function getLessonById($lessonId)
    {
        $lesson = Lessons::with(['lesson_trainer:id,name,school_id', 'lesson_school:id,name', 'image', 'lesson_trainer.trainer_school:id,name', 'lesson_player'])
                ->select('lessons.*')
                ->where(['lessons.id' => $lessonId, 'is_deleted' => UNDELETED])
                ->where(function ($query) {
                    $query->whereHas('lesson_trainer', function ($query2) {
                        $query2->where('lessons.info_type', '=', INFO_TYPE_TRAINER);
                    });
                    $query->orWhereHas('lesson_school', function ($query3) {
                        $query3->where('lessons.info_type', '=', INFO_TYPE_SCHOOL);
                    });
                })
            ->first();

        return $lesson;
    }

    /**
     * Get lesson has register by player.
     *
     * @return array $arr
     */
    public function getLessonHistory()
    {
        $history = $this->playerLesson->with(['lesson', 'lesson.lesson_trainer'])
            ->where([
                'player_lessons.player_id' => Auth::guard('players')->user()->id,
            ])->orderBy('player_lessons.created_at', 'desc')->get();

        return $history;
    }

    /**
     * Get flag by player_lessons_id.
     *
     * @return array $arr
     */
    public function getFlagByPlayerLessonId($player_lessons_id)
    {
        return $this->lesson_flag->where([
            'player_lesson_id' => $player_lessons_id,
        ])
            ->first();
    }

    /**
     * Get email by trainer_id.
     *
     * @return array $arr
     */
    public function getEmailByTrainerId($trainer_id)
    {
        $dataTrainer = $this->trainer->find($trainer_id);

        return $dataTrainer;
    }

    /**
     * Get email by school_id.
     *
     * @return array $arr
     */
    public function getEmailBySchoolId($school_id)
    {
        $dataSchool = $this->school
        ->join('staffs', 'staffs.school_id', '=', 'schools.id')
        ->where('staffs.school_id', '=', $school_id)
        ->where('staffs.login_type', '=', ACCOUNT_TYPE_1)
        ->first();

        return $dataSchool;
    }

    /**
     * save update flag.
     *
     * @return array $arr
     */
    public function saveUpdateLessonFlag(LessonFlags $lessonFlags, $player_lessons)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($lessonFlags->save()) {
                if ($player_lessons) {
                    if (!$player_lessons->save()) {
                        $error = false;
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * save update flag.
     *
     * @return array $arr
     */
    public function saveUpdateFlag(PlayerLesson $playerLesson)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($playerLesson) {
                $playerLesson->save();
                if (!$playerLesson->save()) {
                    $error = false;
                }
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * save update point.
     *
     * @return array $arr
     */
    public function savePointPlayer($playerLesson, $player)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($playerLesson->save()) {
                if ($player) {
                    if (!$player->save()) {
                        $error = false;
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::rollback();
        }

        return $error;
    }

    /**
     * save message in popup.
     *
     * @return bool
     */
    public function saveMessagePopUp(ChatMessages $chatMessages)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$chatMessages->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * Get settlement by player_lesson_id.
     *
     * @param $playerLessonId
     *
     * @return mixed
     */
    public function getSettlementByPlayerLessonId($playerLessonId)
    {
        return $this->settlement
            ->where([
                'settlements.player_lesson_id' => $playerLessonId,
            ])
            ->first();
    }

    /**
     * Get payment by settlement id.
     *
     * @param $settlementId
     *
     * @return mixed
     */
    public function getPaymentBySettId($settlementId)
    {
        return $this->payment
            ->where([
                'payments.settlement_id' => $settlementId,
            ])
            ->get();
    }

    /**
     * Get settlement by player_lesson_id.
     *
     * @return mixed
     */
    public function updateStatusSettlement(Settlements $settlement = null, $payments = null, $lesson = null, $manage_id = null)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($settlement) {
                $settlement->save();
                if (!$settlement->save()) {
                    $error = false;
                } else {
                    if (count($payments)) {
                        foreach ($payments as $payment) {
                            $payment->updated_id = $manage_id;
                            $payment->save();
                            if (!$payment->save()) {
                                $error = false;
                            }
                        }
                    } else {
                        if ($lesson->info_type == INFO_TYPE_TRAINER) {
                            $trainers = Trainers::find($manage_id);
                            if ($trainers) {
                                $schools = Schools::find($trainers->school_id);
                                $trainerPaymentPoint = $settlement->sett_price * ($trainers['ratio'] / 100) * ($schools['ratio'] / 100);
                                $schoolPaymentPoint = ($schools['ratio'] / 100) * $settlement->sett_price;
                            }
                            $newTrainerPayment = new Payments();
                            $newTrainerPayment->payment_date = date('Y-m-d');
                            $newTrainerPayment->settlement_id = $settlement->id;
                            $newTrainerPayment->info_type = INFO_TYPE_TRAINER;
                            $newTrainerPayment->payment_point = $trainerPaymentPoint;
                            $newTrainerPayment->status = PAYMENT_STATUS_0;
                            if ($newTrainerPayment->save()) {
                                $newSchoolPayment = new Payments();
                                $newSchoolPayment->payment_date = date('Y-m-d');
                                $newSchoolPayment->settlement_id = $settlement->id;
                                $newSchoolPayment->info_type = INFO_TYPE_SCHOOL;
                                $newSchoolPayment->payment_point = $schoolPaymentPoint;
                                $newSchoolPayment->status = PAYMENT_STATUS_0;
                                if (!$newSchoolPayment->save()) {
                                    $error = false;
                                }
                            } else {
                                $error = false;
                            }
                        } else {
                            $schoolPaymentPoint = 0;
                            $arrPaymentSchool = [];
                            if ($lesson->info_type == INFO_TYPE_SCHOOL) {
                                $schools = Schools::find($manage_id);
                                if ($schools) {
                                    $schoolPaymentPoint = ($schools['ratio'] / 100) * $settlement->sett_price;
                                }
                                $arrPaymentSchool = [
                                    'settlement_id' => $settlement->id,
                                    'info_type' => INFO_TYPE_SCHOOL,
                                    'payment_point' => $schoolPaymentPoint,
                                    'status' => PAYMENT_STATUS_0,
                                ];
                                $arrPaymentSchool['payment_date'] = date('Y-m-d');
                            }
                            $this->payment->create($arrPaymentSchool);
                        }
                    }
                }
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }
}
