<?php
namespace App\Repositories\Player;

use App\Models\Players;
use App\Models\Prefectures;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class AuthPlayerRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $player, $prefs;

    public function __construct(Players $player, Prefectures $prefs)
    {
        $this->player = $player;
        $this->prefs = $prefs;
    }

    /**
     * Create player
     *
     * @param entity $player
     * @return bool
     */
    public function saveCreatePlayer (Players $player) {
        $error = true;
        DB::beginTransaction();
        try{
            if (!$player->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
            return $player;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Create player
     *
     * @param array $data
     * @return bool
     */
    public function saveResetPasswordPlayer ($data) {
        $error = true;
        DB::beginTransaction();
        try{
            $dataPlayer = $this->player
                        ->where([
                            'players.email' => $data['email'],
                            'players.is_deleted' => UNDELETED
                        ])
                        ->first();
            if ($dataPlayer) {
                $dataPlayer->password = Hash::make($data['password']);
                if (!$dataPlayer->save()) {
                    $error = false;
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
            return $dataPlayer;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Get list prefectures
     *
     * @return mixed
     */
    public function listPrefectures () {
        return $this->prefs->all();
    }


    /**
     * Create player
     *
     * @param entity $player
     * @return bool
     */
    public function saveEditInfoPlayer ($data) {
        $result = true;
        DB::beginTransaction();
        try{
            $editPlayer = $this->player->find($data['id']);
            if ($editPlayer) {
                $editPlayer->name = $data['name'];
                $editPlayer->area = $data['area'];
                $editPlayer->email = $data['email'];
                $password = $data['password'];
                if (Hash::check($password, $data['old_password'])) {
                    $editPlayer->password = $data['old_password'];
                } else {
                    $editPlayer->password = Hash::make($password);
                }
                if (!$editPlayer->save()) {
                    $result = false;
                }
            } else {
                return false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }
    
    /**
     * save time for player's last login
     *
     * @param array $params
     * @return bool
     */
    public function lastLogin (array $params) 
    {
        DB::beginTransaction();
        try {
            $player = $this->player->find($params['id']);
            $res = $player->update($params);
            DB::commit();
            return $res;
        } catch (\Exception $e) {
            DB::rollBack();
            report($e);
            return $e->getMessage();
        }
    }
}
