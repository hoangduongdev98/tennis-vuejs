<?php

namespace App\Repositories\Player;

use App\Repositories\TennisMgtRepository;
use App\Models\ChatRooms;
use App\Models\ChatMessages;
use App\Models\Schools;
use App\Models\Trainers;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\SendMailQueue;
use Exception;


class ChatRoomRepository extends TennisMgtRepository
{
    protected $chatRooms;
    protected $chatMessage;
    protected $school;
    protected $trainer;
    protected $sendMail;
    
    public function __construct(
        ChatRooms $chatRooms,
        ChatMessages $chatMessage,
        Schools $school,
        Trainers $trainer,
        SendMailQueue $sendMail
    ) {
        $this->chatRooms = $chatRooms;
        $this->chatMessage = $chatMessage;
        $this->school = $school;
        $this->trainer = $trainer;
        $this->sendMail = $sendMail;
    }
    /**
     * get list chat history
     * @return array $history
     */
    public function getChatHistory()
    {
        $chatHistory = $this->chatRooms
            ->leftJoin('players', 'chat_rooms.player_id', '=', 'players.id')
            ->leftJoin('lessons', 'lessons.id', '=', 'chat_rooms.lesson_id')
            ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
            ->leftJoin('chat_messages as chat1', 'chat_rooms.id', '=', 'chat1.chat_room_id')
            ->leftJoin('chat_messages as chat2', function ($join) {
                $join->on('chat1.chat_room_id', '=', 'chat2.chat_room_id')->whereRaw(DB::raw('chat1.created_at < chat2.created_at'));
            })    
            ->select(
                'chat_rooms.*', 
                'schools.name as schoolName',
                'trainers.name as trainerName',
                'lessons.name as lessonName',
                'lessons.info_type as lessonType',
                'chat1.unread_message as chatStatus',
                'chat1.from as chatFrom',
                'chat1.info_type as lastType',
                'chat1.message as lastMess',
                'chat1.created_at as lastChatTime',
            )
            ->whereNull('chat2.chat_room_id')
            ->where('lessons.status', '<>', LESSON_STATUS_99)
            ->where([
                'chat_rooms.player_id' => Auth::guard('players')->user()->id,
                'chat_rooms.is_deleted' => UNDELETED,
            ])->distinct('players.id')->orderBy('chat1.created_at', 'DESC')->get();
        return $chatHistory;
    }
    
    /**
     * Get unread message by id participant and info type
     *
     * @param $idParticipant
     * @return mixed
     */
    public function getUnreadMessageById($idParticipant)
    {
        $unread = $this->chatMessage
            ->where([
                'chat_room_id' => $idParticipant,
                'unread_message' => UNREAD_MESSAGE,
                'is_deleted' => UNDELETED
            ])
            ->where('info_type', '!=', INFO_TYPE_MESSAGE_PLAYER)
            ->orderBy('created_at', 'DESC')
            ->get();
            
        return $unread;
    }
    
    /**
     * 
     * @param type $idParticipant
     * @param type $unreadMessages
     * @return type
     */
    public function getMessageById($idParticipant, $unreadMessages = null)
    {
        if(count($unreadMessages))
        {
            foreach ($unreadMessages as $unread)
            {
                $editMessage = $this->chatMessage->find($unread->id);
                $editMessage->unread_message = READ_MESSAGE;
                $editMessage->save();
            }
        }
        $trainerQuery = $this->trainer
                            ->leftJoin('schools','schools.id','=','trainers.school_id')
                            ->select(
                                'trainers.id',
                                'trainers.name',
                                'trainers.status',
                                'schools.status as schoolStatus'
                            );
        $result = $this->chatMessage
            ->leftJoin('chat_rooms', 'chat_rooms.id', '=', 'chat_messages.chat_room_id')
            ->leftJoin('players', 'players.id', '=', 'chat_rooms.player_id')
            ->leftJoin('lessons','lessons.id','=','chat_rooms.lesson_id')
            ->leftJoin('schools','schools.id','=','lessons.table_id')
            ->leftJoin('player_lessons','player_lessons.id','=','chat_rooms.player_lesson_id')
            ->leftJoinSub(
                $trainerQuery,
                'trainer',
                function ($join) {
                    $join->on('trainer.id', '=', 'lessons.table_id');
                }
            )
            ->where('chat_messages.chat_room_id', '=', $idParticipant)
            ->select(
                'chat_rooms.id',
                'chat_messages.created_at as messageTime',
                'chat_messages.from',
                'chat_messages.message',
                'chat_messages.to',
                'chat_messages.info_type as infoType',
                'chat_rooms.lesson_id as lessonId',
                'chat_rooms.player_lesson_id',
                'player_lessons.updated_at as pl_updated_at',
                'player_lessons.is_deleted as pl_deleted',
                'player_lessons.status as player_lesson_status',
                'players.id as playerId',
                'lessons.info_type as roomType',
                'lessons.status as lessonStt',
                'schools.id as schoolId',
                'schools.name as schoolName',
                'schools.status as schoolStt',
                'trainer.id as trainerId',
                'trainer.name as trainerName',
                'trainer.status as trainerStt',
                'trainer.schoolStatus as trainerSchStt',
            )
            ->get();
        
        return $result;
    }
    
    /**
     * Save message has been sent
     * @return mixed
     */
    public function saveMessage(ChatMessages $chatMessage)
    {
        $error = true;
        DB::beginTransaction();
        try{
            if ($chatMessage->save()) {
                $sendMail = new $this->sendMail;
                $sendMail->id_mess = $chatMessage->id;
                if (!$sendMail->save()) {
                    $error = false;
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
            return $chatMessage;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * get chat room
     * 
     * @return mixed
     */
    public function getChatRoom($roomId) 
    {
        $chatRoom = $this->chatRooms
                ->leftJoin('lessons','lessons.id','=','chat_rooms.lesson_id')
                ->select(
                    'chat_rooms.*',
                    'lessons.status as lessonStt'
                )
                ->where('chat_rooms.id', $roomId)
                ->first();
        return $chatRoom;
    }
}
