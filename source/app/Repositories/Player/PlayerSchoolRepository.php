<?php
namespace App\Repositories\Player;

use App\Models\Schools;
use App\Models\Images;
use App\Models\MasterLevels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;

class PlayerSchoolRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $schools;
    protected $images;
    protected $level;

    public function __construct(
        Schools $schools,
        Images $images,
        MasterLevels $level
    )
    {
        $this->schools = $schools;
        $this->images = $images;
        $this->level = $level;
    }

    /**
     * Get list trainer
     *
     * @param array $search
     * @return mixed
     *
     */
    public function getListSchools ($search = null) {
        $data = Schools::select(
                    'schools.id',
                    'schools.name',
                    'schools.address',
                    'schools.day_off',
                    'schools.note',
                    'schools.status',
                    'schools.find_school'
                )
                ->where([
                    'status' => SCHOOL_STATUS_1,
                    'find_school' => FIND_SCHOOL_1,
                    'is_deleted' => UNDELETED
                ])
                ->orderBy('id', 'DESC')
                ->orderBy('updated_at', 'DESC');
        if (!empty($search)) {
            $data = $this->searchSchools($data, $search);
        }
        return $data->get();
    }

    /**
     * Get data school for player after search
     *
     * @param $data
     * @param array $search
     * @return mixed
     */
    public function searchSchools ($data, array $search) 
    {
        if ($search['search_prefectures']) {
            $data = $data->where('prefecture_id', '=', $search['search_prefectures']);
        }
        if (isset($search['search_freeWord'])) {
            $freeWord = $search['search_freeWord'];
            $data = $data->where(function ($query) use($freeWord) {
                        $query->where('name', 'LIKE', "%$freeWord%")
                            ->orWhere('address', 'LIKE', "%$freeWord%")
                            ->orWhere('phone', 'LIKE', "%$freeWord%")
                            ->orWhere('representative', 'LIKE', "%$freeWord%")
                            ->orWhere('highlight', 'LIKE', "%$freeWord%")
                            ->orWhere('achievement', 'LIKE', "%$freeWord%")
                            ->orWhereHas('school_trainers_player', function($query) use($freeWord) {
                                $query->where('name', 'LIKE', "%$freeWord%")
                                    ->orWhere('highlight', 'LIKE', "%$freeWord%")
                                    ->orWhere('achievement', 'LIKE', "%$freeWord%");
                            });
                    });
        }
        if ($search['filter']) {
            $filter = $search['filter'];
            if ($filter == SCHOOL_DATE_DESC) {
                $data = $data->reorder('updated_at', 'DESC')->orderBy('id', 'DESC');
            } elseif ($filter == SCHOOL_DATE_ASC) {
                $data = $data->reorder('updated_at', 'ASC')->orderBy('id', 'ASC');
            }  elseif ($filter == SCHOOL_NEARBY_AREA) {
                $dataPlayerLogin = Auth::guard('players')->user();
                $idArea = $dataPlayerLogin->area;
                $data = $data->where('prefecture_id', '=', $idArea);
            }
        }
        return $data;
    }
    
    /**
     * get data detail of school
     * @param type $id
     * @return type
     */
    public function getSchoolById($id) 
    {
        return Schools::with(['school_trainers' => function ($query) {
            $query->where('is_deleted', UNDELETED);
            $query->where('status', SCHOOL_STATUS_1);
        }])->find($id);
    }
    
    /**
     * Get list master levels
     *
     * @return mixed
     *
     */
    public function listLevels () {
        return MasterLevels::where('master_levels.is_deleted', UNDELETED)->get();
    }
}
