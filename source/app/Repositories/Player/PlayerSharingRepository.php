<?php
namespace App\Repositories\Player;

use App\Models\Players;
use App\Models\Trainers;
use App\Models\Schools;
use App\Models\Staffs;
use App\Models\SharingAnalyses;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class PlayerSharingRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $player, $school, $trainer, $sharingAnalyses;

    public function __construct(
        Players $player,
        Schools $school,
        Staffs $staffs,
        Trainers $trainer,
        SharingAnalyses $sharingAnalyses
    ) {
        $this->player = $player;
        $this->school = $school;
        $this->staffs = $staffs;
        $this->trainer = $trainer;
        $this->sharingAnalyses = $sharingAnalyses;
    }

    /**
     * Get list lesson
     *
     * @param array $search
     * @return mixed
     */
    public function getListSharing () 
    {
        if (Auth::guard('players')->user()) {
            $player_id = Auth::guard('players')->user()->id;
        }
        $data = SharingAnalyses::with(['school:id,name', 'trainer.trainer_school:id,name'])
            ->select('sharing_analyses.*')
            ->where('sharing_analyses.player_id', $player_id)->where(function ($query) {
                $query->whereHas('school', function ($query2) {
                        $query2->whereNotIn('contract_id', [CONTRACT_VALUE_0,CONTRACT_VALUE_3]);
                });
                $query->orWhereHas('trainer.trainer_school', function ($query3) {
                    $query3->where('is_deleted', UNDELETED);
                    $query3->where('status', SCHOOL_STATUS_1);
                    $query3->whereNotIn('contract_id', [CONTRACT_VALUE_0,CONTRACT_VALUE_3]);
                });
            })         
            ->orderByRaw("updated_at DESC, id DESC")->get();
        return $data;
    }
    
    /**
     * Get school/coach for register share analyses
     * @param type $search
     */
    public function searchShare(array $search)
    {
        $player_id = Auth::guard('players')->user()->id;
        $data = [];
        if (!empty($search)) {
            if ($search['search_shareType'] && $search['search_shareType'] == 1 && $search['search_email']) { //search school with email staff
                return $this->getSchool($search['search_email'], $player_id);
            }elseif($search['search_shareType'] && $search['search_shareType'] == 2 && $search['search_email']) { //search trainer with email
                return $this->getTrainer($search['search_email'], $player_id);
            } else {
                return false;
            }
        }
    }
    
    /**
     * get school via email and check exits in share table
     * @param type $email
     * @param type $player_id
     * @return boolean|int
     */
    protected function getSchool($email, $player_id)
    {
        $data = Staffs::with(['staff_schools' => function ($query) {
            $query->select('id', 'name');
            $query->where('is_deleted', UNDELETED);
            $query->where('status', SCHOOL_STATUS_1);
            $query->whereNotIn('contract_id', [CONTRACT_VALUE_0,CONTRACT_VALUE_3]);
        }])
        ->select('staffs.id', 'staffs.school_id','staffs.email')
        ->where('staffs.email', $email)
        ->where('staffs.is_deleted', UNDELETED)
        ->first();
        if($data && $data->staff_schools){ //check data school not null because condition
            if(SharingAnalyses::where('school_id', $data->school_id)->where('player_id', $player_id)->exists()) { // check already share analysis
                $data['already'] = 1;
            }
            return $data;
        } else {
            return false;
        }
    }
    
    /**
     * get trainer via email and check exits in share table
     * @param type $email
     * @param type $player_id
     * @return boolean|int
     */
    protected function getTrainer($email, $player_id)
    {
        $data = Trainers::with(['trainer_school' => function ($query) {
            $query->select('id', 'name');
            $query->where('is_deleted', UNDELETED);
            $query->where('status', SCHOOL_STATUS_1);
            $query->whereNotIn('contract_id', [CONTRACT_VALUE_0,CONTRACT_VALUE_3]);
        }])
        ->select('trainers.id', 'trainers.school_id', 'trainers.email', 'trainers.name')
        ->where('trainers.email', $email)
        ->where('trainers.is_deleted', UNDELETED)
        ->where('trainers.status', SCHOOL_STATUS_1)
        ->first();
        if($data && $data->trainer_school){ //check data school not null because condition
            if(SharingAnalyses::where('trainer_id', $data->id)->where('player_id', $player_id)->exists()) { // check already share analysis
                $data['already'] = 1;
            }
            return $data;
        } else {
            return false;
        }
    }
    
    /**
     * Save register share
     * @return mixed
     */
    public function saveShareAnalysis (SharingAnalyses $sharingAnalyses)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$sharingAnalyses->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
            return $sharingAnalyses;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }
    
    /**
     * delete share
     * @param type $data
     * @return type
     */
    public function deleteShare($data)
    {
        if (SharingAnalyses::where('id', $data['id'])->exists()) {
            return SharingAnalyses::destroy($data['id']);
        } else {
            return false;
        }
    }
}
