<?php
namespace App\Repositories\Player;

use App\Models\SettingSettlementFee;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Exception;

class AddPointRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $settingSettlementFee;

    public function __construct(SettingSettlementFee $settingSettlementFee)
    {
        $this->settingSettlementFee = $settingSettlementFee;
    }

    /**
     * Get all fee to add point
     *
     * @return mixed
     */
    public function getAllFeeAddPoint () {
        return $this->settingSettlementFee->where('setting_settlement_fee.is_deleted', UNDELETED)->get();
    }
}
