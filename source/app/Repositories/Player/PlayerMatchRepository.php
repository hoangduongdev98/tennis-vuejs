<?php

namespace App\Repositories\Player;

use App\Repositories\TennisMgtRepository;
use App\Models\Matches;
use App\Models\Trainers;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;


class PlayerMatchRepository extends TennisMgtRepository
{
    protected $matches;
    protected $trainer;

    public function __construct(
        Matches $matches,
        Trainers $trainer
    ) {
        $this->matches = $matches;
        $this->trainer = $trainer;
    }

    public function getMatchListHistory($search = null)
    {
        $created_id = -1;
        if (Auth::guard('players')->user()) {
            $created_id = Auth::guard('players')->user()->id;
        }
        $matchHistory = $this->matches
            ->where([
                'matches.created_id' => $created_id,
                'matches.is_deleted' => UNDELETED
            ])
            ->orderBy('created_at', 'DESC')
            ->orderBy('id', 'DESC');
        if (!empty($search)) {
            $matchHistory = $this->searchMatchHistory($matchHistory, $search);
        }
        return $matchHistory->get();
    }

    /**
     * Get data lesson for player after search
     *
     * @param $data
     * @param array $search
     * @return mixed
     */
    public function searchMatchHistory($data, array $search)
    {
        if (!empty($search)) {
            if ($search['search_startDate']) {
                $startDate = date('Y-m-d',  strtotime($search['search_startDate']));
                $data = $data->whereBetween('matches.created_at', [$startDate . ' 00:00:00', $startDate . ' 23:59:59']);
            }
        }
        return $data;
    }

    /**
     * Get match by id
     *
     * @param int $id
     * @return mixed
     */
    public function getMatchInfo($id)
    {
        $created_id = -1;
        if (Auth::guard('players')->user()) {
            $created_id = Auth::guard('players')->user()->id;
            return $this->matches->join('players', 'matches.created_id', '=', 'players.id')
                ->select([
                    'matches.*',
                    'players.name as player_name',
                    'players.id as player_id'
                ])
                ->where([
                    'matches.id' => $id,
                    'matches.created_id' => $created_id,
                    'matches.is_deleted' => UNDELETED
                ])
                ->first();
        } else {
            return $this->matches
                ->select([
                    'matches.*'
                ])
                ->where([
                    'matches.id' => $id,
                    'matches.created_id' => $created_id,
                    'matches.is_deleted' => UNDELETED
                ])
                ->first();
        }
    }

    /**
     * Save message has been sent
     * @return mixed
     */
    public function saveMatch(Matches $matches)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$matches->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
            return $matches;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Get trainer by player id 
     * @return mixed
     */
    public function getTrainerByPlayerId($winnerMax, $loserMax, $missMax, $serve1Max, $serve2Max)
    {
        $info = '';
        if ($loserMax == 'advice_return') {
            $info = ADVICE_RETURN;
        } elseif ($loserMax == 'advice_fore_hand') {
            $info = ADVICE_FOREHAND;
        } elseif ($loserMax == 'advice_back_hand') {
            $info = ADVICE_BACKHAND;
        } elseif ($loserMax == 'advice_smash') {
            $info = ADVICE_SMASH;
        } else {
            $info = ADVICE_VOLLEY;
        }
        $trainer = $this->trainer
            ->join('schools', 'trainers.school_id', '=', 'schools.id')
            ->join('winner_setting', 'winner_setting.created_id', '=', 'trainers.id')
            ->join('miss_setting', 'miss_setting.created_id', '=', 'trainers.id')
            ->join('setting_course', 'setting_course.created_id', '=', 'trainers.id')
            ->join('serve_1st_advice', 'serve_1st_advice.created_id', '=', 'trainers.id')
            ->join('serve_2nd_advice', 'serve_2nd_advice.created_id', '=', 'trainers.id')
            ->select([
                'trainers.name as trainerName',
                'trainers.id as trainerId',
                'schools.name as schoolName',
                'schools.contract_id as schoolContract',
                'winner_setting.' . $winnerMax . ' as winner_advice',
                'miss_setting.' . $loserMax . ' as miss_advice',
                'serve_1st_advice.' . $serve1Max . ' as serve1_advice',
                'serve_2nd_advice.' . $serve2Max . ' as serve2_advice',
                'setting_course.info_type as course_info',
                'setting_course.' . $missMax . ' as course_advice',
                'serve_1st_advice.point_rate_adv_content_1 as serve1_content1',
                'serve_1st_advice.point_rate_adv_content_2 as serve1_content2',
                'serve_1st_advice.point_rate_adv_content_3 as serve1_content3',
                'serve_2nd_advice.point_rate_adv_content_1 as serve2_content1',
                'serve_2nd_advice.point_rate_adv_content_2 as serve2_content2',
                'serve_2nd_advice.point_rate_adv_content_3 as serve2_content3',
                'serve_1st_advice.point_rate_in_serve_left_1 as serve1_point_left1',
                'serve_1st_advice.point_rate_in_serve_left_2 as serve1_point_left2',
                'serve_1st_advice.point_rate_in_serve_right_1 as serve1_point_right1',
                'serve_1st_advice.point_rate_in_serve_right_2 as serve1_point_right2',
                'serve_2nd_advice.point_rate_in_serve_left_1 as serve2_point_left1',
                'serve_2nd_advice.point_rate_in_serve_left_2 as serve2_point_left2',
                'serve_2nd_advice.point_rate_in_serve_right_1 as serve2_point_right1',
                'serve_2nd_advice.point_rate_in_serve_right_2 as serve2_point_right2',
            ])
            ->where([
                'trainers.is_deleted' => UNDELETED,
                'setting_course.info_type' => $info
            ])
            ->where('trainers.status', '!=', SCHOOL_STATUS_99)
            ->where('schools.status', '!=', SCHOOL_STATUS_99)
            ->where('trainers.status', '!=', SCHOOL_STATUS_2)
            ->where('schools.status', '!=', SCHOOL_STATUS_2)
            ->distinct('trainerId')->get();
        return $trainer;
    }
}
