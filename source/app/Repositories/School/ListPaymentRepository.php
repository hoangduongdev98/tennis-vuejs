<?php

namespace App\Repositories\School;

use App\Models\Payments;
use App\Models\Trainers;
use App\Repositories\TennisMgtRepository;
use Auth;
use DB;

/**
 * class ListPaymentRepository.
 */
class ListPaymentRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $payment;
    protected $trainer;
    protected $listSettlementRepository;

    public function __construct(
        Payments $payment,
        Trainers $trainer,
        ListSettlementRepository $listSettlementRepository
    ) {
        $this->listSettlementRepository = $listSettlementRepository;
        $this->payment = $payment;
        $this->trainer = $trainer;
    }

    /**
     * all payments.
     *
     * @return mixed
     */
    public function listPayment()
    {
        $staff = Auth::guard('schools')->user();
        $listPayment = $this->payment->leftJoin('settlements', 'settlements.id', '=', 'payments.settlement_id')
                                    ->leftJoin('player_lessons', 'settlements.player_lesson_id', '=', 'player_lessons.id')
                                    ->leftJoin('lessons', 'player_lessons.lesson_id', '=', 'lessons.id')
                                    ->leftJoin('trainers', 'lessons.table_id', '=', 'trainers.id')
                                    ->leftJoin('players', 'player_lessons.player_id', '=', 'players.id')
                                    ->select(
                                            'payments.*',
                                            'trainers.name as trainerName',
                                            'lessons.name as lessonName',
                                            'players.name as playerName',
                                            'trainers.id as trainer_id',
                                            'settlements.id as settlementId',
                                            'players.id as player_id',
                                            'lessons.category')
                                    ->where([
                                        'payments.info_type' => INFO_TYPE_TRAINER,
                                        'lessons.info_type' => INFO_TYPE_TRAINER,
                                        'trainers.school_id' => $staff->school_id,
                                        'trainers.is_deleted' => UNDELETED,
                                    ]);

        return $listPayment;
    }

    /**
     * Get list payments.
     *
     * @param $search
     *
     * @return mixed
     */
    public function getListPayment($search = null)
    {
        $list = $this->listPayment()
                    ->addSelect(
                            'payments.payment_date as paymentDate',
                            DB::raw('SUM(payments.payment_point) AS total'),
                            DB::raw('YEAR(payments.payment_date) year, MONTH(payments.payment_date) month')
                            )
                    ->groupBy('trainers.name', 'year', 'month');
        if (!empty($search)) {
            $list = $this->searchListPayment($list, $search);
            if ($search['search_status'] != SCHOOL_STATUS_99) {
                $list = $list->where('payments.status', '!=', PAYMENT_STATUS_99);
            }
        } else {
            $list = $list->where('payments.status', '!=', PAYMENT_STATUS_99);
        }

        return $list->sortable(['paymentDate' => 'DESC']);
    }

    /**
     * Get data payments after search.
     *
     * @param $data
     * @param array $search
     *
     * @return mixed
     */
    public function searchListPayment($data, $search)
    {
        if (!empty($search)) {
            if ($search['search_month']) {
                $time = explode('/', $search['search_month']);
                $data = $data->whereMonth('payments.payment_date', $time[1])
                                ->whereYear('payments.payment_date', $time[0]);
            }
            if ($idTrainer = $search['search_name']) {
                $data = $data->where('trainers.id', $idTrainer);
            }
            $statusPayment = $search['search_status'];
            if ($statusPayment !== null) {
                $data = $data->where('payments.status', $statusPayment);
            }
        }

        return $data;
    }

    /**
     * Get detail payment.
     *
     * @param int $id
     * @param $date
     *
     * @return mixed
     */
    public function getPaymentDetail($id, $date)
    {
        $result = $this->listPayment()->addSelect(
                                    'payments.payment_date as paymentDate',
                                    'payments.payment_point as paymentPoint')
                        ->where('trainers.id', $id);
        if (!empty($date)) {
            $result = $this->searchPaymentDetail($result, $date);
        }

        return $result->where('payments.status', '!=', PAYMENT_STATUS_99)->sortable(['paymentDate' => 'DESC']);
    }

    /**
     * search detail payment.
     *
     * @param $data
     * @param $date
     *
     * @return mixed
     */
    public function searchPaymentDetail($data, $date)
    {
        if (!empty($date['search_month'])) {
            $time = explode('/', $date['search_month']);
            $data = $data->whereMonth('payments.payment_date', $time[1])
                        ->whereYear('payments.payment_date', $time[0]);
        }

        return $data;
    }

    /**
     * get trainer by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function getNameTrainer($id)
    {
        $trainers = $this->trainer->login()
                    ->select(
                        'trainers.id',
                        'trainers.name')
                    ->where('trainers.id', $id)
                    ->first();

        return $trainers;
    }

    /**
     * get trainer.
     *
     * @return mixed
     */
    public function getAllTrainers()
    {
        $trainers = $this->trainer->login()
                    ->select(
                        'trainers.id',
                        'trainers.name')
                    ->get();

        return $trainers;
    }

    /**
     * get settlement by trainer id.
     *
     * @param $idTrainer
     *
     * @return mixed
     */
    public function getSearchSettlementByTrainer($idTrainer)
    {
        $arr = [];
        $listSettlement = $this->listSettlementRepository->getListSettlement()->get();
        if (count($listSettlement)) {
            foreach ($listSettlement as $settlement) {
                $trainers = $this->listSettlementRepository->getListSettlement()
                        ->where([
                            'lessons.info_type' => INFO_TYPE_TRAINER,
                            'trainers.id' => $idTrainer,
                        ])
                        ->get();
                if (count($trainers)) {
                    foreach ($trainers as $trainer) {
                        array_push($arr, $trainer->id);
                    }
                }
            }
        }

        return $arr;
    }

    /**
     * get payment by trainer id.
     *
     * @return mixed
     */
    public function getDataPaymentByTrainerId(int $trainerlId, $monthYear = null)
    {
        $time = null;
        if ($monthYear) {
            $monthYear = str_replace('-', '/', $monthYear);
            $time = explode('/', $monthYear);
        }
        $arrId = $this->getSearchSettlementByTrainer($trainerlId);
        $trainerPayments = $this->listPayment()
                        ->where([
                            'payments.info_type' => INFO_TYPE_TRAINER,
                        ])
                        ->where('payments.status', '!=', PAYMENT_STATUS_99)
                        ->whereIn('settlements.id', $arrId);
        if ($time) {
            $trainerPayments = $trainerPayments
                                ->whereMonth('payments.payment_date', $time[1])
                                ->whereYear('payments.payment_date', $time[0]);
        }

        return $trainerPayments;
    }

    /**
     * edit payment status.
     *
     * @param $paymentId
     * @param $status
     * @param $userLoginId
     *
     * @return mixed
     */
    public function editPaymentStatus(
        $paymentId,
        $status,
        $userLoginId = null
    ) {
        $result = true;
        DB::beginTransaction();
        try {
            if ($editPayment = $this->payment->find($paymentId)) {
                if ($status == PAYMENT_STATUS_1) {
                    $editPayment->status = PAYMENT_STATUS_1;
                } elseif ($status == PAYMENT_STATUS_2) {
                    $editPayment->status = PAYMENT_STATUS_2;
                } elseif ($status == PAYMENT_STATUS_0) {
                    $editPayment->status = PAYMENT_STATUS_0;
                } else {
                    $editPayment->status = PAYMENT_STATUS_99;
                }
                $editPayment->updated_id = $userLoginId;
                if (!$editPayment->save()) {
                    $result = false;
                }
            }
            $result ? DB::commit() : DB::rollback();

            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);

            return false;
        }
    }

    /**
     * delete payment of school.
     *
     * @param $id
     * @param $monthYear
     * @param $type
     * @param $userLoginId
     *
     * @return mixed
     */
    public function saveDeletePaymentOfSchoolOrTrainer(
        $id,
        $monthYear,
        $type,
        $userLoginId = null
    ) {
        $delete = true;
        DB::beginTransaction();
        try {
            $monthYear = str_replace('-', '/', $monthYear);
            $time = explode('/', $monthYear);
            if ($type == INFO_TYPE_TRAINER) {
                $dataPayments = $this->getDataPaymentByTrainerId($id);
            }
            $dataPayments = $dataPayments->whereMonth('payments.payment_date', $time[1])
                            ->whereYear('payments.payment_date', $time[0])
                            ->get();
            if (count($dataPayments)) {
                foreach ($dataPayments as $data) {
                    $dataPayment = $this->payment->find($data['id']);
                    $dataPayment->status = PAYMENT_STATUS_99;
                    $dataPayment->updated_id = $userLoginId;
                    if (!$dataPayment->save()) {
                        $delete = false;
                    }
                }
            }
            $delete ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $delete = false;
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $delete;
    }

    /**
     * find payment of id.
     *
     * @param $paymentId
     *
     * @return mixed
     */
    public function findPaymentById(int $paymentId)
    {
        return $this->payment
                ->join('settlements', 'settlements.id', '=', 'payments.settlement_id')
                ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
                ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
                ->select(
                    'payments.id',
                    'payments.payment_date',
                    'payments.info_type as paymentInfoType',
                    'payments.payment_point',
                    'payments.status as paymentStatus',
                    'settlements.id as settlementId',
                    'settlements.status as settlementStatus',
                    'lessons.name as lessonName',
                    'lessons.id as lessonId',
                    'lessons.info_type as lessonType'
                )
                ->where([
                    'payments.id' => $paymentId,
                ])
                ->first();
    }

    /**
     * edit payment of id.
     *
     * @param $data
     *
     * @return bool
     */
    public function saveEditPayment($data, $userLoginId = null)
    {
        $result = true;
        DB::beginTransaction();
        try {
            $payment = $this->payment->find($data['id_payment']);
            if ($payment) {
                $payment->payment_point = $data['edit_price'];
                $payment->updated_id = $userLoginId;
                if (!$payment->save()) {
                    $result = false;
                }
            } else {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $result = false;
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $result;
    }

    /**
     * Save for delete payment.
     *
     * @param int $paymentId
     * @param $userLoginId
     *
     * @return bool
     */
    public function saveDeletePayment($paymentId, $userLoginId = null)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            $dataPayment = $this->payment->where('payments.id', $paymentId)->first();
            if ($dataPayment) {
                $dataPayment->status = PAYMENT_STATUS_99;
                $dataPayment->updated_at = $userLoginId;
                if (!$dataPayment->save()) {
                    $delete = false;
                }
            } else {
                $delete = false;
            }
            $delete ? DB::commit() : DB::rollback();

            return $delete;
        } catch (Exception $e) {
            DB::rollback();
            report($e);

            return false;
        }
    }
}
