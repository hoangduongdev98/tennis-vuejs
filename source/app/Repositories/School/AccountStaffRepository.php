<?php
namespace App\Repositories\School;

use App\Models\Staffs;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;


class AccountStaffRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $staff;
    public function __construct(Staffs $staff)
    {
        $this->staff = $staff;
    }

    /**
     * Create account manager
     * @param entity
     * @return bool
     */
    public function saveCreateAccount (Staffs $staff) 
    {
        return $staff->save();
    }

    /**
     * Edit account manager
     * @param entity
     * @return bool
     */
    public function saveEditAccount (Staffs $staff, $idAccount) 
    {
        return $staff->save();
    }

    /**
     * Get list account managers
     * @return mixed
     */
    public function getListAccountManagers () 
    {
        $staffLogin = Auth::guard('schools')->user();
        $lists = $this->staff
                ->select(
                    'staffs.id',
                    'staffs.school_manager',
                    'staffs.email',
                    'staffs.password',
                    'staffs.role',
                    'staffs.updated_at'
                )
                ->where(['staffs.is_deleted' => UNDELETED,
                        'staffs.school_id' =>  $staffLogin->school_id])
                ->whereNull('staffs.login_type')
                ->orderBy('staffs.updated_at', 'DESC');

        return $lists;
    }

    /**
     * Get data account by id
     * @param $id
     * @return mixed
     */
    public function getAccountById(int $id) 
    {
        return $this->staff->where(['staffs.id' => $id])->first();
    }

    /**
     * Get data account logon from admin site by schoold_id
     * @param $schoolId
     * @return mixed
     */
    public function getAccountLogonByShoolId(int $schoolId) 
    {
        return $this->staff->where([
            'staffs.school_id' => $schoolId,
            'staffs.login_type' => INFO_TYPE_SCHOOL,
            'staffs.role' => ROLE_1,
            'staffs.is_deleted' => UNDELETED,
        ])->first();
    }
}
