<?php


namespace App\Repositories\School;


use App\Repositories\TennisMgtRepository;
use App\Models\MasterLevels;
use App\Models\Schools;
use App\Models\Staffs;
use App\Models\Images;
use App\Models\StructureFee;
use DB;
use Exception;
/**
 * Description of EditSchoolRepository
 *
 */
class EditSchoolRepository extends TennisMgtRepository 
{
    /**
     * @var \App\Models\model
     */
    protected $level, $school, $staff, $image, $fee;
    
    public function __construct(
        MasterLevels $level, 
        Schools $school, 
        Staffs $staff,
        Images $image,
        StructureFee $fee
    ) {
        $this->level = $level;
        $this->school = $school;
        $this->staff = $staff;
        $this->image = $image;
        $this->fee = $fee;
    }
    
    /**
     * Get list level
     *
     * @return mixed
     */
    public function getAllLevel() 
    {
        $list = $this->level->select(
                'master_levels.id',
                'master_levels.name')
                ->where('master_levels.is_deleted',UNDELETED)
                ->orderBy('display_order','asc')  
                ->orderBy('updated_at','desc')             
                ->get();
        return $list;
    }
    
    /**
     * Get school by id
     *
     * @param $id 
     * @return mixed
     */
    public function getSchoolById ($id) 
    {
        return $this->school->join('staffs', 'schools.id', '=', 'staffs.school_id')
                ->where([
                    'schools.id' => $id,
                    'schools.is_deleted' => UNDELETED,
                    'staffs.login_type' => ROLE_1,
                    'staffs.is_deleted' => UNDELETED,
                ])
                ->select('schools.*','staffs.*','schools.updated_at as updated','staffs.id as staffId','schools.id as schoolId')
                ->first();
    }
    
    /**
     * Get staff by school id
     *
     * @param $idSchool 
     * @return mixed
     */
    public function findStaffBySchoolId (int $idSchool) 
    {
        return $this->staff->where([
                    'school_id' => $idSchool,
                    'is_deleted' => UNDELETED
                ])->first();
    }
    
    /**
     * Get all image by school id
     *
     * @param $idSchool 
     * @return mixed
     */
    public function findAllImageBySchoolId (int $idSchool) 
    {
        return $this->image->where([
                    'table_id' => $idSchool,
                    'info_types' => INFO_TYPE_SCHOOL,
                    'is_deleted' => UNDELETED
                ])->get();
    }
    
    /**
     * Get all structure fee by school id
     *
     * @param $idSchool 
     * @return mixed
     */
    public function getStructureFee(int $idSchool) 
    {
          return $this->fee->where([
                  'table_id' => $idSchool,
                  'info_type' => INFO_TYPE_SCHOOL,
                  'is_deleted' => UNDELETED
                  ])->get();
    }
    
    /**
     * edit school
     * 
     * @param entity $school
     * @param entity $staff
     * @param $arrImage
     * @param $idSChool
     * @return bool
     */
    public function saveEditSchool (
        Schools $school, 
        Staffs $staff , 
        $arrImage, 
        $idSChool
    ) {
        $error = true;
        DB::beginTransaction();
        try{
            if ($school->save()) {
                if ($staff->save()) {
                    if (!empty($arrImage)) {
                        foreach ($arrImage as $image) {
                            $dirTmp = AVATAR_SCHOOL_SAVE_TMP . $image['name'];
                            $dirSave = AVATAR_SCHOOL_SAVE . $image['name'];
                            if (file_exists($dirTmp)) {
                                @rename($dirTmp, $dirSave);
                            }
                            $image->table_id = $school->id;
                            if (!$image->save()) {
                                $error = false;
                            }
                        }
                    }
                    
                } else {
                    $error = false;
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }
        return $error;
    }
    
    /**
     * Get image by school
     *
     * @param $idSchool 
     * @param $position 
     * @return mixed
     */
    public function findImageBySchool (int $idSchool, $position) 
    {
        return $this->image->where([
            'position' => $position,
            'table_id' => $idSchool,
            'info_types' => INFO_TYPE_SCHOOL,
            'is_deleted' => UNDELETED
        ])->first();
    }
    
    /**
     * create or edit Structure Fee
     * 
     * @param entity $fee
     * @return bool
     */
    public function saveStructureFee (StructureFee $fee) 
    {
        $result = true;
        DB::beginTransaction();
        try {
            if (!$fee->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $result = false;
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $result;
    }
    
    /**
     * Get structure fee by id
     *
     * @param $id 
     * @return mixed
     */
    public function getStructureFeeById (int $id) 
    {
        return $this->fee->where(['structure_fee.id' => $id, 'structure_fee.is_deleted'=> UNDELETED])->first();
    }
}
