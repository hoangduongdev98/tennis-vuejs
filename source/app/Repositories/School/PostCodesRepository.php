<?php
namespace App\Repositories\School;

use App\Models\ZipCodes;
use App\Models\Prefectures;
use Illuminate\Support\Collection;
use App\Repositories\TennisMgtRepository;

class PostCodesRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $post_code;
    public function __construct(
        ZipCodes $post_code, 
        Prefectures $prefs
    ) {
        $this->post_code = $post_code;
        $this->prefs = $prefs;
    }

    /**
     * Find data via postcode
     * @param code
     * @return ZipCodes data
     */
    public function findZipCodeByCode ($code) 
    {
        return $this->post_code
                ->where(['zip_code' => $code])
                ->first();
    }

    /**
     * Get all prefectures
     *
     * @return Prefectures data
     */
    public function getAllPrefectures() 
    {
        return $this->prefs->all();
    }
}
