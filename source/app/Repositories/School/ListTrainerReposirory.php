<?php

namespace App\Repositories\School;

use App\Models\Trainers;
use App\Repositories\TennisMgtRepository;
use App\Models\Lessons;
use App\Models\Schools;
use Illuminate\Support\Facades\DB;

class ListTrainerReposirory extends TennisMgtRepository 
{
    /**
     * @var \App\Models\model
     */
    protected $trainer, $lesson, $school;

    public function __construct(
        Trainers $trainer, 
        Lessons $lesson,
        Schools $school
    ) {
        $this->trainer = $trainer;
        $this->lesson = $lesson;
        $this->school = $school;
    }

    /**
     * Get list trainers has lesson belong to school
     *
     * @param array $search
     * @return mixed
     */
    public function getListTrainerHasLesson($search = null) 
    {       
        $lists = $this->trainer->login()->leftJoin('lessons','lessons.table_id','=','trainers.id')
                                        ->leftJoin('player_lessons','player_lessons.lesson_id','=','lessons.id')
                                        ->select(
                                                'trainers.*',
                                                'trainers.updated_at as update',
                                                DB::raw('Count(distinct(lessons.id)) AS lesson_count'),
                                                DB::raw('Count(distinct(player_lessons.player_id)) as player_count'),
                                                'trainers.status as trainerStatus'
                                                )
                                        ->where('lessons.info_type', INFO_TYPE_TRAINER)
                                        ->where('lessons.is_deleted', UNDELETED)
                                        ->groupBy('trainers.id');       
        if (!empty($search)) {
            $lists = $this->searchTrainer($lists, $search);
            if ($search['search_status'] != SCHOOL_STATUS_99) {
                $lists = $lists->where('trainers.status', '!=', SCHOOL_STATUS_99);
            }
        } else {
            $lists = $lists->where('trainers.status', '!=', SCHOOL_STATUS_99);
        }
        return $lists;
    }
    
    /**
     * Get list trainers has zero lesson belong to school
     *
     * @param array $search
     * @return mixed
     */
    public function getListTrainerHasZeroLesson($search = null) 
    {
        $lists = $this->trainer->login()->select(
                                                'trainers.*',
                                                'trainers.updated_at as update',
                                                DB::raw("0 as lesson_count"),
                                                DB::raw("0 as player_count"),
                                                'trainers.status as trainerStatus')
                                        ->whereNotIn('trainers.id',$this->getListTrainerHasLesson()->pluck('trainers.id'));
        if (!empty($search)) {
            $lists = $this->searchTrainer($lists, $search);
            if ($search['search_status'] != SCHOOL_STATUS_99) {
                $lists = $lists->where('trainers.status', '!=', SCHOOL_STATUS_99);
            }
        } else {
            $lists = $lists->where('trainers.status', '!=', SCHOOL_STATUS_99);
        }
        return $lists;
    }
    
    /**
     * Get data trainers after search
     *
     * @param $data
     * @param array $search
     * @return mixed
     */
    public function searchTrainer($data, $search) 
    {     
        if (!empty($search)) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d',  strtotime($startDate));
                } else {
                    return $data->where('trainers.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d',  strtotime($endDate));
                } else {
                    return $data->where('trainers.id', null);
                }
            }
            if ($startDate && $endDate) {                
                $data = $data->whereBetween('trainers.updated_at',[$startDate. ' 00:00:00', $endDate. ' 23:59:59']);
            } elseif($startDate && !$endDate) {
                $startDate = date('Y-m-d',  strtotime($search['search_startDate']));
                $data = $data->where('trainers.updated_at', '>=' , $startDate. ' 00:00:00');
            } elseif(!$startDate && $endDate) {
                $endDate = date('Y-m-d',  strtotime($search['search_endDate']));
                $data = $data->where('trainers.updated_at', '<=' , $endDate. ' 23:59:59');
            }  
            if ($nameTrainer = $search['search_name']) {
                $data = $data->where('trainers.name', 'LIKE', "%{$nameTrainer}%");
            }
            $statusTrainer = $search['search_status'];
            if ($statusTrainer !== null) {
                $data = $data->where('trainers.status', $statusTrainer);               
            }
        }
        return $data;
    }
    
    /**
     * Get lesson belong to trainer
     *
     * @param $id
     * @return mixed
     */
    public function getTrainerLesson($id) 
    {
        $query = $this->lesson->with(['lesson_player' => function($query) {
                            $query->where(['status' => LESSON_PLAYER_STATUS_2])
                                ->orWhere(['status' => LESSON_PLAYER_STATUS_1]);
                }])
                ->where(['lessons.table_id' => $id,
                         'lessons.info_type' => INFO_TYPE_TRAINER,
                         'lessons.is_deleted' => UNDELETED
                        ]);
                        // dd($query->get());
        return $query->sortable();    
    }
    
    /**
     * Get account by trainer id
     *
     * @param $id
     * @return mixed
     */
    public function getAccountLogonByTrainerId($id) 
    {
        return $this->trainer
                    ->where([
                            'trainers.id' => $id,
                            'trainers.status' => SCHOOL_STATUS_1,
                            'trainers.is_deleted' => UNDELETED
                    ])->first();
    }
    
    /**
     * Get school by staff school_id
     *
     * @param $id
     * @return mixed
     */
    public function getSchoolByStaffLogin($id) 
    {
        return $this->school
                    ->where([
                            'schools.id' => $id,
                    ])->first();    
    }
}

