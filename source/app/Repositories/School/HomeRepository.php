<?php



namespace App\Repositories\School;


use App\Repositories\TennisMgtRepository;
use App\Models\Lessons;
use App\Models\Trainers;
use App\Models\Players;
use App\Models\Matches;
use App\Models\Settlements;
use Auth;
use App\Models\Schools;
use App\Models\SharingAnalyses;
/**
 * Description of HomeRepository
 *
 */
class HomeRepository extends TennisMgtRepository 
{
    /**
     * @var \App\Models\model
     */
    protected $lesson, $trainer, $player, $match, $settlement, $school;

    public function __construct(
        Lessons $lesson,
        Trainers $trainer,
        Players $player,
        Matches $match,
        Settlements $settlement,
        Schools $school,
        SharingAnalyses $sharingAnalyses
    ) {
        $this->lesson = $lesson;
        $this->trainer = $trainer;
        $this->player = $player;
        $this->match = $match;
        $this->settlement = $settlement;
        $this->school = $school;
        $this->sharingAnalyses = $sharingAnalyses;
    }
    
    /**
     * get list lesson of school
     *
     * @return array $data
     */
    public function listLessonOfSchool() 
    {
        $login = $this->staffLogin();
        return $this->lesson
                    ->leftJoin('trainers','trainers.id','=','lessons.table_id')
                    ->leftJoin('schools','schools.id','=','lessons.table_id')        
                    ->where(function($query) use ($login){
                        $query->where(['lessons.info_type' => INFO_TYPE_SCHOOL,
                                       'schools.id' => $login->school_id])
                                ->orWhere(function($q) use ($login){
                                    $q->where(['lessons.info_type' => INFO_TYPE_TRAINER,
                                                'trainers.school_id' => $login->school_id]);
                                });
                    });   
    }

    /**
     * Count data of lessons
     *
     * @return array $data
     */
    public function countDataOfLessons () 
    {       
        $data = [];        
        $lessonOfMonth = $this->listLessonOfSchool()->whereMonth('lessons.created_at', date('m'))->whereYear('lessons.created_at', date('Y'));
        $data['lesson_registration'] = $lessonOfMonth->count();
        $data['lesson_old_register'] = $this->listLessonOfSchool()->whereNotIn('lessons.id',$lessonOfMonth->pluck('lessons.id'))->count();
        return $data;
    }

    /**
     * Count number of lessons one year
     *
     * @return array $data
     */
    public function getNumbersLessonOneYear() 
    {
        $login = $this->staffLogin();
        $data = [];
        for ($i = 1; $i < 13; $i++) {
            $lessons = $this->lesson
                            ->join('trainers','trainers.id','=','lessons.table_id')
                            ->join('schools','schools.id','=','lessons.table_id')        
                            ->where([
                                'lessons.is_deleted' => UNDELETED
                            ])
                            ->whereMonth('lessons.created_at', $i)
                            ->whereYear('lessons.created_at', date('Y'))
                            ->where(function($query) use ($login){
                                $query->where(['lessons.info_type' => INFO_TYPE_SCHOOL,
                                               'schools.id' => $login->school_id])
                                        ->orWhere(function($q) use ($login){
                                            $q->where(['lessons.info_type' => INFO_TYPE_TRAINER,
                                                        'trainers.school_id' => $login->school_id]);
                                        });
                            })
                        ->count();
            array_push($data, $lessons);
        }
        return $data;
    }
    /**
     * Count data of trainers
     *
     * @return array $data
     */
    public function countDataOfTrainers() 
    {
        $data = [];
        $data['trainer_registration'] = $this->trainer->login()->where('trainers.status', SCHOOL_STATUS_0)->count();
        $data['in_service'] = $this->trainer->login()->where('trainers.status', SCHOOL_STATUS_1)->count();
        $data['end_service'] = $this->trainer->login()->where('trainers.status', SCHOOL_STATUS_2)->count();
        return $data;
    }
    
    /**
     * get data of player
     *
     * @return array $data
     */
    public function getDataOfPlayer() 
    {
        $staff = $this->staffLogin();
        $players = $this->player
                    ->leftJoin('player_lessons','players.id','=','player_lessons.player_id')
                    ->leftJoin('lessons','lessons.id','=','player_lessons.lesson_id')
                    ->leftJoin('trainers','trainers.id','=','lessons.table_id')
                    ->leftJoin('schools','schools.id','=','lessons.table_id')
                    ->where(function($query) use($staff){
                        $query->where(['lessons.info_type' => INFO_TYPE_SCHOOL,
                                        'schools.id' => $staff->school_id])
                                ->orWhere(function($q) use ($staff){
                                    $q->where([
                                            'lessons.info_type' => INFO_TYPE_TRAINER,
                                            'trainers.school_id' => $staff->school_id
                                            ]);
                                });        
                    })
                    ->select('players.*')
                    ->distinct('players.id');
        return $players;            
    }

    /**
     * Count data of players
     *
     * @return array $data
     */
    public function countDataOfPlayers() 
    {
        $data = [];
        $playerOfMonth = $this->getDataOfPlayer()->whereMonth('players.created_at', date('m'))->whereYear('players.created_at', date('Y'));
        $data['player_registration'] = $playerOfMonth->count();
        $data['player_old_register'] = $this->getDataOfPlayer()->whereNotIn('players.id',$playerOfMonth->pluck('players.id'))->count();
        return $data;
    }

    /**
     * get data matches of school
     *
     * @return array $data
     */
    public function getDataOfMatchesPLayer($arrPlayerId) 
    {
        $matches = $this->match
                ->whereIn('created_id', $arrPlayerId)
                ->select('matches.*')
                ->distinct('matches.id');                    
        return $matches;                
    }
    
    /**
     * Count data of matches
     *
     * @return array $data
     */
    public function countDataOfMatches () 
    {
        $dataShare = SharingAnalyses::where('sharing_analyses.school_id', $this->staffLogin()->school_id)->where('sharing_analyses.status', SHARE_STATUS_1)->get();
        $arrPlayerId = [];
        foreach($dataShare as $player) {
            $arrPlayerId[] = $player->player_id;
        }
        $dataSchool = $this->school->find($this->staffLogin()->school_id);
        $data = [];
        $data['matches_registration'] = 0;
        $dataMatchesOfMonthPlayer = $this->getDataOfMatchesPLayer($arrPlayerId)->whereMonth('matches.created_at', date('m'))->whereYear('matches.created_at', date('Y'))->where('matches.is_deleted',UNDELETED);       
        if($dataSchool->contract_id == CONTRACT_VALUE_1 || $dataSchool->contract_id == CONTRACT_VALUE_2 || $dataSchool->contract_id == CONTRACT_VALUE_4)
        {
            $data['matches_registration'] = $dataMatchesOfMonthPlayer->count();
        }
        $dataOfPlayer = $this->getDataOfMatchesPLayer($arrPlayerId)->whereNotIn('matches.id',$dataMatchesOfMonthPlayer->pluck('matches.id'))->where('matches.is_deleted',UNDELETED);
        $data['matches_old_register'] = $dataOfPlayer->count();
        return $data;
    }
    
    /**
     * get point of lesson
     *      
     * @return array $data
     */
    public function getPointOneYear() 
    {
        $staff = $this->staffLogin();
        $data = [];
        for ($i = 1; $i < 13; $i++) {
            $sum = 0;
            $settlements = $this->settlement
                        ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
                        ->join('players', 'players.id', '=', 'player_lessons.player_id')
                        ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
                        ->join('schools','lessons.table_id','=','schools.id')                   
                        ->join('trainers','lessons.table_id','=','trainers.id')
                        ->select('lessons.payment_fee as lessonFee')
                        ->whereMonth('settlements.created_at', $i)
                        ->whereYear('settlements.created_at', date('Y'))
                        ->where('settlements.status', '!=', SETTLEMENT_99)
                        ->where(function($q) use ($staff) {
                              $q->where(['trainers.school_id' => $staff->school_id])
                                ->orWhere(['schools.id' => $staff->school_id]);
                            })
                        ->get();
            if (count($settlements)) {
                foreach ($settlements as $sett) {
                    $sum += $sett['lessonFee'];
                }
            }
            array_push($data, $sum);
        }
        return $data;
    }
    
    /**
     * get login staff
     *
     * @return mixed
     */
    public function staffLogin() 
    {
        return Auth::guard('schools')->user();
    }
}
