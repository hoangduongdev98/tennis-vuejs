<?php

namespace App\Repositories\School;


use App\Models\Trainers;
use App\Repositories\TennisMgtRepository;
use App\Models\Lessons;
use App\Models\PlayerLesson;
use App\Models\Payments;
use App\Models\Settlements;
use App\Models\Images;
use DB;
use Exception;
use App\Models\Schools;

class CreateTrainerRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $image, $trainer, $lesson, $playerLesson, $settlement, $payment, $school;
    
    public function __construct(
        Images $image,
        Trainers $trainer,
        Lessons $lesson,
        PlayerLesson $playerLesson,
        Payments $payment,
        Settlements $settlement,
        Schools $school
    ) {       
        $this->image = $image;
        $this->trainer = $trainer;
        $this->lesson = $lesson;
        $this->playerLesson = $playerLesson;
        $this->payment = $payment;
        $this->settlement = $settlement;
        $this->school = $school;
    }
    /**
     * Create trainer
     * 
     * @param entity $trainer
     * @return bool
     */
    public function saveCreateTrainer (Trainers $trainer) 
    {
        $error = true;
        $trainer->save();
        if(!$trainer->save()) {
            $error = false;
        } 
        return $error;
    }
    
    /**
     * Get trainer by id
     *
     * @param $id
     * @return mixed
     */
    public function getTrainerById ($id) 
    {
        return $this->trainer
                    ->join('schools','schools.id','=','trainers.school_id')
                    ->select('trainers.*','schools.name as school_name')
                    ->where([
                        'trainers.id' => $id,
                        'trainers.is_deleted' => UNDELETED,
                    ])
                    ->first();
    }
    
    /**
     * edit trainer
     * 
     * @param entity $trainer
     * @return bool
     */
    public function saveEditTrainer (Trainers $trainer, $idTrainer) 
    {
        $error = true;
        $trainer->save();
        if(!$trainer->save()) {
            $error = false;
        } 
        return $error;
    } 
    
    /**
     * delete trainer
     * 
     * @param int $idTrainer
     * @return bool
     */
    public function deleteDataTrainer(int $idTrainer, $deleted_id = null) 
    {
        $delete = true;
        DB::beginTransaction();
        try {
            // delete school
            $deletetrainer = $this->trainer->find($idTrainer);
            $deletetrainer->status = SCHOOL_STATUS_99;
            if (!$deletetrainer->save()) {
                $delete = false;
            }
            $delete ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
            $delete = false;
            DB::rollback();          
        }

        return $delete;
    }
    
    /**
     * delete trainer other table
     * 
     * @param $datas
     * @param $tableName
     * @param $deleted_id
     * @return bool
     */
    public function deleteTrainerOfOtherTable(
        $datas, 
        $tableName, 
        $deleted_id = null
    ) {
        $result = true;
        if (count($datas)) {
            foreach ($datas as $value) {
                $table = $tableName->find($value['id']);
                $table->status = SCHOOL_STATUS_99;
                $table->deleted_id = $deleted_id;
                if (!$table->save()) {
                    $result = false;
                }
            }
        }
        return $result;
    }
    
    /**
     * delete trainer other table
     * 
     * @param $dataLessons
     * @param $deleted_id
     * @return bool
     */
    public function deleteLessonAndPaymentOfTrainer ($dataLessons, $deleted_id = null) 
    {
        $result = true;
        if (count($dataLessons)) {
            foreach ($dataLessons as $lesson) {
                $dataPlayerLessons = $this->playerLesson
                                    ->where('player_lessons.lesson_id', $lesson->id)
                                    ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_99)
                                    ->get();
                if (count($dataPlayerLessons)) {
                    foreach ($dataPlayerLessons as $playerLessons) {
                        $dataSettlement = $this->settlement
                                        ->where('settlements.player_lesson_id', $playerLessons->id)
                                        ->where('settlements.status', '!=', SETTLEMENT_99)
                                        ->get();
                        if (count($dataSettlement)) {
                            foreach ($dataSettlement as $settlement) {
                                $dataPayment = $this->payment
                                            ->where('payments.settlement_id', $settlement->id)
                                            ->where('payments.status', '!=', PAYMENT_STATUS_99)
                                            ->get();
                                if (!$this->deleteTrainerOfOtherTable($dataPayment, $this->payment, $deleted_id)) {
                                    $result = false;
                                }
                            }
                        }
                        if (!$this->deleteTrainerOfOtherTable($dataSettlement, $this->settlement, $deleted_id)) {
                            $result = false;
                        }
                    }
                }
                if (!$this->deleteTrainerOfOtherTable($dataPlayerLessons, $this->playerLesson, $deleted_id)) {
                    $result = false;
                }
            }
        }
        if (!$this->deleteTrainerOfOtherTable($dataLessons, $this->lesson, $deleted_id)) {
                $result = false;
            }
        return $result;
    }
    
    /**
     * Get school by staff school_id
     *
     * @param $id
     * @return mixed
     */
    public function getSchoolByStaffLogin($id) 
    {
        return $this->school
                    ->where([
                            'schools.id' => $id,
                    ])->first();    
    }

}
