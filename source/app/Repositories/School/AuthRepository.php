<?php



namespace App\Repositories\School;


use App\Repositories\TennisMgtRepository;
use App\Models\Schools;
use App\Models\Staffs;
/**
 * class AuthRepository
 *
 */
class AuthRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */    
    protected $school, $staff;
    
    public function __construct(
        Schools $school,
        Staffs $staff
    ) {
        $this->school = $school;
        $this->staff = $staff;        
    }
    
    /**
     * Get data school by data staff
     *
     * @param $data
     * @return mixed
     */
    public function getSchoolByStaff($data) 
    {
        $staffLogin = $this->staff
                    ->where(['staffs.email' => $data['email'], 'staffs.is_deleted' => UNDELETED,])
                    ->latest()->first();
        if ($staffLogin) {
            $school = $this->school->where([
                            'schools.id' => $staffLogin->school_id,
                            'schools.status' => SCHOOL_STATUS_1,
                            'schools.is_deleted' => UNDELETED])
                    ->select('schools.status')->first();
        } else {
            $school = [];
        }
        return $school;
    }    
}
