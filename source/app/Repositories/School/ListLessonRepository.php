<?php

namespace App\Repositories\School;

use App\Models\ChatMessages;
use App\Models\ChatRooms;
use App\Models\LessonFlags;
use App\Models\Lessons;
use App\Models\MasterLevels;
use App\Models\Payments;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\Schools;
use App\Models\Settlements;
use App\Repositories\TennisMgtRepository;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Description of ListLessonRepository.
 */
class ListLessonRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $lesson;
    protected $level;
    protected $playLesson;
    protected $school;
    protected $settlement;
    protected $player;
    protected $chatRooms;
    protected $payment;
    protected $lessonFlag;

    public function __construct(
        Lessons $lesson,
        PlayerLesson $playLesson,
        Players $player,
        Settlements $settlement,
        MasterLevels $level,
        Schools $school,
        ChatRooms $chatRooms,
        Payments $payment,
        LessonFlags $lessonFlag
    ) {
        $this->lesson = $lesson;
        $this->level = $level;
        $this->player = $player;
        $this->school = $school;
        $this->settlement = $settlement;
        $this->playLesson = $playLesson;
        $this->chatRooms = $chatRooms;
        $this->payment = $payment;
        $this->lessonFlag = $lessonFlag;
    }

    /**
     * Get list trainers belong to school and trainer of school.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function getListLesson($search = null)
    {
        $staff = Auth::guard('schools')->user();
        $lists = $this->lesson->with(['lesson_school', 'lesson_player'])
            ->where(function ($q) use ($staff) {
                $q->whereHas('lesson_school', function ($query) use ($staff) {
                    $query->where('schools.id', $staff->school_id);
                });
            })->where([
                'lessons.is_deleted' => UNDELETED,
                'lessons.info_type' => INFO_TYPE_SCHOOL,
            ])
            ->withCount(['lesson_player' => function ($query) {
                $query->whereNotNull('status')
                    ->where(function ($q) {
                        $q->where('status', '=', LESSON_PLAYER_STATUS_1)
                            ->orWhere('status', '=', LESSON_PLAYER_STATUS_2)
                            ->orWhere('status', '=', LESSON_PLAYER_STATUS_3);
                    });
            }]);
        if (!empty($search)) {
            $lists = $this->searchLesson($lists, $search);
            if ($search['search_status'] != SCHOOL_STATUS_99) {
                $lists = $lists->where('lessons.status', '!=', SCHOOL_STATUS_99);
            }
        } else {
            $lists = $lists->where('lessons.status', '!=', SCHOOL_STATUS_99);
        }

        return $lists->sortable(['updated_at' => 'DESC']);
    }

    /**
     * Get data lesson after search.
     *
     * @param $data
     *
     * @return mixed
     */
    public function searchLesson($data, array $search)
    {
        $arr = [];
        if (!empty($search)) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d', strtotime($startDate));
                } else {
                    return $data->where('lessons.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d', strtotime($endDate));
                } else {
                    return $data->where('lessons.id', null);
                }
            }
            if ($startDate && $endDate) {
                if ($startDate < $endDate) {
                    $data = $data->whereDate('lessons.datetime_lesson', '>=', $startDate)
                        ->whereDate('lessons.datetime_lesson', '<=', $endDate);
                } else {
                    $data = $data->whereIn('lessons.id', $arr);
                }
            } elseif ($startDate && !$endDate) {
                $data = $data->whereDate('lessons.datetime_lesson', '>=', $startDate);
            } elseif (!$startDate && $endDate) {
                $data = $data->whereDate('lessons.datetime_lesson', '<=', $endDate);
            }
            if ($nameLesson = $search['search_name']) {
                $data = $data->where('lessons.name', 'LIKE', "%{$nameLesson}%");
            }
            if ($levelLesson = $search['search_level']) {
                $level = "\"$levelLesson\"";
                $data = $data->where('lessons.levels', 'LIKE', "%{$level}%");
            }
            //status
            $statusLesson = $search['search_status'];
            if ($statusLesson !== null) {
                $data = $data->where('lessons.status', '=', $statusLesson);
            }
        }

        return $data;
    }

    /**
     * Get list master level.
     *
     * @return mixed
     */
    public function getAllLevel()
    {
        $levels = $this->level
            ->select(
                'master_levels.id',
                'master_levels.name',
                'master_levels.display_order'
            )
            ->where('is_deleted', UNDELETED)
            ->orderBy('display_order', 'asc')
            ->get();

        return $levels;
    }

    /**
     * save message in popup.
     *
     * @param entity $lessonFlag
     *
     * @return bool
     */
    public function saveLessonFlag(LessonFlags $lessonFlag)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$lessonFlag->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * Get player lesson by id.
     *
     * @param $lesson_id
     *
     * @return mixed
     */
    public function getPlayerLessonById($lesson_id)
    {
        $dataPlayer = $this->playLesson
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('players', 'players.id', '=', 'player_lessons.player_id')
            ->leftJoin('master_levels', 'master_levels.id', '=', 'lessons.levels')
            ->leftJoin('chat_rooms', 'chat_rooms.player_id', '=', 'players.id')
            ->leftJoin('lesson_flags', 'lesson_flags.player_lesson_id', '=', 'player_lessons.id')
            ->where([
                'player_lessons.lesson_id' => $lesson_id,
                'player_lessons.is_deleted' => UNDELETED,
            ])
            ->select(
                'lessons.name as lesson_name',
                'players.name as player_name',
                'lessons.quantity_player',
                'master_levels.name as level_name',
                'player_lessons.created_at',
                'player_lessons.updated_at',
                'lessons.id as lesson_id',
                'players.id as player_id',
                'player_lessons.status as pl_status',
                'player_lessons.refund_rate',
                'player_lessons.refund_point',
                'player_lessons.id as pl_id',
                'player_lessons.approve_date',
                'lessons.levels as level_id',
                'lesson_flags.flag as pl_flag'
            );

        return $dataPlayer->distinct('chat_rooms.player_id')->sortable(['created_at' => 'DESC']);
    }

    /**
     * Get lesson by id.
     *
     * @param $lessonId
     *
     * @return mixed
     */
    public function getLessonById($lessonId)
    {
        $lesson = $this->lesson
            ->leftJoin('player_lessons', 'lesson_id', '=', 'lessons.id')
            ->with('image')
            ->select(
                'lessons.*',
                DB::raw('COUNT(player_lessons.lesson_id) as lesson_player_count'),
            )
            ->where([
                'lessons.id' => $lessonId,
                'lessons.is_deleted' => UNDELETED,
            ])->first();

        return $lesson;
    }

    /**
     * Get player by id.
     *
     * @param $playerId
     *
     * @return mixed
     */
    public function getPlayerById($playerId)
    {
        $player = $this->player
            ->where([
                'players.id' => $playerId,
                'players.is_deleted' => UNDELETED,
            ])->first();

        return $player;
    }

    /**
     * update status player lesson.
     *
     * @param Players $player
     * @param $playerLessonId
     *
     * @return mixed
     */
    public function updateStatusPlayerLesson(
        PlayerLesson $playerLesson,
        Players $player = null,
        int $playerLessonId
    ) {
        $error = true;
        DB::beginTransaction();
        try {
            if ($playerLesson->save()) {
                if ($player) {
                    $player->save();
                    if (!$player->save()) {
                        $error = false;
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * cancel reject.
     *
     * @return mixed
     */
    public function cancelReject(PlayerLesson $playerLesson)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$playerLesson->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * Get email by member_id.
     *
     * @param $member_id
     *
     * @return email
     */
    public function getPlayerByMemberId($member_id)
    {
        $dataMember = $this->player->find($member_id);

        return $dataMember;
    }

    /**
     * save message in popup.
     *
     * @param entity $chatMessage
     *
     * @return bool
     */
    public function saveMessagePopUp(ChatMessages $chatMessage)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if (!$chatMessage->save()) {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * Get school by id.
     *
     * @param $idSchool
     *
     * @return email
     */
    public function getSchoolById($idSchool)
    {
        return $this->school->where('id', $idSchool)->first();
    }

    /**
     * save player to chat room.
     *
     * @param entity $chatRoom
     *
     * @return bool
     */
    public function saveOneToOneChat(ChatRooms $chatRoom)
    {
        $playerId = null;
        $error = true;
        DB::beginTransaction();
        try {
            if ($chatRoom->save()) {
                $playerId = $chatRoom->id;
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $playerId;
    }

    /**
     * Get settlement by player_lesson_id.
     *
     * @param $playerLessonId
     *
     * @return mixed
     */
    public function getSettlementByPlayerLessonId($playerLessonId)
    {
        return $this->settlement
            ->where([
                'settlements.player_lesson_id' => $playerLessonId,
            ])
            ->first();
    }

    /**
     * Get chat room participant by player_id and chat room id.
     *
     * @param $room_id
     *
     * @return mixed
     */
    public function getChatRoomParticipant($room_id)
    {
        return $this->chatRooms
            ->where([
                'id' => $room_id,
            ])
            ->first();
    }

    /**
     * Get player lesson.
     *
     * @param $playerLessonId
     *
     * @return mixed
     */
    public function getPlayerLesson($playerLessonId)
    {
        return $this->playLesson
            ->where([
                'player_lessons.id' => $playerLessonId,
            ])
            ->first();
    }

    /**
     * Get payment by settlement id.
     *
     * @param $settlementId
     *
     * @return mixed
     */
    public function getPaymentBySettId($settlementId)
    {
        return $this->payment
            ->where([
                'payments.settlement_id' => $settlementId,
            ])
            ->get();
    }

    /**
     * save chat room participant.
     *
     * @param entity $playerLesson
     * @param entity $settelement
     * @param entity $player
     * @param $payments
     *
     * @return bool
     */
    public function cancelPlayerLesson(
        PlayerLesson $playerLesson,
        Settlements $settelement = null,
        Players $player,
        $payments = null
    ) {
        $error = true;
        DB::beginTransaction();
        try {
            if ($player->attributesToArray() != null) {
                $player->save();
                if ($playerLesson->save()) {
                    if ($settelement) {
                        if ($settelement->save()) {
                            if (count($payments)) {
                                foreach ($payments as $payment) {
                                    $payment->save();
                                    if (!$payment->save()) {
                                        $error = false;
                                    }
                                }
                            }
                        } else {
                            $error = false;
                        }
                    }
                } else {
                    $error = false;
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * get player lesson by player lesson id.
     *
     * @param $idPlayerLesson
     *
     * @return array $arr
     */
    public function getPlayerLessonByIdPL($idPlayerLesson)
    {
        return $this->playLesson
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('players', 'players.id', '=', 'player_lessons.player_id')
            ->select(
                'lessons.name as lessonName',
                'players.name as playerName',
                'player_lessons.created_at',
                'player_lessons.approve_date',
                'lessons.levels',
                'lessons.register_expire_date',
                'lessons.lesson_date',
                'lessons.times_study'
            )
            ->where('player_lessons.id', $idPlayerLesson)
            ->first();
    }

    /**
     * Get status player.
     *
     * @return mixed
     */
    public function getNewPlayer()
    {
        $list = $this->playLesson
            ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->select('player_lessons.status as player_lessons_status', 'lessons.id as lessonId')
            ->where([
                'lessons.info_type' => INFO_TYPE_SCHOOL,
                'lessons.table_id' => Auth::guard('schools')->user()->school_id,
            ]);

        return $list;
    }
}
