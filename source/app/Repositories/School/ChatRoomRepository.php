<?php

namespace App\Repositories\School;

use App\Models\ChatMessages;
use App\Models\ChatRooms;
use App\Models\Lessons;
use App\Models\Players;
use App\Models\Schools;
use App\Models\SendMailQueue;
use App\Repositories\TennisMgtRepository;
use Auth;
use Exception;
use Illuminate\Support\Facades\DB;

class ChatRoomRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $chatRooms;
    protected $lesson;
    protected $player;
    protected $chatMessage;
    protected $school;
    protected $sendMess;

    public function __construct(
        ChatRooms $chatRooms,
        Lessons $lesson,
        Players $player,
        ChatMessages $chatMessage,
        Schools $school,
        SendMailQueue $sendMess
    ) {
        $this->chatRooms = $chatRooms;
        $this->lesson = $lesson;
        $this->player = $player;
        $this->chatMessage = $chatMessage;
        $this->school = $school;
        $this->sendMess = $sendMess;
    }

    /**
     * Save message has been send.
     *
     * @return mixed
     */
    public function saveMessage(array $chatMessage)
    {
        DB::beginTransaction();
        $result = [];
        $result['error'] = true;
        try {
            if ($chatMessage) {
                foreach ($chatMessage as $chat) {
                    if ($chat->save()) {
                        $sendMess = $this->sendMess;
                        $sendMess->id_mess = $chat->id;
                        if (!$sendMess->save()) {
                            $result['error'] = false;
                        }
                    } else {
                        $result['error'] = false;
                    }
                    $result['error'] ? DB::commit() : DB::rollback();
                }
                if (count($chatMessage) == 1) {
                    $result['data'] = $chatMessage[0];
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
            $result['error'] = false;
            DB::rollback();
        }

        return $result;
    }

    /**
     * Get list Chat Participants.
     *
     * @param $search
     *
     * @return mixed
     */
    public function getListChatParticipants($search = null)
    {
        $staff = Auth::guard('schools')->user();
        $chatList = $this->chatRooms
                    ->leftJoin('players', 'players.id', '=', 'chat_rooms.player_id')
                    ->leftJoin('lessons', 'chat_rooms.lesson_id', '=', 'lessons.id')
                    ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
                    ->leftJoin('player_lessons', 'player_lessons.id', '=', 'chat_rooms.player_lesson_id')
                    ->leftJoin('chat_messages as chat1', 'chat_rooms.id', '=', 'chat1.chat_room_id')
                    ->leftJoin('chat_messages as chat2', function ($join) {
                        $join->on('chat1.chat_room_id', '=', 'chat2.chat_room_id')->whereRaw(DB::raw('chat1.created_at < chat2.created_at'));
                    })
            ->select(
                'chat_rooms.*',
                'players.id as member_id',
                'players.name as player_name',
                'chat1.created_at as lastChatTime',
                'player_lessons.status as lessonStt',
                'player_lessons.updated_at as pl_updated_at',
                'player_lessons.is_deleted as pl_deleted',
                'lessons.info_type',
                'lessons.name as lessonName',
                'lessons.table_id'
            )
            ->whereNull('chat2.chat_room_id')
            ->where('lessons.status', '<>', LESSON_STATUS_99)
            ->where([
                'chat_rooms.is_deleted' => UNDELETED,
                'lessons.info_type' => INFO_TYPE_SCHOOL,
                'lessons.table_id' => $staff->school_id,
            ]);
        if (!empty($search)) {
            $chatList = $this->searchListParticipant($chatList, $search);
        }

        return $chatList->distinct('players.id');
    }

    /**
     * Get data Chat Participants after search.
     *
     * @param $data
     * @param $search
     *
     * @return mixed
     */
    public function searchListParticipant($data, $search)
    {
        if ($search['search_sort'] !== null) {
            if ($search['search_sort'] == DATE_DESC) {
                $data = $data->orderBy('chat_rooms.updated_at', 'DESC');
            } elseif ($search['search_sort'] == DATE_ASC) {
                $data = $data->orderBy('chat_rooms.updated_at', 'ASC');
            } elseif ($search['search_sort'] == PLAYER_DESC) {
                $data = $data->orderBy('players.name', 'DESC');
            } else {
                $data = $data->orderBy('players.name', 'ASC');
            }
        }
        $playerId = $search['search_player'];
        if ($playerId !== null) {
            $data = $data->where('chat_rooms.player_id', '=', $playerId);
        }
        $lessonId = $search['search_lesson'];
        if ($lessonId !== null) {
            $data = $data->where('lessons.id', '=', $lessonId);
        }
        $playerName = $search['search_playerName'];
        if ($playerName !== null) {
            $data = $data->where('players.name', 'LIKE', "%{$playerName}%");
        }
        $statusLesson = $search['search_status'];
        if ($statusLesson !== null) {
            $data = $data->where('player_lessons.status', $statusLesson);
        }

        return $data;
    }

    /**
     * Get unread message by participant id and info type.
     *
     * @param $idParticipant
     * @param $type
     *
     * @return mixed
     */
    public function getUnreadMessageByParticipantId($idParticipant, $type)
    {
        $unread = $this->chatMessage
            ->where([
                'chat_room_id' => $idParticipant,
                'unread_message' => UNREAD_MESSAGE,
                'is_deleted' => UNDELETED,
                'info_type' => $type,
            ])
            ->orderBy('created_at', 'DESC');

        return  $unread;
    }

    /**
     * Get lastest message  by participant id and info type.
     *
     * @param $idParticipant
     * @param $type
     *
     * @return mixed
     */
    public function getLatestMesssage($idParticipant, $type)
    {
        $lastest = $this->chatMessage
            ->where([
                'chat_room_id' => $idParticipant,
                'is_deleted' => UNDELETED,
                'info_type' => $type,
            ])
            ->orderBy('id', 'DESC')
            ->orderBy('created_at', 'DESC');

        return $lastest;
    }

    /**
     * Get all lesson belong to school and trainer of school.
     *
     * @return mixed
     */
    public function getAllLesson()
    {
        $staff = Auth::guard('schools')->user();
        $lesson = $this->lesson
            ->select(
                'lessons.id',
                'lessons.name'
            )
            ->where([
                'lessons.info_type' => INFO_TYPE_SCHOOL,
                'lessons.is_deleted' => UNDELETED,
            ])
            ->where('lessons.status', '<>', LESSON_STATUS_99);
        if ($staff) {
            $lesson = $lesson->where('lessons.table_id', '=', $staff->school_id);
        }
        $lesson = $lesson->orderBy('lessons.id')
            ->get();

        return $lesson;
    }

    /**
     * Get all player.
     *
     * @return mixed
     */
    public function getAllPlayer()
    {
        $staff = Auth::guard('schools')->user();
        $player = $this->player
                ->leftJoin('chat_rooms', 'chat_rooms.player_id', '=', 'players.id')
                ->leftJoin('lessons', 'lessons.id', '=', 'chat_rooms.lesson_id')
                ->select(
                    'players.id',
                    'players.name'
                )
                ->where('lessons.table_id', '=', $staff->school_id)
                ->orderBy('players.id')
                ->get();

        return $player;
    }

    /**
     * Get chat room by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function getChatRoomById($id)
    {
        $result = $this->chatRooms
                ->leftJoin('player_lessons', 'player_lessons.id', '=', 'chat_rooms.player_lesson_id')
                ->leftJoin('lessons', 'player_lessons.lesson_id', '=', 'lessons.id')
                ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
                ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->where([
                'chat_rooms.id' => $id,
                'lessons.info_type' => INFO_TYPE_SCHOOL,
                'chat_rooms.is_deleted' => UNDELETED,
            ])
            ->select('chat_rooms.*', 'trainers.name as trainerName', 'schools.name as schoolName')
            ->first();

        return $result;
    }

    /**
     * Get list messages by chat room id.
     *
     * @param $participant_id
     * @param $unreadMessages
     *
     * @return mixed
     */
    public function getListMessageByParticipantId($participant_id, $unreadMessages = null)
    {
        $school = Auth::guard('schools')->user();
        $result = $this->chatMessage
            ->leftJoin('chat_rooms', 'chat_rooms.id', '=', 'chat_messages.chat_room_id')
            ->leftJoin('players', 'players.id', '=', 'chat_rooms.player_id')
            ->leftJoin('lessons', 'chat_rooms.lesson_id', '=', 'lessons.id')
            ->leftJoin('player_lessons', 'player_lessons.id', '=', 'chat_rooms.player_lesson_id')
            ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
            ->where([
                'chat_messages.chat_room_id' => $participant_id,
                'lessons.info_type' => INFO_TYPE_SCHOOL,
            ])
            ->select(
                'chat_messages.created_at as message_time',
                'chat_messages.from',
                'chat_messages.message',
                'chat_messages.to',
                'chat_messages.info_type',
                'players.name as player_name',
                'schools.name as school_name',
                'player_lessons.status as lessonStt',
                'player_lessons.is_deleted',
                'player_lessons.updated_at as pl_updated_at',
                'lessons.status as lessStt',
                'players.id as player_id',
                'player_lessons.updated_at',
                'lessons.id as lesson_id'
            )
            ->orderBy('chat_messages.updated_at')
            ->get();
        if (count($result)) {
            if (count($unreadMessages)) {
                foreach ($unreadMessages as $unread) {
                    $editMessage = $this->chatMessage->find($unread->id);
                    $editMessage->unread_message = READ_MESSAGE;
                    $editMessage->save();
                }
            }
        }

        return $result;
    }

    /**
     * Get player by participant Id.
     *
     * @param $participantId
     *
     * @return mixed
     */
    public function getPlayerIdByParticipantId($participantId)
    {
        return $this->chatRooms
            ->leftJoin('lessons', 'lessons.id', '=', 'chat_rooms.lesson_id')
            ->leftJoin('player_lessons', 'player_lessons.id', '=', 'chat_rooms.player_lesson_id')
            ->select(
                'chat_rooms.*',
                'player_lessons.status as lessonStt',
                'lessons.status as lessStt',
            )
            ->where(['chat_rooms.id' => $participantId])->first();
    }

    /**
     * Get school by id.
     *
     * @param $schoolId
     *
     * @return Repository
     */
    public function getSchoolById($schoolId)
    {
        return $this->school->where('id', $schoolId)->first();
    }

    /**
     * Get player by id.
     *
     * @param $member_id
     *
     * @return email
     */
    public function getPlayerByMemberId($member_id)
    {
        return  $this->player->find($member_id);
    }
}
