<?php

namespace App\Repositories\School;

use App\Models\Lessons;
use App\Models\Payments;
use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\Schools;
use App\Models\Settlements;
use App\Models\Trainers;
use App\Repositories\TennisMgtRepository;
use Auth;
use DB;

/**
 * Description of ListSettlementRepository.
 */
class ListSettlementRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $settlement;
    protected $trainer;
    protected $player;
    protected $lesson;
    protected $school;
    protected $playerLesson;

    public function __construct(
        Settlements $settlement,
        Trainers $trainer,
        Players $player,
        Lessons $lesson,
        Payments $payment,
        Schools $school,
        PlayerLesson $playerLesson
    ) {
        $this->settlement = $settlement;
        $this->trainer = $trainer;
        $this->player = $player;
        $this->lesson = $lesson;
        $this->payment = $payment;
        $this->school = $school;
        $this->playerLesson = $playerLesson;
    }

    /**
     * get list settlement.
     *
     * @param $search
     *
     * @return mixed
     */
    public function getListSettlement($search = null)
    {
        $staff = Auth::guard('schools')->user();
        $lists = $this->settlement
                      ->leftJoin('player_lessons', 'settlements.player_lesson_id', '=', 'player_lessons.id')
                      ->leftJoin('lessons', 'player_lessons.lesson_id', '=', 'lessons.id')
                      ->leftJoin('schools', 'lessons.table_id', '=', 'schools.id')
                      ->leftJoin('trainers', 'lessons.table_id', '=', 'trainers.id')
                      ->leftJoin('players', 'player_lessons.player_id', '=', 'players.id')
                      ->select('settlements.*',
                              'lessons.name',
                              'trainers.name as trainerName',
                              'trainers.id as trainerId',
                              'schools.name as schoolName',
                              'players.name as playerName',
                              'lessons.id as lessonId',
                              'lessons.info_type as lessonType',
                              'lessons.payment_fee',
                              'lessons.table_id',
                              'players.id as idPlayer',
                              'lessons.category',
                              'lessons.status as lessonStatus')
                      ->where(function ($q) use ($staff) {
                          $q->where(['trainers.school_id' => $staff->school_id,
                                        'lessons.info_type' => INFO_TYPE_TRAINER,
                                    ])
                            ->orWhere(function ($query) use ($staff) {
                                $query->where(['schools.id' => $staff->school_id,
                                                    'lessons.info_type' => INFO_TYPE_SCHOOL, ]);
                            });
                      });
        if (!empty($search)) {
            $lists = $this->searchSettlement($lists, $search);
            if ($search['search_status'] != SETTLEMENT_99) {
                $lists = $lists->where('settlements.status', '!=', SETTLEMENT_99);
            }
        } else {
            $lists = $lists->where('settlements.status', '!=', SETTLEMENT_99);
        }

        return $lists->sortable(['payment_date' => 'DESC']);
    }

    /**
     * search list settlement.
     *
     * @param $data
     * @param $search
     *
     * @return mixed
     */
    public function searchSettlement($data, $search)
    {
        if (!empty($search)) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d', strtotime($startDate));
                } else {
                    return $data->where('settlements.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d', strtotime($endDate));
                } else {
                    return $data->where('settlements.id', null);
                }
            }
            if ($startDate && $endDate) {
                $data = $data->whereBetween('settlements.payment_date', [$startDate.' 00:00:00', $endDate.' 23:59:59']);
            } elseif ($startDate && !$endDate) {
                $startDate = date('Y-m-d', strtotime($search['search_startDate']));
                $data = $data->where('settlements.payment_date', '>=', $startDate.' 00:00:00');
            } elseif (!$startDate && $endDate) {
                $endDate = date('Y-m-d', strtotime($search['search_endDate']));
                $data = $data->where('settlements.payment_date', '<=', $endDate.' 23:59:59');
            }
            if ($idPlayer = $search['search_namePlayer']) {
                $data = $data->where('players.id', $idPlayer);
            }
            if ($idTrainer = $search['search_nameTrainer']) {
                $data = $data->where([
                                    'lessons.info_type' => INFO_TYPE_TRAINER,
                                    'lessons.table_id' => $idTrainer,
                                ]);
            }
            $statusSettlement = $search['search_status'];
            if ($statusSettlement !== null) {
                $data = $data->where('settlements.status', '=', $statusSettlement);
            }

            return $data;
        }
    }

    /**
     * get trainer name.
     *
     * @return mixed
     */
    public function getTrainerName()
    {
        $trainers = $this->trainer->login()
                    ->select(
                        'trainers.id',
                        'trainers.name')
                    ->get();

        return $trainers;
    }

    /**
     * get player name.
     *
     * @return mixed
     */
    public function getPlayerName()
    {
        $staffLogin = Auth::guard('schools')->user();
        $playerList = $this->player
                    ->leftJoin('player_lessons', 'players.id', '=', 'player_lessons.player_id')
                    ->leftJoin('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
                    ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
                    ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
                    ->select(
                        'players.id',
                        'players.name')
                    ->where(['players.is_deleted' => UNDELETED])
                    ->where(function ($query) use ($staffLogin) {
                        $query->where([
                                'lessons.info_type' => INFO_TYPE_SCHOOL,
                                'schools.id' => $staffLogin->school_id,
                            ])
                            ->orWhere(function ($q) use ($staffLogin) {
                                $q->where([
                                    'lessons.info_type' => INFO_TYPE_TRAINER,
                                    'trainers.school_id' => $staffLogin->school_id,
                                ]);
                            });
                    })
                    ->groupBy('players.id')
                    ->get();

        return $playerList;
    }

    /**
     * get settlement by id.
     *
     * @param $settlementId
     *
     * @return mixed
     */
    public function getDataSettlementById(int $settlementId)
    {
        return $this->settlement->where('settlements.id', $settlementId)->first();
    }

    /**
     * edit settlement.
     *
     * @param $data
     * @param $userLoginId
     *
     * @return bool
     */
    public function saveEditSettlement($data, $userLoginId = null)
    {
        $result = true;
        DB::beginTransaction();
        try {
            $settlement = $this->settlement->where('settlements.id', $data['id_settlement'])->first();
            if ($settlement) {
                if ($settlement->status != $data['edit_status']) {
                    $settlement->status = $data['edit_status'];
                    $settlement->updated_id = $userLoginId;
                    if ($data['edit_status'] == SETTLEMENT_99) {
                        $result = $this->saveDeleteSettlement($data['id_settlement']);
                    } else {
                        $dataPayments = $this->payment
                                    ->where('payments.settlement_id', $data['id_settlement'])
                                    ->get();
                        if (count($dataPayments)) {
                            foreach ($dataPayments as $value) {
                                $editPayment = $this->payment->find($value['id']);
                                $editPayment->status = PAYMENT_STATUS_0;
                                if (!$editPayment->save()) {
                                    $result = false;
                                }
                            }
                        }
                        $playerLesson = $this->playerLesson->find($settlement['player_lesson_id']);
                        $lesson = $this->lesson->find($playerLesson['lesson_id']);
                        $player = $this->player->find($playerLesson['player_id']);
                        if ($lesson['status'] == LESSON_STATUS_1 && $lesson['status'] !== null && $playerLesson['refunded'] == REFUNDED) {
                            $player['point'] = $player['point'] - $lesson['payment_fee'];
                            $playerLesson['refunded'] = UNREFUNDED;
                            if (!$player->save() || !$playerLesson->save()) {
                                $result = false;
                            }
                        }
                    }
                }
                if (!$settlement->save()) {
                    $result = false;
                }
            } else {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $result = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $result;
    }

    /**
     * delete settlement.
     *
     * @param $settlementId
     *
     * @return bool
     */
    public function saveDeleteSettlement(int $settlementId)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            $dataSettlement = $this->settlement
                                ->where('settlements.id', $settlementId)
                                ->where('settlements.status', '!=', SETTLEMENT_99)
                                ->first();
            if ($dataSettlement) {
                $dataSettlement->status = SETTLEMENT_99;
                if ($dataSettlement->save()) {
                    $dataPayments = $this->payment
                                    ->where('payments.settlement_id', $settlementId)
                                    ->where('payments.status', '!=', PAYMENT_STATUS_99)
                                    ->get();
                    if (count($dataPayments)) {
                        foreach ($dataPayments as $value) {
                            $deletePayment = $this->payment->find($value['id']);
                            $deletePayment->status = PAYMENT_STATUS_99;
                            if (!$deletePayment->save()) {
                                $delete = false;
                            }
                        }
                    }
                    $playerLesson = $this->playerLesson->find($dataSettlement['player_lesson_id']);
                    $lesson = $this->lesson->find($playerLesson['lesson_id']);
                    $player = $this->player->find($playerLesson['player_id']);
                    if ($lesson['status'] == LESSON_STATUS_1 && $lesson['status'] !== null && $playerLesson['refunded'] == UNREFUNDED) {
                        $player['point'] = $player['point'] + $lesson['payment_fee'];
                        $playerLesson['refunded'] = REFUNDED;
                        if (!$player->save() || !$playerLesson->save()) {
                            $delete = false;
                        }
                    }
                } else {
                    $delete = false;
                }
            } else {
                $delete = false;
            }
            $delete ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $delete = false;
            DB::rollback();
        }

        return $delete;
    }
}
