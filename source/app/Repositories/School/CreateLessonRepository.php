<?php

namespace App\Repositories\School;

use App\Models\ChatRooms;
use App\Models\Images;
use App\Models\Lessons;
use App\Models\Payments;
use App\Models\PlayerLesson;
use App\Models\Settlements;
use App\Repositories\TennisMgtRepository;
use Auth;
use DB;

/**
 * Description of CreateLessonRepository.
 */
class CreateLessonRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $lesson;
    protected $image;
    protected $chatroom;
    protected $playerLesson;
    protected $settlement;
    protected $payment;

    public function __construct(
        Lessons $lesson,
        PlayerLesson $playerLesson,
        Images $image,
        ChatRooms $chatroom,
        Settlements $settlement,
        Payments $payment
    ) {
        $this->lesson = $lesson;
        $this->image = $image;
        $this->chatroom = $chatroom;
        $this->playerLesson = $playerLesson;
        $this->settlement = $settlement;
        $this->payment = $payment;
    }

    /**
     * create lesson.
     *
     * @param entity $lesson
     * @param $newImage
     *
     * @return bool
     */
    public function saveCreateLesson(Lessons $lesson, $newImage)
    {
        $error = true;
        DB::beginTransaction();
        try {
            if ($lesson->save()) {
                if (!empty($newImage)) {
                    $dirTmp = AVATAR_LESSON_SAVE_TMP.$newImage['name'];
                    $dirSave = AVATAR_LESSON_SAVE.$newImage['name'];
                    if (file_exists($dirTmp)) {
                        @rename($dirTmp, $dirSave);
                    }
                    $newImage->table_id = $lesson->id;
                    if (!$newImage->save()) {
                        $error = false;
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * Get lesson by id.
     *
     *@param $idLesson
     *
     * @return mixed
     */
    public function getLessonById($idLesson)
    {
        $staffLogin = Auth::guard('schools')->user();

        return $this->lesson
                ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
                ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
                ->where([
                    'lessons.id' => $idLesson,
                    'lessons.is_deleted' => UNDELETED,
                ])
                ->where(function ($query) use ($staffLogin) {
                    $query->where(['lessons.info_type' => INFO_TYPE_SCHOOL,
                                    'schools.id' => $staffLogin->school_id, ])
                            ->orWhere(function ($q) use ($staffLogin) {
                                $q->where(['lessons.info_type' => INFO_TYPE_TRAINER,
                                            'trainers.school_id' => $staffLogin->school_id, ]);
                            });
                })
                ->select('lessons.*', 'trainers.school_id as trainerSchoolId', 'schools.id as schoolId')
                ->first();
    }

    /**
     * find image by lesson id.
     *
     * @param $idLesson
     *
     * @return mixed
     */
    public function findImageByLessonId(int $idLesson)
    {
        return $this->image->where([
                    'table_id' => $idLesson,
                    'info_types' => INFO_TYPE_LESSON,
                    'is_deleted' => UNDELETED,
                ])->get();
    }

    /**
     * find image by lesson.
     *
     * @param $idLesson
     * @param $position
     *
     * @return mixed
     */
    public function findImageByLesson(int $idLesson, $position)
    {
        return $this->image->where([
            'position' => $position,
            'table_id' => $idLesson,
            'info_types' => INFO_TYPE_LESSON,
            'is_deleted' => UNDELETED,
        ])->first();
    }

    /**
     * edit lesson.
     *
     * @param entity $lesson
     * @param $arrImage
     *
     * @return bool
     */
    public function saveEditLesson(
        Lessons $lesson,
        $arrImage,
        $idLesson
    ) {
        $error = true;
        DB::beginTransaction();
        try {
            if ($lesson->save()) {
                if (!empty($arrImage)) {
                    foreach ($arrImage as $image) {
                        $dirTmp = AVATAR_LESSON_SAVE_TMP.$image['name'];
                        $dirSave = AVATAR_LESSON_SAVE.$image['name'];
                        if (file_exists($dirTmp)) {
                            @rename($dirTmp, $dirSave);
                        }
                        $image->table_id = $lesson->id;
                        if (!$image->save()) {
                            $error = false;
                        }
                    }
                }
            } else {
                $error = false;
            }
            $error ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $error = false;
            DB::beginTransaction();
            DB::rollback();
        }

        return $error;
    }

    /**
     * delete lesson.
     *
     * @param $idLesson
     * @param $deleted_id
     *
     * @return bool
     */
    public function deleteDataLesson(int $idLesson, $deleted_id = null)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            $dataLesson = $this->lesson
                                ->where(['lessons.id' => $idLesson])
                                ->where('lessons.status', '!=', LESSON_STATUS_99)
                                ->get();
            if (!$this->deleteLessonAndPaymentOfTrainer($dataLesson, $deleted_id)) {
                $delete = false;
            }
            $deletelesson = $this->lesson->find($idLesson);
            $deletelesson->status = LESSON_STATUS_99;
            if (!$deletelesson->save()) {
                $delete = false;
            }
            $delete ? DB::commit() : DB::rollback();
        } catch (Exception $e) {
            $delete = false;
            DB::rollback();
            throw new Exception($e->getMessage());
        }

        return $delete;
    }

    /**
     * delete other lesson table.
     *
     * @param $datas
     * @param $tableName
     * @param $deleted_id
     *
     * @return bool
     */
    public function deleteLessonOfOtherTable($datas, $tableName, $deleted_id = null)
    {
        $result = true;
        if (count($datas)) {
            foreach ($datas as $value) {
                $table = $tableName->find($value['id']);
                $table->status = LESSON_STATUS_99;
                $table->deleted_id = $deleted_id;
                if (!$table->save()) {
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * delete other lesson table.
     *
     * @param $dataLessons
     * @param $deleted_id
     *
     * @return bool
     */
    public function deleteLessonAndPaymentOfTrainer($dataLessons, $deleted_id = null)
    {
        $result = true;
        if (count($dataLessons)) {
            foreach ($dataLessons as $lesson) {
                $dataPlayerLessons = $this->playerLesson
                                    ->where('player_lessons.lesson_id', $lesson->id)
                                    ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_99)
                                    ->get();
                if (!$this->deleteLessonOfOtherTable($dataPlayerLessons, $this->playerLesson, $deleted_id)) {
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * get player lesson by lesson id.
     *
     * @return repository
     */
    public function getPlayerLessonByLessonId(int $idLesson)
    {
        return $this->playerLesson
                        ->where('player_lessons.lesson_id', $idLesson)
                        ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_99)
                        ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_3)
                        ->get();
    }
}
