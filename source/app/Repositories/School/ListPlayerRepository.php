<?php

namespace App\Repositories\School;


use App\Repositories\TennisMgtRepository;
use App\Models\Players;
use App\Models\PlayerLesson;
use Auth;
use App\Models\ChatRooms;
use App\Models\Matches;
use App\Models\Schools;
use App\Models\SharingAnalyses;
use Illuminate\Support\Facades\DB;
use Exception;
/**
 * class ListPlayerRepository
 */
class ListPlayerRepository extends TennisMgtRepository 
{
    /**
     * @var \App\Models\model
     */
    protected $player, $player_lesson, $chatRoom, $match, $school;

    public function __construct(
        Players $player, 
        PlayerLesson $player_lesson,
        ChatRooms $chatRoom,
        Matches $match,
        Schools $school,
        SharingAnalyses $sharingAnalyses
    ) {
        $this->player = $player;
        $this->player_lesson = $player_lesson;
        $this->chatRoom = $chatRoom;
        $this->match = $match;
        $this->school = $school;
        $this->sharingAnalyses = $sharingAnalyses;
    }
    
    /**
     * Get list players has lesson belong to school
     *
     * @param array $search
     * @return mixed
     */
    public function getListPlayer($search = null) 
    {       
        $playerList = $this->player->all();
        $playerLessonList = $this->getPlayerLesson();
        $arrEmpty = [];
        if (!empty($search)) {
            $playerLessonList = $this->searchPlayer($playerLessonList, $search);
        }   
        $playerLessonList = $playerLessonList->get();
        foreach ($playerList as $player) {
            $arrLesson = []; 
            $arrLessonId = [];
            $arrStatus = []; 
            $arrAuthor = [];
            $arrAuthorId = [];
            $arrLessonType = [];
            if (count($playerLessonList)) {
                foreach ($playerLessonList as $playerLesson) {                
                    if ($player->id == $playerLesson->playerID) {
                        array_push($arrLesson, $playerLesson['lessonName']);
                        $player['lessonName'] = $arrLesson;
                        array_push($arrStatus, $playerLesson['plStatus']);
                        $player['plStatus'] = $arrStatus;
                        array_push($arrLessonId, $playerLesson['lessonId']);
                        $player['lessonId'] = $arrLessonId;
                        array_push($arrLessonType, $playerLesson['lessonType']);
                        $player['lessonType'] = $arrLessonType;
                        array_push($arrAuthor, $playerLesson['schoolName']);
                        $player['author'] = $arrAuthor;
                        array_push($arrAuthorId, $playerLesson['authorId']);
                        $player['authorId'] = $arrAuthorId;
                    }               
                }
            }
            $player['totalMatches'] = $this->countMatchesByPlayerId($player['id']);
        }
        $playerList = $playerList->whereNotNull('plStatus');
        return $playerList;
    }
    
    /**
     * Get count matches by player id
     *
     * @param $playerId
     * @return mixed
     */
    public function countMatchesByPlayerId(int $playerId) 
    {
        return $this->match->where([
                    'matches.created_id' => $playerId,
                    'matches.is_deleted' => UNDELETED
                ])->count();
    }
    
    /**
     * Get data player lesson
     *
     * @return mixed
     */
    public function getPlayerLesson() 
    {
        $staff = Auth::guard('schools')->user();
        return $this->player_lesson
                ->leftJoin('lessons','lessons.id','=','player_lessons.lesson_id')
                ->leftJoin('schools','schools.id','=','lessons.table_id')
                ->leftJoin('trainers','trainers.id','=','lessons.table_id')  
                ->leftJoin('players','players.id','=','player_lessons.player_id') 
                ->where([
                        'lessons.info_type' => INFO_TYPE_SCHOOL,
                        'lessons.table_id' => $staff->school_id
                    ])              
                ->select(
                        'player_lessons.player_id as playerID',
                        'lessons.name as lessonName',
                        'player_lessons.id as plId',
                        'player_lessons.status as plStatus',
                        'lessons.id as lessonId',
                        'schools.name as schoolName',
                        'lessons.info_type as lessonType',
                        'lessons.table_id as authorId',
                        'players.name as playerName')  
                ->orderBy('player_lessons.created_at','desc');        
    }
    
    /**
     * Get data players after search
     *
     * @param $data
     * @param array $search
     * @return mixed
     */
    public function searchPlayer($data, $search) 
    {
        $staff = Auth::guard('schools')->user();
        if (!empty($search)) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d',  strtotime($startDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d',  strtotime($endDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($startDate && $endDate) {                
                $data = $data->whereBetween('players.last_login',[$startDate. ' 00:00:00', $endDate. ' 23:59:59']);
            } elseif($startDate && !$endDate) {
                $startDate = date('Y-m-d',  strtotime($search['search_startDate']));
                $data = $data->where('players.last_login', '>=' , $startDate. ' 00:00:00');
            } elseif(!$startDate && $endDate) {
                $endDate = date('Y-m-d',  strtotime($search['search_endDate']));
                $data = $data->where('players.last_login', '<=' , $endDate. ' 23:59:59');
            } 
            if ($namePlayer = $search['search_name']) {
                $data = $data->where('players.name','LIKE',"%{$namePlayer}%");
            }
            $statusPlayer = $search['search_status'];
            if ($statusPlayer !== null) {
                $data = $data->where('player_lessons.status','=',$statusPlayer);
            }                     
        }      
        return $data;
    }
    
    /**
     * Get data account player to login from admin site by player id
     *
     * @param $playerId
     * @return mixed
     */
    public function getAccountLogonByPlayerId(int $playerId) 
    {
        return $this->player->where([
                    'players.id' => $playerId,
                    'players.is_deleted' => UNDELETED
                ])->first();
    }
    
    /**
     * Sort list player
     *
     * @param array $playerList
     * @param $sort
     * @param $direction
     * @return mixed
     */
    public function sortListPlayer(
        $playerList, 
        $sort, 
        $direction
    ) {        
        if (isset($sort) && isset($direction) && count($playerList)) {
            if ($sort == 'playerLesson') {
                if (strtolower($direction) == 'desc') {
                    $playerList = $playerList->sortByDesc(function($status) {
                                        return $status['plStatus'][0];
                                    });
                            
                } else {
                    $playerList = $playerList->sortBy(function($status) {
                                        return $status['plStatus'][0];
                                    });
                }
            } else {
                if (strtolower($direction) == 'desc') {
                    $playerList = $playerList->sortByDesc($sort);
                } else {
                    $playerList = $playerList->sortBy($sort);
                }
            }
        }
        return $playerList->values();
    }
    
    /**
     * get school
     *
     * @return mixed
     */
    public function getSchool() 
    {
        $staff = Auth::guard('schools')->user();
        return $this->school->find($staff->school_id);
    }
    
    /**
     * list shared player
     * @param type $param
     */
    public function getListSharedPlayer($search = null)
    {
        $staff = Auth::guard('schools')->user();
        $data = SharingAnalyses::leftJoin('players', 'sharing_analyses.player_id', '=', 'players.id')
            ->leftJoin('matches', function($join)
            {
                $join->on('sharing_analyses.player_id', '=', 'matches.created_id')->where('matches.is_deleted', UNDELETED);
            })
            ->select(
                'sharing_analyses.*',
                'players.name as player_name',
                'players.last_login as player_last_login',
                DB::raw('COUNT(matches.id) as total_matches')
            )
            ->where([
                'sharing_analyses.school_id' => $staff->school_id,
            ])
            ->groupBy('sharing_analyses.player_id')        
            ->sortable([
                'updated_at' => 'DESC',
                'id' => 'DESC',
            ]);
            if (!empty($search)) {
                $data = $this->searchSharedPlayer($data, $search);
            }
        return $data->get();
    }
    
    /**
     * Get data shared player after search
     *
     * @param $data
     * @param array $search
     * @return mixed
     */
    public function searchSharedPlayer($data, array $search)
    {
        if (!empty($search)) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d',  strtotime($startDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d',  strtotime($endDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($startDate && $endDate) {                
                $data = $data->whereBetween('players.last_login',[$startDate. ' 00:00:00', $endDate. ' 23:59:59']);
            } elseif($startDate && !$endDate) {
                $startDate = date('Y-m-d',  strtotime($search['search_startDate']));
                $data = $data->where('players.last_login', '>=' , $startDate. ' 00:00:00');
            } elseif(!$startDate && $endDate) {
                $endDate = date('Y-m-d',  strtotime($search['search_endDate']));
                $data = $data->where('players.last_login', '<=' , $endDate. ' 23:59:59');
            } 
            if ($namePlayer = $search['search_name']) {
                $data = $data->where('players.name','LIKE',"%{$namePlayer}%");
            }
            $statusPlayer = $search['search_status'];
            if ($statusPlayer !== null) {
                $data = $data->where('sharing_analyses.status','=',$statusPlayer);
            }                     
        }
        return $data;
    }
    
    /**
     * update status share
     * @param type $data
     * @return type
     */
    public function acceptShare($data)
    {
        return SharingAnalyses::where('id', $data['id'])
                ->update(['status' => 1]);
    }
    
    /**
     * delete share
     * @param type $data
     * @return type
     */
    public function deleteShare($data)
    {
        if (SharingAnalyses::where('id', $data['id'])->exists()) {
            return SharingAnalyses::destroy($data['id']);
        } else {
            return false;
        }
    }
    
    /**
     * get account share
     * @param type $data
     * @return type
     */
    public function getAccountSharedPlayer($schoolId, $playerId)
    {
        if(SharingAnalyses::where('school_id', $schoolId)->where('player_id', $playerId)->exists()){ // check account exits in share table
            return $this->player->where([
                    'players.id' => $playerId,
                    'players.is_deleted' => UNDELETED
                ])->first();
        } else {
            return false;
        }
    }
}
