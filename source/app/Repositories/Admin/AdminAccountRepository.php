<?php
namespace App\Repositories\Admin;

use App\Models\AdminLogin;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class AdminAccountRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $account;
    public function __construct(AdminLogin $account)
    {
        $this->account = $account;
    }

    /**
     * Create account manager
     *
     * @param entity
     * @return bool
     */
    public function saveCreateAccount (AdminLogin $account) {
        return $account->save();
    }

    /**
     * Edit account manager
     *
     * @param entity
     * @return bool
     */
    public function saveEditAccount (AdminLogin $account, $idAccount) {
        return $account->save();
    }

    /**
     * Get list account managers
     *
     * @return mixed
     * 
     * 
     */
    public function getListAccountManagers () {
        $lists = $this->account
                ->select(
                    'admin_login.id',
                    'admin_login.user_name',
                    'admin_login.email',
                    'admin_login.password',
                    'admin_login.role',
                    'admin_login.updated_at'
                )
                ->where([['admin_login.is_deleted', UNDELETED], ['admin_login.email', '<>', 'admin@gmail.com']]);

        return $lists->sortable(['updated_at' => 'DESC']);
    }

    /**
     * Get data account by id
     *
     * @param int $id
     * @return mixed
     * 
     */
    public function getAccountById(int $id) {
        return $this->account->where(['admin_login.id' => $id, ])->first();
    }

}
