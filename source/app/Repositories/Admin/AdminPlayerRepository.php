<?php

namespace App\Repositories\Admin;

use App\Models\PlayerLesson;
use App\Models\Players;
use App\Models\Schools;
use App\Models\Trainers;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class AdminPlayerRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $school;
    protected $trainer;
    protected $playerLesson;

    public function __construct(
        Players $player,
        Schools $school,
        Trainers $trainer,
        PlayerLesson $playerLesson
    ) {
        $this->player = $player;
        $this->school = $school;
        $this->trainer = $trainer;
        $this->playerLesson = $playerLesson;
    }

    /**
     * Get list players has lesson.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function getListPlayersHasLesson($search = null)
    {
        $lists = $this->player
                ->join('player_lessons', 'player_lessons.player_id', '=', 'players.id')
                ->join('lessons', 'player_lessons.lesson_id', '=', 'lessons.id')
                ->select(
                    'players.id',
                    'players.updated_at as updatedPlayer',
                    'players.name',
                    'player_lessons.id as playerLessonId',
                    'player_lessons.status as playerLessonStatus',
                    'player_lessons.created_at as timeRegisterLesson',
                    'lessons.id as lessonId',
                    'lessons.name as lessonName',
                    'lessons.info_type as lessonType',
                    'lessons.lesson_date'
                )
                ->where([
                    'players.is_deleted' => UNDELETED,
                    'player_lessons.is_deleted' => UNDELETED,
                    'lessons.is_deleted' => UNDELETED,
                ]);

        $lists = $this->getListPlayerLesson($lists);
        if (!empty($search)) {
            $lists = $this->searchPlayers($lists, $search);
        }

        return $lists;
    }

    /**
     * Get list players has no lesson.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function getListPlayerHasNoLesson($search = null)
    {
        $lists = $this->player
                ->select(
                    'players.id',
                    'players.updated_at as updatedPlayer',
                    'players.name',
                    DB::raw('null as playerLessonId'),
                    DB::raw('null as playerLessonStatus'),
                    DB::raw('null as timeRegisterLesson'),
                    DB::raw('null as lessonId'),
                    DB::raw('null as lessonName'),
                    DB::raw('null as lessonType'),
                    DB::raw('null as lesson_date')
                )
                ->where([
                    'players.is_deleted' => UNDELETED,
                ])
                ->whereNotIn('players.id', $this->getListPlayersHasLesson()->pluck('players.id'));
        if (!empty($search)) {
            $lists = $this->searchPlayers($lists, $search);
        }

        return $lists;
    }

    /**
     * Get list players.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function getListPlayers($search = null)
    {
        $playerHasLesson = $this->getListPlayersHasLesson($search);
        $playerNoLesson = $this->getListPlayerHasNoLesson($search);

        return $playerHasLesson->union($playerNoLesson)->sortable(['updatedPlayer' => 'DESC']);
    }

    /**
     * Get data players after search.
     *
     * @param $data
     *
     * @return mixed
     */
    public function searchPlayers($data, array $search)
    {
        if ($search) {
            $arr = [];
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d', strtotime($startDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d', strtotime($endDate));
                } else {
                    return $data->where('players.id', null);
                }
            }
            if ($startDate && $endDate) {
                $data = $data->whereBetween('players.updated_at', [$startDate.' 00:00:00', $endDate.' 23:59:59']);
            } elseif ($startDate && !$endDate) {
                $data = $data->where('players.updated_at', '>=', $startDate.' 00:00:00');
            } elseif (!$startDate && $endDate) {
                $data = $data->where('players.updated_at', '<=', $endDate.' 23:59:59');
            }

            if ($namePlayer = $search['search_name']) {
                $data = $data->where('players.name', 'LIKE', "%{$namePlayer}%");
            }
            if ($idSchoolName = $search['search_schoolName']) {
                if (Collection::make($data->getQuery()->joins)->pluck('table')->contains('lessons')) {
                    $arrBySchool = $this->getArrayIdPlayerBySchool($idSchoolName);
                    $data = $data->whereIn('lessons.id', $arrBySchool);
                } else {
                    $data = $data->whereIn('players.id', $arr);
                }
            }
            if ($idTrainerName = $search['search_trainerName']) {
                if (Collection::make($data->getQuery()->joins)->pluck('table')->contains('lessons')) {
                    $arrByTrainer = $this->getArrayIdPlayerByTrainer($idTrainerName);
                    $data = $data->whereIn('lessons.id', $arrByTrainer);
                } else {
                    $data = $data->whereIn('players.id', $arr);
                }
            }
            //search by status
            $statusPlayerLesson = $search['search_status'];
            if ($statusPlayerLesson !== null) {
                if (Collection::make($data->getQuery()->joins)->pluck('table')->contains('player_lessons')) {
                    $data = $data->where('player_lessons.status', '=', $statusPlayerLesson);
                } else {
                    $data = $data->whereIn('players.id', $arr);
                }
            }
        }

        return $data;
    }

    /**
     * Get list player lesson.
     *
     * @param array $datas
     *
     * @return mixed
     */
    public function getListPlayerLesson($datas)
    {
        $idPlayerLesson = [];
        $allPlayers = $this->getAllPlayers();
        $playerLessons = $datas->get();
        if (count($allPlayers) && count($playerLessons)) {
            foreach ($allPlayers as $player) {
                if ($id = $this->getIdPlayerLesson($player['id'], LESSON_PLAYER_STATUS_2)) {
                    array_push($idPlayerLesson, $id);
                } else {
                    if ($id = $this->getIdPlayerLesson($player['id'], LESSON_PLAYER_STATUS_3)) {
                        array_push($idPlayerLesson, $id);
                    } else {
                        if ($id = $this->getIdPlayerLesson($player['id'], LESSON_PLAYER_STATUS_1)) {
                            array_push($idPlayerLesson, $id);
                        } else {
                            if ($id = $this->getIdPlayerLesson($player['id'], LESSON_PLAYER_STATUS_0)) {
                                array_push($idPlayerLesson, $id);
                            } else {
                                if ($id = $this->getIdPlayerLesson($player['id'], LESSON_PLAYER_STATUS_99)) {
                                    array_push($idPlayerLesson, $id);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!empty($idPlayerLesson)) {
            $datas = $datas->whereIn('player_lessons.id', $idPlayerLesson);
        }

        return $datas;
    }

    /**
     * Get id player_lesson by id player and status.
     *
     * @param int $idPlayer
     * @param int $status
     *
     * @return $idPlayerLesson
     */
    public function getIdPlayerLesson($idPlayer, $status)
    {
        $dataPlayerLesson = $this->playerLesson
                ->join('lessons', 'player_lessons.lesson_id', '=', 'lessons.id')
                ->select(
                    'player_lessons.id',
                    'player_lessons.lesson_id',
                    'lessons.lesson_date'
                )
                ->where([
                    'player_lessons.player_id' => $idPlayer,
                    'player_lessons.status' => $status,
                ]);
        $datas = $dataPlayerLesson->get();
        if (count($datas) == 1) {
            $idPlayerLesson = $datas[0]->id;
        } elseif (count($datas) > 1) {
            $lesson_date = $dataPlayerLesson->max('lessons.lesson_date');
            $dataPlayerLesson = $dataPlayerLesson->where('lessons.lesson_date', $lesson_date)->first();
            if ($dataPlayerLesson) {
                $idPlayerLesson = $dataPlayerLesson->id;
            }
        } else {
            $idPlayerLesson = null;
        }

        return $idPlayerLesson;
    }

    /**
     * Get detail player.
     *
     * @param int $playerId
     *
     * @return mixed
     */
    public function getDetailPlayers($playerId)
    {
        return $this->player
                ->join('player_lessons', 'player_lessons.player_id', '=', 'players.id')
                ->join('lessons', 'player_lessons.lesson_id', '=', 'lessons.id')
                ->select(
                    'players.id',
                    'players.updated_at',
                    'players.name',
                    'player_lessons.id as playerLessonId',
                    'player_lessons.status as playerLessonStatus',
                    'player_lessons.created_at as timeRegisterLesson',
                    'lessons.id as lessonId',
                    'lessons.name as lessonName',
                    'lessons.info_type as lessonType',
                    'lessons.lesson_date',
                    'lessons.register_expire_date'
                )
                ->where('players.id', $playerId)
                ->orderBy('player_lessons.updated_at', 'DESC')
                ->get();
    }

    /**
     * Get detail player has no lesson.
     *
     * @param int $playerId
     *
     * @return mixed
     */
    public function detailPlayerHasNoLesson($playerId)
    {
        return $this->player
                    ->where([
                        'players.id' => $playerId,
                        'players.is_deleted' => UNDELETED,
                    ])
                    ->first();
    }

    /**
     * Get all players.
     *
     * @return mixed
     */
    public function getAllPlayers()
    {
        return $this->player
                    ->select(
                        'players.id',
                        'players.name',
                        'players.updated_at'
                    )
                    ->where(['players.is_deleted' => UNDELETED])
                    ->get();
    }

    /**
     * Get array id of lessons.
     *
     * @param int $idSchool
     *
     * @return array $arr
     */
    public function getArrayIdPlayerBySchool($idSchool)
    {
        $arr = [];
        $listPlayers = $this->getListPlayers()->get();
        if (count($listPlayers)) {
            foreach ($listPlayers as $player) {
                if ($player['lessonType'] == INFO_TYPE_SCHOOL) {
                    $schools = $this->getListPlayers()
                                ->join('schools', 'schools.id', '=', 'lessons.table_id')
                                ->where([
                                    'lessons.info_type' => INFO_TYPE_SCHOOL,
                                    'schools.id' => $idSchool,
                                    'schools.is_deleted' => UNDELETED,
                                ])
                                ->get();
                    if (count($schools)) {
                        foreach ($schools as $school) {
                            array_push($arr, $school->lessonId);
                        }
                    }
                } elseif ($player['lessonType'] == INFO_TYPE_TRAINER) {
                    $trainers = $this->getListPlayers()
                                ->join('trainers', 'trainers.id', '=', 'lessons.table_id')
                                ->join('schools', 'schools.id', '=', 'trainers.school_id')
                                ->where([
                                    'lessons.info_type' => INFO_TYPE_TRAINER,
                                    'schools.id' => $idSchool,
                                    'schools.is_deleted' => UNDELETED,
                                ])
                                ->get();
                    if (count($trainers)) {
                        foreach ($trainers as $trainer) {
                            array_push($arr, $trainer->lessonId);
                        }
                    }
                }
            }
        }

        return $arr;
    }

    /**
     * Get array id of lessons.
     *
     * @param int $idTrainer
     *
     * @return array $arr
     */
    public function getArrayIdPlayerByTrainer($idTrainer)
    {
        $arr = [];
        $listPlayers = $this->getListPlayers()->get();
        if (count($listPlayers)) {
            foreach ($listPlayers as $player) {
                $trainers = $this->getListPlayers()
                        ->join('trainers', 'trainers.id', '=', 'lessons.table_id')
                        ->where([
                            'lessons.info_type' => INFO_TYPE_TRAINER,
                            'trainers.id' => $idTrainer,
                            'trainers.is_deleted' => UNDELETED,
                        ])
                        ->get();
                if (count($trainers)) {
                    foreach ($trainers as $trainer) {
                        array_push($arr, $trainer->lessonId);
                    }
                }
            }
        }

        return $arr;
    }

    /**
     * Get data account player to login from admin site by player id.
     *
     * @param $playerId
     *
     * @return mixed
     */
    public function getAccountLogonByPlayerId(int $playerId)
    {
        return $this->player->where([
                    'players.id' => $playerId,
                    'players.is_deleted' => UNDELETED,
                ])->first();
    }
}
