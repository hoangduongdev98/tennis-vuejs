<?php
namespace App\Repositories\Admin;

use App\Models\Schools;
use App\Models\Staffs;
use App\Models\Trainers;
use App\Models\Prefectures;
use Illuminate\Support\Collection;
use App\Repositories\TennisMgtRepository;

class AdminTrainerRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $school, $trainer, $prefs;
    public function __construct(Schools $school, Trainers $trainer, Prefectures $prefs)
    {
        $this->school = $school;
        $this->trainer = $trainer;
        $this->prefs = $prefs;
    }

    /**
     * Get list trainers
     *
     * @param array $search
     * @return mixed
     */
    public function getListTrainers ($search = null) {
        $lists = $this->trainer->join('schools', 'schools.id', '=', 'trainers.school_id')
                ->select(
                    'trainers.id',
                    'trainers.updated_at',
                    'trainers.name',
                    'trainers.address',
                    'trainers.prefecture_id',
                    'trainers.status',
                    'trainers.license_date',
                    'schools.id as schoolId',
                    'schools.name as schoolName',
                    'schools.address as schollAddress',
                    'schools.register_coach as schoolCoachAllow'
                )
                ->where([
                    'trainers.is_deleted' => UNDELETED,
                    'schools.is_deleted' => UNDELETED,
                ])
                ->where('schools.status', '!=', SCHOOL_STATUS_99);
        if (!empty($search)) {
            $lists = $this->searchTrainer($lists, $search);
            if ($search['search_status'] != SCHOOL_STATUS_99) {
                $lists = $lists->where('trainers.status', '!=', SCHOOL_STATUS_99);
            }
        } else {
            $lists = $lists->where('trainers.status', '!=', SCHOOL_STATUS_99);
        }
        
        return $lists->sortable(['updated_at' => 'DESC']);
    }

    /**
     * Get data trainer after search
     *
     * @param array $search
     * @param $data
     * @return mixed
     */
    public function searchTrainer ($data, array $search) {
        if (!empty($search)) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d',  strtotime($startDate));
                } else {
                    return $data->where('trainers.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d',  strtotime($endDate));
                } else {
                    return $data->where('trainers.id', null);
                }
            }
            if ($startDate && $endDate) {
                $data = $data->whereBetween('trainers.updated_at', [$startDate.' 00:00:00', $endDate.' 23:59:59']);
            } elseif ($startDate && !$endDate) {
                $data = $data->where('trainers.updated_at', '>=', $startDate.' 00:00:00');
            } elseif (!$startDate && $endDate) {
                $data = $data->where('trainers.updated_at', '<=', $endDate.' 23:59:59');
            }

            if ($nameTrainer = $search['search_name']) {
                $data = $data->where('trainers.name', 'LIKE', "%{$nameTrainer}%");
            }
            if ($schoolId = $search['search_schoolId']) {
                $data = $data->where('trainers.school_id', '=', $schoolId);
            }
            //search_prefectures
            if ($addressTrainer = $search['search_prefectures']) {
                $data = $data->where('trainers.prefecture_id', '=', $addressTrainer);
            }
            //status
            $statusTrainer = $search['search_status'];
            if ($statusTrainer !== null) {
                $data = $data->where('trainers.status', '=', $statusTrainer);
            }
        }
        return $data;
    }

    /**
     * Get list trainers
     *
     * @return mixed
     */
    public function getAllTrainers () {
        return $this->trainer
                    ->select(
                        'id',
                        'name'
                    )
                    ->where(['trainers.is_deleted' => UNDELETED])
                    ->get();
    }

    /**
     * Get trainer by id
     *
     * @param int $trainerId
     * @return mixed
     */
    public function getTrainerById ($trainerId) {
        $trainer = $this->trainer->join('schools', 'schools.id', '=', 'trainers.school_id')
                ->select(
                    'trainers.*',
                    'schools.id as schoolId',
                    'schools.name as schoolName',
                    'schools.address as schollAddress'
                )
                ->where([
                    'trainers.id' => $trainerId,
                    'trainers.is_deleted' => UNDELETED,
                    'schools.is_deleted' => UNDELETED
                ])
                ->first();

        return $trainer;
    }

    /**
     * Get data account trainer to login from admin site by trainer_id
     *
     * @param $trainerId
     * @return mixed
     */
    public function getAccountLogonByTrainerId(int $trainerId) {
        return $this->trainer->where([
                    'trainers.id' => $trainerId,
                    'trainers.status' => SCHOOL_STATUS_1,
                    'trainers.is_deleted' => UNDELETED
                ])->first();
    }
}
