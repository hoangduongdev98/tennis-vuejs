<?php

namespace App\Repositories\Admin;

use App\Models\MasterLevels;
use App\Models\Trainers;
use App\Models\Schools;
use App\Models\Lessons;
use App\Models\SettingSettlementFee;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class AdminSettingRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $level, $school, $trainer, $lesson, $settingSettlementFee;
    public function __construct(
        MasterLevels $level,
        Schools $school,
        Trainers $trainer,
        Lessons $lesson,
        SettingSettlementFee $settingSettlementFee
    ) {
        $this->level = $level;
        $this->school = $school;
        $this->trainer = $trainer;
        $this->lesson = $lesson;
        $this->settingSettlementFee = $settingSettlementFee;
    }

    /**
     * Create master level
     *
     * @param entity $level
     * @return bool
     */
    public function saveCreateMasterLevels(MasterLevels $level)
    {
        return $level->save();
    }

    /**
     * Edit master level
     *
     * @param entity $level
     * @param int $idMasterLevel
     * @return bool
     */
    public function saveEditMasterLevels(MasterLevels $level, $idMasterLevel)
    {
        return $level->save();
    }

    /**
     * Get list account managers
     *
     * @return mixed
     */
    public function getListMasterLevels()
    {
        $lists = $this->level
            ->select(
                'master_levels.id',
                'master_levels.name',
                'master_levels.display_order',
                'master_levels.updated_at'
            )
            ->where([
                'master_levels.is_deleted' => UNDELETED
            ]);

        return $lists->sortable(['display_order' => 'ASC']);
    }

    /**
     * Get data master level by id
     *
     * @param int $id
     * @return mixed
     */
    public function getMasterLevelById(int $id)
    {
        return $this->level->where(['master_levels.id' => $id, 'master_levels.is_deleted' => UNDELETED])->first();
    }

    /**
     * Delete data master level
     *
     * @param int $idMasterLevel
     * @param int $deleted_id
     * @return bool
     */
    public function deleteDataMasterLevel($idMasterLevel, $deleted_id = null)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            $dataSchool = $this->school->where(['is_deleted' => UNDELETED])->get();
            $checkLevelInSchool = $this->checkExistMasterLevelInOtherTable($dataSchool, $idMasterLevel);

            $dataTrainer = $this->trainer->where(['is_deleted' => UNDELETED])->get();
            $checkLevelInTrainer = $this->checkExistMasterLevelInOtherTable($dataTrainer, $idMasterLevel);

            $dataLesson = $this->lesson->where(['is_deleted' => UNDELETED])->get();
            $checkLevelInLesson = $this->checkExistMasterLevelInOtherTable($dataLesson, $idMasterLevel);
            if ($checkLevelInSchool || $checkLevelInTrainer || $checkLevelInLesson) {
                return false;
            } else {
                //delete master level
                $deleteMasterLevel = $this->level->find($idMasterLevel);
                $deleteMasterLevel->is_deleted = DELETED;
                $deleteMasterLevel->deleted_id = $deleted_id;
                if (!$deleteMasterLevel->save()) {
                    $delete = false;
                }
                $delete ? DB::commit() : DB::rollback();
                return $delete;
            }
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Check data master level of others table
     *
     * @param $datas
     * @return bool
     */
    public function checkExistMasterLevelInOtherTable($datas, $idMasterLevel)
    {
        $result = false;
        if (count($datas)) {
            foreach ($datas as $value) {
                if (!empty($value['levels'])) {
                    $levels = json_decode($value['levels'], true);
                    if (in_array($idMasterLevel, $levels)) {
                        return true;
                    }
                }
            }
        }
        return $result;
    }


    //settlement-fee-related-settings
    /**
     * Get list settlement fee related settings
     *
     * @return mixed
     */
    public function getSettingSettlementFee()
    {
        return $this->settingSettlementFee->where(['setting_settlement_fee.is_deleted' => UNDELETED])->get();
    }

    /**
     * Save create settlement fee related settings
     *
     * @return bool
     */
    public function saveSettlementFee(SettingSettlementFee $settingSettlementFee)
    {
        $result = true;
        DB::beginTransaction();
        try {
            if (!$settingSettlementFee->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Get data settlement fee related settings by id
     *
     * @param int $id
     * @return mixed
     */
    public function getSettlementFeeById(int $id)
    {
        return $this->settingSettlementFee->where(['setting_settlement_fee.id' => $id, 'setting_settlement_fee.is_deleted' => UNDELETED])->first();
    }

    /**
     * Get max time of settlement fee related settings
     *
     * @return $time
     */
    public function getNewUpdatedAtSettlementFee()
    {
        return $this->settingSettlementFee->max('setting_settlement_fee.updated_at');
    }
}
