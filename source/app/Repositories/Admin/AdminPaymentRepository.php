<?php

namespace App\Repositories\Admin;

use App\Models\Schools;
use App\Models\Trainers;
use App\Models\Players;
use App\Models\Payments;
use App\Models\Settlements;
use App\Models\PlayerLesson;
use App\Models\Lessons;
use Illuminate\Support\Collection;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\DB;
use Exception;

class AdminPaymentRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $payment, $settlement, $school, $trainer, $playerLesson, $lesson, $player;
    public function __construct(
        Payments $payment,
        Settlements $settlement,
        Schools $school,
        Trainers $trainer,
        PlayerLesson $playerLesson,
        Lessons $lesson,
        Players $player
    ) {
        $this->payment = $payment;
        $this->settlement = $settlement;
        $this->school = $school;
        $this->trainer = $trainer;
        $this->playerLesson = $playerLesson;
        $this->lesson = $lesson;
        $this->player = $player;
    }

    /**
     * Get list settlements
     *
     * @param array $search
     * @return mixed
     */
    public function getListSettlements($search = null)
    {
        $lists = $this->settlement
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('players', 'players.id', '=', 'player_lessons.player_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->leftJoin('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->leftJoin('schools', 'schools.id', '=', 'lessons.table_id')
            ->select(
                'settlements.id',
                'settlements.payment_date',
                'settlements.sett_price',
                'settlements.status',
                'players.id as playerId',
                'players.name as playerName',
                'lessons.name as lessonName',
                'lessons.category as lessonCategory',
                'lessons.id as lessonId',
                'lessons.table_id as idCreatorLesson',
                'lessons.info_type as lessonType',
                'lessons.payment_fee',
                'lessons.status as lessonStatus',
                'trainers.id as trainerId',
                'trainers.status as trainerSt',
                'schools.id as schoolId',
                'schools.status as schoolSt',
            );
        if (!empty($search)) {
            $lists = $this->searchSettlements($lists, $search);
            if ($search['search_status'] != SETTLEMENT_99) {
                $lists = $lists->where('settlements.status', '!=', SETTLEMENT_99);
            }
        } else {
            $lists = $lists->where('settlements.status', '!=', SETTLEMENT_99);
        }
        return $lists;
    }

    /**
     * Get data settlements after search
     *
     * @param $data
     * @param array $search
     * @return mixed
     */
    public function searchSettlements($data, array $search)
    {
        if ($search) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d',  strtotime($startDate));
                } else {
                    return $data->where('settlements.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d',  strtotime($endDate));
                } else {
                    return $data->where('settlements.id', null);
                }
            }
            if ($startDate && $endDate) {
                $data = $data->whereBetween('settlements.payment_date', [$startDate, $endDate]);
            } elseif ($startDate && !$endDate) {
                $data = $data->where('settlements.payment_date', '>=', $startDate);
            } elseif (!$startDate && $endDate) {
                $data = $data->where('settlements.payment_date', '<=', $endDate);
            }

            if ($idPlayer = $search['search_playerName']) {
                $data = $data->where('players.id', '=', $idPlayer);
            }

            if ($idSchool = $search['search_schoolName']) {
                $arr = $this->getSearchSettlementBySchool($idSchool);
                $data = $data->whereIn('settlements.id', $arr);
            }

            if ($idTrainer = $search['search_trainerName']) {
                $arrId = $this->getSearchSettlementByTrainer($idTrainer);
                $data = $data->whereIn('settlements.id', $arrId);
            }

            $statusSettlement = $search['search_status'];
            if ($statusSettlement !== null) {
                $data = $data->where('settlements.status', '=', $statusSettlement);
            }
        }
        return $data;
    }

    /**
     * Get detail settlement
     *
     * @param int $settlementId
     * @return mixed
     */
    public function getDataSettlementById(int $settlementId)
    {
        return $this->settlement
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->select(
                'settlements.id',
                'settlements.payment_date',
                'settlements.sett_price',
                'settlements.status'
            )
            ->where('settlements.id', $settlementId)
            ->first();
    }

    /**
     * Save edit settlement
     *
     * @param array $data
     * @param $userLoginId
     * @return bool
     */
    public function saveEditSettlement($data, $userLoginId = null)
    {
        $result = true;
        DB::beginTransaction();
        try {
            $settlement = $this->settlement->where('settlements.id', $data['id_settlement'])->first();
            if ($settlement) {
                $settlement->status = $data['edit_status'];
                $settlement->updated_id = $userLoginId;
                // Edit payment
                $dataPayments = $this->payment->where(['payments.settlement_id' => $data['id_settlement']])->get();
                if (count($dataPayments)) {
                    foreach ($dataPayments as $dataPayment) {
                        $paymentItem = $this->payment->find($dataPayment['id']);
                        if ($data['edit_status'] != SETTLEMENT_99) {
                            $paymentItem->status = PAYMENT_STATUS_0;
                        } else {
                            $paymentItem->status = PAYMENT_STATUS_99;
                        }
                        $paymentItem->updated_id = $userLoginId;
                        if (!$paymentItem->save()) {
                            $result = false;
                        }
                    }
                }
                // Refund points for players
                if (isset($data['player_id']) && $data['player_id']) {
                    $dataPlayer = $this->player->find($data['player_id']);
                    $playerLesson = $this->playerLesson->find($settlement['player_lesson_id']);
                    if ($data['edit_status'] == SETTLEMENT_99) {
                        if ($playerLesson['status'] == LESSON_PLAYER_STATUS_1 && $playerLesson['refunded'] == UNREFUNDED) {
                            $dataPlayer->point = $dataPlayer->point + $settlement->sett_price;
                            $playerLesson['refunded'] = REFUNDED;
                        }
                    } else if ($data['edit_status'] == SETTLEMENT_1) {
                        if ($playerLesson['refunded'] == REFUNDED) {
                            $dataPlayer->point = $dataPlayer->point - $settlement->sett_price;
                            $playerLesson['refunded'] = UNREFUNDED;
                        }
                    }
                    if (!$dataPlayer->save() || !$playerLesson->save()) {
                        $result = false;
                    }
                }
                if (!$settlement->save()) {
                    $result = false;
                }
            } else {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Get array id of settlement
     *
     * @param int $idSchool
     * @return array $arr
     */
    public function getSearchSettlementBySchool($idSchool)
    {
        $arr = [];
        $schoolSettlements = $this->settlement
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('players', 'players.id', '=', 'player_lessons.player_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->join('schools', 'schools.id', '=', 'lessons.table_id')
            ->where([
                'lessons.info_type' => INFO_TYPE_SCHOOL,
                'schools.id' => $idSchool
            ])
            ->select(
                'settlements.id'
            )
            ->get();
        if (count($schoolSettlements)) {
            foreach ($schoolSettlements as $schoolSettlement) {
                array_push($arr, $schoolSettlement->id);
            }
        }

        $trainerSchoolSett = $this->settlement
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('players', 'players.id', '=', 'player_lessons.player_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->join('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->join('schools', 'schools.id', '=', 'trainers.school_id')
            ->where([
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'schools.id' => $idSchool
            ])->select(
                'settlements.id'
            )
            ->get();
        if (count($trainerSchoolSett)) {
            foreach ($trainerSchoolSett as $trainerSett) {
                array_push($arr, $trainerSett->id);
            }
        }
        return $arr;
    }

    /**
     * Get array id of settlement
     *
     * @param int $idTrainer
     * @return array $arr
     */
    public function getSearchSettlementByTrainer($idTrainer)
    {
        $arr = [];
        $trainerSettlements = $this->settlement
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('players', 'players.id', '=', 'player_lessons.player_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->join('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->where([
                'lessons.info_type' => INFO_TYPE_TRAINER,
                'trainers.id' => $idTrainer
            ])->select(
                'settlements.id'
            )
            ->get();
        if (count($trainerSettlements)) {
            foreach ($trainerSettlements as $sett) {
                array_push($arr, $sett->id);
            }
        }
        return $arr;
    }

    /**
     * Delete settlement
     *
     * @param int $settlementId
     * @return bool
     */
    public function saveDeleteSettlement(int $settlementId)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            $dataSettlement = $this->settlement
                ->where('settlements.id', $settlementId)
                ->where('settlements.status', '!=', SETTLEMENT_99)
                ->first();
            if ($dataSettlement) {
                $dataSettlement->status = SETTLEMENT_99;
                if ($dataSettlement->save()) {
                    $dataPayments = $this->payment
                        ->where('payments.settlement_id', $settlementId)
                        ->where('payments.status', '!=', PAYMENT_STATUS_99)
                        ->get();
                    if (count($dataPayments)) {
                        foreach ($dataPayments as $value) {
                            $deletePayment = $this->payment->find($value['id']);
                            $deletePayment->status = PAYMENT_STATUS_99;
                            if (!$deletePayment->save()) {
                                $delete = false;
                            }
                        }
                    }
                } else {
                    $delete = false;
                }
            } else {
                $delete = false;
            }
            $delete ? DB::commit() : DB::rollback();
            return $delete;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Get list payments
     *
     * @return mixed
     */
    public function getListPayments()
    {
        return $this->payment
            ->join('settlements', 'settlements.id', '=', 'payments.settlement_id')
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('players', 'players.id', '=', 'player_lessons.player_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->select(
                'payments.id',
                'payments.payment_date',
                'payments.info_type',
                'payments.payment_point',
                'payments.status as paymentStatus',
                'settlements.id as settlementId',
                'settlements.status as settlementStatus',
                'players.id as playerId',
                'players.name as playerName',
                'lessons.id as lessonId',
                'lessons.name as lessonName',
                'lessons.category as lessonCategory',
                'lessons.info_type as lessonType',
                'lessons.payment_fee as lessonPrice',
                'lessons.table_id as lessonCreatorId'
            );
    }

    /**
     * Get list all payments
     *
     * @param array $search
     * @return mixed
     */
    public function getAllDataPayments($search = null)
    {
        isset($search['search_status']) && !empty($search['search_status']) ? $searchStatus = $search['search_status'] : $searchStatus = null;
        $data = $this->listPaymentOfSchool($searchStatus)->sortByDesc('monthYear');
        if (isset($search['sort']) && isset($search['direction'])) {
            $sort = $search['sort'];
            $direction = $search['direction'];
            if (strtolower($direction) == 'desc') {
                $data = $data->sortByDesc($sort);
            } else {
                $data = $data->sortBy($sort);
            }
        }
        if (!empty($search)) {
            $data = $this->searchDataPayment($data, $search);
            if ($search['search_status'] != PAYMENT_STATUS_99) {
                $data = $data->where('paymentStatus', '!=', PAYMENT_STATUS_99);
            }
        }

        return $data->values();
    }

    /**
     * Get payments by school id
     *
     * @param int $schoolId
     * @param string $monthYear
     * @return mixed
     */
    public function getDataPaymentBySchoolId(int $schoolId, $monthYear = null, $type = null)
    {
        $time = null;
        if ($monthYear) {
            $monthYear = str_replace('-', '/', $monthYear);
            $time = explode('/', $monthYear);
        }
        $schoolPayments1 = $this->getListPayments()
            ->join('schools', 'lessons.table_id', '=', 'schools.id')
            ->where([
                'payments.info_type' => INFO_TYPE_SCHOOL,
                'lessons.info_type' => INFO_TYPE_SCHOOL
            ])
            ->where('schools.id', $schoolId);
        if (!empty($type)) {
            $schoolPayments1 = $schoolPayments1->where('payments.status', '!=', PAYMENT_STATUS_99);
        }
        if ($time) {
            $schoolPayments1 = $schoolPayments1->whereMonth('payments.payment_date', $time[1])->whereYear('payments.payment_date', $time[0]);
        }
        $schoolPayments1 = $schoolPayments1->addSelect(
            'payments.payment_date as paymentDate',
            'payments.payment_point as paymentPoint'
        );

        $schoolPayments2 = $this->getListPayments()
            ->join('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->join('schools', 'schools.id', '=', 'trainers.school_id')
            ->where([
                'payments.info_type' => INFO_TYPE_SCHOOL,
                'lessons.info_type' => INFO_TYPE_TRAINER
            ])
            ->where('schools.id', $schoolId);
        if (!empty($type)) {
            $schoolPayments2 = $schoolPayments2->where('payments.status', '!=', PAYMENT_STATUS_99);
        }
        if ($time) {
            $schoolPayments2 = $schoolPayments2->whereMonth('payments.payment_date', $time[1])->whereYear('payments.payment_date', $time[0]);
        }
        $schoolPayments2 = $schoolPayments2->addSelect(
            'payments.payment_date as paymentDate',
            'payments.payment_point as paymentPoint'
        );

        return $schoolPayments1->union($schoolPayments2)->sortable(['paymentDate' => 'DESC']);
    }

    /**
     * Get payments by trainer id
     *
     * @param int $trainerlId
     * @param string $monthYear
     * @return mixed
     */
    public function getDataPaymentByTrainerId(int $trainerlId, $monthYear = null)
    {
        $time = null;
        if ($monthYear) {
            $monthYear = str_replace('-', '/', $monthYear);
            $time = explode('/', $monthYear);
        }
        $arrId = $this->getSearchSettlementByTrainer($trainerlId);
        $trainerPayments = $this->getListPayments()
            ->where([
                'payments.info_type' => INFO_TYPE_TRAINER,
            ])
            ->where('payments.status', '!=', PAYMENT_STATUS_99)
            ->whereIn('settlements.id', $arrId);
        if ($time) {
            $trainerPayments = $trainerPayments
                ->whereMonth('payments.payment_date', $time[1])
                ->whereYear('payments.payment_date', $time[0]);
        }
        $trainerPayments = $trainerPayments->addSelect(
            'payments.payment_date as paymentDate',
            'payments.payment_point as paymentPoint'
        );
        return $trainerPayments->sortable(['paymentDate' => 'DESC']);
    }


    /**
     * Get list payments of school
     *
     * @return mixed
     */
    public function listPaymentOfSchool($searchStatus = null)
    {
        $schoolPayments = collect();
        $schoolPayments1 = $this->listPaymentLessonCreateBySchool()->get();
        $schoolPayments2 = $this->listPaymentLessonCreateByTrainerOfSchool()->get();
        if (count($schoolPayments1)) {
            foreach ($schoolPayments1 as $payments1) {
                $schoolPayments2->add($payments1);
            }
        }
        if (empty($searchStatus)) {
            $datas = $schoolPayments2->where('paymentStatus', '!=', PAYMENT_STATUS_99);
        } else {
            $datas = $schoolPayments2;
        }
        if (count($datas)) {
            $yearMonth = collect();
            foreach ($datas as $key => $data) {
                $yearMonthItem = $data->year . $data->month . $data->schoolId;
                if (isset($yearMonth[$yearMonthItem])) {
                    $yearMonth[$yearMonthItem]->setAttribute('statusTemp', $data->paymentStatus);
                    $yearMonth[$yearMonthItem]->payment_point += $data->payment_point;
                } else {
                    $yearMonth->put($yearMonthItem, $data);
                }
                $yearMonth[$yearMonthItem]->setAttribute('totalPayment', $yearMonth[$yearMonthItem]->payment_point);
                if ($yearMonth[$yearMonthItem]->statusTemp !== null && $yearMonth[$yearMonthItem]->statusTemp != PAYMENT_STATUS_99) {
                    $yearMonth[$yearMonthItem]->paymentStatus = $yearMonth[$yearMonthItem]->statusTemp;
                }
            }
            $schoolPayments = $yearMonth;
        }
        return $schoolPayments;
    }

    /**
     * Get list payments lesson created by school
     *
     * @return mixed
     */
    public function listPaymentLessonCreateBySchool()
    {
        return $this->payment
            ->join('settlements', 'settlements.id', '=', 'payments.settlement_id')
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('players', 'players.id', '=', 'player_lessons.player_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->join('schools', 'lessons.table_id', '=', 'schools.id')
            ->where([
                'payments.info_type' => INFO_TYPE_SCHOOL,
                'lessons.info_type' => INFO_TYPE_SCHOOL
            ])
            ->select(
                'schools.id as schoolId',
                'schools.name as schoolName',
                'schools.status as schoolSt',
                'payments.status as paymentStatus',
                'payments.payment_date as monthYear',
                'payments.payment_point',
                DB::raw('null as trainerId'),
                DB::raw('null as trainerName'),
                DB::raw('YEAR(payments.payment_date) year, MONTH(payments.payment_date) month')
            );
    }

    /**
     * Get list payments lesson created by trainer of school
     *
     * @return mixed
     */
    public function listPaymentLessonCreateByTrainerOfSchool()
    {
        return $this->payment
            ->join('settlements', 'settlements.id', '=', 'payments.settlement_id')
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('players', 'players.id', '=', 'player_lessons.player_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->join('trainers', 'trainers.id', '=', 'lessons.table_id')
            ->join('schools', 'schools.id', '=', 'trainers.school_id')
            ->where([
                'payments.info_type' => INFO_TYPE_SCHOOL,
                'lessons.info_type' => INFO_TYPE_TRAINER
            ])
            ->select(
                'schools.id as schoolId',
                'schools.name as schoolName',
                'schools.status as schoolSt',
                'payments.status as paymentStatus',
                'payments.payment_date as monthYear',
                'payments.payment_point',
                DB::raw('null as trainerId'),
                DB::raw('null as trainerName'),
                DB::raw('YEAR(payments.payment_date) year, MONTH(payments.payment_date) month')
            );
    }

    /**
     * Get list payments of trainer
     *
     * @param array $search
     * @return mixed
     */
    public function listPaymentOfTrainer()
    {
        $trainerPayments = $this->payment
            ->join('settlements', 'settlements.id', '=', 'payments.settlement_id')
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('players', 'players.id', '=', 'player_lessons.player_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->join('trainers', 'lessons.table_id', '=', 'trainers.id')
            ->where([
                'payments.info_type' => INFO_TYPE_TRAINER,
                'lessons.info_type' => INFO_TYPE_TRAINER
            ])
            ->select(
                DB::raw('null as schoolId'),
                DB::raw('null as schoolName'),
                'payments.status as paymentStatus',
                'payments.payment_date as monthYear',
                'payments.payment_point',
                'trainers.id as trainerId',
                'trainers.name as trainerName',
                DB::raw('YEAR(payments.payment_date) year, MONTH(payments.payment_date) month'),
                DB::raw('SUM(payments.payment_point) as totalPayment')
            )
            ->groupBy('trainers.id', 'year', 'month');
        return $trainerPayments->sortable(['monthYear' => 'DESC'])->get();
    }

    /**
     * Get data list payments after search
     *
     * @param array $data
     * @param array $search
     * @return array $result
     */
    public function searchDataPayment($data, $search)
    {
        if (!empty($search)) {
            if ($search['search_monthPayment']) {
                $time = explode('/', $search['search_monthPayment']);
                $data = $data->where('month', $time[1])->where('year', $time[0]);
            }
            if ($idSchool = $search['search_schoolName']) {
                $data = $data->where('schoolId', $idSchool);
            }
            if ($idTrainer = $search['search_trainerName']) {
                $data = $data->whereIn('trainerId', $idTrainer);
            }

            $statusPayment = $search['search_status'];
            if ($statusPayment !== null) {
                $data = $data->where('paymentStatus', '=', $statusPayment);
            }
        }
        return $data;
    }

    /**
     * Get data payment by id
     *
     * @param int $paymentId
     * @return \Illuminate\Http\Response
     */
    public function findPaymentById(int $paymentId)
    {
        return $this->payment
            ->join('settlements', 'settlements.id', '=', 'payments.settlement_id')
            ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
            ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
            ->select(
                'payments.id',
                'payments.payment_date',
                'payments.info_type as paymentInfoType',
                'payments.payment_point',
                'payments.status as paymentStatus',
                'settlements.id as settlementId',
                'settlements.status as settlementStatus',
                'lessons.name as lessonName',
                'lessons.id as lessonId',
                'lessons.info_type as lessonType'
            )
            ->where([
                'payments.id' => $paymentId
            ])
            ->first();
    }

    /**
     * Save edit payment status
     *
     * @param array $arrId
     * @param int $userLoginId
     * @return bool
     */
    public function editPaymentStatus($paymentId, $status, $userLoginId = null)
    {
        $result = true;
        DB::beginTransaction();
        try {
            if ($editPayment = $this->payment->find($paymentId)) {
                $editPayment->status = $status;
                $editPayment->updated_id = $userLoginId;
                if (!$editPayment->save()) {
                    $result = false;
                }
            } else {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Save edit settlement
     *
     * @param array $data
     * @param int $userLoginId
     * @return bool
     */
    public function saveEditPayment($data, $userLoginId = null)
    {
        $result = true;
        DB::beginTransaction();
        try {
            $payment = $this->payment->find($data['id_payment']);
            if ($payment) {
                $payment->payment_point = $data['edit_price'];
                $payment->updated_id = $userLoginId;
                if (!$payment->save()) {
                    $result = false;
                }
            } else {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Save for delete payment
     *
     * @param int $paymentId
     * @param $userLoginId
     * @return bool
     */
    public function saveDeletePayment($paymentId, $userLoginId = null)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            $dataPayment = $this->payment->where('payments.id', $paymentId)->first();
            if ($dataPayment) {
                $dataPayment->status = PAYMENT_STATUS_99;
                $dataPayment->updated_at = $userLoginId;
                if (!$dataPayment->save()) {
                    $delete = false;
                }
            } else {
                $delete = false;
            }
            $delete ? DB::commit() : DB::rollback();
            return $delete;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Save for delete payment of school by month, year
     *
     * @param int $schoolId
     * @param $monthYear
     * @param $userLoginId
     * @return bool
     */
    public function saveDeletePaymentOfSchoolOrTrainer($id, $monthYear, $type, $userLoginId = null)
    {
        $delete = true;
        DB::beginTransaction();
        try {
            if ($type == INFO_TYPE_SCHOOL) {
                $dataPayments = $this->getDataPaymentBySchoolId($id, $monthYear);
            } else if ($type == INFO_TYPE_TRAINER) {
                $dataPayments = $this->getDataPaymentByTrainerId($id, $monthYear);
            }
            $dataPayments = $dataPayments->get();
            if (count($dataPayments)) {
                foreach ($dataPayments as $data) {
                    $dataPayment = $this->payment->find($data['id']);
                    $dataPayment->status = PAYMENT_STATUS_99;
                    $dataPayment->updated_id = $userLoginId;
                    if (!$dataPayment->save()) {
                        $delete = false;
                    }
                }
            }
            $delete ? DB::commit() : DB::rollback();
            return $delete;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

}
