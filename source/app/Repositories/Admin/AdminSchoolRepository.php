<?php
namespace App\Repositories\Admin;

use App\Models\Schools;
use App\Models\Staffs;
use App\Models\Images;
use App\Models\AdminLogin;
use App\Models\Lessons;
use App\Models\PlayerLesson;
use App\Models\Trainers;
use App\Models\Payments;
use App\Models\Settlements;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class AdminSchoolRepository extends TennisMgtRepository
{
    /**
     * @var \App\Models\model
     */
    protected $school, $staff, $image, $trainer, $lesson, $playerLesson, $settlement, $payment;

    public function __construct(
        Schools $school,
        Staffs $staff,
        Images $image,
        Trainers $trainer,
        Lessons $lesson,
        PlayerLesson $playerLesson,
        Payments $payment,
        Settlements $settlement
    )
    {
        $this->school = $school;
        $this->staff = $staff;
        $this->image = $image;
        $this->trainer = $trainer;
        $this->lesson = $lesson;
        $this->playerLesson = $playerLesson;
        $this->payment = $payment;
        $this->settlement = $settlement;
    }

    /**
     * Create school
     *
     * @param entity $school
     * @param entity $staff
     * @param array $arrImage
     * @return bool
     */
    public function saveCreateSchool (Schools $school, Staffs $staff, $arrImage) {
        $return = true;
        DB::beginTransaction();
        try{
            if ($school->save()) {
                $staff->school_id = $school->id;
                if ($staff->save()) {
                    if (!empty($arrImage)) {
                        foreach ($arrImage as $image) {
                            $dirTmp = AVATAR_SCHOOL_SAVE_TMP . $image['name'];
                            $dirSave = AVATAR_SCHOOL_SAVE . $image['name'];
                            if (file_exists($dirTmp)) {
                                @rename($dirTmp, $dirSave);
                            }
                            $image->table_id = $school->id;
                            if (!$image->save()) {
                                $return = false;
                            }
                        }
                    }
                } else {
                    $return = false;
                }
            } else {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }

    /**
     * Get list school
     *
     * @param array $search
     * @return mixed
     */
    public function getListSchool ($search = null) {
        $lists = $this->school
                ->join('staffs', 'schools.id', '=', 'staffs.school_id')
                ->select(
                    'schools.id',
                    'schools.updated_at',
                    'schools.name',
                    'schools.contract_id',
                    'schools.address',
                    'schools.prefecture_id',
                    'schools.status',
                    'schools.representative',
                    'schools.license_date',
                    'schools.phone',
                    'staffs.id as staffs_id',
                    'staffs.school_manager',
                    'staffs.email',
                    'staffs.password',
                )
                ->where('staffs.login_type', INFO_TYPE_SCHOOL)
                ->groupBy('schools.id');
        if (!empty($search)) {
            $lists = $this->searchSchool($lists, $search);
            if ($search['search_status'] != SCHOOL_STATUS_99) {
                $lists = $lists->where('schools.status', '!=', SCHOOL_STATUS_99);
            }
        } else {
            $lists = $lists->where('schools.status', '!=', SCHOOL_STATUS_99);
        }

        return $lists->sortable(['updated_at' => 'DESC']);
    }

    /**
     * Get data school after search
     *
     * @param $data
     * @param array $search
     * @return mixed
     */
    public function searchSchool ($data, array $search) {
        if ($search) {
            $startDate = $search['search_startDate'];
            $endDate = $search['search_endDate'];
            if ($startDate) {
                if (checkValidDate($startDate)) {
                    $startDate = date('Y-m-d',  strtotime($startDate));
                } else {
                    return $data->where('schools.id', null);
                }
            }
            if ($endDate) {
                if (checkValidDate($endDate)) {
                    $endDate = date('Y-m-d',  strtotime($endDate));
                } else {
                    return $data->where('schools.id', null);
                }
            }
            if ($startDate && $endDate) {
                $data = $data->whereBetween('schools.updated_at', [$startDate.' 00:00:00', $endDate.' 23:59:59']);
            } elseif ($startDate && !$endDate) {
                $data = $data->where('schools.updated_at', '>=', $startDate.' 00:00:00');
            } elseif (!$startDate && $endDate) {
                $data = $data->where('schools.updated_at', '<=', $endDate.' 23:59:59');
            }

            if ($nameSchool = $search['search_name']) {
                $data = $data->where('schools.name', 'LIKE', "%{$nameSchool}%");
            }
            $planSchool = $search['search_plan'];
            if ($planSchool !== null) {
                $data = $data->where('schools.contract_id', '=', $planSchool);
            }
            //search_prefectures
            if ($addressSchool = $search['search_prefectures']) {
                $data = $data->where('schools.prefecture_id', '=', $addressSchool);
            }
            //status
            $statusSchool = $search['search_status'];
            if ($statusSchool !== null) {
                $data = $data->where('schools.status', '=', $statusSchool);
            }
        }
        return $data;
    }

    /**
     * Get school by id
     *
     * @param int $id
     * @return mixed
     */
    public function getSchoolById ($id) {
        return $this->school->join('staffs', 'schools.id', '=', 'staffs.school_id')
                ->where([
                    'schools.id' => $id,
                    'staffs.role' => ROLE_1,
                    'staffs.login_type' => ROLE_1
                ])
                ->select('schools.*', 'staffs.*', 'schools.updated_at as updated_time', 'staffs.id as staffId', 'schools.id as schoolId')
                ->first();
    }

    /**
     * Edit school
     *
     * @param entity $school
     * @param entity $staff
     * @param array $arrImage
     * @param int $idSchool
     * @return bool
     */
    public function saveEditSchool (Schools $school, Staffs $staff, $arrImage, $idSchool) {
        $return = true;
        DB::beginTransaction();
        try{
            if ($school->save()) {
                if ($staff->save()) {
                    if (!empty($arrImage)) {
                        foreach ($arrImage as $image) {
                            $dirTmp = AVATAR_SCHOOL_SAVE_TMP . $image['name'];
                            $dirSave = AVATAR_SCHOOL_SAVE . $image['name'];
                            if (file_exists($dirTmp)) {
                                @rename($dirTmp, $dirSave);
                            }
                            $image->table_id = $school->id;
                            if (!$image->save()) {
                                $return = false;
                            }
                        }
                    }
                } else {
                    $return = false;
                }
            } else {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }

    /**
     * Get staff by id of school
     *
     * @param int $idSchool
     * @return mixed
     */
    public function findStaffBySchoolId (int $idSchool) {
        return $this->staff->where([
                    'staffs.school_id' => $idSchool,
                    'staffs.login_type' => INFO_TYPE_SCHOOL,
                    'staffs.role' => ROLE_1
                ])->first();
    }

    /**
     * Get images by school_id
     *
     * @param int $idSchool
     * @param int $position
     * @return mixed
     */
    public function findImageBySchool (int $idSchool, $position) {
        return $this->image->where([
            'position' => $position,
            'table_id' => $idSchool,
            'info_types' => INFO_TYPE_SCHOOL,
            //'is_deleted' => UNDELETED
        ])->first();
    }

    /**
     * Get all images by school_id
     *
     * @param int $idSchool
     * @return mixed
     */
    public function findAllImageBySchoolId (int $idSchool) {
        return $this->image->where([
                    'table_id' => $idSchool,
                    'info_types' => INFO_TYPE_SCHOOL,
                    //'is_deleted' => UNDELETED
                ])->get();
    }

    /**
     * Save delete school
     *
     * @param int $idSchool
     * @param int $deleted_id
     * @return bool
     */
    public function deleteDataSchool(int $idSchool, $deleted_id = null) {
        $return = true;
        DB::beginTransaction();
        try {
            // delete school
            $deleteSchool = $this->school->find($idSchool);
            $deleteSchool->status = SCHOOL_STATUS_99;
            if ($deleteSchool->save()) {
                $data = $this->staff->where('school_id', $idSchool)->get();
                $deteled = $this->deleteSchoolOfOtherTable($data, $this->staff);
                if (!$deteled) {
                    $return = false;
                }
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Save delete data school of others table
     *
     * @param array $datas
     * @param $tableName
     * @param $deleted_id
     * @return bool
     */
    public function deleteSchoolOfOtherTable($datas, $tableName, $deleted_id = null) {
        $result = true;
        if (count($datas)) {
            foreach ($datas as $value) {
                $table = $tableName->find($value['id']);
                //$table->is_deleted = DELETED;
                if (isset($table->status)) {
                    $table->status = SCHOOL_STATUS_99;
                }
                $table->is_deleted = DELETED;
                $table->deleted_id = $deleted_id;
                if (!$table->save()) {
                    $result = false;
                }
            }
        }
        return $result;
    }

    /**
     * Save delete data school for table lessons and payments
     *
     * @param array $dataLessons
     * @param $deleted_id
     * @return bool
     */
    public function deleteLessonAndPaymentOfSchool ($dataLessons, $deleted_id = null) {
        $result = true;
        if (count($dataLessons)) {
            foreach ($dataLessons as $lesson) {
                $dataPlayerLessons = $this->playerLesson
                                    ->where('player_lessons.lesson_id', $lesson->id)
                                    ->where('player_lessons.status', '!=', LESSON_PLAYER_STATUS_99)
                                    ->get();
                if (count($dataPlayerLessons)) {
                    foreach ($dataPlayerLessons as $playerLessons) {
                        $dataSettlement = $this->settlement
                                        ->where('settlements.player_lesson_id', $playerLessons->id)
                                        ->where('settlements.status', '!=', SETTLEMENT_99)
                                        ->get();
                        if (count($dataSettlement)) {
                            foreach ($dataSettlement as $settlement) {
                                $dataPayment = $this->payment
                                            ->where('payments.settlement_id', $settlement->id)
                                            ->where('payments.status', '!=', PAYMENT_STATUS_99)
                                            ->get();
                                if (!$this->deleteSchoolOfOtherTable($dataPayment, $this->payment, $deleted_id)) {
                                    $result = false;
                                }
                            }
                        }
                        if (!$this->deleteSchoolOfOtherTable($dataSettlement, $this->settlement, $deleted_id)) {
                            $result = false;
                        }
                    }
                }
                if (!$this->deleteSchoolOfOtherTable($dataPlayerLessons, $this->playerLesson, $deleted_id)) {
                    $result = false;
                }
            }
        }
        return $result;
    }

    /**
     * Get list schools
     *
     * @return mixed
     */
    public function getAllSchools () {
        return $this->school
                    ->select(
                        'schools.id',
                        'schools.name'
                    )
                    ->get();
    }

}
