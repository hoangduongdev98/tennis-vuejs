<?php

namespace App\Repositories\Admin;

use App\Models\Lessons;
use App\Models\Matches;
use App\Models\Players;
use App\Models\Schools;
use App\Models\Settlements;
use App\Models\Trainers;
use App\Repositories\TennisMgtRepository;
use Illuminate\Support\Facades\Config;

class HomeRepository extends TennisMgtRepository
{
    protected $school;
    protected $lesson;
    protected $trainer;
    protected $player;
    protected $matches;
    protected $settlement;

    public function __construct(
        Schools $school,
        Lessons $lesson,
        Trainers $trainer,
        Players $player,
        Matches $matches,
        Settlements $settlement
    ) {
        $this->school = $school;
        $this->lesson = $lesson;
        $this->trainer = $trainer;
        $this->player = $player;
        $this->matches = $matches;
        $this->settlement = $settlement;
    }

    /**
     * Count data of schools.
     *
     * @return array $data
     */
    public function countDataOfSchools()
    {
        $data = [];
        $data['school_registration'] = $this->school->where('schools.status', SCHOOL_STATUS_0)->count();
        $data['in_service'] = $this->school->where('schools.status', SCHOOL_STATUS_1)->count();
        $data['end_service'] = $this->school->where('schools.status', SCHOOL_STATUS_2)->count();

        return $data;
    }

    /**
     * Count data of trainers.
     *
     * @return array $data
     */
    public function countDataOfTrainers()
    {
        $data = [];
        $data['trainer_registration'] = $this->trainer->where(['trainers.status' => SCHOOL_STATUS_0, 'trainers.is_deleted' => UNDELETED])->count();
        $data['in_service'] = $this->trainer->where(['trainers.status' => SCHOOL_STATUS_1, 'trainers.is_deleted' => UNDELETED])->count();
        $data['end_service'] = $this->trainer->where(['trainers.status' => SCHOOL_STATUS_2, 'trainers.is_deleted' => UNDELETED])->count();

        return $data;
    }

    /**
     * Count data of players.
     *
     * @return array $data
     */
    public function countDataOfPlayers()
    {
        $data = [];
        $players = $this->player->whereMonth('players.created_at', date('m'))->whereYear('players.created_at', date('Y'));
        $data['player_registration'] = $players->count();
        $data['player_old_register'] = $this->player->whereNotIn('players.id', $players->pluck('players.id'))->count();

        return $data;
    }

    /**
     * Count data lessons;.
     *
     * @return array $data
     */
    public function countDataOfLessons()
    {
        $data = [];
        $lessonOfMonth = $this->lesson->whereMonth('lessons.created_at', date('m'))->whereYear('lessons.created_at', date('Y'));
        $data['lesson_registration'] = $lessonOfMonth->count();
        $data['lesson_old_register'] = $this->lesson->whereNotIn('lessons.id', $lessonOfMonth->pluck('lessons.id'))->count();

        return $data;
    }

    /**
     * Count data lessons;.
     *
     * @return array $data
     */
    public function countDataOfMatches()
    {
        $data = [];
        $dataMatches = $this->matches->whereMonth('matches.created_at', date('m'))->whereYear('matches.created_at', date('Y'))->where('matches.is_deleted', UNDELETED);
        $data['matches_registration'] = $dataMatches->count();
        $data['matches_old_register'] = $this->matches->whereNotIn('matches.id', $dataMatches->pluck('matches.id'))->where('matches.is_deleted', UNDELETED)->count();

        return $data;
    }

    /**
     * Count number lessons in a year.
     *
     * @return array $data
     */
    public function getNumbersLessonOneYear()
    {
        $data = [];
        for ($i = 1; $i < 13; $i++) {
            $lessons = $this->lesson
                        ->where([
                            'lessons.is_deleted' => UNDELETED,
                        ])
                        ->whereMonth('lessons.created_at', $i)
                        ->whereYear('lessons.created_at', date('Y'))
                        ->where('lessons.status', '!=', LESSON_STATUS_99)
                        ->count();
            array_push($data, $lessons);
        }
        return $data;
    }

    /**
     * Count number lessons in a year.
     *
     * @return array $data
     */
    public function getContractFeeSchoolOneYear()
    {
        $data = [];
        for ($i = 1; $i < 13; $i++) {
            $sum = 0;
            $schools = $this->school
                        ->whereMonth('schools.created_at', $i)
                        ->whereYear('schools.created_at', date('Y'))
                        ->where('schools.status', '!=', SCHOOL_STATUS_99)
                        ->get();
            if (count($schools)) {
                foreach ($schools as $value) {
                    $sum += Config::get('const.list_price_contracts')[$value['contract_id']];
                }
            }
            array_push($data, $sum);
        }

        return $data;
    }

    /**
     * Count number lessons in a year.
     *
     * @return array $data
     */
    public function getPointOneYear()
    {
        $data = [];
        for ($i = 1; $i < 13; $i++) {
            $sum = 0;
            $settlements = $this->settlement
                        ->join('player_lessons', 'player_lessons.id', '=', 'settlements.player_lesson_id')
                        ->join('players', 'players.id', '=', 'player_lessons.player_id')
                        ->join('lessons', 'lessons.id', '=', 'player_lessons.lesson_id')
                        ->select('lessons.payment_fee as lessonFee')
                        ->whereMonth('settlements.created_at', $i)
                        ->whereYear('settlements.created_at', date('Y'))
                        ->where('settlements.status', '!=', SETTLEMENT_99)
                        ->get();
            if (count($settlements)) {
                foreach ($settlements as $sett) {
                    $sum += $sett['lessonFee'];
                }
            }
            array_push($data, $sum);
        }

        return $data;
    }
}
