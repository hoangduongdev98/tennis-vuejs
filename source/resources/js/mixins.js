import moment from 'moment';
import constValue from "./const";

export default {
    data: () => ({
        errorImg: [],
        srcNoImage: '/dist/img/no-image.png',
        store: null,
        status_post_code: null,
        inputStatus: null,
        prefixPath: location.pathname,
        formSearch: {
            page: 1,
            limit: '20',
            col: '',
            type: '',
            userStatus:'',
            started_at: '',
            ended_at: '',
            name: '',
            lessonName: '',
            level:'',
        },
        PAGINATE_LIMIT_20: constValue.PAGINATE_LIMIT_20,
        hiddenPassword: true,
        subPostCode: '',
        message: '',
        route_name: '',
        totalValue: '',
        totalPage: '',
        disableInput: false,
        searchPostCodeInEdit: false,
        loadIcon: false,
        parameters: ['name', 'plan', 'prefecture', 'limit', 'started_at', 'ended_at', 'kameiten_id', 'account_type_id', 'phone_number', 'page', 'col', 'type'],
        disableButton: false,
        disableTargetMember: false,
    }),
    computed: {
        // route laravel
        routeName() {
            return route;
        },
    },
    mounted() {
        // this.removeLocalStoge();
        this.disableInput = localStorage.getItem('disable' + '-' + this.prefixPath) ? true : false;
        // $("input[name=plan_options]").attr('disabled', 'disabled');
        // this.exit = localStorage.getItem('exit') ? localStorage.getItem('exit') : '';
        // this.submit = localStorage.getItem('submit') ? localStorage.getItem('submit') : '更新';
    },
    methods: {
        callApiInAdminList(url, store) {
            this.loadIcon = true;
            axios.post(route(url), this.formSearch)
                .then((response) => {
                    this.$store.commit(store, response.data.data)
                    this.totalPage = response.data.last_page;
                    this.totalValue = response.data.total;
                    this.setTimeOutLoading();
                })
        },
        setTimeOutLoading() {
            setTimeout(() => {
                this.loadIcon = false;
            }, 1200);
        },
        goToDetail(router, id = 0) {
            let url = '';
            if (id !== 0) {
                url = route(router, id)
            } else {
                url = route(router)
            }
            url += '?page=' + this.formSearch.page;

            this.parameters.forEach((v) => {
                if (this.formSearch[v] !== '') {
                    url += '&' + v + '=' + this.formSearch[v]
                }
            })
            window.location.href = url;
        },

        formatDate(datetime, format = "YYYY/MM/DD") {
            if (datetime === undefined || datetime === null) {
                return '';
            }
            return moment(String(datetime)).format(format)
        },
        formatTime(datetime, format = "HH:mm") {
            if (datetime === undefined || datetime === null) {
                return '';
            }
            return moment(String(datetime)).format(format)
        },
        formatDateTime(datetime, format = "YYYY/MM/DD HH:mm") {
            if (datetime === undefined || datetime === null) {
                return '';
            }
            return moment(String(datetime)).format(format)
        },
        formatMoney(money) {
            if (money === undefined) {
                return '';
            }
            return money.toLocaleString('en');
        },
          // add value input search date
        handleBlurInputSearchDate(nameInputDateSearch) {
            this.formSearch[nameInputDateSearch] = $('input[name=' + nameInputDateSearch + ']').val();
        },
        // hidden error input validate
        queryForKeywords(name) {
            if (this.errors) {
                this.errors[name] = null;
            }
            if (this.error_name_category) {
                this.error_name_category[name] = null;
            }
        },
        //hidden error input Date validate
        handleBlurInputDate(nameInputDate) {
            this.formData[nameInputDate] = $('input[name=' + nameInputDate + ']').val();
            if (this.errors) {
                this.errors[nameInputDate] = null;
            }
        },

        //hidden error input Date validate
        handleBlurInputTime(nameInputDate, key) {
            this.formData[nameInputDate][key] = $('input[name=' + nameInputDate + '_' + key + ']').val();
            if (this.errors) {
                this.errors[nameInputDate] = null;
            }
        },
        getLocalStorage(param = null, id = 0) {
            if (param == null) {
                localStorage.clear();
            } else {
                var results = [];
                const objects = Object.keys(localStorage);
                for (let i = 0; i < objects.length; i++) {
                    if (objects[i].indexOf(param) !== -1) {
                        results.push(objects[i]);
                    }
                }
                if (results.length) {
                    this.store = localStorage.getItem(param + '-' + this.prefixPath);
                    this.inputStatus = localStorage.getItem('disable' + '-' + this.prefixPath);
                    if (this.store != null && this.prefixPath != '') {
                        localStorage.setItem(param + '-' + this.prefixPath, this.store);
                    }
                    if (this.inputStatus != null && this.prefixPath != '') {
                        localStorage.setItem('disable' + '-' + this.prefixPath, this.inputStatus);
                    }
                } else {
                    localStorage.clear();
                }

            }
        },
        setEmptyErrors() {
            this.errors = {};
        },
        setLocalStorageValue(key, data) {
            const results = [];
            const path = this.prefixPath.split('/');
            const objects = Object.keys(localStorage);
            const checkPoint = path.length > 2 ? path[path.length - 1] : path[path.length];
            for (let i = 0; i < objects.length; i++) {
                if (objects[i].indexOf('disable') !== -1 && objects[i].indexOf(checkPoint) !== -1) {
                    results.push(objects[i]);
                }
            }
            if (results.length < 2) {
                this.errors = this.$options.data().errors;
                this.errorImg = this.$options.data().errorImg;
                localStorage.setItem('disable' + '-' + this.prefixPath, true);
                localStorage.setItem(key + '-' + this.prefixPath, data);
                this.disableInput = true;
            } else {
                this.message = "errors";
                this.openModal();
            }
        },
        backSubmit() {
            localStorage.removeItem('disable' + '-' + this.prefixPath);
            localStorage.removeItem(this.storeKey + '-' + this.prefixPath);
            this.disableInput = false;
            // this.setDisableForOptionPlan(this.formData.organization_plan_id);
        },
        getSearchHistory() {
            this.parameters.forEach((v) => {
                let urlParams = new URLSearchParams(window.location.search.substring(1));
                if (urlParams.has(v)) {
                    this.formSearch[v] = urlParams.get(v);
                }
            });
        },
    },
}

