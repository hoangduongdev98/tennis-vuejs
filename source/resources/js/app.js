/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');
 require('./jsmixin');
 window.Vue = require('vue').default;
 import store from './store/store';
 import VueAxios from 'vue-axios';
 import axios from 'axios';
 import vSelect from 'vue-select'
 import 'vue-select/dist/vue-select.css';
 Vue.component('v-select', vSelect)
 Vue.use(VueAxios, axios);
 /**
  * The following block of code may be used to automatically register your
  * Vue components. It will recursively scan this directory for the Vue
  * components and automatically register them with their "basename".
  *
  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
  */
 
 // const files = require.context('./', true, /\.vue$/i)
 // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
 window.route = require('./routes/route');
 require('./components');
 
 /**
  * Next, we will create a fresh Vue application instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */
 
 if(document.getElementById("app")){
     new Vue(Vue.util.extend({ store })).$mount('#app');
 } else if (document.getElementById("auth")) {
     new Vue(Vue.util.extend({ store })).$mount('#auth');
 }
 