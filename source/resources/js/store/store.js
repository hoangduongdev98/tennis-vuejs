import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const stores = new Vuex.Store({
    state: {
        player: {},
        lesson: {},
        settlement: {},
    },
    getters: {
        getDataListPlayer: state => {
            return state.player
        },
        getDataListLesson: state => {
            return state.lesson
        },
        getDataListSettlement: state => {
            return state.settlement
        },
    },
    mutations: {
        setDataListPlayer: (state, payload) => {
            state.player = payload;
        },
        setDataListLesson: (state, payload) => {
            state.lesson = payload;
        },
        setDataListSettlement: (state, payload) => {
            state.settlement = payload;
        },
    },
    actions: {}
});
export default stores;
