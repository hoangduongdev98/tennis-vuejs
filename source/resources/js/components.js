/*
    Trainer
*/

Vue.component('list-player-trainer', require('./components/trainer/player/ListPlayer').default);

Vue.component('list-lesson-trainer', require('./components/trainer/lesson/ListLesson').default);
Vue.component('create-lesson-trainer', require('./components/trainer/lesson/CreateLesson').default);
Vue.component('edit-lesson-trainer', require('./components/trainer/lesson/EditLesson').default);
Vue.component('detail-lesson-trainer', require('./components/trainer/lesson/DetailLesson').default);

Vue.component('list-settlement', require('./components/trainer/settlement/ListSettlement').default);