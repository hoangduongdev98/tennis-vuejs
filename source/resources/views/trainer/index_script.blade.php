<script>
    $(function() {
        //input mask
        'use strict';
        /* Chart.js Charts */
        var ctx = document.getElementById('revenue-chart-canvas').getContext('2d');
        var dataLessons = JSON.parse($('#data-lessons-for-chart').val());
        var chartData = {
            labels: ['2021年01月', '2021年02月', '2021年03月', '2021年04月', '2021年05月', '2021年06月', '2021年07月',
                '2021年08月', '2021年09月', '2021年10月', '2021年11月', '2021年12月'
            ],
            datasets: [{
                label: 'レッスン',
                backgroundColor: 'rgba(60,141,188,1)',
                data: dataLessons
            }, ]
        };

        var chartOptions = {
            maintainAspectRatio: false,
            responsive: true,
            plugins: {
                legend: {
                    position: 'bottom'
                }
            },
            scales: {
                x: {
                    grid: {
                        display: false
                    }
                }
            }
        };

        // This will get the first returned node in the jQuery collection.
        // eslint-disable-next-line no-unused-vars
        var chart = new Chart(ctx, { // lgtm[js/unused-local-variable]
            type: 'bar',
            data: chartData,
            options: chartOptions
        });
    });
</script>