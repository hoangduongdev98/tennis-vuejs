@extends('layouts.trainer.default')

@section('title', 'コーチ管理画面 | レッスン受講プレーヤー')

@section('content')
    <list-player-trainer :status="{{ json_encode($status) }}"></list-player-trainer>
@endsection