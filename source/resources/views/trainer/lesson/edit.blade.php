@extends('layouts.trainer.default')

@section('title', 'コーチ管理画面 | レッスン受講プレーヤー')

@section('content')
    <edit-lesson-trainer :lesson="{{$lesson}}" :arr_level="{{ json_encode($arrLevel) }}" :arr_name_categories="{{ json_encode($arrNameCategories) }}"></edit-lesson-trainer>
@endsection