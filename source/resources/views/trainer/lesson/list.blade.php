@extends('layouts.trainer.default')

@section('title', 'コーチ管理画面 | レッスン受講プレーヤー')

@section('content')
    <list-lesson-trainer :status="{{ json_encode($status) }}" :level="{{ json_encode($arrLevel) }}" :today="{{ json_encode($lessonToday) }}"></list-lesson-trainer>
@endsection
