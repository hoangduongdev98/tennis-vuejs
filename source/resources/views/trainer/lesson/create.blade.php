@extends('layouts.trainer.default')

@section('title', 'コーチ管理画面 | レッスン受講プレーヤー')

@section('content')
    <create-lesson-trainer :arr_level="{{ json_encode($arrLevel) }}" :arr_name_categories="{{ json_encode($arrNameCategories) }}"></create-lesson-trainer>
@endsection