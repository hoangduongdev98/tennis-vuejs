@extends('layouts.trainer.default')

@section('title', 'コーチ管理画面 | レッスン受講プレーヤー')

@section('content')
    <detail-lesson-trainer :category="{{ json_encode($arrCategories) }}" :lesson="{{ json_encode($lesson) }}">
    </detail-lesson-trainer>
@endsection
