@extends('layouts.trainer.auth')

@section('title', 'コーチ管理画面 | パスワード再発行')

@section('content')
    <div class="login-logo">パスワードをお忘れですか</div>
    <!-- /.login-logo -->

    <div class="card">
        <div class="card-body login-card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @else
                <p class="login-box-msg">パスワードを忘れましたか？ ここでは、新しいパスワードを簡単に取得できます。</p>
                {!! Form::open(['method' => 'POST', 'url' => [route('trainer.forgot')]]) !!}
                <div class="input-group mb-3">
                    {!! Form::text('email', old('email'), ['placeholder' => 'メールアドレス', 'class' => 'form-control', 'data-inputmask' => '"alias": "email"', 'data-mask']) !!}
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @error('email')
                        <div id="emailForgotPassFeedback" class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <!-- /.input-group -->
                {!! Form::submit('送信', ['class' => 'btn btn-outline-trainer-primary btn-block']) !!}
                {!! Form::close() !!}
                <!-- /.form -->
            @endif
            <p class="mt-3 mb-1">
                <a class="text-color-trainer" href="{{ route('trainer.login') }}">ログインページに戻る</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
@stop
