@extends('layouts.trainer.auth')

@section('title', 'コーチ管理画面 | ログイン')

@section('content')
    <div class="login-logo">ログイン</div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            {!! Form::open(['method' => 'POST', 'url' => [route('trainer.login')]]) !!}
                <div class="input-group mb-3">
                    @php $errors->has('email') ? $class='form-control is-invalid' : $class='form-control' @endphp
                    {!! Form::text('email', old('email'), ['class' => $class, 'placeholder' => 'ID', 'aria-describedby' => 'emailFeedback']) !!}
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @error('email')
                        <div id="emailFeedback" class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    @php $errors->has('password') ? $class='form-control is-invalid' : $class='form-control' @endphp
                    {!! Form::password('password', ['class' => $class, 'placeholder' => 'パスワード', 'aria-describedby' => 'passwordFeedback']) !!}
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                        <div id="passwordFeedback" class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3 text-sm">
                    @if (Session::has('error'))
                        <p class="error-message">{{ session('error') }}</p>
                    @endif
                    パスワード忘れの場合は、管理者までご連絡ください。
                </div>
                {!! Form::submit('ログイン', ['class' => 'btn btn-outline-trainer-primary btn-block', 'disabled' => 'disabled']) !!}
            {!! Form::close() !!}
        </div>
        <!-- /.login-card-body -->
    </div>
    <!-- /.login-box -->
@stop
