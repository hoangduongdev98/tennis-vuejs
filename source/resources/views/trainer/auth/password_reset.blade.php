Hi,

A request has been received to change the password for your account.


{{ route('trainer.resetPassword', [ 'token' => $token ]) }}?email={{ $trainer->email }}

　
If you did not initiate this request, please contact us immediately at oic@ninesigma.com.



Ninesigma Team
oic@ninesigma.com
1-3-3 Uchikanda, Chiyoda
Tokyo, 101-0047, Japan

If you would not like to receive any solicitation from NineSigma, contact oic@ninesigma.com to let us unsubscribe you.
