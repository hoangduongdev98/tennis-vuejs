@extends('layouts.trainer.auth')

@section('title', 'コーチ管理画面 | パスワード復旧')

@section('content')
    <div class="login-logo">パスワード復旧</div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">新しいパスワードから一歩進んだだけです。今すぐパスワードを回復してください。</p>
            {!! Form::open(['method' => 'POST', 'url' => [route('trainer.reset')], 'class' => 'form-horizontal']) !!}
            <div class="input-group mb-3">
                @php $errors->has('password') ? $class='form-control is-invalid' : $class='form-control' @endphp
                {!! Form::hidden('email', $email, ['placeholder' => 'email', 'aria-describedby' => 'emailForgotPassFeedback']) !!}
                {!! Form::hidden('token', $token, ['placeholder' => 'token', 'aria-describedby' => 'emailForgotPassFeedback']) !!}
                {!! Form::password('password', ['class' => $class, 'placeholder' => 'パスワード', 'id'=>'password']) !!}
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            @error('password')
                <div class="error-message">{{ $message }}</div>
            @enderror
            <!-- /.input-group -->
            @php $errors->has('confirm_password') ? $class='form-control is-invalid' : $class='form-control' @endphp
            <div class="input-group mb-3">
                {!! Form::password('confirm_password', ['class' => $class, 'placeholder' => 'パスワードを認証する','id' => 'cfPassword']) !!}
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            @error('confirm_password')
                <div class="error-message">{{ $message }}</div>
            @enderror
            @foreach ($errors->all() as $error)
                {{ $error }}
            @endforeach
            <!-- /.input-group -->
            {!! Form::submit('確認', ['class' => 'btn btn-outline-trainer-primary btn-block"','disabled' =>'disabled']) !!}
            <!-- /.form -->
            <p class="mt-3 mb-1">
                <a class="text-color-trainer" href="route('trainer.login')">ログインページに戻る</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
@stop
