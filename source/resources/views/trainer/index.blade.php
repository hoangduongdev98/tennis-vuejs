@extends('layouts.trainer.default')

@section('title', 'コーチ管理画面 | ダッシュボード')

@section('content')
@php
$trainer = Auth::guard('trainers')->user();
$school = getSchool($trainer->school_id);
@endphp
    <!-- Add flash message -->
    @include('layouts.trainer.flash')
    <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="{!! asset('dist/img/logo-3.png') !!}" alt="logo" height="46" width="46">
    </div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">ダッシュボード</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1)
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">登録状況</h3>
                        </div>
                        <div class="card-body">
                            <!-- Info boxes -->
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <div class="bg-lightblue rounded shadow-sm p-3 h-100 elevation-1">
                                        <h6>プレーヤー登録</h6>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-blue-1 px-2 py-1"><a class="text-danger" href="{{ route('trainer.players') }}">新規登録</a></div>
                                            <div class="col-2 bg-blue-1 text-right px-2 py-1"><a href="{{ route('trainer.players') }}"><span class="text-danger">{{ $countPlayers['player_registration'] }}</span></a>
                                            </div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-blue-1 px-2 py-1"><span class="text-dark">会員登録プレーヤー</span></div>
                                            <div class="col-2 bg-blue-1 text-right px-2 py-1"><span class="text-dark">{{ $countPlayers['player_old_register'] }}</span></div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-blue-1 px-2 py-1"><span class="text-dark">&nbsp;</span></div>
                                            <div class="col-2 bg-blue-1 text-right px-2 py-1"><span class="text-dark">&nbsp;</span>
                                            </div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-blue-2 px-2 py-1"><span class="text-dark">合計</span></div>
                                            <div class="col-2 bg-blue-2 text-right px-2 py-1"><span class="text-dark">{{ $countPlayers['player_registration'] + $countPlayers['player_old_register']  }}</span></div>
                                        </div>
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    @endif
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">利用状況</h3>
                        </div>
                        <div class="card-body">
                            <!-- Info boxes -->
                            <div class="row">
                                @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1)
                                <div class="col-12 col-sm-6 mb-2 mb-md-0">
                                    <div class="bg-gray rounded shadow-sm p-3 h-100 elevation-1">
                                        <h6>レッスン登録数</h6>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-gray-1 px-2 py-1"><a class="text-danger" href="{{ route('lesson.list') }}">新規登録</a></div>
                                            <div class="col-2 bg-gray-1 text-right px-2 py-1"><span class="text-danger">{{ $countLessons['lesson_registration'] }}</span>
                                            </div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-gray-1 px-2 py-1"><span class="text-dark">登録数</span></div>
                                            <div class="col-2 bg-gray-1 text-right px-2 py-1"><span class="text-dark">{{ $countLessons['lesson_old_register'] }}</span></div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-gray-2 px-2 py-1"><span class="text-dark">合計</span></div>
                                            <div class="col-2 bg-gray-2 text-right px-2 py-1"><span class="text-dark">{{ $countLessons['lesson_registration'] + $countLessons['lesson_old_register'] }}</span></div>
                                        </div>
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                                @endif
                                <div class="col-12 col-sm-6 mb-2 mb-md-0">
                                    <div class="bg-gray-dark rounded shadow-sm p-3 h-100 elevation-1">
                                        <h6>試合解析数</h6>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-gray-1 px-2 py-1"><a class="text-danger" href="{{ route('trainer.listShare') }}">新規登録</a></div>
                                            @if($school->contract_id == 0 || $school->contract_id == 3)
                                                <div class="col-2 bg-gray-1 text-right px-2 py-1"><span class="text-danger">0</span></div>
                                            @else
                                                <div class="col-2 bg-gray-1 text-right px-2 py-1"><span class="text-danger">{{ $countMatches['matches_registration'] }}</span></div>
                                            @endif
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-gray-1 px-2 py-1"><span class="text-dark">登録数</span></div>
                                            @if($school->contract_id == 0 || $school->contract_id == 3)
                                                <div class="col-2 bg-gray-1 text-right px-2 py-1"><span class="text-dark">0</span></div>
                                            @else
                                                <div class="col-2 bg-gray-1 text-right px-2 py-1"><span class="text-dark">{{ $countMatches['matches_old_register'] }}</span></div>
                                            @endif
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="col-10 bg-gray-2 px-2 py-1"><span class="text-dark">合計</span></div>
                                            @if($school->contract_id == 0 || $school->contract_id == 3)
                                                <div class="col-2 bg-gray-2 text-right px-2 py-1"><span class="text-dark">0</span></div>
                                            @else
                                                <div class="col-2 bg-gray-2 text-right px-2 py-1"><span class="text-dark">{{ $countMatches['matches_registration'] + $countMatches['matches_old_register'] }}</span></div>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1)
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">売上推移</h3>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <input type="hidden" id="data-lessons-for-chart" value="{{ json_encode($dataLessonsForChart) }}">
                            <canvas id="revenue-chart-canvas" style="min-height: 300px; height: 300px; max-height: 300px; max-width: 100%;"></canvas>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    @endif
                </div> <!-- /.col -->
            </div> <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    </div>
    <!-- /.content -->
@stop
@section('footer_js')
    @include('trainer.index_script')
@stop