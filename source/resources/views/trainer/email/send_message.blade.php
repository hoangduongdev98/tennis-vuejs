{{ $sendMail['player_name'] }} 様
「ポケコーチ」をご利用いただきまして誠にありがとうございます。

{{ $sendMail['trainer_name'] }}様から新しいメッセージが届いています。下記の内容をご確認ください。

申込日： {{ date("Y/m/d H:i:s",strtotime($sendMail['date_register'])) }}
レッスン名：{{ $sendMail['lesson_name'] }}
コーチ名：{{ $sendMail['trainer_name'] }} 
所属スクール: {{ $sendMail['school_name'] }} 
レベル: @if($sendMail['level'] != null) @foreach($sendMail['level'] as $idLevel => $level) @if($idLevel == count($sendMail['level'])-1) {{ $sendMail['arrLevel'][$level] }} @else {{ $sendMail['arrLevel'][$level] ."\n\t\x20\x20\x20\x20" }} @endif @endforeach @endif
{{"\r"}}コメント内容：{{ $sendMail['content'] }}

※こちらのメールは送信専用のメールアドレスより送信しています。恐れ入りますが、直接返信しないよう願い致します。

━━━━━━━━━━━━━
運営会社　合同会社シェノン
https://poke-coach.com
━━━━━━━━━━━━━