{{ $playerCancel['player_name'] }}様
「ポケコーチ」をご利用いただきまして誠にありがとうございます。

{{ $playerCancel['trainer_name'] }} 様からレッスンのキャンセル申請が届いています。下記の内容をご確認ください。

申込日： {{ date("Y/m/d H:i:s",strtotime($playerCancel['date_register'])) }}
レッスン名： {{ $playerCancel['lesson_name'] }}
コーチ名：{{ $playerCancel['trainer_name'] }} 
所属スクール: {{ $playerCancel['school_name'] }} 
レベル: @if($playerCancel['level'] != null) @foreach($playerCancel['level'] as $idLevel => $level) @if($idLevel == count($playerCancel['level'])-1) {{ $playerCancel['arrLevel'][$level] }} @else {{ $playerCancel['arrLevel'][$level] ."\n\t\x20\x20\x20\x20" }} @endif @endforeach @endif
{{"\r"}}コメント内容：{{ $playerCancel['comment'] }}

-------------------------------------
★レッスンキャンセル申請の流れ
-------------------------------------
マイページのレッスン内容を開き。返還ポイント（キャンセル料率）をご確認後、「承認する」または「差し戻す」を選択します。
返還ポイント（キャンセル料率）に問題がない場合は、「承認する」を、修正が必要な場合は「差し戻す」を押してください。
※24時間以内にどちらも選択されない場合は、自動的に「承認する」が選択されますのでご注意ください。

◆「承認する」を選択した場合
レッスンのキャンセルが確定します。返還ポイントがあなたのポイントに追加された事をご確認ください。
また72時間後にトークルームでのメッセージのやりとりができなくなります。問い合わせ事項等がある場合はそれまでに済ませましょう。

◆「差し戻す」を選択した場合
ボタンの押下後、理由を入力し差し戻しを行ってください。

※こちらのメールは送信専用のメールアドレスより送信しています。恐れ入りますが、直接返信しないよう願い致します。

━━━━━━━━━━━━━━━━━━━━━━━━━━
運営会社　合同会社シェノン
https://poke-coach.com
━━━━━━━━━━━━━━━━━━━━━━━━━━