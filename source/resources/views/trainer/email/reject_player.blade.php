{{ $playerReject['player_name'] }}様
「ポケコーチ」をご利用いただきまして誠にありがとうございます。

レッスンの申し込みがキャンセルされました。下記の内容をご確認ください。

申込日： {{ date("Y/m/d H:i:s",strtotime($playerReject['date_register'])) }}
レッスン名： {{ $playerReject['lesson_name'] }}
コーチ名: {{ $playerReject['trainer_name'] }}
所属スクール:  {{$playerReject['school_name']}}
レベル: @if($playerReject['level'] != null) @foreach($playerReject['level'] as $idLevel => $level) @if($idLevel == count($playerReject['level'])-1) {{ $playerReject['arrLevel'][$level] }} @else {{ $playerReject['arrLevel'][$level]."\n\t\x20\x20\x20\x20" }} @endif @endforeach @endif
{{"\r"}}コメント内容：{{ $playerReject['comment'] }}


※こちらのメールは送信専用のメールアドレスより送信しています。恐れ入りますが、直接返信しないよう願い致します。

━━━━━━━━━━━━━━━━━━━━━━━━
運営会社　合同会社シェノン
https://poke-coach.com
━━━━━━━━━━━━━━━━━━━━━━━━