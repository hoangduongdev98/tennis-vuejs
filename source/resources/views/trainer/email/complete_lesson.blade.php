{{ $playerComplete['player_name'] }}様
「ポケコーチ」をご利用いただきまして誠にありがとうございます。

{{ $playerComplete['trainer_name'] }} 様からレッスンの完了申請が届いています。下記の内容をご確認ください。

申込日： {{ date("Y/m/d H:i:s",strtotime($playerComplete['date_register'])) }}
レッスン名： {{ $playerComplete['lesson_name'] }}
コーチ名：{{ $playerComplete['trainer_name'] }} 
所属スクール: {{ $playerComplete['school_name'] }} 
レベル: @if($playerComplete['level'] != null) @foreach($playerComplete['level'] as $idLevel => $level) @if($idLevel == count($playerComplete['level'])-1) {{ $playerComplete['arrLevel'][$level] }} @else {{ $playerComplete['arrLevel'][$level] ."\n\t\x20\x20\x20\x20" }} @endif @endforeach @endif
{{"\r"}}コメント内容：{{ $playerComplete['comment'] }}

-------------------------------------
★レッスン完了申請の流れ
-------------------------------------
マイページのレッスン内容を開き「レッスンを完了する」または「差し戻す」を選択します。
レッスンが完了している場合は「レッスンを完了する」を、レッスンが完了していない場合は「差し戻す」を押して下さい。
※24時間以内にどちらも選択されない場合は、自動的に「レッスンを完了する」が選択されますのでご注意ください。

◆「レッスンを完了する」を選択した場合
72時間後にトークルームでのメッセージのやりとりができなくなります。レッスンに関する質問等がある場合はそれまでに済ませましょう。

◆「差し戻す」を選択した場合
ボタンの押下後、理由を入力し差し戻しを行ってください。

※こちらのメールは送信専用のメールアドレスより送信しています。恐れ入りますが、直接返信しないよう願い致します。

━━━━━━━━━━━━━━━━━━━━━━━━━
運営会社　合同会社シェノン
https://poke-coach.com
━━━━━━━━━━━━━━━━━━━━━━━━━