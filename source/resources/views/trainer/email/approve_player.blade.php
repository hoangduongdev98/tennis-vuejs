{{ $playerApprove['player_name'] }}様
「ポケコーチ」をご利用いただきまして誠にありがとうございます。

レッスンの申し込みが確定しました。下記の内容をご確認ください。

申込日： {{ date("Y/m/d H:i:s",strtotime($playerApprove['date_register'])) }}
レッスン名： {{ $playerApprove['lesson_name'] }}
コーチ名: {{ $playerApprove['trainer_name'] }}
所属スクール:  {{$playerApprove['school_name']}}
レベル: @if($playerApprove['level'] != null) @foreach($playerApprove['level'] as $idLevel => $level) @if($idLevel == count($playerApprove['level'])-1) {{ $playerApprove['arrLevel'][$level] }} @else {{ $playerApprove['arrLevel'][$level]."\n\t\x20\x20\x20\x20" }} @endif @endforeach @endif
{{"\r"}}コメント内容：{{ $playerApprove['comment'] }}

※こちらのメールは送信専用のメールアドレスより送信しています。恐れ入りますが、直接返信しないよう願い致します。

━━━━━━━━━━━━━━━━━━━━━━━━━
運営会社　合同会社シェノン
https://poke-coach.com/
━━━━━━━━━━━━━━━━━━━━━━━━━