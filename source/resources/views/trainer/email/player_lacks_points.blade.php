{{ $player['player_name'] }}様
「ポケコーチ」をご利用いただきまして誠にありがとうございます。

お申込みをいただいておりますレッスンのポイントが不足しております。
お手数ではございますがご登録内容の確認をお願いいたします。

※本メールは配信専用のため、ご返信いただきましてもスクールへは届きません。

以下のレッスンをお申し込みいただいております。
　
申込日： {{ date("Y/m/d H:i:s",strtotime($player['date_register'])) }}
レッスン名： {{ $player['lesson_name'] }}
プレーヤー名: {{ $player['player_name'] }}
レベル: @if($player['level'] != null) @foreach($player['level'] as $idLevel => $level) @if($idLevel == count($player['level'])-1) {{ $player['arrLevel'][$level] }} @else {{ $player['arrLevel'][$level]."\n\t\x20\x20\x20\x20" }} @endif @endforeach @endif
{{"\r"}}受講回数: {{ $player['player_class'] }}/{{ $player['classes'] }}
コメント内容：{{ $player['comment'] }}


{{ $player['trainer_name'] }}
{{ $player['trainer_email'] }}
{{ $player['trainer_address'] }}
{{ $player['trainer_phone'] }}

