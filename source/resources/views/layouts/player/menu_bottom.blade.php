<div class="fixed-bottom m-auto navbar-light bg-light navbar-custom-bottom">
    <div class="container-fluid">
        @php
        $isLogon = 0;
        if(session()->has('name_school_login')) {
            $isLogon = 1;
        } else if(session()->has('name_trainer_login')) {
            $isLogon = 1;
        }
        @endphp
        @if(Route::current()->getName() == 'player.matchListHistory' || Route::current()->getName() == 'player.searchMatchHistory') 
            @if($isLogon == 0)
            <div class="row bg-dark py-2">
                <div class="col-12">
                    <a href="{{ route('player.createMatch') }}" class="btn btn-player-blue d-block ">新しい試合を記録する</a>
                </div>
            </div>
            @endif
        @else
            @if(!Auth::guard('players')->check() && !Auth::guard('players')->user() && Route::current()->getName() != 'player.createMatch'&& Route::current()->getName() != 'player.startMatch' && Route::current()->getName() != 'player.matchAnalysis' && Route::current()->getName() != 'player.matchRecord' && Route::current()->getName() != 'player.guest')
            <div class="row bg-dark py-2">
                <div class="col-6">
                    <a href="{{ route('player.getPlayerRegister') }}" class="btn btn-player-blue d-block">無料会員登録</a>
                </div> <!-- /.col -->
                <div class="col-6">
                    <a href="{{ route('player.login') }}" class="btn btn-player-green d-block">ログイン</a>
                </div> <!-- /.col -->
            </div> <!-- /.row -->
            @endif
        @endif
    </div> <!-- /.container-fluid -->    
    <ul class="nav justify-content-center text-center py-2">
        <li class="nav-item">
            <a class="nav-link rounded {{ getActiveCurrentRoute('player.index') }}" @if(getActiveCurrentRoute('player.index') == 'active') aria-current="page" @endif href="{{ route('player.index') }}">
                <i class="d-block mx-auto mb-1 fas fa-home"></i>ホーム
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link rounded {{ getActiveCurrentController('PlayerMatchController') }}" @if(getActiveCurrentController('PlayerMatchController') == 'active') aria-current="page" @endif href="{{ route('player.matchListHistory') }}">
                <i class="d-block mx-auto mb-1 fas fa-chart-bar"></i>試合解析
            </a>
        </li>
        @if(Auth::guard('players')->check() && Auth::guard('players')->user() && $isLogon == 0)
            <li class="nav-item">
                <a class="nav-link rounded {{ getActiveCurrentRoute('player.list_lesson', 'player.searchListLesson') }}" @if(getActiveCurrentRoute('player.list_lesson', 'player.searchListLesson') == 'active') aria-current="page" @endif href="{{ route('player.list_lesson') }}">
                    <i class="d-block mx-auto mb-1 fas fa-chalkboard-teacher"></i>レッスン
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link rounded {{ getActiveCurrentController('PlayerSchoolController') }}" @if(getActiveCurrentController('PlayerSchoolController') == 'active') aria-current="page" @endif href="{{ route('player.listSchool') }}"><i class="d-block mx-auto mb-1 fas fa-search"></i>スクール検索</a>
            </li>
            <li class="nav-item">
                <a class="nav-link rounded {{ getActiveCurrentRoute('player.accountManager', 'player.edit_info') }}" @if(getActiveCurrentRoute('player.accountManager', 'player.edit_info') == 'active') aria-current="page" @endif href="{{ route('player.accountManager') }}">
                    <i class="d-block mx-auto mb-1 fas fa-user"></i>マイページ
                </a>
            </li>
        @else
            <li class="nav-item">
                <a class="nav-link rounded disabled" href="#" disabled><i class="d-block mx-auto mb-1 fas fa-chalkboard-teacher"></i>レッスン</a>
            </li>
            <li class="nav-item">
                <a class="nav-link rounded disabled" href="#" disabled><i class="d-block mx-auto mb-1 fas fa-search"></i>スクール検索</a>
            </li>
            <li class="nav-item">
                <a class="nav-link rounded disabled" href="#" disabled><i class="d-block mx-auto mb-1 fas fa-user"></i>マイページ</a>
            </li>
        @endif
    </ul>
</div>
