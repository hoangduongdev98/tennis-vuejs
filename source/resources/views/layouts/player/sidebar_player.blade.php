@php
$isLogon = 0;
if(session()->has('name_school_login')) {
    $isLogon = 1;
} else if(session()->has('name_trainer_login')) {
    $isLogon = 1;
}
@endphp
<!-- Sidebar -->
<nav class="sidebar">
    <!-- close sidebar menu -->
    <div class="clearfix">
        <div class="dismiss float-right">
            <i class="fas fa-arrow-left"></i>
        </div>
    </div>

    <ul class="list-unstyled menu-elements clearfix">
        <li class="nav-item">
            <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.index') }}" href="{{ route('player.index') }}"><i class="fas fa-home"></i> ホーム</a>
        </li>
        @if(Auth::guard('players')->check() && Auth::guard('players')->user() && $isLogon == 0)
        <li class="nav-item">
            <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.listSharing') }}" href="{{ route('player.listSharing') }}"><i class="fas fa-chart-bar"></i> 試合解析情報共有</a>
        </li>
        @endif
        <li class="nav-item">
            @if(Auth::guard('players')->check() && Auth::guard('players')->user())
                <a class="nav-link font-weight-bold log-out {{ getActiveCurrentRoute('player.logout') }}" href="{{ route('player.logout') }}"><i class="fas fa-sign-out-alt"></i> ログアウト</a>
            @else
                <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.login') }}" href="{{ route('player.login') }}"><i class="fas fa-sign-in-alt"></i> ログイン</a>
            @endif
        </li>
        @if(!Auth::guard('players')->check() && !Auth::guard('players')->user())
        <li class="nav-item">
            <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.getPlayerRegister') }}" href="{{ route('player.getPlayerRegister') }}"><i class="fas fa-user-plus"></i> 会員登録</a>
        </li>
        @endif
        <li class="nav-item">
            <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.terms_of_use') }}" href="{{ route('player.terms_of_use') }}"><i class="fas fa-file-signature"></i> 利用規約</a>
        </li>
        <li class="nav-item">
            <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.privacyPolicy') }}" href="{{ route('player.privacyPolicy') }}"><i class="fas fa-file-signature"></i> プライバシーポリシー</a>
        </li>
        <li class="nav-item">
            <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.specifiedCommercialTransactions') }}" href="{{ route('player.specifiedCommercialTransactions') }}"><i class="fas fa-file-signature"></i> 特定商取引法に基づく表記</a>
        </li>
    </ul>
</nav> <!-- /.sidebar -->

<!--Navbar-->
<nav class="navbar navbar-expand-xl navbar-dark navbar-custom">
    <div class="container-fluid">
        <!-- navbar-brand -->
        <a class="navbar-brand logo-text" href="{{ route('player.index') }}">ポケコーチ</a>
        <!-- open sidebar menu -->
        <button class="navbar-toggler open-menu" type="button" aria-expanded="false">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
        </button>

        <div class="collapse navbar-collapse" id="main-menu">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.index') }}" href="{{ route('player.index') }}"><i class="fas fa-home"></i> ホーム</a>
                </li>
                @if(Auth::guard('players')->check() && Auth::guard('players')->user() && $isLogon == 0)
                <li class="nav-item">
                    <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.listSharing') }}" href="{{ route('player.listSharing') }}"><i class="fas fa-chart-bar"></i> 試合解析情報共有</a>
                </li>
                @endif
                <li class="nav-item">
                    @if(Auth::guard('players')->check() && Auth::guard('players')->user())
                        <a class="nav-link font-weight-bold log-out {{ getActiveCurrentRoute('player.logout') }}" href="{{ route('player.logout') }}"><i class="fas fa-sign-out-alt"></i> ログアウト</a>
                    @else
                        <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.login') }}" href="{{ route('player.login') }}"><i class="fas fa-sign-in-alt"></i> ログイン</a>
                    @endif
                </li>
                @if(!Auth::guard('players')->check() && !Auth::guard('players')->user())
                <li class="nav-item">
                    <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.getPlayerRegister') }}" href="{{ route('player.getPlayerRegister') }}"><i class="fas fa-user-plus"></i> 会員登録</a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.terms_of_use') }}" href="{{ route('player.terms_of_use') }}"><i class="fas fa-file-signature"></i> 利用規約</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.privacyPolicy') }}" href="{{ route('player.privacyPolicy') }}"><i class="fas fa-file-signature"></i> プライバシーポリシー</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold {{ getActiveCurrentRoute('player.specifiedCommercialTransactions') }}" href="{{ route('player.specifiedCommercialTransactions') }}"><i class="fas fa-file-signature"></i> 特定商取引法に基づく表記</a>
                </li>
            </ul>
        </div> <!-- /.collapse -->
    </div> <!-- /.container-fluid -->
</nav> <!-- /.navbar -->

<!-- Dark overlay -->
<div class="overlay"></div>
