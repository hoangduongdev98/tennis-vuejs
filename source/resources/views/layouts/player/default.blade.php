<!DOCTYPE html>
<html class="no-js" lang="ja">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <title>@yield('title')</title>
        <link rel="icon" href="{!! asset('players/favicon.ico') !!}" type="image/x-icon">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{!! asset('plugins/fontawesome-free/css/all.min.css') !!}">
        <!-- Bootstrap 4 -->
        <link rel="stylesheet" href="{!! asset('players/plugins/bootstrap/css/bootstrap.min.css') !!}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{!! asset('plugins/daterangepicker/daterangepicker.css') !!}">
        <!-- common css -->
        <link rel="stylesheet" href="{!! asset('players/dist/css/brite.player.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/brite.player.css') !!}">
        <!-- Adobe Font: Noto Sans CJK Jp -->
        <script>
            (function(d) {
                var config = {
                    kitId: 'cwg2zre',
                    scriptTimeout: 3000,
                    async: true
                },
                h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
            })(document);
        </script>
    </head>

    <body>
        @php
            $classDivContent = '';
            if (Route::current()->getName() == 'player.matchListHistory' || Route::current()->getName() == 'player.searchMatchHistory') {
                $classDivContent = 'wrapper-higher';
            } elseif (Route::current()->getName() == 'player.createMatch' || Route::current()->getName() == 'player.startMatch' || Route::current()->getName() == 'player.matchRecord' || Route::current()->getName() == 'player.guest'){
                $classDivContent = 'wrapper2';
            } else {
                if (!Auth::guard('players')->check() && !Auth::guard('players')->user()) {
                    $classDivContent = 'wrapper-higher';
                }
            }
        @endphp
        <!--<h2 id="result">detecting…</h2>-->
        <div class="wrapper {{ $classDivContent }}">
            @include('layouts.player.sidebar_player')

            @yield('content')

            @include('layouts.player.menu_bottom')
        </div> <!-- /.wrapper -->
        
        <!-- jQuery -->
        <script src="{!! asset('plugins/jquery/jquery.min.js') !!}"></script>
        <!-- Bootstrap 4 -->
        <script src="{!! asset('players/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
        <!-- date-range-picker -->
        <script src="{!! asset('plugins/moment/moment-with-locales.min.js') !!}"></script>
        <script src="{!! asset('plugins/daterangepicker/daterangepicker.js') !!}"></script>
        <!-- common js -->
        <script src="{!! asset('players/dist/js/brite.player.js') !!}"></script>
        <script>
            $(function() {
                $('.custom-alert').fadeOut(5000);
            });
        </script>
        @yield('footer-scripts')
    </body>
</html>
