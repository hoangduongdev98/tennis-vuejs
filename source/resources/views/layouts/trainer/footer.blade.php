@php $year = date("Y");  @endphp
<footer class="main-footer">
    <strong>Copyright &copy; {{$year}} Tennis.</strong>All rights reserved.
</footer>
