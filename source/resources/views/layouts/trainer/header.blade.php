<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- User Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link py-0" data-toggle="dropdown" href="#">
                <div class="user-panel">
                    @php
                        $name = $url = '';
                        if(session()->has('name_admin_login')) {
                            $name = session()->get('name_admin_login');
                            $name .= '（管理者としてログイン）';
                        } elseif(session()->has('name_school_login')) {
                            $name = session()->get('name_school_login');
                            $name .= '（管理者としてログイン）';
                        } else {
                            if (Auth::guard('trainers')->check()) {
                                $trainerLogin = Auth::guard('trainers')->user();
                                $name = $trainerLogin->name;
                                $image = getImageTrainer($trainerLogin->id);
                                if($image)
                                {
                                    $url = AVATAR_TRAINER_SAVE_JS . $image;
                                }
                            }
                        }
                    @endphp
                     @if($url)                   
                     <!-- use it when no avatar available -->
                        <img src="{{ $url }}" class="img-circle border" alt="User Image"> 
                    @else
                        <i class="fas fa-user"></i>  
                    @endif
                        <i class="fas fa-caret-down mr-2"></i>                   
                        {{ $name }}
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="{{ route('trainer.logout') }}" class="dropdown-item"><i class="fas fa-sign-out-alt"></i> ログアウト</a>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
