<aside class="main-sidebar sidebar-dark-trainer elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('trainer.index') }}" class="brand-link" id="bg-logo">
        <img src="{!! asset('dist/img/logo-3.png') !!}" alt="logo" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">コーチ管理画面</span>
    </a>
    @php
        $trainer = Auth::guard('trainers')->user();
        $school = getSchoolByTrainerId($trainer->id);
    @endphp
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('trainer.index') }}" class="nav-link {{ getActiveCurrentController('HomeController') }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            ダッシュボード
                        </p>
                    </a>
                </li>
                @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1 || ($school->contract_id == 1 || $school->contract_id == 2 || $school->contract_id == 4))
                <li class="nav-item {{ getActiveCurrentController('PlayerController')=='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('PlayerController') }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            プレーヤー管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1)
                        <li class="nav-item">
                            <a href="{!! route('trainer.players') !!}" class="nav-link {{ getActiveCurrentRoute('trainer.players') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>レッスン受講プレーヤー</p>
                            </a>
                        </li>
                        @endif
                        @if($school && ($school->contract_id == 1 || $school->contract_id == 2 || $school->contract_id == 4))
                        <li class="nav-item">
                            <a href="{!! route('trainer.listShare') !!}" class="nav-link {{ getActiveCurrentRoute('trainer.listShare') }} ">
                                <i class="nav-icon far fa-circle"></i>
                                <p>試合解析共有プレーヤー</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1)
                <li class="nav-item {{ getActiveCurrentController('LessonController')=='active' ? 'menu-open' : '' }} ">
                    <a href="" class="nav-link {{ getActiveCurrentController('LessonController') }}">
                        <i class="nav-icon fas fa-book-open"></i>
                        <p>
                            レッスン管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview ">
                        <li class="nav-item">
                            <a href="{{ route('lesson.list') }}" class="nav-link {{ getActiveCurrentRoute('lesson.list', 'lesson.search', 'lesson.detail') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>レッスン一覧・検索</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('lesson.create') }}" class="nav-link {{ getActiveCurrentRoute('lesson.create', 'lesson.create_lesson_confirm', 'lesson.edit_lesson', 'lesson.edit_lesson_confirm') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>レッスン登録</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('trainer.settlements') }}" class="nav-link {{ getActiveCurrentController('SettlementController') }}">
                        <i class="nav-icon fas fa-credit-card"></i>
                        <p>決済一覧</p>
                    </a>
                </li>
                @endif
                <li class="nav-item {{ getActiveCurrentController('TrainerController','MatchController')=='active' ? 'menu-open' : ''  }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('TrainerController') }}">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            設定・マスター管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item ">
                            <a href="{{ route('trainer.edit') }}" class="nav-link  {{ getActiveCurrentRoute('trainer.edit', 'trainer.editTrainerConfirm') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>コーチ情報登録</p>
                            </a>
                        </li>
                        @if($school->contract_id == 2 || $school->contract_id == 4)
                        <li class="nav-item">
                            <a href="{{ route('trainer.match') }}" class="nav-link {{ getActiveCurrentRoute('trainer.match') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>試合解析アドバイス設定</p>
                            </a>
                        </li>
                        @endif   
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
