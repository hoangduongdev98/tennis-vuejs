「ポケコーチ」をご利用いただきまして誠にありがとうございます。

ご予約いただいたレッスンが明日となりましたので再度ご連絡させていただきます。
※本メールは配信専用のため、ご返信いただきましてもスクールへは届きません。

受講中のレッスン

申込日： {{ $info['dateOfApplicate'] }}
レッスン名： {{ $info['lessonName'] }}
プレーヤー名: {{ $info['playerName'] }}
レベル: @if($info['levels'] != null) @foreach($info['levels'] as $idLevel => $level) @if($idLevel == count($info['levels'])-1) {{ $info['arrLevel'][$level]; }} @else {{ $info['arrLevel'][$level]."\n\t\x20\x20\x20\x20"; }} @endif @endforeach @endif

{{"\r";}}現在のお客様のポイントは下記となります。

保有ポイント：{{ $info['player_point'] }}ポイント　

レッスン開始日当日にポイントが不足している場合はレッスンの受講ができなくなります。ご注意ください。
レッスン開始日の前日までにポイントが足りない場合はポイントご購入をください。

{{ $info['name'] }}
{{ $info['email'] }}
{{ $info['address'] }}
{{ $info['phone'] }}
