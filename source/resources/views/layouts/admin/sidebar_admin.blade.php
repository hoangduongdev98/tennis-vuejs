<aside class="main-sidebar sidebar-dark-admin elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.index') }}" class="brand-link">
        <img src="{{ url('dist/img/logo.png') }}" alt="logo" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">システム管理画面</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('admin.index') }}" class="nav-link {{ getActiveCurrentController('HomeController') }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            ダッシュボード
                        </p>
                    </a>
                </li>

                <li class="nav-item {{ getActiveCurrentController('SchoolManagerController')=='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('SchoolManagerController') }}">
                        <i class="nav-icon fas fa-school"></i>
                        <p>
                            スクール管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.list_school') }}" class="nav-link {{ getActiveCurrentRoute('admin.list_school', 'admin.searchSchool', 'admin.detail_school') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>スクール一覧・検索</p>
                            </a>
                        </li>
                        @if(Auth::guard('admin')->check() && Auth::guard('admin')->user()->role != ROLE_0)
                            <li class="nav-item">
                                <a href="{{ route('admin.create_school') }}" class="nav-link {{ getActiveCurrentRoute('admin.create_school', 'admin.edit_school', 'admin.create_school_confirm', 'admin.edit_school_confirm') }}">
                                    <i class="nav-icon far fa-circle"></i>
                                    <p>スクール登録</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.list_trainer') }}" class="nav-link {{ getActiveCurrentController('TrainerManagerController') }}">
                        <i class="nav-icon fas fa-chalkboard-teacher"></i>
                        <p>コーチ管理</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.list_player') }}" class="nav-link {{ getActiveCurrentController('PlayerManagerController') }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>プレーヤー管理</p>
                    </a>
                </li>
                <li class="nav-item {{ getActiveCurrentController('PaymentManagerController')=='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('PaymentManagerController') }}">
                        <i class="nav-icon fas fa-credit-card"></i>
                        <p>
                            販売管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.list_payment') }}" class="nav-link {{ getActiveCurrentRoute('admin.list_payment', 'admin.searchListPayment', 'admin.school_payment_detail', 'admin.search_school_payment_detail', 'admin.trainer_payment_detail', 'admin.search_trainer_payment_detail') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>支払一覧</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.listSettlements') }}" class="nav-link {{ getActiveCurrentRoute('admin.listSettlements', 'admin.searchSettlement') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>決済一覧</p>
                            </a>
                        </li>
                    </ul>
                </li>
                {{-- <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-ad"></i>
                        <p>広告管理</p>
                    </a>
                </li> --}}

                <li class="nav-item {{ getActiveCurrentController('SettingMasterManagerController')=='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('SettingMasterManagerController') }}">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            設定・マスター管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.settingMasterLevel') }}" class="nav-link {{ getActiveCurrentRoute('admin.settingMasterLevel', 'admin.createMasterLevelConfirm', 'admin.editMasterLevelConfirm') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>レベルマスター</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.settlementAndFee') }}" class="nav-link {{ getActiveCurrentRoute('admin.settlementAndFee', 'admin.settingSettlementAndFeeConfirm') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>決済・手数料関係設定</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.accountManager') }}" class="nav-link {{ getActiveCurrentController('AccountManagerController') }}">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>アカウント管理</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
