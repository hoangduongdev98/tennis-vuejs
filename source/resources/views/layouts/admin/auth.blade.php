<!DOCTYPE html>
<html class="no-js" lang="ja">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{!! asset('plugins/fontawesome-free/css/all.min.css') !!}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{!! asset('dist/css/adminlte.min.css') !!}">
        <!-- common css -->
        <link rel="stylesheet" href="{!! asset('dist/css/brite.common.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/brite.admin.css') !!}">
        <!-- Adobe Font: Noto Sans CJK Jp -->
        <script>
            (function(d) {
                var config = {
                    kitId: 'cwg2zre',
                    scriptTimeout: 3000,
                    async: true
                },
                h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
            })(document);
        </script>
    </head>
    <body class="hold-transition login-page">
        <h1 class="position-absolute logo-custom m-0">
            <img src="{{ asset('dist/img/logo.png') }}" alt="logo" class="brand-image img-circle elevation-3">
            <span class="d-none d-sm-inline-block">システム管理画面</span>
        </h1>
        <div class="login-box">
            @yield('content')
        </div>
        <!-- /.login-box -->

        <!-- jQuery -->
        <script src="{!! asset('plugins/jquery/jquery.min.js') !!}"></script>
        <!-- Bootstrap 4 -->
        <script src="{!! asset('plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
        <!-- InputMask -->
        <script src="{!! asset('plugins/inputmask/jquery.inputmask.min.js') !!}"></script>
        <!-- AdminLTE App -->
        <script src="{!! asset('dist/js/adminlte.min.js') !!}"></script>
        <script>
            $(function () {
                'use strict';
                //input mask
                $('[data-mask]').inputmask();
                $(".custom-alert").fadeTo(4000, 500).slideUp(500, function() {
                    $(".custom-alert").slideUp(500);
                });
                $("input[name=email], input[name=password]").on("keyup change", function(e) {
                    let pass = encodeURI($("input[name=password]").val());
                    if (pass && $('input[name=email]').val()) {
                        $(':input[type="submit"]').prop('disabled', false);
                    } else {
                        $(':input[type="submit"]').prop('disabled', true);
                    }
                });
            });
        </script>
    </body>
</html>
