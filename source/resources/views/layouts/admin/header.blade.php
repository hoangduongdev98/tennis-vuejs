<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- User Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <div class="user-panel">
                    <i class="fas fa-user"></i> <!-- use it when no avatar available -->
                    <!--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
                    <i class="fas fa-caret-down mr-2"></i>
                    @php
                        $nameUserLogin = '';
                        if (Auth::guard('admin')->check()) {
                            $nameUserLogin = Auth::guard('admin')->user()->user_name;
                        }
                    @endphp
                    {{ $nameUserLogin }}
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="{{ route('admin.logout') }}" class="dropdown-item"><i class="fas fa-sign-out-alt"></i> ログアウト</a>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
