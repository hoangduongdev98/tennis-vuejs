<!DOCTYPE html>
<html class="no-js" lang="ja">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>@yield('title')</title>
        <link rel="icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{!! asset('plugins/fontawesome-free/css/all.min.css') !!}">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{!! asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') !!}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{!! asset('dist/css/adminlte.min.css') !!}">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{!! asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') !!}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{!! asset('plugins/daterangepicker/daterangepicker.css') !!}">
        <!-- common css -->
        <link rel="stylesheet" href="{!! asset('dist/css/brite.common.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/brite.admin.css') !!}">
        <!-- Adobe Font: Noto Sans CJK Jp -->
        <script>
            (function(d) {
                var config = {
                    kitId: 'cwg2zre',
                    scriptTimeout: 3000,
                    async: true
                },
                h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
            })(document);
        </script>
    </head>
    <body class="hold-transition sidebar-mini layout-fixed text-sm">
        <div class="wrapper">
            <!-- Navbar header -->
            @include('layouts.admin.header')
            <!-- /.navbar header -->

            <!-- Main Sidebar Container -->
            @include('layouts.admin.sidebar_admin')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- /.content-wrapper -->

            <!-- Footer -->
            @include('layouts.admin.footer')
            <!-- /.footer -->
        </div>
        <!-- ./wrapper -->

        <!-- jQuery -->
        <script src="{!! asset('plugins/jquery/jquery.min.js') !!}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{!! asset('plugins/jquery-ui/jquery-ui.min.js') !!}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 4 -->
        <script src="{!! asset('plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
        <!-- ChartJS -->
        <script src="{!! asset('plugins/chart.js/Chart.min.js') !!}"></script>
        <!-- daterangepicker -->
        <script src="{!! asset('plugins/moment/moment-with-locales.min.js') !!}"></script>
        <script src="{!! asset('plugins/inputmask/jquery.inputmask.min.js') !!}"></script>
        <script src="{!! asset('plugins/daterangepicker/daterangepicker.js') !!}"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{!! asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') !!}"></script>
        <!-- overlayScrollbars -->
        <script src="{!! asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') !!}"></script>
        <!-- AdminLTE App -->
        <script src="{!! asset('dist/js/adminlte.min.js') !!}"></script>
        <!-- common js -->
        <script src="{!! asset('dist/js/brite.common.js') !!}"></script>
        <script src="{!! asset('dist/js/brite.dragdrop.js') !!}"></script>
        <script>
            $(".custom-alert").fadeTo(2000, 500).slideUp(500, function() {
                $(".custom-alert").slideUp(500);
            });
        </script>
        @yield('footer-scripts')
    </body>
</html>
