<aside class="main-sidebar sidebar-dark-school elevation-4">
    <!-- Brand Logo -->
    <a href="{!! route('school.index') !!}" class="brand-link">
        <img src="{!! asset('favicon-2.ico') !!}" alt="logo" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">スクール管理画面</span>
    </a>
    @php
        $staffRole = Auth::guard('schools')->user()->role;       
        $school = get_school_by_staff(Auth::guard('schools')->user()->id);
    @endphp
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{!! route('school.index') !!}" class="nav-link {{ getActiveCurrentController('HomeController') }}" >
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            ダッシュボード
                        </p>
                    </a>
                </li>
                
                <li class="nav-item {{ getActiveCurrentController('ListTrainerController', 'TrainerManagerController')  =='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('ListTrainerController', 'TrainerManagerController')  }}">
                        <i class="nav-icon fas fa-chalkboard-teacher"></i>
                        <p>
                            コーチ管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">                   
                            <a href="{!! route('school.list_trainer') !!}" class="nav-link {{ getActiveCurrentRoute('school.list_trainer','school.searchListTrainer','school.detail_trainer','school.trainer_lesson') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>コーチ一覧・検索</p>
                            </a>
                        </li>
                        @if($school && $school['register_coach'] !== null && $school['register_coach'] == REGISTER_COACH_1)
                        @if($staffRole == ROLE_1)
                        <li class="nav-item">                                       
                            <a href="{!! route('school.create') !!}" class="nav-link {{ getActiveCurrentRoute('school.create','school.create_trainer_confirm','school.edit','school.edit_trainer_confirm') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>コーチ登録</p>
                            </a>
                        </li>
                        @endif
                        @endif
                    </ul>
                </li>    
                @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1 || ($school->contract_id == 1 || $school->contract_id == 2 || $school->contract_id == 4))
                <li class="nav-item {{ getActiveCurrentController('ListPlayerController')=='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('ListPlayerController') }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            プレーヤー管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1)
                        <li class="nav-item">
                            <a href="{!! route('school.list_player') !!}" class="nav-link {{ getActiveCurrentRoute('school.list_player') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>レッスン受講プレーヤー</p>
                            </a>
                        </li>
                        @endif
                        @if($school && ($school->contract_id == 1 || $school->contract_id == 2 || $school->contract_id == 4))
                        <li class="nav-item">
                            <a href="{!! route('school.listShare') !!}" class="nav-link {{ getActiveCurrentRoute('school.listShare') }} ">
                                <i class="nav-icon far fa-circle"></i>
                                <p>試合解析共有プレーヤー</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1)
                <li class="nav-item {{ getActiveCurrentController('PaymentManagerController','SettlementController')=='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('PaymentManagerController','SettlementController') }}">
                        <i class="nav-icon fas fa-credit-card"></i>
                        <p>
                            販売管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{!! route('school.list_payment') !!}" class="nav-link {{ getActiveCurrentController('PaymentManagerController') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>支払一覧</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('school.list_settlement') !!}" class="nav-link {{ getActiveCurrentController('SettlementController') }} ">
                                <i class="nav-icon far fa-circle"></i>
                                <p>決済一覧</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                @if($staffRole == ROLE_1)
                <li class="nav-item {{ getActiveCurrentController('EditSchoolController')=='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('EditSchoolController') }}">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            設定・マスター管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{!! route('school.editInfo') !!}" class="nav-link {{ getActiveCurrentController('EditSchoolController') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>スクール情報登録</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                @if($school && $school['register_lesson'] !== null && $school['register_lesson'] == REGISTER_LESSON_1)
                <li class="nav-item {{ getActiveCurrentController('LessonManagerController')=='active' ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ getActiveCurrentController('LessonManagerController') }}">
                        <i class="nav-icon fas fa-book-open"></i>
                        <p>
                            レッスン管理
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{!! route('school.listLesson') !!}" class="nav-link {{ getActiveCurrentRoute('school.listLesson', 'school.searchListLesson','school.detail_lesson') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>レッスン一覧・検索</p>
                            </a>
                        </li>
                        @if($staffRole == ROLE_1)
                        <li class="nav-item">
                            <a href="{!! route('school.create_lesson') !!}" class="nav-link {{ getActiveCurrentRoute('school.create_lesson','school.create_lesson_confirm','school.edit_lesson','school.edit_lesson_confirm') }}">
                                <i class="nav-icon far fa-circle"></i>
                                <p>レッスン登録</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                <li class="nav-item">
                    <a href="{!! route('school.accountManager') !!}" class="nav-link {{ getActiveCurrentController('AccountManagerController') }}">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>アカウント管理</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

